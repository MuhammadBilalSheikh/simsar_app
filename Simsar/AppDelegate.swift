//
//  AppDelegate.swift
//  Simsar
//
//  Created by Sarmad Malik on 16/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import GoogleSignIn

@UIApplicationMain

    class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        
        
        GIDSignIn.sharedInstance()?.clientID = "602938109768-396ic4okco23q0rb11f05s16gc3qtvb0.apps.googleusercontent.com"
        GIDSignIn.sharedInstance()?.delegate = self
        
        
        GMSServices.provideAPIKey("AIzaSyDljiLYYqcHH-HiUqnVhgXL2RkUj3Uk80g")
        GMSPlacesClient.provideAPIKey("AIzaSyDljiLYYqcHH-HiUqnVhgXL2RkUj3Uk80g")
        
        //starting location manager
        LocationManager.shared.startUpdatingLocation()
        
        return true
    }
        
        
        
        func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
            print("User Email \(user.profile.email ?? "No Email")")
        }
        
        
        func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            return GIDSignIn.sharedInstance().handle(url)
        }
        
        
        

//    // MARK: UISceneSession Lifecycle
//
//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }


}

