//
//  LoginModel.swift
//  Simsar
//
//  Created by hamza Ahmed on 04.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginResponseModel: Codable {
    let status, message: String?
    let errors: [String]?
    let user: User?
}

// MARK: - User
struct User: Codable {
    let id: Int
    let email,phone, authToken: String?
    
    enum CodingKeys: String, CodingKey {
        case id, email,phone
        case authToken = "auth_token"
    }
}

