//
//  SubCategoriesModel.swift
//  Simsar
//
//  Created by Hamza Khan on 12.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
struct SubCategoriesService{
    static let shared =  SubCategoriesService()
    
    let urlString = Constant.base_url + Constant.eSubCategories
    
    func getSubCategoriesResponse(apiParams:[String : Any] , completion: @escaping(_ res : [SubCategoriesData]?, _ error:String?)-> Void){   
            let parameters = Parameter()
            parameters.dictionary = apiParams
            let serviceObj = Service(url: urlString, parameters: parameters, method: .post)
            ServiceManager.shared.request(service: serviceObj, model: SubCategoriesStruct.self) { (result, error) in
                if (result != nil ){
                    print ("result is not nil")
                    completion(result?.data,nil)
                }else{
                    completion(nil , error?.localizedDescription)
                }
            }
    }
}



// MARK: - SubCategoriesStruct
struct SubCategoriesStruct: Codable {
    let status: String?
    let message:String?
    let data: [SubCategoriesData]?
    let errors: [String]?
}

// MARK: - Datum
struct SubCategoriesData: Codable {
    let id: Int?
    let title, datumDescription, image: String?

    enum CodingKeys: String, CodingKey {
        case id, title
        case datumDescription = "description"
        case image
    }
}
