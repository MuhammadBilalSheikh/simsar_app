//
//  ForgetPasswordModel.swift
//  Simsar
//
//  Created by Hamza Khan on 08.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation

struct ResetPasswordService{
    static var byEmail = ResetPasswordService(urlString: Constant.base_url + Constant.eResetPasswordEmailUrl)
    static var byNumber = ResetPasswordService(urlString: Constant.base_url + Constant.eResetPasswordNumberUrl)
    static var recoveryCodeByEmail = ResetPasswordService(urlString: Constant.base_url + Constant.eResetPasswordRecoveyCodeByEmailUrl)
    static var recoveryCodeByNumber = ResetPasswordService(urlString: Constant.base_url + Constant.eResetPasswordRecoveyCodeByNumberUrl)
    static let changePassword = ResetPasswordService(urlString: Constant.base_url + Constant.eChangePasswordUrl)
    private var urlString:String?
    init(urlString:String) {
        self.urlString = urlString
    }
    init() {}
    
    func getRecoveryCodeResponse(apiParams:Parameter , completion: @escaping(_ res : ResetPasswordRecoveryCodeStruct?, _ error:String?)-> Void){
        let serviceObj = Service(url: Constant.base_url + Constant.eResetPasswordRecoveyCodeByEmailUrl , parameters: apiParams,method: .post)
        serviceObj.headers =  Constant.default_header
        var resetPasswordResponce:ResetPasswordRecoveryCodeStruct?
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { (result, error) in
            if (result != nil){
                let decoder = JSONDecoder()
                print ("RecoveryCode result is not nil")
                do {
                    let data = result?.data
                    resetPasswordResponce =  try decoder.decode(ResetPasswordRecoveryCodeStruct.self, from: data!)
                    if resetPasswordResponce?.status == "success"{
                        completion(resetPasswordResponce , nil)
                    }else {
                        completion(nil , resetPasswordResponce?.message ?? "")
                    }
                } catch let error {
                    completion(nil , error.localizedDescription )
                }
            }else {
                completion(nil , error?.description)
            }
        }
    }
    
    
    
    func getRecoveryCodeNum(apiParams:Parameter , completion: @escaping(_ res : ResetPasswordRecoveryCodeStruct?, _ error:String?)-> Void){
        let serviceObj = Service(url: Constant.base_url + Constant.eResetPasswordRecoveyCodeByNumberUrl , parameters: apiParams,method: .post)
        serviceObj.headers =  Constant.default_header
        var resetPasswordResponce:ResetPasswordRecoveryCodeStruct?
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { (result, error) in
            if (result != nil){
                let decoder = JSONDecoder()
                print ("RecoveryCode result is not nil")
                do {
                    let data = result?.data
                    resetPasswordResponce =  try decoder.decode(ResetPasswordRecoveryCodeStruct.self, from: data!)
                    if resetPasswordResponce?.status == "success"{
                        completion(resetPasswordResponce , nil)
                    }else {
                        completion(nil , resetPasswordResponce?.message ?? "")
                    }
                } catch let error {
                    completion(nil , error.localizedDescription )
                }
            }else {
                completion(nil , error?.description)
            }
        }
    }
    
    
    
    
    
    
    
    
    func getChangePasswordResponse(apiParams:Parameter , completion: @escaping(_ res : ForgotPasswordStruct?, _ error:String?)-> Void){
        let serviceObj = Service(url: urlString!, parameters: apiParams)
        serviceObj.headers =  Constant.default_header
        var resetPasswordResponce:ForgotPasswordStruct?
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { (result, error) in
            if (result != nil){
                let decoder = JSONDecoder()
                print ("changePassword result is not nil")
                do {
                    let data = result?.data
                    resetPasswordResponce =  try decoder.decode(ForgotPasswordStruct.self, from: data!)
                    if resetPasswordResponce?.status == "success"{
                        completion(resetPasswordResponce , nil)
                    }else {
                        completion(nil ,resetPasswordResponce?.message )
                    }
                } catch let error {
                    completion(nil , error.localizedDescription )
                }
            }else {
                completion(nil , error?.description)
            }
        }
    }
    
    func getResetPasswordResponse(apiParams:Parameter , completion: @escaping(_ res : ForgotPasswordStruct?, _ error:String?)-> Void){
        let serviceObj = Service(url: urlString!, parameters: apiParams)
        serviceObj.headers =  Constant.default_header
        var resetPasswordResponce:ForgotPasswordStruct?
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { (result, error) in
            if (result != nil){
                let decoder = JSONDecoder()
                print ("resetPassword result is not nil")
                do {
                    let data = result?.data
                    resetPasswordResponce =  try decoder.decode(ForgotPasswordStruct.self, from: data!)
                    if resetPasswordResponce?.status == "success"{
                        completion(resetPasswordResponce , nil)
                    }else {
                        completion(nil , resetPasswordResponce?.errors?[0] ?? resetPasswordResponce?.message )
                    }
                } catch let error {
                    completion(nil , error.localizedDescription )
                }
            }else {
                completion(nil , error?.description)
            }
        }
    }
}



// MARK: - ForgotPassword
struct ForgotPasswordStruct: Codable {
    var status, message: String?
    let userId, verificationCode,verificationcode: Int?
    let errors: [String]?
    
    enum CodingKeys: String, CodingKey {
        case status, message
        case userId = "user_id"
        case verificationCode, errors,verificationcode
    }
}
// MARK: - ResetPasswordRecoveryCodeStruct
struct ResetPasswordRecoveryCodeStruct: Codable {
    let status, userId, message: String?

    enum CodingKeys: String, CodingKey {
        case status
        case userId = "user_id"
        case message
    }
}
// MARK: - changePasswordStruct
struct ChangePasswordStruct: Codable {
    let status, message: String?
}
