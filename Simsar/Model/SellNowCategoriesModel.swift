//
//  SellNowCategoriesModel.swift
//  Simsar
//
//  Created by Hamza Khan on 11.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
enum err: Swift.Error {
    case requestFailed
}
struct SellNowCategoriesService{
    static let shared =  SellNowCategoriesService()
    let urlString = Constant.base_url + Constant.eSellNowCategoriesUrl
    
    func getSellNowCategoriesResponse(completion: @escaping(_ res : [SellNowCategoryData]?, _ error:String?)-> Void){
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key": Constant.client_id
        ]
        let serviceObj = Service(url: urlString, parameters: apiParams)
        serviceObj.headers =  Constant.default_header
        ServiceManager.shared.request(service: serviceObj, model: SellNowCategoriesStruct.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                completion(result?.data,nil)
            }else{
                completion(nil , error?.localizedDescription)
            }
        }
    }
}


// MARK: - SellNowCategoriesStruct
struct SellNowCategoriesStruct: Codable {
    let message,status: String?
    let data: [SellNowCategoryData]?
}

// MARK: - Datum
struct SellNowCategoryData: Codable {
    let id: Int?
    let title, datumDescription, iconImage: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case datumDescription = "description"
        case iconImage = "icon_image"
    }
}



// MARK: - Datum
struct SellNowCategoryDataTop: Codable {
    let id: Int?
    let title, datumDescription, iconImage: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case datumDescription = "description"
        case iconImage = "icon_image"
    }
}



// MARK: - Datum
struct sellNowBrands: Codable {
    let id: Int?
    let title, datumDescription, iconImage: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case datumDescription = "description"
        case iconImage = "image_logo"
    }
}

// MARK: - Datum
struct sellNowSubBrands: Codable {
    let id: Int?
    let title, datumDescription, cityname, long_description,price, del_price, state, createdDate, priceOnCall, wishlist, auction, discPrice, sparkLight ,iconImage, expiryDate: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, cityname, long_description, del_price, price, state, createdDate, priceOnCall, wishlist, auction, discPrice,   sparkLight, expiryDate
        case datumDescription = "description"
        case iconImage = "image_logo"
        
    }
}


// MARK: - Datum
struct searchPurchases: Codable {
    let id: Int?
    let title, price ,keyWords, items, isBuy : String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, price, keyWords, items, isBuy
       
    }
}

struct servicePlan: Codable {
    let id: Int?
    let title, price, isBuy: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, price, isBuy
       
    }
}



struct myBnkAccount: Codable {
    let id: Int?
    let accountTitle, accountNo, bankName, bankAddress, ibanNo, branchCode, swiftCode : String?
    
    enum CodingKeys: String, CodingKey {
        case id,accountTitle,accountNo, bankName, bankAddress, ibanNo, branchCode, swiftCode
    
    }
}



struct myLocation: Codable {
    let id: Int?
    let country, state, city, address : String?
    
    enum CodingKeys: String, CodingKey {
        case id,country,state, city, address
    }
}




