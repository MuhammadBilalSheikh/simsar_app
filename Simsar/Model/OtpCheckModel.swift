//
//  OtpCheckModel.swift
//  Simsar
//
//  Created by Hamza Khan on 05.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation


struct OtpCheckServiceForNumber {
    static let shared = OtpCheckServiceForNumber()
    
    func getOtpCheckForNumber(apiParams:Parameter,completion: @escaping(_ res : OtpCheckStructForNumber? ,_ error : String?  ) -> Void) {
        let serviceObj = Service(url: Constant.base_url + Constant.eOtpVerificationForNum , parameters: apiParams,method: .post )
        serviceObj.headers = Constant.default_header
        ServiceManager.shared.request(service: serviceObj, model: OtpCheckStructForNumber.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                if result?.status == "success"{
                    completion(result,nil)
                }else{
                    completion(nil,result?.message)
                }
            }else{
                completion(nil , error?.localizedDescription)
            }
        }//CLOSURE
    }
    
    
}






struct OtpCheckService{
    
    static let shared = OtpCheckService()
    
    func getOtpCheckResponceData(apiParams:Parameter,completion: @escaping(_ res : OtpCheckStruct? ,_ error : String?  ) -> Void) {
        let serviceObj = Service(url: Constant.base_url + Constant.eOtpVerificationEmailUrl , parameters: apiParams,method: .post )
        serviceObj.headers = Constant.default_header
        ServiceManager.shared.request(service: serviceObj, model: OtpCheckStruct.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                if result?.status == "success"{
                    completion(result,nil)
                }else{
                    completion(nil,result?.message)
                }
            }else{
                completion(nil , error?.localizedDescription)
            }
        }//CLOSURE
    }
}


// MARK: - OtpCheckStruct
public struct OtpCheckStruct: Codable {
    let status, message: String?
    let userID: String?
    
    enum CodingKeys: String, CodingKey {
        case status, message
        case userID = "user_id"
    }
}



// MARK: - OtpCheckStruct
public struct OtpCheckStructForNumber: Codable {
    let status, message: String?
    let userID: String?
    
    enum CodingKeys: String, CodingKey {
        case status, message
        case userID = "user_id"
    }
}
