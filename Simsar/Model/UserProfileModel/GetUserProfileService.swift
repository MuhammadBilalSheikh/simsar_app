//
//  GetUserProfileService.swift
//  Simsar
//
//  Created by Hamza Khan on 29.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation

struct GetUserProfileService {
    static  var shared =  GetUserProfileService()
    let urlString = Constant.base_url + Constant.eGetprofile
    var userId:Int?
    
    mutating func getUserProfileDataResponse( completion: @escaping(_ res : UserProfileData?, _ error:String?)-> Void){
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        let parameter = Parameter()
        parameter.dictionary = ["client_key":Constant.client_id,
                                "user_id": userId!
        ]
        
        print("++++UserContact DetailParams ****** \(userId)")
        let serviceObj = Service(url: urlString, parameters: parameter, method: .post)
        ServiceManager.shared.request(service: serviceObj, model: UserProfileStruct.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                if result?.data != nil{
                    completion((result?.data)!,nil)
                }else {
                    completion(nil, "data not found")
                }
               
            }else{
                completion( nil , error?.localizedDescription ?? "")
            }
        }
    }
}











struct GetUserProductDetail {
    static  var shared =  GetUserProductDetail()
    let urlString = Constant.base_url + Constant.eGetprofile
    var userId:Int?
    
    mutating func getUserProfileDataResponse( completion: @escaping(_ res : UserProfileData?, _ error:String?)-> Void){
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        let parameter = Parameter()
        parameter.dictionary = ["client_key":Constant.client_id,
                                "user_id": userId!
        ]
        let serviceObj = Service(url: urlString, parameters: parameter, method: .post)
        ServiceManager.shared.request(service: serviceObj, model: UserProfileStruct.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                if result?.data != nil{
                    completion((result?.data)!,nil)
                }else {
                    completion(nil, "data not found")
                }
               
            }else{
                completion( nil , error?.localizedDescription ?? "")
            }
        }
    }
}








//struct GetUserEditProfileService {
//    static  var shared =  GetUserProfileService()
//    let urlString = Constant.base_url + Constant.eGetprofile
//    var userId:Int?
//
//    mutating func getUserProfileDataResponse( completion: @escaping(_ res : UserProfileData?, _ error:String?)-> Void){
//
//        if Model_UserStatus.shared.isUserIdThere(){
//            userId = Model_UserStatus.shared.getUserId()
//        }else {
//            return
//        }
//        let parameter = Parameters()
//        parameter.dictionary = ["client_key":Constant.client_id,
//                                "user_id": userId!
//        ]
//        let serviceObj = Service(url: urlString, parameters: parameter, method: .post)
//        ServiceManager.shared.request(service: serviceObj, model: UserProfileStruct.self) { (result, error) in
//            if (result != nil ){
//                print ("result is not nil")
//                if result?.data != nil{
//                    completion((result?.data)!,nil)
//                }else {
//                    completion(nil, "data not found")
//                }
//
//            }else{
//                completion( nil , error?.localizedDescription ?? "")
//            }
//        }
//    }
//}
