//
//  GetUserProfileModel.swift
//  Simsar
//
//  Created by Hamza Khan on 29.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
// MARK: - SubBrand

struct UserProfileStruct: Codable {
    let status: String?
    let data: UserProfileData?
    let message: String?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
        case message = "message"
    }
}

// MARK: - DataClass
struct UserProfileData: Codable {
    let id: Int?
    let uniqueId: String?
    let name: String?
    let email: String?
    let phone: String?
    let address: String?
    let aboutMe: String?
    let profilePic: String?
    let profileBanner: String?
    let facebookLink: String?
    let googleLink: String?
    let twitterLink: String?
    let linkedInLink: String?
    let skypeLink: String?
    let emailVerifiedAt: String?
    let providerId: String?
    let fatherName: String?
    let cnic: String?
    let dateOfBirth: String?
    let placeOfBirth: String?
    let employmentStatus: String?
    let gender: String?
    let mapId: String?
    let avatar: String?
    let authToken: String?
    let roleId: String?
    let isActive: String?
    let isBan: String?
    let isProfile: String?
    let isPhoneVerified: String?
    let isEmailVerified: String?
    let isCompany: String?
    let activeStatus: String?
    let darkMode: String?
    let messengerColor: String?
    let earnedCoins: String?
    let isBlockCount: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?
    let country: String?
    let city: String?
    let state: String?
    let bio: String?
    let activeSellNowCount : String?
    let expiredSellNowCount : String?
    let totalSellNowCount : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case activeSellNowCount = "active_SellNowCount"
        case expiredSellNowCount = "expired_SellNowCount"
        case totalSellNowCount = "total_SellNowCount"
        case uniqueId = "unique_id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case address = "address"
        case aboutMe = "about_me"
        case profilePic = "profile_pic"
        case profileBanner = "profile_banner"
        case facebookLink = "facebook_link"
        case googleLink = "google_link"
        case twitterLink = "twitter_link"
        case linkedInLink = "linkedIn_link"
        case skypeLink = "skype_link"
        case emailVerifiedAt = "email_verified_at"
        case providerId = "provider_id"
        case fatherName = "father_name"
        case cnic = "cnic"
        case dateOfBirth = "date_of_birth"
        case placeOfBirth = "place_of_birth"
        case employmentStatus = "employment_status"
        case gender = "gender"
        case mapId = "map_id"
        case avatar = "avatar"
        case authToken = "auth_token"
        case roleId = "role_id"
        case isActive = "is_active"
        case isBan = "is_ban"
        case isProfile = "Is_profile"
        case isPhoneVerified = "is_phone_verified"
        case isEmailVerified = "is_email_verified"
        case isCompany = "is_company"
        case activeStatus = "active_status"
        case darkMode = "dark_mode"
        case messengerColor = "messenger_color"
        case earnedCoins = "earned_coins"
        case isBlockCount = "is_block_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case country = "country"
        case city = "city"
        case state = "state"
        case bio = "bio"
    }
}
