//
//  MyProfileChat.swift
//  Simsar
//
//  Created by NxGeN on 2/15/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation


class chatCardModel {
    var adsData:[AdsApiData]?
    
    
     func getChatCards(urlExtention:String ,completion: @escaping(_ complete:Bool,_ data:[AdsApiData],_ error:String?)-> Void){
         let url =  Constant.base_url + urlExtention
         let apiParams  = Parameter()
         if (Model_UserStatus.shared.isUserIdThere() ){
             
             apiParams.dictionary = ["client_key" : Constant.client_id , "user_id" : Model_UserStatus.shared.getUserId() ]
             
         } else {
             apiParams.dictionary = ["client_key" : Constant.client_id]
         }
         
         let service = Service(url: url, parameters: apiParams, method: .post)
         ServiceManager.shared.request(service: service, model: HomeStruct.self) { (result, error) in
             if (result != nil ){
                 print ("result is not nil")
                 //self.adsData = result?.data ?? []
                 completion(true,result?.data ?? [],nil)
             }else{
                 completion(false,[],error?.localizedDescription)
     
             }
         }
     }
    
}
