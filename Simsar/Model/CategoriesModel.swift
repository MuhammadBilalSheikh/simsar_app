//
//  CategoriesModel.swift
//  Simsar
//
//  Created by Hamza Khan on 29.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation

struct CategoriesService{
    static let shared = CategoriesService()
    let urlString = Constant.base_url + Constant.eCategories
    
    func getCategoriesResponse( completion: @escaping(_ res : CategoriesData?, _ error:String?)-> Void){
            let parameters = Parameter()
        parameters.dictionary = ["client_key":Constant.client_id]
            let serviceObj = Service(url: urlString, parameters: parameters, method: .post)
            ServiceManager.shared.request(service: serviceObj, model: CategoriesStruct.self) { (result, error) in
                if (result != nil ){
                    print ("result is not nil")
                    completion(result?.data,nil)
                }else{
                    completion(nil , error?.localizedDescription)
                }
            }
    }
}

// MARK: - CategoryStruct
struct CategoriesStruct: Codable {
    let status: String?
    let data: CategoriesData?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

// MARK: - DataClass
struct CategoriesData: Codable {
    let topCategories: [Cat]?
    let categories: [Cat]?

    enum CodingKeys: String, CodingKey {
        case topCategories = "top_categories"
        case categories = "categories"
    }
}

// MARK: - Category
struct Cat: Codable {
    let id: Int?
    let title: String?
    let categoryDescription: String?
    let iconImage: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case categoryDescription = "description"
        case iconImage = "icon_image"
    }
}
