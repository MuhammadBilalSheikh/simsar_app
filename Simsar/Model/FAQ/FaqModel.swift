//
//  FaqModel.swift
//  Simsar
//
//  Created by Hamza Khan on 23.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation

// MARK: - FAQModel
struct FaqModel: Codable {
    let status: String?
    let data: [FaqData]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

// MARK: - Datum
class FaqData: Codable {
    let id: Int?
    let title: String?
    let slug: String?
    let status: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?
    let answer: [FaqAnswer]?
    var isOpened: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case slug = "slug"
        case status = "status"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case answer = "answer"
    }
}

// MARK: - Answer
struct FaqAnswer: Codable {
    let id: Int?
    let question: String?
    let answer: String?
    let createdAt: String?
    let updatedAt: String?
    let slug: String?
    let categoryId: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case question = "question"
        case answer = "answer"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case slug = "slug"
        case categoryId = "category_id"
    }
}
