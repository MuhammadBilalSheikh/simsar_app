//
//  FaqViewModel.swift
//  Simsar
//
//  Created by Hamza Khan on 23.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation


class FaqViewModel {
    
    
    private var data:[FaqData] = []
    func getFaqDataFromAPi(completion:@escaping( _ success:Bool, _ error:String?) -> Void)
    {
//        let url =  Constant.base_url + Constant.eFaq
        let parm  = Parameter()
        parm.dictionary = ["client_key":Constant.client_id]
        let service = Service(url: "https://simsar.com/api/v1/faqs", parameters: parm, method: .post)
        ServiceManager.shared.request(service: service, model: FaqModel.self) { (result, error) in
            
            guard let result = result else {
                completion(false,error?.localizedDescription)
                return
            }
            if result.status == "success" {
                if result.data?.count != 0 {
                    self.data = result.data!
                    completion(true,nil)
                }else{
                    
                    print("No FAQ data")
                    
                }
            }else{
                completion(false,error?.localizedDescription)
            }

        }
        
    }
    
    
    
    func getFaqSectionCount()-> Int{
        
        return data.count 
    }
    func getFaqRowCount(section:Int) -> Int{
        return data[section].answer?.count ?? 0
    }
    func getFaqArray(section:Int) ->[FaqAnswer]{
        guard let FaqArrayOfSection = data[section].answer else { return [] }
        return FaqArrayOfSection
    }
    func getFaqSectionTitle(section:Int) -> String{
        if let sectionTitle = data[section].title {
            return sectionTitle
        }
        return ""
    }
    func getFaqSection(section:Int) -> FaqData{
        if data.count != 0{
            return data[section]
        }else{
            
            return data[section]
        }
       
    }
    
}
