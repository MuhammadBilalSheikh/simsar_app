//
//  LocationModel.swift
//  Simsar
//
//  Created by NxGeN on 1/28/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation
// MARK: - SlidesModel
struct SearchCntForKuwModel: Codable {
    let status: String?
    let data: [CountryForKuw]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

// MARK: - Datum
struct CountryForKuw: Codable {
    let latitude: String?
    let longitude: String?
    let id: String?
    let country: String?

    enum CodingKeys: String, CodingKey {
        case latitude = "latitude"
        case longitude = "longitude"
        case id = "id"
        case country = "country"
    }
}

//   var userId: Int!
//        userId = Model_UserStatus.shared.getUserId()

struct SearchStateForKuwModel: Codable {
    let status: String?
    let data: [DataStateForKuw]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

// MARK: - Datum
struct DataStateForKuw: Codable {
    let id: Int?
    let name: String?
    let countryId: String?
    let latitude: String?
    let longitude: String?
    let createdAt: String?
    let updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case countryId = "country_id"
        case latitude = "latitude"
        case longitude = "longitude"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}



struct DataForBankModel: Codable {
    let id: Int?
    let ac_title: String?


    enum CodingKeys: String, CodingKey {
        case id = "id"
        case ac_title = "ac_title"
    }
}


struct SearchCntForBankModel: Codable {
    let status: String?
    let my_banks: [DataForBankModel]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case my_banks = "my_banks"
    }
}




struct SearchCityForKuwModel: Codable {
    let status: String?
    let data: [DataCityForKuw]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

// MARK: - Datum
struct DataCityForKuw: Codable {
    let id: Int?
    let name: String?
    let countryId: String?
    let stateId: String?
    let latitude: String?
    let longitude: String?
    let createdAt: String?
    let updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case countryId = "country_id"
        case stateId = "state_id"
        case latitude = "latitude"
        case longitude = "longitude"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}





class updateLocModel  {
    
    var stateId: Int!
    
    func getCountries(completion: @escaping(_ Country:[CountryForKuw],_ error:String?)-> Void){
        
        let url  = Constant.base_url +  Constant.eSearchCountForKuw
        
        let apiParams  = Parameter()
        
        apiParams.dictionary = ["client_key" : Constant.client_id]
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SearchCntForKuwModel.self) { (result, error) in
            if (result?.data != nil ){
                print ("result is not nil")
                print("****Result \(result)")
                let countriesDic = result?.data
//                var countries:[String] = []
//                for c in countriesDic! {
//                    countries.append(c.country ?? "")
//
//                }
                completion(countriesDic!, nil)
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
    
    
    
    func getState(completion: @escaping (_ state:[DataStateForKuw]?,_ error:String?) -> Void){
        
        let url =  Constant.base_url + Constant.eSearchStateForKuw
        let apiParams  = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id]
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SearchStateForKuwModel.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                let statesDic = result?.data
                completion(statesDic, "")
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
    
    
    func getBank(completion: @escaping (_ bnk:[DataForBankModel]?,_ error:String?) -> Void){
        
        let url =  Constant.base_url + Constant.eBankWithdraw
        let apiParams  = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "user_id" : 471
        ]
        // userI Model_UserStatus.shared.getUserId()
                                     
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SearchCntForBankModel.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                let statesDic = result?.my_banks
                completion(statesDic, "")
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
    
    
    
    
    
    func getCity(id: Int, completion: @escaping (_ cities:[DataCityForKuw],_ error:String?) -> Void){
        
        let url =  Constant.base_url + Constant.eSearchCityForKuw
        let apiParams  = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "state_id" : id]
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SearchCityForKuwModel.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                let cityDic = result?.data
                completion(cityDic!, "")
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
    
    
    
}




// MARK: - Datum get saved user location Data
struct UserSavedLocation : Codable {
    let status : String?
    let data : [UserSavedData]?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        data = try values.decodeIfPresent([UserSavedData].self, forKey: .data)
    }

}
struct UserSavedData : Codable {
    let id : Int?
    let user_id : String?
    let country : String?
    let state : String?
    let city : String?
    let lati : String?
    let lon : String?
    let address : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let deleted_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case country = "country"
        case state = "state"
        case city = "city"
        case lati = "lati"
        case lon = "lon"
        case address = "address"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case deleted_at = "deleted_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        lati = try values.decodeIfPresent(String.self, forKey: .lati)
        lon = try values.decodeIfPresent(String.self, forKey: .lon)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
    }

}

class UpdateSavedLocModel {
    func getSavedData(id: Int, completion: @escaping (_ cities:[UserSavedData],_ error:String?) -> Void){
        
        let url =  Constant.base_url + Constant.mapListening
        let apiParams  = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "user_id" : id]
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: UserSavedLocation.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                let cityDic = result?.data
                completion(cityDic ?? [], "")
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
}
