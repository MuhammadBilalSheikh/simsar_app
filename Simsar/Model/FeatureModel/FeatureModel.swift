//
//  FeatureModel.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let categoryStruct = try? newJSONDecoder().decode(CategoryStruct.self, from: jsonData)

import Foundation

// MARK: - CategoryStruct
struct FeatureStruct: Codable {
    let status: String?
    let data: [AdsApiData]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

