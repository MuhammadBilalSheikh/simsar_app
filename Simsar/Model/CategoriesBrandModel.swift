//
//  SibCategoriesBrand.swift
//  Simsar
//
//  Created by Hamza on 14.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
struct SubCategoriesBrandService{
    
    static  let shared =  SubCategoriesBrandService()
    let urlString = Constant.base_url + Constant.eSubCategoriesBrand
    
    func getSubCategoriesBrandResponse(apiParams:[String:Any] , completion: @escaping(_ res : [Adbrand]?, _ error:String?)-> Void){
        let parameters = Parameter()
        parameters.dictionary = apiParams
        let serviceObj = Service(url: urlString, parameters: parameters, method: .post)
        ServiceManager.shared.request(service: serviceObj, model: SubCategoriesBrandStruct.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                completion(result?.adbrand,nil)
            }else{
                completion(nil , error?.localizedDescription)
            }
        }
    }
}


// MARK: - SubCategoriesBrand
struct SubCategoriesBrandStruct: Codable {
    let status: String
    let message: String?
    let adbrand: [Adbrand]?
    let dropDown: [Button]?
    let button: [Button]?
    let inputBox: [Button]?
    let errors: [String]?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case status = "status"
        case adbrand = "adbrand"
        case dropDown = "dropDown"
        case button = "button"
        case inputBox = "inputBox"
        case errors = "errors"
    }
}

// MARK: - Adbrand
struct Adbrand: Codable {
    let id: Int
    let brandsId: String?
    let categoryId: String?
    let subCategoryId: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?
    let brandid: String?
    let brandtitle: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case brandsId = "brands_id"
        case categoryId = "category_id"
        case subCategoryId = "sub_category_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case brandid = "brandid"
        case brandtitle = "brandtitle"
    }
}

// MARK: - Button
struct Button: Codable {
    let id: Int
    let attributesId: String?
    let categoryId: String?
    let subCategoryId: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?
    let title: String?
    let buttonDescription: String?
    let slug: String?
    let ids: String?
    let textBoxType: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case attributesId = "attributes_id"
        case categoryId = "category_id"
        case subCategoryId = "sub_category_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case title = "title"
        case buttonDescription = "description"
        case slug = "slug"
        case ids = "ids"
        case textBoxType = "text_box_type"
    }
}
