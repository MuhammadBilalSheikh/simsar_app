//
//  HomeViewModel.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation


class HomeViewModel{
    
    var adsData:[AdsApiData]?
   
    func getAds(urlExtention:String ,completion: @escaping(_ complete:Bool,_ data:[AdsApiData],_ error:String?)-> Void){
        let url =  Constant.base_url + urlExtention
        let apiParams  = Parameter()
        if (Model_UserStatus.shared.isUserIdThere() ){
            
            apiParams.dictionary = ["client_key" : Constant.client_id , "user_id" : Model_UserStatus.shared.getUserId() ]
            
        } else {
            apiParams.dictionary = ["client_key" : Constant.client_id]
        }
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: HomeStruct.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                //self.adsData = result?.data ?? []
                completion(true,result?.data ?? [],nil)
            }else{
                completion(false,[],error?.localizedDescription)
    
            }
        }
    }
    
    func getSliderData(completion: @escaping(_ complete:Bool,_ data:[slidesData],_ error:String?)-> Void){
        let url =  Constant.base_url + Constant.eHomeSlider
        let apiParams  = Parameter()
        if (Model_UserStatus.shared.isUserIdThere() ){
            
            apiParams.dictionary = ["client_key" : Constant.client_id , "user_id" : Model_UserStatus.shared.getUserId() ]
            
        } else {
            apiParams.dictionary = ["client_key" : Constant.client_id]
        }
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SlidesStruct.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                //self.adsData = result?.data ?? []
                completion(true,result?.data ?? [],nil)
            }else{
                completion(false,[],error?.localizedDescription)
    
            }
        }
    }
    
//    func getHomeAds() -> [AdsApiData]
//    {
//        var data:[AdsApiData] = []
//        self.getAds(urlExtention: Constant.eHome) { (success, error) in
//            if success{
//                data = self.adsData ?? []
//
//            }else{
//                data = self.adsData ?? []
//            }
//        }
//        return data
//    }
//
//    func getFeaturedAds() -> [AdsApiData]?{
//        var data:[AdsApiData] = []
//        self.getAds(urlExtention: Constant.eHome) { (success, error) in
//            if success{
//                data = self.adsData ?? []
//
//            }else{
//                data = self.adsData ?? []
//            }
//        }
//        return data
//
//    }
//    func getOfferedAds() -> [AdsApiData]?{
//        
//    }
//    func getLatestAds() -> [AdsApiData]?{
//        
//    }
    
}
//var collectionOfHomeAds : [AdsApiData] = []
//var collectionOfFeaturedAdds : [AdsApiData] = []
//var collectionOfOfferedAdds : [AdsApiData] = []
//var collectionOfLatestAdds : [AdsApiData] = []
