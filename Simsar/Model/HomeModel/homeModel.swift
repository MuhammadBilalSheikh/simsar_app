//
//  homeModel.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let categoryStruct = try? newJSONDecoder().decode(CategoryStruct.self, from: jsonData)

import Foundation

// MARK: - CategoryStruct
struct HomeStruct: Codable {
    let status: String?
    let data: [AdsApiData]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

// MARK: - Datum
struct AdsApiData: Codable {
    let id: Int?
    let categoryId: String?
    let postId: String?
    let userId: String?
    let subCategoryId: String?
    let brandId: String?
    let subBrandId: String?
    let adType: String?
    let adTitle: String?
    let price: String?
    let disPrice: String?
    let disPercentage: String?
    let isFeatured: String?
    let isBumpupFeatured: String?
    let isSearchFeatured: String?
    let isCategoryFeatured: String?
    let isBrandFeatured: String?
    let isShopFeatured: String?
    let isHotFeatured: String?
    let isLatest: String?
    let isOffer: String?
    let isCallPrice: String?
    let mapId: String?
    let isAuction: String?
    let likeCount: String?
    let viewCount: String?
    let offerStartDate: String?
    let offerEndDate: String?
    let shortDescription: String?
    let longDescription: String?
    let mainImage: String?
    let hoverImage: String?
    let countryId: String?
    let stateId: String?
    let cityId: String?
    let latitude: String?
    let longitude: String?
    let countryIp: String?
    let sellerName: String?
    let sellerNumber: String?
    let isNumber: String?
    let isApprove: String?
    let statusId: String?
    let expiryDate: String?
    let createddate: String?
    let updatedAt: String?
    let deletedAt: String?
    let subCategory: String?
    let category: String?
    let statename: String?
    let cityname: String?
    let isWishlist: Int?
    let checkFor: String?
   

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case categoryId = "category_id"
        case postId = "post_id"
        case userId = "user_id"
        case subCategoryId = "sub_category_id"
        case brandId = "brand_id"
        case subBrandId = "sub_brand_id"
        case adType = "ad_type"
        case adTitle = "ad_title"
        case price = "price"
        case disPrice = "dis_price"
        case disPercentage = "dis_percentage"
        case isFeatured = "is_featured"
        case isBumpupFeatured = "is_bumpup_featured"
        case isSearchFeatured = "is_search_featured"
        case isCategoryFeatured = "is_category_featured"
        case isBrandFeatured = "is_brand_featured"
        case isShopFeatured = "is_shop_featured"
        case isHotFeatured = "is_hot_featured"
        case isLatest = "is_latest"
        case isOffer = "is_offer"
        case isCallPrice = "is_call_price"
        case mapId = "map_id"
        case isAuction = "is_auction"
        case likeCount = "like_count"
        case viewCount = "view_count"
        case offerStartDate = "offer_start_date"
        case offerEndDate = "offer_end_date"
        case shortDescription = "short_description"
        case longDescription = "long_description"
        case mainImage = "main_image"
        case hoverImage = "hover_image"
        case countryId = "country_id"
        case stateId = "state_id"
        case cityId = "city_id"
        case latitude = "latitude"
        case longitude = "longitude"
        case countryIp = "country_ip"
        case sellerName = "seller_name"
        case sellerNumber = "seller_number"
        case isNumber = "is_number"
        case isApprove = "is_approve"
        case statusId = "status_id"
        case expiryDate = "expiry_date"
        case createddate = "created_date"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case subCategory = "sub_category"
        case category = "category"
        case statename = "statename"
        case cityname = "cityname"
        case isWishlist = "is_wishlist"
        case checkFor = "is_CheckFor"
    }
  
}

// MARK: - SlidesModel
struct SlidesStruct: Codable {
    let status: String?
    let data: [slidesData]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
    }
}

// MARK: - Datum
struct slidesData: Codable {
    let id: Int?
    let slide: String?
    let backLink: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case slide = "slide"
        case backLink = "back_link"
    }
}
