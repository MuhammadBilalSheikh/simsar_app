//
//  Model_UserStatus.swift
//  Simsar
//
//  Created by NxGeN on 11/25/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation

struct Model_UserStatus {
    
    static var shared = Model_UserStatus()
    var userId : Int?
    let clientKey = Constant.client_id
    init() {
        //MOCK ID
        self.userId = UserDefaults.standard.integer(forKey: Constant.userDefaultUserId)
    }
    
    ///Checking If User Logged In ?
    mutating func isUserIdThere() -> Bool {
        userId = UserDefaults.standard.integer(forKey: Constant.userDefaultUserId)
        if userId != 0 {
            return true
        }else{
            return false
        }
    }
    mutating func getUserId()-> Int{
        if isUserIdThere(){
            self.userId = UserDefaults.standard.integer(forKey: Constant.userDefaultUserId)
            return userId ?? 0
        }else
        {
            return 0
        }
    }
}
