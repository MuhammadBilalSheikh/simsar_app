//
//  SellNowPostAdViewModel.swift
//  Simsar
//
//  Created by Hamza Khan on 02.02.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation

class SellNowPostAdViewModel{
    
    
    let urlString = Constant.base_url + Constant.eSubmitSellnow
    
    func postAd(apiParams:[String:Any] , completion: @escaping(_ success : Bool?,_ message:String?,_ adId:Int?,_ error:String?) -> Void){
        let parameters = Parameter()
        parameters.dictionary = apiParams
        let serviceObj = Service(url: urlString, parameters: parameters, method: .post  )
        ServiceManager.shared.request(service: serviceObj, model: PostAdResponceModel.self) { (result, error) in
            if (result?.status == "success" ){
                print ("result is not nil")
                print("Responce******* \(result!)")
                completion(true,result!.message,result?.sellnowId,nil)
            }else{
                completion(false ,result!.message ?? "",0, error?.localizedDescription)
            }
        }
    }
    
}
// MARK: - SlidesModel
struct PostAdResponceModel: Codable {
    let status: String?
    let message: String?
    let sellnowId:Int?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case sellnowId = "sellnow_id"
    }
}
