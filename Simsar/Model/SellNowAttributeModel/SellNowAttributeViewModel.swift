//
//  SellNowAttributeViewModel.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class SellNowAttributeViewModel{
    
    
    var subcategoryattributeArray: [Attribute]?
    var brandattributeArray: [Attribute]?
    var subbrandattributeArray: [Attribute]?
    var arrData : [[Attribute]?] = []
    
    func getData(categoryId:Int,subCategoryId:Int,brandId:Int,subBrandId:Int,  completion: @escaping(_ complete:Bool,_ error:String?)-> Void)
    {
        let url =  Constant.base_url + Constant.eSellProductDetail
        let parm  = Parameter()
        parm.dictionary = ["client_key":Constant.client_id,"category_id": categoryId,"sub_category_id": subCategoryId,"brand_id": brandId,"sub_brand_id": subBrandId]
        let service = Service(url: url, parameters: parm, method: .post)
        ServiceManager.shared.request(service: service, model: SellNowAttributeStruct.self) { (result, error) in
          //  print("Result Of Attibute*************%$#@ \(result)")
            //
            guard let result = result else {
                completion(false,error?.localizedDescription)
                return
            }
            if let subcategoryattribute = result.subcategoryattribute {
                self.arrData.append(subcategoryattribute)
            }
            if let brandattribute = result.brandattribute {
                self.arrData.append(brandattribute)
            }
            if let subbrandattribute = result.subbrandattribute {
                self.arrData.append(subbrandattribute)
            }
            completion(true,nil)
        }
    }
    func validateData()-> Bool{
        var isTextSelected = true
        var isPropertySelected = true

        for arrSection in arrData{
            for data in arrSection!{
                if data.attributeTypeId == "3"{
                    if (data.selectedText ?? "") == ""{
                        isTextSelected = false
                        return false
                    }
                }
                else if data.attributeTypeId == "2" || data.attributeTypeId == "1"{
                    var arrPropertyBool : [Bool] = []
                    // true , false , false
                    for property in data.property!{
                        guard let isSelected = property.isSelected else {
                            arrPropertyBool.append(false)
                            continue }
                        arrPropertyBool.append(isSelected)
                        
                        
                    }
                    if arrPropertyBool.contains(true){
                        isPropertySelected = true
                    }
                    else{
                        isPropertySelected = false
                        return false
                    }
                }
                
            }
        }
        
        return isTextSelected && isPropertySelected
    }
    func getDataForDetail(completionHandler: ( _ success : Bool, _ message: String, _ data: [Int : String]?)->Void){
        var attDict : [Int : String] = [:]

        if self.validateData(){
            for arrSection in arrData{
                let _ = arrSection.map { (attributes) in
                    let _ = attributes.map { (att) in
                        let attID = att.id ?? -1
                        
                        if att.attributeTypeId == "3"{
                            attDict[attID] = att.selectedText
                        }
                        else{
                            let selectedPropertyId = att.property.map { (properties) -> String in
                                let selectedProperty = properties.filter { (property) -> Bool in
                                    if property.isSelected ?? false{
                                        return true
                                    }
                                    return false
                                }
                                return String(selectedProperty.first!.id ?? -1)
                            }
                            attDict[attID] = selectedPropertyId ?? ""

                            
                        }
                    }
                }
            }
            print(attDict)
            completionHandler(true,"", attDict)
            
        }
        else{
            completionHandler(false,"Please fill all fields", nil)
        }
        
        
        
    }
    func didSelectValueFromDropDown(section: Int, row: Int, isSelected: Bool, propertyRow: Int){
        var properties =  self.arrData[section]![row].property!
        for i in 0 ..< properties.count{
            if propertyRow == i{
                properties[i].isSelected = isSelected
            }
            else{
                properties[i].isSelected = false
            }
        }
        self.arrData[section]![row].property = properties
    }
    func didWriteTextForAttribute(section: Int, attributeId: Int , selectedText: String){
        self.arrData[section]![attributeId].selectedText = selectedText
    }
    func didSelectRadioCell(section: Int, row: Int, isSelected: Bool, propertyRow: Int){
        var properties =  self.arrData[section]![row].property!
        for i in 0 ..< properties.count{
            if propertyRow == i{
                properties[i].isSelected = isSelected
            }
            else{
                properties[i].isSelected = false
            }
        }
        self.arrData[section]![row].property = properties
    }
    
    func getSectionCount()->Int{
        return arrData.count
    }
    func getRowForSection(section: Int) -> Int{
        guard let arrSection = arrData[section] else { return 0}
        return arrSection.count
    }
    func cellViewModelForRow(section: Int , row: Int)->SellNowCellViewModel{
        guard let arrSection = arrData[section] else { return SellNowCellViewModel(property: [], cellTypeID: "", title: "", attirbuteId: -1, selectedText: "")}
        let data = arrSection[row]
        return SellNowCellViewModel(property: data.property ?? [], cellTypeID: data.attributeTypeId ?? "", title: data.title ?? "", attirbuteId: data.id ?? -1, selectedText: data.selectedText ?? "")
        
    }
}

struct SellNowCellViewModel {
    var property : [Property]
    var cellTypeID : String
    var title : String
    var attirbuteId : Int
    var selectedText : String
}

enum SellNowCellType : Int{
    case dropdown = 0
    case radio
    case textfield
    
    //    func getCellIdentifier()-> String{
    //        switch self {
    //        case .dropdown:
    //            return "DropCell"
    //        case .radio:
    //            return "radio"
    //        case .textfield:
    //            return "textfield"
    //        }
    //    }
    
}
