//
//  SellNowAttributeModel.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let categoryStruct = try? newJSONDecoder().decode(CategoryStruct.self, from: jsonData)

import Foundation

// MARK: - CategoryStruct
struct SellNowAttributeStruct: Codable {
    let status: String?
    let subcategoryattribute: [Attribute]?
    let brandattribute: [Attribute]?
    let subbrandattribute: [Attribute]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case subcategoryattribute = "subcategoryattribute"
        case brandattribute = "brandattribute"
        case subbrandattribute = "subbrandattribute"
    }
}



enum DatedAt: String, Codable {
    case the20201028070051 = "2020-10-28 07:00:51"
    case the20210103125128 = "2021-01-03 12:51:28"
}

// MARK: - Property
struct Property: Codable {
    let id: Int?
    let title: String?
    let propertyDescription: String?
    let slug: String?
    let status: String?
    let attributeId: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?
    var isSelected : Bool? = false
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case propertyDescription = "description"
        case slug = "slug"
        case status = "status"
        case attributeId = "attribute_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case isSelected = "isSelected"


    }
}

// MARK: - Subcategoryattribute
struct Attribute: Codable {
    let id: Int?
    let attributesId: String?
    let categoryId: String?
    let subCategoryId: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?
    let title: String?
    let Description: String?
    let attributeTypeId: String?
    let slug: String?
    let ids: String?
    let subBrandId: String?
    let brandId: String?
    var property: [Property]?
    var selectedText: String?



    enum CodingKeys: String, CodingKey {
        case id = "id"
        case attributesId = "attributes_id"
        case categoryId = "category_id"
        case subCategoryId = "sub_category_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case title = "title"
        case Description = "description"
        case attributeTypeId = "attribute_type_id"
        case slug = "slug"
        case ids = "ids"
        case brandId = "brand_id"
        case subBrandId = "sub_brand_id"
        case property = "property"
        case selectedText = "selectedText"
    }
}

