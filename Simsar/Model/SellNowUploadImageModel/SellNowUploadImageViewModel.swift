//
//  SellNowUploadImageViewModel.swift
//  Simsar
//
//  Created by Hamza Khan on 06.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class SellNowUploadImageViewModel{
    
    static let shared = SellNowUploadImageViewModel()
    

    func updatePicture(imageData:Data,mainImageType:Int, completion: @escaping( _ isSuccess : Bool , _ imageName : String)->Void){
       // let param = Parameter()
        let param:[String: Any] = ["client_key" : Constant.client_id,"main_image_type":mainImageType]
        let url = Constant.base_url + Constant.eSellNoeUploadImage
       // let service = Service(url: url,parameters: param,method: .post)
        
        BaseRepository.instance.uploadImage(url: url, method: .post, params: param, imageData: imageData, header: Constant.default_header) { (success, responseMsg, data) in
            //self.isSuccess = success
            if success {
                guard let data = data else {return}
                let decoder = JSONDecoder()
                let model = try? decoder.decode(SellNowUploadImageModel.self, from: data.rawData())
                if model?.status == "success" {
                    print(model?.image ?? "")
                    completion(true,model?.image ?? "")
                }
            }else{
            completion(false,"")
            }
        }
    }
}
