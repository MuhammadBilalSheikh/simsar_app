//
//  SellNowUploadImageModel.swift
//  Simsar
//
//  Created by Hamza Khan on 06.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation
// MARK: - CategoryStruct
struct SellNowUploadImageModel: Codable {
    let status: String?
    let message: String?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case image = "image"
    }
}
