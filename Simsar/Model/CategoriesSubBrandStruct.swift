//
//  CategoriesSubBrandModel.swift
//  Simsar
//
//  Created by Hamza Khan on 22.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation


// MARK: - SubBrand
struct CategoriesSubBrandStruct: Codable {
    let status: String?
    let subbrand: [Subbrand]?
    let branddropdown: [Brand]?
    let brandbutton: [Brand]?
    let brandinputbox: [Brand]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case subbrand = "subbrand"
        case branddropdown = "branddropdown"
        case brandbutton = "brandbutton"
        case brandinputbox = "brandinputbox"
    }
}

// MARK: - Brand
struct Brand: Codable {
    let id: Int?
    let attributeId: String?
    let brandId: String?
    let subBrandId: String?
    let createdAt: String?
    let updatedAt: String?
    let title: String?
    let brandDescription: String?
    let slug: String?
    let ids: String?
    let textBoxType: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case attributeId = "attribute_id"
        case brandId = "brand_id"
        case subBrandId = "sub_brand_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case title = "title"
        case brandDescription = "description"
        case slug = "slug"
        case ids = "ids"
        case textBoxType = "text_box_type"
    }
}

// MARK: - Subbrand
struct Subbrand: Codable {
    let id: Int?
    let title: String?
    let subbrandDescription: String?
    let image: String?
    let iconImage: String?
    let metaKeywords: String?
    let metaDescription: String?
    let slug: String?
    let status: String?
    let brandId: String?
    let createdAt: String?
    let updatedAt: String?
    let deletedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case subbrandDescription = "description"
        case image = "image"
        case iconImage = "icon_image"
        case metaKeywords = "meta_keywords"
        case metaDescription = "meta_description"
        case slug = "slug"
        case status = "status"
        case brandId = "brand_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }
}
