//
//  SignUpModel.swift
//  Simsar
//
//  Created by Hamza  on 05.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation


struct SignUpServicesWithNum {
    static let shared = SignUpServicesWithNum()
    
    
    func getSignUpWithNumber(apiParams:Parameter,completion: @escaping(_ res : SignUpWithNumberResponseModel? ,_ error : String?  ) -> Void) {
        let serviceObj = Service(url: Constant.base_url + Constant.eSignUpWithNumberUrl , parameters: apiParams,method: .post )
        serviceObj.headers = Constant.default_header
        ServiceManager.shared.request(service: serviceObj, model: SignUpWithNumberResponseModel.self) { (result, error) in
            if (result != nil ){
        
              print ("result is not nil")
                 completion(result,nil)
           }else{
            completion(nil , error?.localizedDescription)
           }
        }//CLOSURE
    }
    
    
    
}





struct SignUpService {
    static let shared = SignUpService()
    
    func getSignUpData(apiParams:Parameter,completion: @escaping(_ res : SignUpResponseModel? ,_ error : String?  ) -> Void) {
        let serviceObj = Service(url: Constant.base_url + Constant.eSignUpUrl , parameters: apiParams,method: .post )
        serviceObj.headers = Constant.default_header
        ServiceManager.shared.request(service: serviceObj, model: SignUpResponseModel.self) { (result, error) in
            if (result != nil ){
        
              print ("result is not nil")
                 completion(result,nil)
           }else{
            completion(nil , error?.localizedDescription)
           }
        }//CLOSURE
    }
    
    
    
    
}


// MARK: - SignUpStruct
struct SignUpResponseModel: Codable {
    let status, message: String?
    let userID, verificationCode: Int?
    let errors: [String]?
    
    enum CodingKeys: String, CodingKey {
        case status, message
        case userID = "user_id"
        case verificationCode, errors
    
        
    }
}

// MARK: - SignUpStruct
struct SignUpWithNumberResponseModel: Codable {
    let status, message: String?
    let userID, verificationCode: Int?
    let errors: [String]?
    
    enum CodingKeys: String, CodingKey {
        case status, message
        case userID = "user_id"
        case verificationCode, errors
    
        
    }
}




