//
//  HomeSearchViewModel.swift
//  Simsar
//
//  Created by Hamza Khan on 19.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation

class HomeSearchViewModel{
    
 
    
    func getCountries(completion: @escaping(_ Country:[String],_ error:String?)-> Void){
        
        let url  = Constant.base_url +  Constant.eSearchCountry
        
        let apiParams  = Parameter()
        
        apiParams.dictionary = ["client_key" : Constant.client_id]
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SearchCountryModel.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                let countriesDic = result?.countries
                var countries:[String] = []
                for c in countriesDic! {
                    countries.append(c.country ?? "")
                }
                completion(countries, nil)
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
    
    func getStateByCountry(country:String, completion: @escaping (_ state:[String],_ error:String?) -> Void){
        
        let url =  Constant.base_url + Constant.eSearchStateByCountry
        let apiParams  = Parameter()
        apiParams.dictionary = ["country" : country]
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SearchStateByCountryModel.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                let statesDic = result?.state
                var states:[String] = []
                for c in statesDic! {
                    states.append(c.state ?? "")
                }
                completion(states, "")
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
    
    func getCityByState(state:String, completion: @escaping (_ state:[String],_ error:String?) -> Void){
        
        let url =  Constant.base_url + Constant.eSearchCityByState
        let apiParams  = Parameter()
        apiParams.dictionary = ["city" : state]
        
        let service = Service(url: url, parameters: apiParams, method: .post)
        ServiceManager.shared.request(service: service, model: SearchCityByStateModel.self) { (result, error) in
            if (result != nil ){
                print ("result is not nil")
                let cityDic = result?.city
                var cities:[String] = []
                for c in cityDic! {
                    cities.append(c.city ?? "")
                }
                completion(cities, "")
            }else{
                completion([],error?.localizedDescription)
                
            }
        }
    }
}
