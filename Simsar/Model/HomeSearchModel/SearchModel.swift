//
//  HomeSearchModel.swift
//  Simsar
//
//  Created by Hamza Khan on 19.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import Foundation

// MARK: - SearchCountryModel
struct SearchCountryModel: Codable {
    let status: String?
    let countries: [Country]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case countries = "countries"
    }
}

// MARK: - Country
struct Country: Codable {
    let country: String?

    enum CodingKeys: String, CodingKey {
        case country = "country"
    }
}

// MARK: - SearchStateByCountryModel
struct SearchStateByCountryModel: Codable {
    let status: String?
    let state: [State]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case state = "state"
    }
}

// MARK: - State
struct State: Codable {
    let state: String?

    enum CodingKeys: String, CodingKey {
        case state = "state"
    }
}

// MARK: - SearchCityByStateModel
struct SearchCityByStateModel: Codable {
    let status: String?
    let city: [City]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case city = "city"
    }
}

// MARK: - City
struct City: Codable {
    let city: String?

    enum CodingKeys: String, CodingKey {
        case city = "city"
    }
}
