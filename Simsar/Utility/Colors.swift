//
//  Colors.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit


class Colors {
  let colorTop = UIColor(red: 192.0/255.0, green: 38.0/255.0, blue: 42.0/255.0, alpha: 1.0)
  let colorBottom = UIColor(red: 35.0/255.0, green: 2.0/255.0, blue: 2.0/255.0, alpha: 1.0)

  let gl: CAGradientLayer

  init() {
    gl = CAGradientLayer()
    gl.colors = [ colorTop, colorBottom]
    gl.locations = [ 0.0, 1.0]
  }
}


//
//https://simsar.com/testsimsar/api/v1/my-ads
//client_key === wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy
//user_id === 308





//"status": "success",
//"data": {
//    "SellNow": [
//        {
//            "id": 440,
//            "category_id": "56",
//            "post_id": "396627761",
//            "user_id": "308",
//            "sub_category_id": "96",
//            "brand_id": "85",
//            "sub_brand_id": "421",
//            "ad_type": "Normal",
//            "ad_title": "dsvdsv",
//            "price": "0",
//            "dis_price": "0",
//            "dis_percentage": "0",
//            "is_featured": "0",
//            "is_bumpup_featured": "0",
//            "is_search_featured": "0",
//            "is_category_featured": "0",
//            "is_brand_featured": "0",
//            "is_shop_featured": "0",
//            "is_hot_featured": "0",
//            "is_latest": "0",
//            "is_offer": "0",
//            "is_call_price": "1",
//            "map_id": "179",
//            "is_auction": "0",
//            "like_count": "0",
//            "view_count": "0",
//            "offer_start_date": null,
//            "offer_end_date": null,
//            "short_description": null,
//            "long_description": "sdvdsvds",
//            "main_image": "1605532920.png",
//            "hover_image": "1605532929.png",
//            "country_id": "Pakistan",
//            "state_id": "Sindh",
//            "city_id": "Karachi",
//            "latitude": "24.9020",
//            "longitude": "67.0324",
//            "country_ip": "0",
//            "seller_name": "ahmed",
//            "seller_number": "03470000000",
//            "is_number": "1",
//            "is_approve": "0",
//            "status_id": "1",
//            "expiry_date": "2020-12-16",
//            "created_at": "2020-11-16 13:22:18",
//            "updated_at": "2020-11-16 13:22:18",
//            "deleted_at": null,
//            "sub_category": "Asian Cars",
//            "category": "Cars",
//            "statename": null,
//            "cityname": null,
//            "is_wishlist": 0
//        }




//filter api for my ads
//https://simsar.com/testsimsar/api/v1/my-ads-status
//client_key === wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy
//user_id === 308
//status === 0




//{
//"status": "success",
//"data": {
//    "myAdsStatusWise": [
//        {
//            "id": 440,
//            "category_id": "56",
//            "post_id": "396627761",
//            "user_id": "308",
//            "sub_category_id": "96",
//            "brand_id": "85",
//            "sub_brand_id": "421",
//            "ad_type": "Normal",
//            "ad_title": "dsvdsv",
//            "price": "0",
//            "dis_price": "0",
//            "dis_percentage": "0",
//            "is_featured": "0",
//            "is_bumpup_featured": "0",
//            "is_search_featured": "0",
//            "is_category_featured": "0",
//            "is_brand_featured": "0",
//            "is_shop_featured": "0",
//            "is_hot_featured": "0",
//            "is_latest": "0",
//            "is_offer": "0",
//            "is_call_price": "1",
//            "map_id": "179",
//            "is_auction": "0",
//            "like_count": "0",
//            "view_count": "0",
//            "offer_start_date": null,
//            "offer_end_date": null,
//            "short_description": null,
//            "long_description": "sdvdsvds",
//            "main_image": "1605532920.png",
//            "hover_image": "1605532929.png",
//            "country_id": "Pakistan",
//            "state_id": "Sindh",
//            "city_id": "Karachi",
//            "latitude": "24.9020",
//            "longitude": "67.0324",
//            "country_ip": "0",
//            "seller_name": "ahmed",
//            "seller_number": "03470000000",
//            "is_number": "1",
//            "is_approve": "0",
//            "status_id": "1",
//            "expiry_date": "2020-12-16",
//            "created_at": "2020-11-16 13:22:18",
//            "updated_at": "2020-11-16 13:22:18",
//            "deleted_at": null,
//            "sub_category": "Asian Cars",
//            "category": "Cars",
//            "statename": null,
//            "cityname": null,
//            "is_wishlist": 0
//        }
