//
//  File.swift
//  TabMenu
//
//  Created by Sarmad Ashfaq on 10/19/1398 AP.
//  Copyright © 1398 Softgear. All rights reserved.
//

import UIKit

extension UIColor {
    public class var PrimaryColor : UIColor {
           return UIColor(red: 0.18, green: 0.81, blue: 0.79, alpha: 1.00)
       }
}








extension UIViewController{
    func openVC(_ identifier : String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller  = storyboard.instantiateViewController(withIdentifier: identifier)
        navigationController?.pushViewController(controller, animated: true)
        }
    
    func openSecondVC(_ identifier : String) {
        let storyboard = UIStoryboard(name: "mainTwo", bundle: nil)
        let controller  = storyboard.instantiateViewController(withIdentifier: identifier)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func openMyPurchasesVC(_ identifier : String) {
        let storyboard = UIStoryboard(name: "MyPurchases", bundle: nil)
        let controller  = storyboard.instantiateViewController(withIdentifier: identifier)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func openSellNowVC(_ identifier : String) {
        let storyboard = UIStoryboard(name: "SellNow", bundle: nil)
        let controller  = storyboard.instantiateViewController(withIdentifier: identifier)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    func showAlertView(message : String,title : String){
         let alertController = UIAlertController(title: title, message:message, preferredStyle: UIAlertController.Style.alert)
         alertController.addAction(UIAlertAction(title: "Ok" , style: UIAlertAction.Style.default,handler: nil))
         present(alertController, animated: false, completion: nil)
     }
    
    func showAlertView(message : String,title : String,action : UIAlertAction){
        let alertController = UIAlertController(title: title, message:message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(action)
        present(alertController, animated: false, completion: nil)
    }
}

extension UIImageView{
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 0.2
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}

extension UIView {
    
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
    
    func addLine(position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)

        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))

        switch position {
        case .LINE_POSITION_TOP:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity

        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    func applyGradient(){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [Constant.themeColor.cgColor,Constant.themeColor_Light.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        if UIDevice.current.userInterfaceIdiom == .pad{
            gradientLayer.endPoint = CGPoint(x: 2, y: 0)
        }else{
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        }
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func applyGradient(colors: [CGColor]){
          let gradientLayer = CAGradientLayer()
          gradientLayer.colors = colors
          gradientLayer.startPoint = CGPoint(x: 0, y: 0)
          if UIDevice.current.userInterfaceIdiom == .pad{
              gradientLayer.endPoint = CGPoint(x: 2, y: 0)
          }else{
              gradientLayer.endPoint = CGPoint(x: 1, y: 0)
          }
          gradientLayer.frame = self.bounds
          self.layer.insertSublayer(gradientLayer, at: 0)
      }
    
    func DropCardView(){
        self.backgroundColor = UIColor.white
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 10 / 2.0
        self.layer.shadowOpacity = 0.3
    }
    func dropCardView(){
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 5 / 2.0
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
    }
    
    func dropbtnShadow(){
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 6)
        self.layer.shadowOpacity = 0.5
    }
    
    func makeRound(radius : CGFloat){
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
    func makeCircle(){
        self.layer.cornerRadius = self.frame.width / 2
    }
    func makeBorder(size : CGFloat){
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = size
    }
    func roundCurve(cornerRadius : CGFloat){
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.maskedCorners = [.layerMinXMaxYCorner , .layerMaxXMaxYCorner] //Bottom left , Bottom Right Respectively
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }

    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func makeViewRoundCorner(){
        self.layer.cornerRadius = self.frame.height * 0.1
        self.layer.masksToBounds =  true
        self.clipsToBounds = true
    }
}

extension String{
    var htmlToAttributedString: NSAttributedString? {
           guard let data = data(using: .utf8) else { return nil }
           do {
               return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
           } catch {
               return nil
           }
       }
       var htmlToString: String {
           return htmlToAttributedString?.string ?? ""
       }
}
   extension Array where Element: Equatable {

    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
}

extension UITextField {
func setLeftPaddingPoints(_ amount:CGFloat){
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
    self.leftView = paddingView
    self.leftViewMode = .always
}
func setRightPaddingPoints(_ amount:CGFloat) {
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
    self.rightView = paddingView
    self.rightViewMode = .always
}
  func MakeOTPFeild(){
      self.layer.borderColor = UIColor.black.cgColor
      self.layer.borderWidth = 1
      self.layer.cornerRadius = 12
  }
    func changePlaceHolder (PlaceHoldertxt : String , Color : UIColor){
        self.attributedPlaceholder = NSAttributedString(string: PlaceHoldertxt,
        attributes: [NSAttributedString.Key.foregroundColor: Color])
    }
    func addInputViewDatePicker(target: Any, selector: Selector , textFieldText:String?) {


    //Add DatePicker as inputView
        let datePicker = UIDatePicker()
    //Set date for date Picker
      if textFieldText != nil {
          /// Set Textfield date & time
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .none
            dateFormatter.timeStyle = .short
            dateFormatter.locale = Locale.init(identifier: "it_IT")
        datePicker.date = dateFormatter.date(from: textFieldText ?? "" )!
        }
       else {
        /// set current date & time
        datePicker.date = Date()
      }

         //Add Tool Bar as input AccessoryView
      }
}


//extension to sett Back Button
extension UIViewController {
    
    func setBackNavButton( ) {
        
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
     
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(localBackToMain))
        view.addGestureRecognizer(backTap)
        
        let leftBarButtonItem = UIBarButtonItem(customView: view )
        
        
        
        
      
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func localBackToMain() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
}


///FOR NAV BAR WITH NOTIFICATION ITEM
extension UIViewController {
    
    ///Navigation Bar With Notification Icon By Side
    func setNavBarWithNotificationIcon(){
       
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
       self.navigationItem.setHidesBackButton(true, animated:false)

        //Making Background NIL
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default )
        //MAKING LINE SHADER OF NAViGTAION BAR TO NIL
        self.navigationController?.navigationBar.shadowImage = UIImage()
       //your custom view for back image with custom size
       let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
       let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

       if let imgBackArrow = UIImage(named: "Shape") {
           imageView.image = imgBackArrow
       }
       view.addSubview(imageView)

       let backTap = UITapGestureRecognizer(target: self, action: #selector(localBackToMainWithNotification))
       view.addGestureRecognizer(backTap)

       let leftBarButtonItem = UIBarButtonItem(customView: view)
       self.navigationItem.leftBarButtonItem = leftBarButtonItem
    
        let notification_btn = UIBarButtonItem(image: UIImage(named: "notification-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(localOpenNotification))
       self.navigationItem.rightBarButtonItem = notification_btn
    }
    
    @objc func localBackToMainWithNotification(){
        print("-> back Button Clicked -> localBackToMainWithNotification ")
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true , completion: nil )
    }
    @objc func localOpenNotification(){
     openVC("VC_Notifications")
    }
}



extension UIViewController {
    
    func makeSimsarThemedButton( outerView : UIView , button : UIButton ,needGradient:Bool){
        
        button.titleLabel?.textColor = UIColor.white
        button.makeRound(radius: button.frame.height/2)
        if needGradient{
            button.applyGradient()
        }
        CustomizationOfView(view: outerView)
    }
    
    func makeSimsarThemedButton( outerView : UIView , buttonView : UIView,needGradient:Bool ){
       // buttonView.titleLabel?.textColor = UIColor.white
        buttonView.makeRound(radius: buttonView.frame.height/2)
        if needGradient{
            buttonView.applyGradient()
        }
        CustomizationOfView(view: outerView)
    }
    
    private func CustomizationOfView(view:UIView){
        view.layer.masksToBounds = false
        view.layer.cornerRadius = view.frame.height/2
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 3
        view.layer.shadowOffset = CGSize( width: 0, height: 3 )
    }
}

extension UIViewController {
    
    func startAnimatingButton(_ enable : Bool ,_ btn : UIButton ,_ indicator : UIActivityIndicatorView ){
        if (enable){
            btn.isEnabled = false
            indicator.isHidden = false
            indicator.startAnimating()
        }else {
            btn.isEnabled = true
            indicator.isHidden = true
            indicator.stopAnimating()
        }
    }
}

extension UIViewController {
    
    func createGlobalIndicator() -> UIViewController? {
        let sb = UIStoryboard(name: "Main", bundle: nil )
        var controller = sb.instantiateViewController(withIdentifier: "VC_CustomIndicator" )
        return controller
    }
    
    func showGlobalIndicator(_ indicator : UIViewController? , _ parent : UIViewController){
        parent.view.showAnimatedGradientSkeleton()
        self.view.showAnimatedSkeleton()
    }
    
    func stopGlobalIndicator(_ indicator : UIViewController ){
        print ("Stop Global indicator Called")
        self.view.hideSkeleton()
    }
}

extension UIViewController {
    
    func createGlobalBumpup() -> VC_GlobalBumpup {
        let sb = UIStoryboard(name: "Main", bundle: nil )
        var controller = sb.instantiateViewController(withIdentifier: "VC_GlobalBumpup" ) as! VC_GlobalBumpup
        return controller
    }
    
    func showGlobalBumpup(_ indicator : UIViewController? , _ parent : UIViewController ){
        parent.addChild( indicator! )
        guard let vc = indicator else { print ("No ViewController Found " );  return }
        vc.view.frame = parent.view.frame
        parent.view.addSubview( vc.view )
        vc.view.alpha = 0
        UIView.animate(withDuration: 0.55 , animations:{ vc.view.alpha = 1 } ) { (done) in vc.didMove(toParent: parent )}
    }
    
    func removeGlobalBumpup(_ indicator : UIViewController ){
        print ("Remove Global Bumpup Called")
        UIView.animate(withDuration: 0.55 , animations:
            {
                indicator.view.alpha = 0
        }) { (done) in indicator.view.removeFromSuperview() }
    }
}

extension UIViewController {
    /// Embeds a UIViewController inside of another UIViewController using its view.
    /// - Parameters:
    ///   - Parameter viewController: UIViewController to embed
    ///   - Parameter frame:  A frame to be used. Nil by default and used view's frame.
    func embed(viewController: UIViewController, frame: CGRect? = nil) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.frame = view.bounds
        viewController.didMove(toParent: self)
    }
    
    /// Removes an embedded UIViewController from a UIVIewController
    /// - Parameters:
    ///   - Parameter embeddedViewController: UIViewController to remove
    func remove(embeddedViewController: UIViewController) {
        guard children.contains(embeddedViewController) else {
            return
        }
        
        embeddedViewController.willMove(toParent: nil)
        embeddedViewController.view.removeFromSuperview()
        embeddedViewController.removeFromParent()
    }
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

extension UIButton
{
///buttonn customization for sell now module /// 
    
    func cutomizingButtonNavigation(btnTitle:String,xAxis:CGFloat,yAxis:CGFloat){
        let buttonFontSize:CGFloat = 19
        let buttonTitleSize = (btnTitle as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: buttonFontSize)]).width
        self.frame = CGRect(x: xAxis, y: yAxis, width: (buttonTitleSize <= 100 ) ? 100 : buttonTitleSize, height: 30)
        self.setTitle(btnTitle, for: .normal)
        self.backgroundColor = .systemOrange
        self.applyGradient()
        self.roundCorners(corners: .allCorners, radius: 15)
        self.setTitleColor(.white, for: .normal)
    }

}



extension UICollectionViewCell{
    
    func cellCustomization(){
        self.contentView.layer.cornerRadius = 15
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 10 / 2.0
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 15
    }
    
    
}
