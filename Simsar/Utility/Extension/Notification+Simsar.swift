//
//  Notification+Simsar.swift
//  Simsar
//
//  Created by NxGeN on 10/21/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    // triggers when app receives location for the first time in app cycle.
    static let receivedLocationFirstTime = Notification.Name("com.simsar.receivedLocationFirstTime")
//
//    // triggers when user's ride has been accepted by any driver
//    static let driverAcceptedRide = Notification.Name("com.hustle.driverAcceptedRide")
//
//    // triggers when driver has arrived at the requested location from the user
//    static let driverArrived = Notification.Name("com.hustle.driverArrived")
//
//    // triggers when driver has started the ride
//    static let rideStarted = Notification.Name("com.hustle.rideStarted")
//
//    // triggers when driver has cancelled the ride after accepting it.
//    static let driverCancelledRide = Notification.Name("com.hustle.driverCancelledRide")
//
//    // triggers when drivers ended the ride.
//    static let rideEnded = Notification.Name("com.hustle.rideEnded")
//
//    // triggers when user receives a new message
//    static let newMessage = Notification.Name("com.hustle.newMessage")
    
}
