//
//  AppThemes.swift
//  Simsar
//
//  Created by NxGeN on 10/21/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class AppTheme{
    
    private static let screenBackgroundColor = "f4f4f4"
    private static let primaryColor = "AC2625"
    private static let secondaryColor = "394b83"
    private static let textColor = "757575"
    private static let successColor = "1eba23"
    private static let errorColor = "e32424"
    private static let darkColor = "e9b821"
    private static let redColor = "ff5c64"
    private static let greenColor = "0bcc7b"
    private static let deliveryPrimaryColor = "10C388"
    
    //MARK:- Colors
    static var colorScreenBackground: UIColor { get { return color(from: screenBackgroundColor)}}
    static var colorPrimary: UIColor { get { return color(from: primaryColor) }}
    static var colorSecondary: UIColor { get { return color(from: secondaryColor) }}
    static var colorText: UIColor { get { return color(from: textColor) }}
    static var colorSuccess: UIColor { get { return color(from: successColor)}}
    static var colorError: UIColor { get { return color(from: errorColor)}}
    static var colorDark: UIColor { get { return color(from: darkColor)}}
    static var colorRed: UIColor { get { return color(from: redColor)}}
    static var colorGreen: UIColor { get { return color(from: greenColor)}}
    static var colorDeliveryPrimary: UIColor { get { return color(from: deliveryPrimaryColor)}}
    
    //MARK:- Fonts
    static func fontText(ofSize: CGFloat) -> UIFont{
        return textFont(of: ofSize)
    }
    
    static func fontHeading(ofSize: CGFloat) -> UIFont{
        return headingFont(of: ofSize)
    }
}

extension AppTheme{
    private static func color(from hexString: String) -> UIColor{
        let cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    private static func textFont(of size: CGFloat) -> UIFont{
        guard let font = UIFont.init(name: "Lato-Regular", size: size) else {
            assert(false, "Custom font not found")
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }
    
    private static func headingFont(of size: CGFloat) -> UIFont{
        guard let font = UIFont.init(name: "Lato-Bold", size: size) else{
            assert(false, "Custom font not found")
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }
}
