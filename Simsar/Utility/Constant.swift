//
//  Constant.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
struct Constant {
    
    static let themeColor = UIColor(red: 0.75, green: 0.00, blue: 0.06, alpha: 1)
    static let themeColor_Light = UIColor(red: 1.00, green: 0.28, blue: 0.34, alpha: 1)
    
    static let userDefaultUserId = "userid"
    static let internetError = "Internet not working properly"
    ///constants for services https://simsar.com/public/asset/images/sell-now/1611235691.png
    static let baserUrlImage = "https://simsar.com/public/asset/images/sell-now/"
    static let baseUrlImageForCategories = "https://simsar.com/public/assets/media/category/icon/"
    static let baseUrlImageForSlider = "https://simsar.com/public/assets/media/applicationslider/"
    static let productDetailSlider = "https://simsar.com/public/asset/images/sell-now/"
    static let client_id = "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy"
    static let base_url = "https://simsar.com/api/v1/"
    
//  static let base_url = "https://minibigerp.com/simsar/api/v1/"
      
    static let default_header = ["Content-Type" : "application/json"]
    
    /// extention for service Url
    static let eEmailLoginUrl = "email_login"
    static let ePhoneLoginUrl = "phone_login"
    static let eSignUpUrl = "register_with_email"
    static let eSignUpWithNumberUrl = "register_with_phone"
    static let eOtpVerificationEmailUrl = "verify-code-via-email"
    static let eOtpVerificationForNum = "verify-code-via-phone"
    static let eResetPasswordEmailUrl = "reset-password-via-email"
    static let eResetPasswordNumberUrl = "reset-password-via-phone"
    static let eResetPasswordRecoveyCodeByEmailUrl = "verify-code-via-email"
    static let eResetPasswordRecoveyCodeByNumberUrl = "verify-code-via-phone"
    static let eChangePasswordUrl = "change-password-via-reset"
    static let eSellNowCategoriesUrl = "sell-now-categories"
    static let eSubCategories = "sub-categories"
    static let eSubCategoriesBrand = "sub-categories-brand"
    static let eSubBrand = "brand-attribute"
    static let eGetprofile = "get_profile_dashboard"
    static let eCategories = "categories"
    static let eProductDetail = "get_ProductDetail"
    static let eFaq = "get_ProductDetail"
    static let eSellProductDetail = "sell-product-detail"
    static let eSellNoeUploadImage = "sell-upload-image-one"
    static let eSearchCountry = "search_country"
    static let eSearchStateByCountry = "search_state_by_country"
    static let eSearchCityByState = "search_city_by_state"
    static let eHomeAds = "hometest"
    static let eFeaturedAds = "featured_ads"
    static let eOfferedAds = "offered_ads"
    static let eLatestAds = "latest_ads"
    static let eHomeSlider = "slides"
    static let eSearchCountForKuw = "is-kuwait-country"
    static let eSearchStateForKuw = "is-kuwait-state"
    static let eSearchCityForKuw = "is-kuwait-city"
    static let eSubmitSellnow = "submit-sellnow"
    static let eEdirProfilePost = "set_profile"
    static let eChatProduct = "chat-product"
    static let eBuyServicePlan = "buyserviceadsplans"
    static let eBankWithdraw = "walletfunds"
    static let mapListening = "map-listing"
}
