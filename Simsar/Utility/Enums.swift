//
//  Enums.swift
//  Simsar
//
//  Created by Hamza Khan on 06.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation

enum VcIdentifier:String{
    case home = "HomeVC"
    case login = "VC_Login"
    case myBrands = "VC_BrandTab"
    case categories = "VC_TabbarCategories"
    case aboutSimsar = "VC_AboutSimsar"
    case aboutKnow = "KnowMoreVC"
    case termsAndConditions = "VC_TermsAndConditions"
    case chat = "VC_ChatsCards"
    case contactUs = "VC_ContactUS"
    case faq = "FaqVC"
    case signUp = "VC_SignUp"
    case selectCategories = "VC_SelectCategories"
    case termsOfUse = "VC_TermsOfUse"
    case paymentMethod = "VC_PaymentMethods"
    case privacyPolicy = "VC_PrivacyPolicy"
    case boostAds = "VC_BoostAds"
    case changePasswordConfirm = "VC_ChangePassword"
    case subCategories = "VC_ChooseSubCategories"
    case chooseBrands = "VC_ChooseBrands"
    case chooseSubBrands = "VC_ChooseSubBrands"
    //User Dashboard
  //  case myAdds = "VC_MyAdds"
    case wishlist = "VC_WishList"
    case services = "VC_Services"
    case rating = "VC_RatingAndComments"
    case viewAll = "VC_MyProfile"
    case setting = "SettingVC"
    case search = "VC_Search"
    case myAds = "VC_MyAds"
    case MyAdds = "VC_MyAdds"
    case brand2 = "VC_Brand2"
    case productDetail = "VC_ProductDetail"
    case sellNowAttribute = "VC_SellNowAttribute"
    case sellNowUploadPhoto =  "VC_UploadPhoto"
    case sellNowContactDetail = "VC_ContactDetail"
    case sellNowUpdateLocation = "VC_UpdateLocation"
    case sellNowUpdateLocForKuw = "UpdateLocVCForKuw"
    case welcome = "VC_Welcome"
    case Wallett = "VC_WalletProcessing"
    case sellNowPostAnotherAdd = "VC_PostAnotherAdd"
    
}
enum StoaryBoard:String {
    case main = "Main"
    case sellNow = "SellNow"
}
