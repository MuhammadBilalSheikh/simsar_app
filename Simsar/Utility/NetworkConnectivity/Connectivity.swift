//
//  Connectivity.swift
//  Simsar
//
//  Created by NxGeN on 10/17/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//


import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}


