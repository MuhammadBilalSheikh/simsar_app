//
//  LocationManager.swift
//  Simsar
//
//  Created by NxGeN on 10/21/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate{
    
    static let shared = LocationManager()
   
    private override init(){
        
    }
    private var lastKnownLocation: CLLocationCoordinate2D? // keeps the last received location of user, and will be updated through out the app cycle.
    private var locationManger = CLLocationManager()
    
    // start updating the location of the user and saves it in lastKnownLocation variable.
    func startUpdatingLocation(){
        locationManger = CLLocationManager()
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.startUpdatingLocation()
        
        
        
  
    }
    
    // stops updating the location of user, don't know when to call it, but implementing for future 🧐
    func stopUpdatingLocation(){
        locationManger.delegate = nil
        locationManger.stopUpdatingLocation()
      //  locationManger = nil
    }
    
    // return the last received location of the user.
    func lastReceivedLocation() -> CLLocationCoordinate2D?{
        return self.lastKnownLocation
    }
    
    // requests the location permission from the user
    func requestForLocationPermission(){
        let authorizationStatus = CLLocationManager.authorizationStatus()

        if (authorizationStatus == CLAuthorizationStatus.notDetermined)  {
            locationManger.requestWhenInUseAuthorization()
           } else {
            locationManger.startUpdatingLocation()
           }

        UserDefaults.standard.set(true, forKey: "hasAskedForLocationPermission")
    }
    func ifUserDenied()-> Bool{
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if (authorizationStatus == CLAuthorizationStatus.denied ) {
          // Display a message, do what you want
            return true
        }
        return false
        
    }
    // returns true if user has seen the location permission dialog atleast once.
    func hasAskedForLocationPermission() -> Bool{
        if let permissionStatus = UserDefaults.standard.value(forKey: "hasAskedForLocationPermission") as? Bool, permissionStatus{
            return true
        }
        return false
    }
    
    // location permission status
    func isLocationPermissionGranted() -> Bool{
        guard CLLocationManager.locationServicesEnabled() else { return false }
        return [.authorizedAlways, .authorizedWhenInUse].contains(CLLocationManager.authorizationStatus())
    }
    
 
    //MARK:- CLLocationDelegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if lastKnownLocation == nil && locations.first != nil {
            lastKnownLocation = locations.first!.coordinate
            NotificationCenter.default.post(name: .receivedLocationFirstTime, object: nil)
        }
        lastKnownLocation = locations.first?.coordinate
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error in getting location of user")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(status)
    }
}
