//
//  ServiceManager.swift
//  Simsar
//
//  Created by NxGeN on 10/17/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import Alamofire
import Kingfisher
import SwiftyJSON
enum URLError: Error {
    case noData, decodingError
}

class ServiceManager{
    
    static let shared = ServiceManager()
    private init(){}
    private let decoder = JSONDecoder()
    
    
    func request<T : Codable> (service : Service,model:T.Type ,showSpinner: Bool? = true, completion : @escaping(T?,Error?) -> Void)
    {
        if showSpinner ?? true{
           DispatchQueue.main.async{
              ActivityIndicator.shared.showSpinner(nil, title: nil)
           }
        }
        let config = URLSessionConfiguration.ephemeral
        config.waitsForConnectivity = true
        config.allowsCellularAccess = true
        let sesh = URLSession(configuration: config)
        if let url = URL(string: service.url!){
            var request = URLRequest(url: url)
            request.httpMethod = service.method.rawValue
            request.httpBody = service.parameters.dictionary.percentEscaped().data(using: .utf8)
            request.allHTTPHeaderFields =  [
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            sesh.dataTask(with: request) {[weak self] (data, response, error) in
                DispatchQueue.main.async {
                    if showSpinner ?? true{
                        ActivityIndicator.shared.hideSpinner()
                    }
                    
                
                    guard let data = data else {
                        if error == nil{
                            print(error?.localizedDescription ?? "Unknown Error")
                        }
                        completion(nil,URLError.noData)
                        return
                    }
                    if let response = response as? HTTPURLResponse{
                        guard (200 ... 299) ~= response.statusCode else {
                            print("Status code :- \(response.statusCode)")
                            print(response)
                            if response.statusCode == 422{
                                do{
                                    let decoded = try self!.decoder.decode(model, from: data)
                                    print(decoded)
                                    completion(decoded,nil)
                                    
                                }catch let error{
                                    print(error.localizedDescription)
                                    completion(nil, URLError.decodingError)
                                    
                                }
                            }
                            return
                        }
                    }
                    do{
                        let decoded = try self!.decoder.decode(model, from: data)
                        print(decoded)
                        completion(decoded,nil)
                        
                    }catch let error{
                        print(error.localizedDescription)
                        completion(nil, URLError.decodingError)
                        
                    }
                }
              
            }.resume()
        }
        
        
    }
    
    
    func getRequestForResponse(service : Service, completion: @escaping(_ res : DataResponse<Any>? ,_ error : String?  ) -> Void){
        Alamofire.request(service.url!, method: service.method, parameters: service.parameters.dictionary, encoding : JSONEncoding.default, headers : service.headers as? HTTPHeaders).responseJSON { (response) in
                do{
                    if response.response?.statusCode == 200{
                     //   let res = response.result as? DataResponse<Any>
                        completion( response , nil )
                    }else{
                        print ( "Error Ocurred" )
                        print("result",response.value as Any)
                        completion(nil , "Got error" )
                    }
                }
                catch {
                    print("Error in decoding json \n error: \(error.localizedDescription)")
                    completion( nil , " GOT ERROR" )
                }
            }

    }
    
    func downloadImageWithName(_ constUrl : String , _ urlName : String ,_ loader: UIActivityIndicatorView , _ imageView : UIImageView ){
        let url = constUrl + urlName
        let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let convertedUrl = URL(string: urlString!)
        loader.isHidden = false
        loader.startAnimating()
        
        guard convertedUrl != nil else {
            print("No URL Found\(convertedUrl?.absoluteString ?? "")")
            DispatchQueue.main.async {
                loader.stopAnimating()
                loader.isHidden = true
            }
            return
        }
        DispatchQueue.main.async {
            imageView.kf.setImage(with: convertedUrl)
            loader.stopAnimating()
            loader.isHidden = true
        }
    }
    
    func downloadImageWithName(_ constUrl : String , _ urlName : String ,  closure :  @escaping (_ img :UIImage? ) -> Void )  {
        let urlString = constUrl + urlName
        let convertedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL.init(string: convertedUrlString!) else {
            return  closure(nil)
        }
        let resource = ImageResource(downloadURL: url)
        
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                closure(value.image)
            case .failure:
                closure(nil)
            }
        }
    }
    
    func downloadImageWithName(_ constUrl : String , _ urlName : String , _ imageView : UIImageView ){
        //   let previousImage = imageView.image
        let url = constUrl + urlName
        print(url)
        let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let convertedUrl = URL(string: urlString!)
        guard convertedUrl != nil else {
            print("No URL Found\(convertedUrl?.absoluteString ?? "")")
            return
        }
        DispatchQueue.main.async {
            imageView.kf.setImage(with: convertedUrl)
        }
    }

}


//MARK: Service class
class Service
{
    var url : String?
    var parameters = Parameter()
    var method : HTTPMethod = .post
    var headers = Dictionary<String, String>()
    
    init(url : String) {
        self.url = url
    }
    
    convenience init (url : String , parameters : Parameter)
    {
        self.init(url: url)
        self.parameters = parameters
    }
    
    convenience init(url : String , method : HTTPMethod)
    {
        self.init(url: url)
        self.method = method
    }
    
    convenience init(url : String , parameters : Parameter , method : HTTPMethod) {
        self.init(url: url)
        self.parameters = parameters
        self.method = method
    }
    
    convenience init(url: String, parameters: Parameter, headers: Dictionary<String, String>){
        self.init(url: url, parameters: parameters)
        self.headers = headers
    }
}

//MARK: parameter class

class Parameter
{
    var dictionary = Dictionary<String , Any>()
    
    init(){}
    
    init(keys : [String] , values : [Any]) {
        
        self.populateDictionary(keys: keys, values: values)
    }
    
    init(dictionary : Dictionary<String , Any>)
    {
        self.dictionary = dictionary
    }
    
    func populateDictionary(keys keyArray : [String] , values valueArray : [Any])
    {
        for i in 0..<min(keyArray.count, valueArray.count)
        {
            dictionary[keyArray[i]] = valueArray[i]
        }
    }
}


