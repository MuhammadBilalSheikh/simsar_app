//
//  DashBoardCell.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class DashBoardCell: UICollectionViewCell {
    
    let themeColor = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.00).cgColor
    let themeColor_Darker = UIColor(red: 0.87, green: 0.87, blue: 0.87, alpha: 1.00).cgColor
    
    @IBOutlet weak var containverView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var favView: UIView!
    @IBOutlet weak var addUserView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var hourView: UIView!
    @IBOutlet weak var minuteView: UIView!
    @IBOutlet weak var secondsView: UIView!
    
    override func awakeFromNib() {
        gradientView.backgroundColor = UIColor.clear
        gradientView.applyGradient(colors: [themeColor , themeColor_Darker])
        gradientView.roundCurve(cornerRadius: 7)
        favView.makeCircle()
        addUserView.makeCircle()
        hourView.makeCircle()
        minuteView.makeCircle()
        secondsView.makeCircle()
        timeView.layer.cornerRadius = 6
          
      }
}
