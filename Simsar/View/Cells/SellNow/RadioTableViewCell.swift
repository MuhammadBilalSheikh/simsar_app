//
//  RadioTableViewCell.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
protocol RadioTableViewCellProtocol : class{
    func sendSelectionToSellNowViewModel(attributeIndex: Int, section: Int, isSelected: Bool, propertyRow: Int)->Void
}
class RadioTableViewCell: UITableViewCell {
    static let identifier = "RadioTableViewCell"
    weak var delegate : RadioTableViewCellProtocol!
    var section : Int!
    @IBOutlet weak var radioButtonTable: UITableView!{
        didSet{
            radioButtonTable.register(RadioCell.nib(), forCellReuseIdentifier: RadioCell.identifier)
            radioButtonTable.delegate = self
            radioButtonTable.dataSource = self
            radioButtonTable.estimatedRowHeight = 30
            radioButtonTable.rowHeight = UITableView.automaticDimension
        }
    }
    var cellViewModel : SellNowCellViewModel!{
        didSet{
            title.text = cellViewModel.title
        }
    }
    
    @IBOutlet weak var title: UILabel!
    
    static func nib() -> UINib {
        return UINib(nibName: "RadioTableViewCell" , bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   
    func cellViewModelForRadioCell(row: Int)->RadioCellViewModel{
        let data = cellViewModel.property[row]
        return RadioCellViewModel(isSelected: data.isSelected ?? false, title: data.title ?? "", id: data.id ?? -1)
    }
    
}

extension RadioTableViewCell: UITableViewDelegate, UITableViewDataSource, RadioCellProtocol{
    func didTapOnRadioButton(id: Int, isSelected: Bool, row: Int) {
        delegate.sendSelectionToSellNowViewModel(attributeIndex: self.tag, section: section, isSelected: isSelected, propertyRow: row)

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RadioCell.identifier, for: indexPath) as! RadioCell
        let cellViewModel = cellViewModelForRadioCell(row: indexPath.row)
        cell.delegate = self
        cell.cellViewModel = cellViewModel
        cell.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModel.property.count
    }
    
}


