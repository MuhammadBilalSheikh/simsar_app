//
//  TextFieldTableViewCell.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
protocol TextFieldTableViewCellProtocol : class {
    func didWriteOnTextField(selectedText : String , section : Int , attributeId : Int )

}
class TextFieldTableViewCell: UITableViewCell {
    static let identifier = "TextFieldTableViewCell"
    var cellViewModel : SellNowCellViewModel!{
        didSet{
            title.text = cellViewModel.title
            attributeTextField.text = cellViewModel.selectedText
        }
    }
    var section  : Int!
    weak var delegate : TextFieldTableViewCellProtocol!
    @IBOutlet weak var attributeTextField: UITextField!
    @IBOutlet weak var title: UILabel!
    static func nib() -> UINib {
        return UINib(nibName: "TextFieldTableViewCell" , bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.attributeTextField.addTarget(self, action: #selector(self.didChangeText), for: .editingChanged)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func didChangeText(){
        delegate.didWriteOnTextField(selectedText: attributeTextField.text ?? "", section: section, attributeId: self.tag)
    }
}
