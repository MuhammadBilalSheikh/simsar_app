//
//  SellNowUploadImageCollectionViewCell.swift
//  Simsar
//
//  Created by Hamza Khan on 06.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class SellNowUploadImageCollectionViewCell: UICollectionViewCell {
    static let identifier = "SellNowUploadImageCollectionViewCell"
    @IBOutlet weak var  selectedImage: UIImageView!
    @IBOutlet weak var selectImageButton: UIButton!

    static func nib() -> UINib {
        return UINib(nibName: "SellNowUploadImageCollectionViewCell" , bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
