//
//  RadioCell.swift
//  Simsar
//
//  Created by Hamza Khan on 05.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

protocol RadioCellProtocol: class {
    func didTapOnRadioButton(id: Int, isSelected: Bool, row:Int)->Void
}
class RadioCell: UITableViewCell {
    
    
    static let identifier = "RadioCell"
    weak var delegate : RadioCellProtocol!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var btnRadioMain: UIButton!{
        didSet{
            btnRadioMain.isSelected = false
        }
    }
    @IBOutlet weak var btnRadio : UIButton!{
        didSet{
            btnRadio.setImage(UIImage.init(named: "Ellipse 1")?.withRenderingMode(.alwaysOriginal), for: .normal)
            btnRadio.setImage(UIImage.init(named: "Circle")?.withRenderingMode(.alwaysOriginal), for: .selected)

        }
    }
    
    var id : Int!
    var cellViewModel : RadioCellViewModel!{
        didSet{
            title.text = cellViewModel.title
            id = cellViewModel.id
            btnRadio.isSelected = cellViewModel.isSelected
        }
    }
    static func nib() -> UINib {
        return UINib(nibName: "RadioCell" , bundle: nil)
        
    }
    override func prepareForReuse() {
        btnRadioMain.isSelected = cellViewModel.isSelected
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        btnRadio.isSelected = btnRadioMain.isSelected
        btnRadioMain.addTarget(self, action: #selector(self.didTapOnRadioButton(_:)), for: .touchUpInside)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func didTapOnRadioButton( _ sender : UIButton ){
        delegate.didTapOnRadioButton(id: id, isSelected: !sender.isSelected, row: self.tag)

        cellViewModel.isSelected = !sender.isSelected
        btnRadio.isSelected = !sender.isSelected
        btnRadioMain.isSelected = !sender.isSelected
    }
}

struct RadioCellViewModel{
    var isSelected: Bool
    var title : String
    var id : Int
}
