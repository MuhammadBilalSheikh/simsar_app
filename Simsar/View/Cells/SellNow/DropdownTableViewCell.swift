//
//  DropdownTableViewCell.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

protocol DropdownTableViewCellProtocol : class {
    func didSelectValueFromDropDown(attributeIndex: Int, section: Int, isSelected: Bool, propertyRow: Int)
    func didTapOnDropDown(row: Int, section: Int, cell: DropdownTableViewCell)

}
class DropdownTableViewCell: UITableViewCell {
    static let identifier = "DropdownTableViewCell"
    weak var delegate : DropdownTableViewCellProtocol!
    @IBOutlet weak var txtDropDown: UITextField!
    @IBOutlet weak var lblDropDown: UILabel!
    var section : Int!
    private var nameArray:[String] = []
    var cellViewModel : SellNowCellViewModel!{
        didSet{
            lblDropDown.text = cellViewModel.title
            for property in cellViewModel.property{
                
                if property.isSelected ?? false{
                    txtDropDown.text = property.title
                    break
                }
                else{
                    txtDropDown.text = ""
                    
                }
            }

        }
    }
    static func nib() -> UINib {
        return UINib(nibName: "DropdownTableViewCell" , bundle: nil)
    }
  
    override func awakeFromNib()  {
                let categorySet = UITapGestureRecognizer(target: self , action: #selector( categoryTapped ) )
                txtDropDown.addGestureRecognizer(categorySet)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

    
    
//    func setupDropDown(){
//        for data in arraData{
//            nameArray.append(data.title!)
//        }
//        ddDataSource.dataSource = nameArray
//        ddDataSource.anchorView =  txtDropDown
//        ddDataSource.selectionAction = {index , title in
//            self.txtDropDown.text = title
//            self.delegate.didSelectValueFromDropDown(attributeIndex: self.tag, section: self.section, isSelected: true, propertyRow: index)
//        }
//
//
//        // SET ACTION
//    }
    //DROP_DOWNS
//    var ddDataSource : DropDown = {
//        let dd = DropDown()
//        
//        return dd
//    }()
    
    @objc func categoryTapped(){
        
//        print("Show Drop Down .")
//        ddDataSource.show()
//
        self.delegate.didTapOnDropDown(row: self.tag, section: section, cell: self)
    }
    
    
    
}



