//
//  NextCancelTableViewCell.swift
//  Simsar
//
//  Created by Hamza Khan on 05.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
protocol NextCancelTableViewCellProtocol : class{
    func didTapOnNext()->Void
}
class NextCancelTableViewCell: UITableViewCell {
    weak var delegate : NextCancelTableViewCellProtocol!
    static let identifier = "NextCancelTableViewCell"
    @IBOutlet weak var nextButton: UIButton!

//    @IBOutlet weak var cancelButton: UIButton!

    static func nib() -> UINib {
        return UINib(nibName: "NextCancelTableViewCell" , bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nextButton.addTarget(self, action: #selector(self.didTapOnNextButton), for: .touchUpInside)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func didTapOnNextButton(){
        delegate.didTapOnNext()
    }
    
}
