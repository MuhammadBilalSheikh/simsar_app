//
//  addBanner2Class.swift
//  Simsar
//
//  Created by NxGeN on 11/9/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class addBanner2Class : UICollectionViewCell {
    
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var imageIndicator : UIActivityIndicatorView!
    
    static func nib() -> UINib {
        return UINib(nibName: "addBanner2", bundle: nil )
    }
    
    static let Identifier = "addBanner2"
    
    override func awakeFromNib() {
        
        updateUI()
    }
    
    func updateUI(){
        
        self.layer.cornerRadius = 15
        self.layer.shadowOpacity = 0.04
        self.layer.shadowRadius = 2
        self.imageIndicator.isHidden = true 
        
    }
    
}
