//
//  brandCardClass.swift
//  Simsar
//
//  Created by NxGeN on 11/5/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class brandCardClass  :UICollectionViewCell  {
    

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgView: UIImageView!
    static let identifier = "brandCard"
    
    static func nib() -> UINib {
        return UINib(nibName: "brandCard" , bundle: nil)
    }
    
    override func awakeFromNib() {
        
        updateUI()
    }
    
    func updateUI(){
        layer.cornerRadius = 8
        layer.shadowColor = UIColor.gray.cgColor
        //layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
        self.contentView.clipsToBounds = true
        self.backgroundView?.clipsToBounds = true 
    }
    
}
