//
//  CategoryButtonsTableViewCell.swift
//  Simsar
//
//  Created by NxGeN on 10/19/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

protocol CategoryButtonsTableViewCellDelegate{
    func didTapButton(with index: Int)
}

class CategoryButtonsTableViewCell: UICollectionViewCell {
    
    @IBOutlet var featuredButton: UIButton!
    @IBOutlet var offeredButton: UIButton!
    @IBOutlet var latestButton: UIButton!
 

    var model = [Model]()
    var delegate: CategoryButtonsTableViewCellDelegate? = nil
    static let identifier = "CategoryButtonsTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "CategoryButtonsTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    func setupUI(){
        //selectionStyle = .none // BECAUSE CONVERTED TO COLLECTION VIEW
        
        [featuredButton, offeredButton, latestButton].forEach { (button) in
            button?.layer.cornerRadius = 15
            button?.layer.masksToBounds = true
        }
        
        offeredButton.backgroundColor = AppTheme.colorPrimary
    }
    
    func configure(with models: [Model]) {
    }
    
    //BECAUSE CONVERTED TO COLLECTION VIEW
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    @IBAction func didTapFeaturedButton(_ sender: Any) {
        delegate?.didTapButton(with: 1)
    }
    
    @IBAction func didTapOfferedButton(_ sender: Any) {
        delegate?.didTapButton(with: 2)
    }
    
    @IBAction func didTapLatestButton(_ sender: Any) {
        delegate?.didTapButton(with: 3)
    }
    
    @IBAction func didTapFilterButton(_ sender: Any) {
        delegate?.didTapButton(with: 4)
    }
    
    
}
