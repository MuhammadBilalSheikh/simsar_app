//
//  FeaturedTableViewCell.swift
//  Simsar
//
//  Created by NxGeN on 10/19/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

protocol FeaturedTableViewCellDelegate{
    func didTapAd(with model: AdsApiData)
}

class FeaturedTableViewCell : UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView: UICollectionView!
    
    static let identifier = "FeaturedTableViewCell"
    var delegate: FeaturedTableViewCellDelegate? = nil
    
    var model = [AdsApiData]()
    static func nib() -> UINib {
        return UINib(nibName: "FeaturedTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.register(FeaturedCollectionViewCell.nib(), forCellWithReuseIdentifier: FeaturedCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    func configure(with models: [AdsApiData]) {
        self.model = models
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedCollectionViewCell.identifier, for: indexPath) as! FeaturedCollectionViewCell
        cell.configure(with: model[indexPath.row])
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow:CGFloat = 2
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        return CGSize(width: itemWidth, height: 215)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        self.delegate?.didTapAd(with: model[indexPath.row])
    }
}
