//
//  AllAdsTableViewCell.swift
//  Simsar
//
//  Created by NxGeN on 10/19/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

protocol AllAdsTableViewCellDelegate{
    func didTapProductAdd(with model: Model)
    func placeAdbtnClicked()
}

class AllAdsTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var featuredCellCount = 0
    var model = [Model]()
    var delegate: AllAdsTableViewCellDelegate? = nil
    static let identifier = "AllAdsTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "AllAdsTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.register(AllAdsCollectionViewCell.nib(), forCellWithReuseIdentifier: AllAdsCollectionViewCell.identifier)
        collectionView.register(PlaceAdsCollectionViewCell.nib(), forCellWithReuseIdentifier: PlaceAdsCollectionViewCell.identifier)
        collectionView.register(addBanner2Class.nib() , forCellWithReuseIdentifier: addBanner2Class.Identifier )
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 10
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        }
    }
    
    func configure(with models: [Model]) {
        self.model = models
        self.collectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return model.count + 1
        return 30
    }
    
    
    
    
    ///---CELL FOT ITEM
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //CELL -> (PLACE YOUR ADD HERE )
        if(indexPath.row == 4  || indexPath.row == 10  || indexPath.row == 20 ){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlaceAdsCollectionViewCell.identifier, for: indexPath ) as! PlaceAdsCollectionViewCell
            cell.clickDelegate = self
            return cell
        }
        
        //CELL -> AddBanner cell 2
        if (indexPath.row == 8 || indexPath.row == 16 ){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addBanner2Class.Identifier , for: indexPath ) as! addBanner2Class
            return cell
            
        }
        
        //MAIN CELL
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AllAdsCollectionViewCell.identifier, for: indexPath) as! AllAdsCollectionViewCell
        
//        if ( indexPath.row >= 3 ) {
//        cell.configure(with: model[indexPath.row - 1 ])
//        }else {
//            cell.configure(with: model[indexPath.row  ])
//        }
        if( featuredCellCount < model.count )
        {
        cell.configure(with: model[featuredCellCount])
        featuredCellCount = featuredCellCount + 1
        }
        return cell
    }
    
    
    
    ////-------- VIEW LAYOUT - CG SIZE
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let collectionViewSize = collectionView.frame.size.width - padding
        //For Place Ad Row
//        if ( indexPath.row == 2 ){
//            return CGSize ( width : collectionViewSize , height : 60 )
//        }//
        return CGSize(width: collectionViewSize/2, height: 270)
    
    }
    
    //EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    //DID SELECT AT
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        //self.delegate?.didTapProductAdd(with: model[indexPath.row]) //COMMITING DUE TO INSUFFICIENT DATA 
   
    }//Did_Seleect_Item_At .
    
}


//MARK: PlaceAdDelegate -> CALLED WHEN CLICKED ON PLACE YOUR AD HERE
extension AllAdsTableViewCell : PlaceAdsCellDelegate {
    //USED_ON PLACE_AD_HERE
    func itemClicked() {
        print("itemClicked")
        self.delegate?.placeAdbtnClicked()
    }
}
