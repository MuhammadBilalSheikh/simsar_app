//
//  FaqTblCell.swift
//  Simsar
//
//  Created by NxGeN on 1/15/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class FaqTblCell: UITableViewCell {

    @IBOutlet var imgDropdown: UIImageView!
    @IBOutlet var lblText: UILabel!
    @IBOutlet var viewDivider: UIView!
    @IBOutlet var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
