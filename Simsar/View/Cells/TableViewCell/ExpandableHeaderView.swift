////
////  ExpandableHeaderView.swift
////  Simsar
////
////  Created by NxGeN on 1/15/21.
////  Copyright © 2021 Sarmad Malik. All rights reserved.
////
//
//import UIKit
//
//protocol ExpandableHeaderViewDelegate {
//    func toggleSection(header: ExpandableHeaderView, section: Int)
//}
//
//class ExpandableHeaderView: UITableViewHeaderFooterView {
//
//    var delegate:  ExpandableHeaderViewDelegate?
//    var section: Int!
//    
//    
//    @IBOutlet var lblText: UILabel!
//    
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectHeaderView)))
//    }
//    
////    required init?(coder aDecoder: NSCoder) {
////        super.init(coder: aDecoder)
//        
//    }
//    
//    
//    
//    
//    func customInit(title: String, section: Int, delegate: ExpandableHeaderViewDelegate) {
//        self.lblText.text = title
//        self.section = section
//        self.delegate = delegate
//    }
//    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.lblText.textColor = UIColor.white
//        self.contentView.backgroundColor = UIColor.clear
//    }
//    
//}
