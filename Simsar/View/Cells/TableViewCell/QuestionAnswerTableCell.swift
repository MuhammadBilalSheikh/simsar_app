//
//  QuestionAnswerTableCell.swift
//  Simsar
//
//  Created by Hamza Khan on 23.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class QuestionAnswerTableCell: UITableViewCell {

    static let identifier = "QuestionAnswerTableCell"
    @IBOutlet weak var  question : UILabel!
    @IBOutlet weak var answer: UILabel!

    static func nib() -> UINib {
        return UINib(nibName: "QuestionAnswerTableCell" , bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
