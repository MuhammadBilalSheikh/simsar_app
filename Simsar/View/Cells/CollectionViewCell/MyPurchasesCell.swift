//
//  MyPurchasesCell.swift
//  Simsar
//
//  Created by NxGeN on 2/18/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SkeletonView

class MyPurchasesCell: UICollectionViewCell {

    @IBOutlet var btnViewDetails: UIButton!
    @IBOutlet var S: UILabel!
    @IBOutlet var lblEndDate: UILabel!
    @IBOutlet var lblStDate: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblMAinTitle: UILabel!
    
    
    static func nib() -> UINib {
        return UINib(nibName: "MyPurchasesCell", bundle: nil)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        makeUI()
        
    }
    
    
    
    
    static let identifier = "MyPurchasesCell"
    
    
    @IBAction func lblViewDetailAct(_ sender: Any) {
    }
    
    
    func makeUI() {
        btnViewDetails.applyGradient()
        btnViewDetails.makeRound(radius: 10)
        let radius: CGFloat = 15
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
    }
    
}
