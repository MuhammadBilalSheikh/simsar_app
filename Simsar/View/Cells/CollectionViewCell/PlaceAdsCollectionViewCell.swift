//
//  PlaceAdsCollectionViewCell.swift
//  Simsar
//
//  Created by NxGeN on 10/21/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit


protocol PlaceAdsCellDelegate {
    func itemClicked()
}



class PlaceAdsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var sellingButton: UIButton!
    static let identifier = "PlaceAdsCollectionViewCell"
    let radius: CGFloat = 10
    var clickDelegate : PlaceAdsCellDelegate? = nil
    
    static func nib() -> UINib {
        return UINib(nibName: "PlaceAdsCollectionViewCell", bundle: nil)
    }
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupUI()
    }
    
    func setupUI(){
        
        sellingButton.layer.cornerRadius = radius
        sellingButton.layer.backgroundColor = UIColor.white.cgColor
        sellingButton.titleLabel?.backgroundColor = UIColor.clear
        layer.backgroundColor = UIColor.white.cgColor
        layer.borderColor = UIColor.clear.cgColor
        
        [contentView].forEach { (view) in
            view.layer.cornerRadius = radius
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.clear.cgColor
            view.layer.masksToBounds = true
        
        }
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
         
        sellingButton.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(btnTapped))
        sellingButton.addGestureRecognizer( tap )
        
        
    }//sepUI
    
    @objc func btnTapped(){
        print ("Post Job Posted .")
        if (clickDelegate != nil ) {
            clickDelegate!.itemClicked()
        }
    }
    
}





