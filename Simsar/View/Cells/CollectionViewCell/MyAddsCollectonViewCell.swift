//
//  MyAddsCollectonViewCell.swift
//  Simsar
//
//  Created by NxGeN on 2/13/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class MyAddsCollectonViewCell: UICollectionViewCell {
   
    //MARK: OUTLETS
    
    @IBOutlet var imgMain: UIImageView!
    @IBOutlet var productView: UIView!
    @IBOutlet var btnActiveOrDeactive: UIButton!
    @IBOutlet var imgAcInd: UIActivityIndicatorView!
    @IBOutlet var lblAddres: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDiscPrice: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSeconds: UILabel!
    @IBOutlet var lblMinutes: UILabel!
    @IBOutlet var lblHours: UILabel!
    @IBOutlet var lblDays: UILabel!
    @IBOutlet var lblMinAgo: UILabel!
    @IBOutlet var featureViewImg: UIImageView!
    @IBOutlet var timmerViewMain: UIView!
    @IBOutlet var featureViewMain: UIView!
    @IBOutlet var btnPercent: UIButton!
    @IBOutlet var btnSpark: UIButton!
    @IBOutlet var btnActForUpateAndActive: UIButton!
    
    @IBOutlet var btnHammer: UIButton!
    @IBOutlet var btnFav: UIButton!
    var idUsedInServer : Int!
    var expiryDate : String?
    var countdownComp : DateComponents? = nil
    
    var debugString = ""
    var debugCounter = 0
    
    static let identifier = "MyAddsCollectonViewCell"
    
    
    
    static func nib() -> UINib {
        return UINib(nibName: "MyAddsCollectonViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnActiveOrDeactive.applyGradient()
        btnActiveOrDeactive.makeRound(radius: 5)
        setupUI()
        runCountdown()
    //    btnHammer.isHidden = true
        btnPercent.isHidden = true
        btnSpark.isHidden = true
        featureViewMain.transform = CGAffineTransform(rotationAngle: -0.785398 )
        featureViewMain.isHidden = true
        
    }
    
    
    
    
    public func isFeatured (_ bool : Bool ){
        
        
            print ("isFeatured Called with val  \(bool)")
            if ( bool == true ){
                self.featureViewMain.isHidden = false
            }else { self.featureViewMain.isHidden = true }
    }

    public func isWishListed(_ bool : Bool ){
        
        if ( bool == true ){
            self.btnFav.isSelected = true
        } else { self.btnFav.isSelected = false }
        
    }

    
    
    
    
    func getTimerComponents(_ date : String ) -> DateComponents {
        
        //DATE FORMATING - CONVERTING STRING INTO DATE
        let dateFormatter = DateFormatter()
        
        //CONVERT STRING DATE TO DATE OBJECT
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let finishDate = dateFormatter.date(from: date )
        let monthOfDate = Calendar.current.component(.month , from: finishDate! )
        let dayOfDate   = Calendar.current.component(.day , from: finishDate!   )
        let yearOfDate  = Calendar.current.component(.year , from: finishDate!  )
            
        let diff = Calendar.current.dateComponents([.day, .hour, .minute, .second ], from: Date(), to: finishDate! )
           return diff
        
    }
    
    
    
    @IBAction func btnActForActEdit(_ sender: UIButton) {
        
    }
    
    
    @objc func updateTime() {
        
     var  datePresent = true
     if expiryDate == nil { datePresent = false ; expiryDate = "2021-1-7" ; print("no Date Found ") }
     if countdownComp == nil { print("Getting Var of countDownComp"); countdownComp = getTimerComponents( expiryDate! ) }
        
        
       
   //  print("******\(countdownComp!.second)")
        if countdownComp!.second! == 0 {
            if countdownComp!.minute! == 0  {
                if countdownComp!.hour! == 0 {
                    if countdownComp!.day! == 0 {
                        /*if days is 0 */
                    }else {  countdownComp!.day! -= 1 ; countdownComp!.hour! = 24}
                }else { countdownComp!.hour! -= 1 ; countdownComp!.minute! = 60 }
            }else { countdownComp!.minute! -= 1 ; countdownComp!.second! = 60 }
        } else {  //print("second Deprication Func Called ")
         countdownComp!.second! -= 1 }
        
        DispatchQueue.main.async {
        
         self.lblDays.text = String( self.countdownComp!.day! )
            self.lblHours.text = String( self.countdownComp!.hour! )
            self.lblMinutes.text = String( self.countdownComp!.minute! )
            self.lblSeconds.text = String( self.countdownComp!.second! )
    
        }
     
     
     //FOR TESTING PURPOSE ()
     //COUNTER TIMES IT RUN
     //FIRST DATE OR NIL
//        DispatchQueue.main.async {
//            self.debugCounter += 1
//            self.debugString = "counter == \(self.debugCounter) ,,, previousDate \(self.expiryDate) \n datePresents == \(datePresent) ... computeVariables == \(self.countdownComp!.second!)"
//            self.productDetailLabel.text = self.debugString
//            }
    
    }
    
    
    func runCountdown() {
    
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    
    }
    
    
    
    func setupUI(){
        btnActForUpateAndActive.isHidden = true
        self.imgAcInd.isHidden = true
        let radius: CGFloat = 10
        
        [contentView, timmerViewMain].forEach { (view) in
            view?.layer.cornerRadius = radius
            view?.layer.borderWidth = 1
            view?.layer.borderColor = UIColor.clear.cgColor
            view?.layer.masksToBounds = true
        }
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
        
        [btnFav, btnHammer, btnSpark, btnSpark, lblHours, lblMinutes, lblSeconds , lblDays     ].forEach { (view) in
            view?.layer.cornerRadius = 0.5 * (view?.layer.bounds.size.width ?? 0.0)
            view?.clipsToBounds = true
        }
        
        [btnFav, btnHammer].forEach { (view) in
            view?.backgroundColor = .white
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "13500")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        lblDiscPrice.attributedText = attributeString
        
    }
    
    

}
