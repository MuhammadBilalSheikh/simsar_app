//
//  AllAdsCollectionViewCell.swift
//  Simsar
//
//  Created by NxGeN on 10/19/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import DCToastView
import SwiftyJSON

class AllAdsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblKDDisc: UILabel!
    @IBOutlet var timerStackView: UIStackView!
    @IBOutlet var lblKd: UILabel!
    @IBOutlet var lblState: UILabel!
    @IBOutlet var productView: UIView!
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var timerView: UIView!
    @IBOutlet var favouriteButton: UIButton!
    @IBOutlet var betButton: UIButton!
    @IBOutlet var percentButton: UIButton!
    @IBOutlet var boostButton: UIButton!
    @IBOutlet var dayLabel : UILabel!
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var mintLabel: UILabel!
    @IBOutlet var secLabel: UILabel!
    @IBOutlet var remainingTimeLabel: UILabel!
    @IBOutlet var productTitleLabel: UILabel!
    @IBOutlet var productDetailLabel: UILabel!
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var productAddressLabel: UILabel!
    @IBOutlet var previousPriceLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var featuredLabel : UIView!
    
    var idUsedInServer : Int!
    var expiryDate : String?
    var countdownComp : DateComponents? = nil
    
    var debugString = ""
    var debugCounter = 0

    
    
    static let identifier = "AllAdsCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "AllAdsCollectionViewCell", bundle: nil)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productImageView.DropCardView()
       // timerStackView.isHidden = true
        favouriteButton.isHidden = true
        runCountdown()
        betButton.isHidden = true
        percentButton.isHidden = true
        boostButton.isHidden = true
        // Initialization code
        
        setupUI()
      //  runCountdown()
       // getTimerComponents("4-1-2021")
        
       //FeaturedLabel
            featuredLabel.transform = CGAffineTransform(rotationAngle: -0.785398 )
            featuredLabel.isHidden = true
        }

        public func isFeatured (_ bool : Bool ){
            
            
                print ("isFeatured Called with val  \(bool)")
                if ( bool == true ){
                    self.featuredLabel.isHidden = false
                }else { self.featuredLabel.isHidden = true }
        }
    
        public func isWishListed(_ bool : Bool ){
            
            if ( bool == true ){
                self.favouriteButton.isSelected = true
            } else { self.favouriteButton.isSelected = false }
            
        }
    
    
       
       func getTimerComponents(_ date : String ) -> DateComponents {
           
           //DATE FORMATING - CONVERTING STRING INTO DATE
           let dateFormatter = DateFormatter()
           
           //CONVERT STRING DATE TO DATE OBJECT
           dateFormatter.dateFormat = "yyyy-MM-dd"
           
           let finishDate = dateFormatter.date(from: date )
           let monthOfDate = Calendar.current.component(.month , from: finishDate! )
           let dayOfDate   = Calendar.current.component(.day , from: finishDate!   )
           let yearOfDate  = Calendar.current.component(.year , from: finishDate!  )
               
           let diff = Calendar.current.dateComponents([.day, .hour, .minute, .second ], from: Date(), to: finishDate! )
              return diff
           
       }
       
       @objc func updateTime() {
           
        var  datePresent = true
        if expiryDate == nil { datePresent = false ; expiryDate = "2021-1-7" ; print("no Date Found ") }
        if countdownComp == nil { print("Getting Var of countDownComp"); countdownComp = getTimerComponents( expiryDate! ) }
           
           
          
      //  print("******\(countdownComp!.second)")
           if countdownComp!.second! == 0 {
               if countdownComp!.minute! == 0  {
                   if countdownComp!.hour! == 0 {
                       if countdownComp!.day! == 0 {
                           /*if days is 0 */
                       }else {  countdownComp!.day! -= 1 ; countdownComp!.hour! = 24}
                   }else { countdownComp!.hour! -= 1 ; countdownComp!.minute! = 60 }
               }else { countdownComp!.minute! -= 1 ; countdownComp!.second! = 60 }
           } else {  //print("second Deprication Func Called ")
            countdownComp!.second! -= 1 }
           
           DispatchQueue.main.async {
           
            self.dayLabel.text = String( self.countdownComp!.day! )
               self.hourLabel.text = String( self.countdownComp!.hour! )
               self.mintLabel.text = String( self.countdownComp!.minute! )
               self.secLabel.text = String( self.countdownComp!.second! )
       
           }
        
        
        //FOR TESTING PURPOSE ()
        //COUNTER TIMES IT RUN
        //FIRST DATE OR NIL
//        DispatchQueue.main.async {
//            self.debugCounter += 1
//            self.debugString = "counter == \(self.debugCounter) ,,, previousDate \(self.expiryDate) \n datePresents == \(datePresent) ... computeVariables == \(self.countdownComp!.second!)"
//            self.productDetailLabel.text = self.debugString
//            }
       
       }
           
       func runCountdown() {
       
           Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
       
       }
    
    func setupUI(){
        
        self.activityIndicator.isHidden = true 
        let radius: CGFloat = 10
        
        [contentView, timerView].forEach { (view) in
            view?.layer.cornerRadius = radius
            view?.layer.borderWidth = 1
            view?.layer.borderColor = UIColor.clear.cgColor
            view?.layer.masksToBounds = true
        }
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
        
        [favouriteButton, betButton, boostButton, percentButton, hourLabel, mintLabel, secLabel , dayLabel 	].forEach { (view) in
            view?.layer.cornerRadius = 0.5 * (view?.layer.bounds.size.width ?? 0.0)
            view?.clipsToBounds = true
        }
        
        [favouriteButton, betButton].forEach { (view) in
            view?.backgroundColor = .white
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "13500")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        previousPriceLabel.attributedText = attributeString
        
    }
    
    public func configure(with model: Model){
        
    }
    
    
    
     
     @IBAction func favouriteButtonClicked(){
         
//         print ("Favourite Button Called")
//          //CHECK IF USER LOGGED THEN IT WILL ADD IT SERVER
//         if (Model_UserStatus.shared.isUserIdThere() == true ){
            // apiAddProductToServer()
//          }else {
//              print ("User Not Loggged In")
//          }
         
     }
     
     
     @IBAction func betButtonClicked (){
        
         //CHECK IF USER LOGGED THEN IT WILL ADD IT SERVER
         print ("Bet Button Called")
         
         self.betButton.isSelected = !self.betButton.isSelected
         
     }

    
}







extension  AllAdsCollectionViewCell{
    
    
    //MARK:- Api for Home
    //USER ID I OPTIONAL -- IF USER LOGGED IN
    
        func apiAddProductToServer(){
            
            print("API ADD PRODUCT CALLED .. with id === \(self.idUsedInServer)")
            
            
            
            let apiParams = Parameter()
            
            apiParams.dictionary = [
                "client_key" :  Model_UserStatus.shared.clientKey,
                "user_id"    :  Model_UserStatus.shared.getUserId(),            //Model_UserStatus.shared.userId!,
                    "ad_id"      :  idUsedInServer!       //   self.idUsedInServer
            ]
            
            let serviceObj = Service(url: "https://simsar.com/api/v1/wishlist" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    //self.stopGlobalIndicator( self.globalIndicator!  )
                    
                   
                    print ("result is not nil")
                    do {
                        

                        let jObj : JSON = try JSON(data: (result?.data)! )
                        print("Data from Server = \(jObj)")
                        let statusOfRequest   = jObj["status"]
                        let messagefromServer = jObj["message"]
                        
                        if ( statusOfRequest == "success" ){
                          
                            
                            if (messagefromServer == "Wishlist added" ){
                                ToastPresenter.shared.show(in: self.contentView, message: "Wishlist-added", place: .down, backgroundColor: .systemRed, textColor: .white, timeOut: 1.0, roundness: .mid)
                                self.favouriteButton.isSelected = true
                               
                                
                            }else if ( messagefromServer ==  "Wishlist deleted") {
                                ToastPresenter.shared.show(in: self.contentView, message: "Wishlist-deleted", place: .down, backgroundColor: .black, textColor: .white, timeOut: 1.0, roundness: .mid)
                                self.favouriteButton.isSelected = false
                                
                                
                                
                            }
                            
                        }else {
                            print ("request fRom Server == \(statusOfRequest)" )
                        }
                        
//                            {
//                                "status": "success",
//                                "message": "Wishlist added"
//                            }
//                        {
//                            "status": "success",
//                            "message": "Wishlist deleted"
//                        }
                       
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
            
        }
}
