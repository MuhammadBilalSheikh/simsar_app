//
//  TopBidderCollectionViewCell.swift
//  Simsar
//
//  Created by NxGeN on 10/20/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class TopBidderCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "TopBidderCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "TopBidderCollectionViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(with model: Model){
        
    }

}
