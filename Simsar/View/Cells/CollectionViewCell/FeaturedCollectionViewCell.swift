//
//  FeaturedCollectionViewCell.swift
//  Simsar
//
//  Created by NxGeN on 10/19/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON



class FeaturedCollectionViewCell: UICollectionViewCell {

    //productAddressLabel
    //                        mainImage "main_image": "1604930519.png"    productImageView
    //                        adTitle "ad_title": "car sample 44",        productTitleLabel
    //
    //                        price "price": "0",                         productPriceLabel
    //                        disPrice "dis_price": "0",                  previousPriceLabel
    //                        longDescription"long_description",          productDetailLabel
    //                        id = "id": 425,
    //                        categoryID "category_id": "56",
    //                        postID "post_id": "203868855",
    //                        userID "user_id": "309",
    
    @IBOutlet var imageIndicator : UIActivityIndicatorView!
    @IBOutlet var productView: UIView!
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var timerView: UIView!
    @IBOutlet var favouriteButton: UIButton!
    @IBOutlet var betButton: UIButton!
    @IBOutlet var percentButton: UIButton!
    @IBOutlet var boostButton: UIButton!
    @IBOutlet var dayLabel : UILabel!
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var mintLabel: UILabel!
    @IBOutlet var secLabel: UILabel!
    @IBOutlet var remainingTimeLabel: UILabel!
    @IBOutlet var productTitleLabel: UILabel!
    @IBOutlet var productDetailLabel: UILabel!
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var productAddressLabel: UILabel!
    @IBOutlet var previousPriceLabel: UILabel!
    @IBOutlet var featuredLabel     : UIView!
    
    
    
    var expiryDate : String?
    var countdownComp : DateComponents? = nil
    var idUsedInServer : Int!
    
    var debugString = ""
    var debugCounter = 0

    
    //"expiry_date": "2020-12-10",
    
    
    
    static let identifier = "FeaturedCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "FeaturedCollectionViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
        runCountdown()
        
        //FeaturedLabel
        featuredLabel.transform = CGAffineTransform(rotationAngle: -0.785398 )
        featuredLabel.isHidden = true
    }
    
    public func isFeatured (_ bool  : Bool ){
            
        if ( bool == true ){
            self.featuredLabel.isHidden = false
        } else { self.featuredLabel.isHidden = true }
        
    }
    
    public func isWishListed(_ bool : Bool ){
        
        if ( bool == true ){
            self.favouriteButton.isSelected = true
        } else { self.favouriteButton.isSelected = false }
        
    }
    
    func getTimerComponents(_ date : String ) -> DateComponents {
        
        //DATE FORMATING - CONVERTING STRING INTO DATE
        let dateFormatter = DateFormatter()
        
        //CONVERT STRING DATE TO DATE OBJECT
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if date == "0000-00-00" {
            let finishDate = dateFormatter.date(from: "2021-07-15" )
            let diff = Calendar.current.dateComponents([.day, .hour, .minute, .second ], from: Date(), to: finishDate! )
            return diff
        }else {
            let finishDate = dateFormatter.date(from: date )
            print("===============================================\(finishDate)=======")
            let monthOfDate = Calendar.current.component(.month , from: finishDate ?? Date())
            let dayOfDate   = Calendar.current.component(.day , from: finishDate ?? Date()  )
            let yearOfDate  = Calendar.current.component(.year , from: finishDate ?? Date() )
                
            let diff = Calendar.current.dateComponents([.day, .hour, .minute, .second ], from: Date(), to: finishDate ?? Date() )
            return diff
        }
       
        
       
        
    }
    
    @objc func updateTime() {
                  
               var  datePresent = true
                       if expiryDate == nil { datePresent = false ; expiryDate = "2020-12-3" ; print("no Date Found ") }
                       if countdownComp == nil { print("Getting Var of countDownComp"); countdownComp = getTimerComponents( expiryDate! ) }
                          
                          
                         
                       //print("\(countdownComp!.second)")
                          if countdownComp!.second! == 0 {
                              if countdownComp!.minute! == 0  {
                                  if countdownComp!.hour! == 0 {
                                      if countdownComp!.day! == 0 {
                                          /*if days is 0 */
                                      }else {  countdownComp!.day! -= 1 ; countdownComp!.hour! = 24}
                                  }else { countdownComp!.hour! -= 1 ; countdownComp!.minute! = 60 }
                              }else { countdownComp!.minute! -= 1 ; countdownComp!.second! = 60 }
                          } else {  //print("second Deprication Func Called ")
                           countdownComp!.second! -= 1 }
                          
                          DispatchQueue.main.async {
                          
                           self.dayLabel.text = String( self.countdownComp!.day! )
                              self.hourLabel.text = String( self.countdownComp!.hour! )
                              self.mintLabel.text = String( self.countdownComp!.minute! )
                              self.secLabel.text = String( self.countdownComp!.second! )
                      
                          }
                       
                       
                       //FOR TESTING PURPOSE ()
                       //COUNTER TIMES IT RUN
                       //FIRST DATE OR NIL
               //        DispatchQueue.main.async {
               //            self.debugCounter += 1
               //            self.debugString = "counter == \(self.debugCounter) ,,, previousDate \(self.expiryDate) \n datePresents == \(datePresent) ... computeVariables == \(self.countdownComp!.second!)"
               //            self.productDetailLabel.text = self.debugString
               //            }
        
    }
        
    func runCountdown() {
    
        
        //print("runCountDown")
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
    
    }
    
    
    
    
    
    
    
    func setupUI(){
        
        let radius: CGFloat = 10
        
        self.imageIndicator.isHidden = true 
        [contentView, timerView].forEach { (view) in
            view?.layer.cornerRadius = radius
            view?.layer.borderWidth = 1
            view?.layer.borderColor = UIColor.clear.cgColor
            view?.layer.masksToBounds = true
        }
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
        
        [favouriteButton, betButton, boostButton, percentButton, hourLabel, mintLabel, secLabel , dayLabel ].forEach { (view) in
            view?.layer.cornerRadius = 0.5 * (view?.layer.bounds.size.width ?? 0.0)
            view?.clipsToBounds = true
        }
        
        [favouriteButton, betButton].forEach { (view) in
            view?.backgroundColor = .white
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "13500")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        previousPriceLabel.attributedText = attributeString
        
    }
    
    public func configure(with model: AdsApiData){
        DispatchQueue.main.async {
            
            
            
            ServiceManager.shared.downloadImageWithName(Constant.baserUrlImage , model.mainImage! , self.imageIndicator , self.productImageView )
            print(Constant.baserUrlImage + model.mainImage!)
            
            self.productTitleLabel.text = model.adTitle
         //   self.productPriceLabel.text = model.price
            self.remainingTimeLabel.text = model.createddate
            self.idUsedInServer = model.id
            self.expiryDate = model.expiryDate
            print("=====================================================\(self.expiryDate) and model \(model.expiryDate)")
            if (model.cityname != nil) ||  model.statename != nil {
                self.productAddressLabel.text = model.cityname! ?? ""  + " " + model.statename!
            }else{
                self.productAddressLabel.text = "No Address"
            }
          
            self.productDetailLabel.text = model.longDescription
            self.isFeatured(model.isFeatured == "0" ? false : true)
            self.isWishListed(model.isWishlist == 0 ? false : true)
            
            
            
            let Auction = (model.isAuction)
            if Auction == "1" {
                self.betButton.isHidden = false
            }else {
                self.betButton.isHidden  = true
            }
            
            
            
            let check = (model.checkFor)
            if check == "1" {
                self.boostButton.isHidden = false
            }else {
                self.boostButton.isHidden = true
            }
            
            
            let offer = (model.isOffer)
            if offer == "1" {
                self.percentButton.isHidden = false
                
            }else {
                self.percentButton
                    .isHidden = true
            }
            
            
            let feature = (model.isFeatured)
            if feature == "1" {
                self.featuredLabel.isHidden = false
            }else{
                self.featuredLabel.isHidden = true
            }
            
            let isCall = (model.isCallPrice)
            if isCall == "1" {
                self.productPriceLabel.text = "Is Call Price"
                self.previousPriceLabel.isHidden = true
               
            }else {
                self.productPriceLabel.text = "KD \(model.price!)"
                self.previousPriceLabel.text = "KD \(model.disPrice!)"
            }
            
            
        }
        
    }
    
  
    
    @IBAction func favouriteButtonClicked(){
        
        print ("Favourite Button Called")
        //CHECK IF USER LOGGED THEN IT WILL ADD IT SERVER
       if (Model_UserStatus.shared.isUserIdThere() == true ){
            apiAddProductToServer()
        }else {
            print ("User Not Loggged In")
        }
    }
    
    
    @IBAction func betButtonClicked (){
       
        self.betButton.isSelected = !self.betButton.isSelected
        
    }

}



//https://simsar.com/testsimsar/api/v1/wishlist
//client_key === wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy
//user_id === 308
//ad_id == 426

//only login user can add wishlist

extension  FeaturedCollectionViewCell{
    
    
    //MARK:- Api for Home
    //USER ID I OPTIONAL -- IF USER LOGGED IN
    
        func apiAddProductToServer(){
            
            print("API ADD PRODUCT CALLED .. with id === \(self.idUsedInServer)")
            
            
            
            var apiParams = Parameter()
            
            apiParams.dictionary = [
                "client_key" :  Model_UserStatus.shared.clientKey,
                "user_id"    :  Model_UserStatus.shared.userId!,
                "ad_id"      :  self.idUsedInServer
            ]
            
            var serviceObj = Service(url: "https://simsar.com/api/v1/wishlist" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    //self.stopGlobalIndicator( self.globalIndicator!  )
                    
                   
                    print ("result is not nil")
                    do {
                        

                        let jObj : JSON = try JSON(data: (result?.data)! )
                        print("Data from Server = \(jObj)")
                        let statusOfRequest   = jObj["status"]
                        let messagefromServer = jObj["message"]
                        
                        if ( statusOfRequest == "success" ){
                            print ("Succesfull request from server" )
                            
                            if (messagefromServer == "Wishlist added" ){
                                
                                self.favouriteButton.isSelected = true
                                
                            }else if ( messagefromServer ==  "Wishlist deleted") {
                                
                                self.favouriteButton.isSelected = false
                                
                            }
                            
                        }else {
                            print ("request fRom Server == \(statusOfRequest)" )
                        }
                        
//                            {
//                                "status": "success",
//                                "message": "Wishlist added"
//                            }
//                        {
//                            "status": "success",
//                            "message": "Wishlist deleted"
//                        }
                       
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
            
        }
    
    
}
