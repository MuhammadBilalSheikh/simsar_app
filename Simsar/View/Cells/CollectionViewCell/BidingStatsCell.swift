//
//  BidingStatsCell.swift
//  Simsar
//
//  Created by NxGeN on 10/20/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class BidingStatsCell: UICollectionViewCell {
    
    static let identifier = "BidingStatsCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "BidingStatsCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
