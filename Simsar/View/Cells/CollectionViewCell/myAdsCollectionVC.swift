//
//  myAdsCollectionVC.swift
//  Simsar
//
//  Created by NxGeN on 2/6/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class myAdsCollectionVC: UICollectionViewCell {
    



override  func awakeFromNib() {
    super.awakeFromNib()
    setupUI()
}


func setupUI() {
    
    let radius: CGFloat = 15
    layer.shadowColor = UIColor.gray.cgColor
    layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
    layer.shadowRadius = 2.0
    layer.shadowOpacity = 0.3
    layer.masksToBounds = false
    layer.cornerRadius = radius
}


}
