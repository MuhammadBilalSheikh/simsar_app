//
//  CardWishlistCell.swift
//  Simsar
//
//  Created by NxGeN on 1/8/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON

class CardWishlistCell: UICollectionViewCell {
    
    @IBOutlet var timmerStackView: UIStackView!
    @IBOutlet var featuredLabel: UIView!
    @IBOutlet var imgLoader: UIActivityIndicatorView!
    @IBOutlet var lblCutPrice: UILabel!
    @IBOutlet var lblCity: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblLongDesc: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblAgo: UILabel!
    @IBOutlet var lblSec: UILabel!
    @IBOutlet var lblMin: UILabel!
    @IBOutlet var lblHour: UILabel!
    @IBOutlet var lblDay: UILabel!
    @IBOutlet var btnFav: UIButton!
    @IBOutlet var btnSpark: UIButton!
    
    @IBOutlet var btnPecent: UIButton!
    @IBOutlet var imgViewMain: UIImageView!
    
    @IBOutlet var viewTime: UIView!
    @IBOutlet var productView: UIView!
    
    
    @IBOutlet var btnHammer: UIButton!
    
    
    
    
    var expiryDate : String?
    var countdownComp : DateComponents? = nil
    var idUsedInServer : Int!
    var userId : Int!
    var debugString = ""
    var debugCounter = 0
    
    
    
    static let identifier = "CardWishlistCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "CardWishlistCell", bundle: nil)
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        setupUI()
        runCountdown()
        
        featuredLabel.transform = CGAffineTransform(rotationAngle: -0.785398 )
        featuredLabel.isHidden = true
        
    }
    
    
    func getTimerComponents(_ date : String ) -> DateComponents {
        
        //DATE FORMATING - CONVERTING STRING INTO DATE
        let dateFormatter = DateFormatter()
        
        //CONVERT STRING DATE TO DATE OBJECT
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let finishDate = dateFormatter.date(from: date )
        let monthOfDate = Calendar.current.component(.month , from: finishDate! )
        let dayOfDate   = Calendar.current.component(.day , from: finishDate!   )
        let yearOfDate  = Calendar.current.component(.year , from: finishDate!  )
            
        let diff = Calendar.current.dateComponents([.day, .hour, .minute, .second ], from: Date(), to: finishDate! )
           return diff
        
    }
    
    @objc func updateTime() {
                  
               var  datePresent = true
        if expiryDate == nil { datePresent = false ; expiryDate = "2020-12-3" ; Swift.print("no Date Found ") }
        if countdownComp == nil { Swift.print("Getting Var of countDownComp"); countdownComp = getTimerComponents( expiryDate! ) }
                          
                          
                         
                       //print("\(countdownComp!.second)")
                          if countdownComp!.second! == 0 {
                              if countdownComp!.minute! == 0  {
                                  if countdownComp!.hour! == 0 {
                                      if countdownComp!.day! == 0 {
                                          /*if days is 0 */
                                      }else {  countdownComp!.day! -= 1 ; countdownComp!.hour! = 24}
                                  }else { countdownComp!.hour! -= 1 ; countdownComp!.minute! = 60 }
                              }else { countdownComp!.minute! -= 1 ; countdownComp!.second! = 60 }
                          } else {  //print("second Deprication Func Called ")
                           countdownComp!.second! -= 1 }
                          
                          DispatchQueue.main.async {
                          
                           self.lblDay.text = String( self.countdownComp!.day! )
                              self.lblHour.text = String( self.countdownComp!.hour! )
                              self.lblMin.text = String( self.countdownComp!.minute! )
                              self.lblSec.text = String( self.countdownComp!.second! )
                      
                          }
                       
                       
                       //FOR TESTING PURPOSE ()
                       //COUNTER TIMES IT RUN
                       //FIRST DATE OR NIL
               //        DispatchQueue.main.async {
               //            self.debugCounter += 1
               //            self.debugString = "counter == \(self.debugCounter) ,,, previousDate \(self.expiryDate) \n datePresents == \(datePresent) ... computeVariables == \(self.countdownComp!.second!)"
               //            self.productDetailLabel.text = self.debugString
               //            }
        
    }
        
    func runCountdown() {
    
        
        //print("runCountDown")
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
    
    }
    
    
    
    
    
    
    
    func setupUI(){
        
        let radius: CGFloat = 10
        
        self.imgLoader.isHidden = true
        [contentView, viewTime].forEach { (view) in
            view?.layer.cornerRadius = radius
            view?.layer.borderWidth = 1
            view?.layer.borderColor = UIColor.clear.cgColor
            view?.layer.masksToBounds = true
        }
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
        
        [btnFav, btnHammer, btnSpark, btnPecent, lblHour, lblMin, lblSec , lblDay ].forEach { (view) in
            view?.layer.cornerRadius = 0.5 * (view?.layer.bounds.size.width ?? 0.0)
            view?.clipsToBounds = true
        }
        
        [btnFav, btnHammer].forEach { (view) in
            view?.backgroundColor = .white
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "13500")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        lblCutPrice.attributedText = attributeString
        
    }
    
    
    
    
    
    
    @IBAction func btnFavAvt(_ sender: Any) {
        
        print ("Favourite Button Called")
        //CHECK IF USER LOGGED THEN IT WILL ADD IT SERVER
       if (Model_UserStatus.shared.isUserIdThere() == true ){
            apiAddProductToServer()
        }else {
            print ("User Not Loggged In")
        }
        
        
    }
  
    
    

}


extension  CardWishlistCell{
    
    
    //MARK:- Api for Home
    //USER ID I OPTIONAL -- IF USER LOGGED IN
    
        func apiAddProductToServer(){
            
            print("API ADD PRODUCT CALLED .. with id === \(self.idUsedInServer)")
            
            
            
            var apiParams = Parameter()
            
            apiParams.dictionary = [
                "client_key" :  Model_UserStatus.shared.clientKey,
                "user_id"    :   471,  //Model_UserStatus.shared.userId!,
                "ad_id"      :   451  //self.idUsedInServer
            ]
            
            var serviceObj = Service(url: "https://simsar.com/api/v1/wishlist" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    //self.stopGlobalIndicator( self.globalIndicator!  )
                    
                   
                    print ("result is not nil")
                    do {
                        

                        let jObj : JSON = try JSON(data: (result?.data)! )
                        print("Data from Server = \(jObj)")
                        let statusOfRequest   = jObj["status"]
                        let messagefromServer = jObj["message"]
                        
                        if ( statusOfRequest == "success" ){
                            print ("Succesfull request from server" )
                            
                            if (messagefromServer == "Wishlist added" ){
                                
                                self.btnFav.isSelected = true
                                
                            }else if ( messagefromServer ==  "Wishlist deleted") {
                                
                                self.btnFav.isSelected = true
                                self.productView.isHidden = true
                                
                            }
                            
                        }else {
                            print ("request fRom Server == \(statusOfRequest)" )
                        }
                        
//                            {
//                                "status": "success",
//                                "message": "Wishlist added"
//                            }
//                        {
//                            "status": "success",
//                            "message": "Wishlist deleted"
//                        }
                       
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
            
        }
    
    
}
