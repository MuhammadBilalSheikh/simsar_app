//
//  Services_Cell.swift
//  Simsar
//
//  Created by Sarmad Malik on 02/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class Services_Cell: UICollectionViewCell {
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
}
