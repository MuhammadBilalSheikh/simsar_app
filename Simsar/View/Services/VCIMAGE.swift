//
//  VCIMAGE.swift
//  Simsar
//
//  Created by NxGeN on 1/12/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class VCIMAGE: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    let btn = UIImagePickerController()
    @IBOutlet var image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn.delegate = self
       
    }
    

    @IBAction func SELECT(_ sender: UIButton) {
       
     
        btn.sourceType = .photoLibrary
        self.present(btn, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
               image.contentMode = .scaleAspectFit
               image.image = pickedImage
           }

           dismiss(animated: true, completion: nil)
    }
    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        image.image = info[.originalImage] as? UIImage
//        self.dismiss(animated: true, completion: nil)
//    }
//

}
