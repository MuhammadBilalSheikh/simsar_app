//
//  searchFeatureCell.swift
//  Simsar
//
//  Created by NxGeN on 11/9/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class searchFeatureCell : UICollectionViewCell {
    
    @IBOutlet var lblPerDay: UILabel!
    @IBOutlet var lblKeyWord: UILabel!
    @IBOutlet var lblSearchPlan: UILabel!
    
    @IBOutlet var viewMain: UIView!
    @IBOutlet var btnBuyNow: UIButton!
    @IBOutlet var lblPrice: UILabel!
    
    @IBOutlet var lblItems: UILabel!
    
    static func cell() -> UICollectionViewCell {
        return searchFeatureCell()
        

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
     setupUI()
        
    }
    
    
    
    
    
    func setupUI(){
        btnBuyNow.isHidden = true
        let radius: CGFloat = 10
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
        
        btnBuyNow.makeRound(radius: 8)
        btnBuyNow.applyGradient()
        
        
//        layer.shadowColor = UIColor.gray.cgColor
//        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
//        layer.shadowRadius = 2.0
//        layer.shadowOpacity = 0.3
//        layer.masksToBounds = false
//        layer.cornerRadius = radius
//
        
    }
    
 
    
    
    static let identifier = "searchFeatureCell"
}
