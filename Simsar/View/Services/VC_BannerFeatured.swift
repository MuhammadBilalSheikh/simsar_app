//
//  VC_BannerFeatured.swift
//  Simsar
//
//  Created by NxGeN on 11/9/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class VC_BannerFeatured:UIViewController{
    
    @IBOutlet var collectionView: UICollectionView!
    var  globalIndicator : UIViewController? = nil
    
    var dataFromServer : [searchPurchases] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Called")
        Api()
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    func Api() {
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "user_id" : 471]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/bump-up-ads" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
             //   self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let Data = jObj["data"]
                    print("categories ==== \(Data)" )

                    for countOfObjectsInArray in 0..<Data.count {

                        print ("Loop For Time === \(countOfObjectsInArray)")


                        let singleObj  = Data[countOfObjectsInArray]// may be error in this
                        let obj = searchPurchases(id: singleObj["id"].intValue, title: singleObj["title"].stringValue, price: singleObj["price"].stringValue, keyWords: singleObj["placement"].stringValue, items: singleObj["quantity"].stringValue, isBuy: singleObj["is_buy"].stringValue)
                            
                        
                            
                            

                        self.dataFromServer.append( obj )


                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.collectionView.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
}

extension VC_BannerFeatured: UICollectionViewDelegate, UICollectionViewDataSource {

func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataFromServer.count
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchFeatureCell", for: indexPath ) as! searchFeatureCell
    
    
    cell.contentView.layer.cornerRadius = 15
    //cell.contentView.layer.backgroundColor =  UIColor.red.cgColor
    cell.contentView.layer.shadowOpacity = 0.4
    cell.contentView.layer.shadowRadius  =  2
    
    cell.lblSearchPlan.text = dataFromServer[indexPath.row].title
    cell.lblPrice.text = dataFromServer[indexPath.row].price
    cell.lblKeyWord.text = dataFromServer[indexPath.row].keyWords
    cell.lblItems.text = dataFromServer[indexPath.row].items
    let status = dataFromServer[indexPath.row].isBuy
    if status == "10" {
        cell.btnBuyNow.isHidden = false
    }else{
        cell.btnBuyNow.isHidden = true
    }
    
    
    return cell
}
}
