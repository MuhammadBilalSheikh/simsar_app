//
//  VC_SignUp.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import SwiftyJSON



@available(iOS 13.0, *)
class VC_SignUp: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var viewFb: UIView!
    @IBOutlet var viewGoogle: UIView!
    @IBOutlet var viewTwitter: UIView!
    @IBOutlet var viewMobile: UIView!
    @IBOutlet var txtConfrmPass: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var viewSignup: UIView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var mainError :UILabel!
  //  @IBOutlet weak var buttonIndicator : UIActivityIndicatorView!
    
    
    var messageFromServer : String = ""
    var statusOfRequestFromServer = ""
    var anyErrorFromServer = ""
    var alertMsg: String!
    var a = false
    var b = false
    
    
  
    
    //MARK :- controller Funcs
    override func viewDidLoad(){
        
    super.viewDidLoad()
        
        let gestLoginWithNumber = UITapGestureRecognizer(target: self , action: #selector(openPopUp_SignUpWitNumber) )
        self.viewMobile.addGestureRecognizer( gestLoginWithNumber )
        
        
        setupUI()
    }
    
    @objc func openPopUp_SignUpWitNumber(){
       let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_SignUpWithNumber" )
       self.addChild( ppWithNumber! )
       guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
       vc.view.frame = self.view.frame
       self.view.addSubview( vc.view )
       
       vc.didMove(toParent: self )
    }
    
    private func setupUI()
    {
      //  buttonIndicator.isHidden = true
        resetErrors()
//        txtnumber.delegate = self
//        txtnumber.setFlag(key: .PK)
//        txtnumber.placeholder = ""
        btnSignUp.applyGradient()
        btnSignUp.makeRound(radius: 20)
        viewSignup.clipsToBounds = false
        viewSignup.layer.cornerRadius = 20
        viewSignup.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        DesignNavBar()
        DesignStatusBar()
        txtName.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        txtEmail.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
       // txtnumber.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        txtPassword.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        txtConfrmPass.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        
        viewMobile.makeCircle()
        viewGoogle.makeCircle()
        viewFb.makeCircle()
        viewTwitter.makeCircle()
    }
    
    
    
   
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear( true )
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
//    func disableSignup()
//    {
//        self.startAnimatingButton(false , btnSignUp, buttonIndicator )
//    }
//    func enableSignup()
//    {
//        self.startAnimatingButton(true , btnSignUp , buttonIndicator )
//    }
    
    //MARK:- SignIn
    @IBAction func signIn(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpWithEmail() {
        if checkFields() == true
        {
            if Connectivity.isConnectedToInternet
            {
                if txtPassword.text! == txtConfrmPass.text! {
                a = true
            } else {
                showAlertView(message: "Please Enter Your Password Again", title: "Password Do Not Match")
            }

            if(txtPassword.text! == "" || txtConfrmPass.text! == "") {
                showAlertView(message: "Password Fields Are Empty", title: "Error")
            } else {
                b = true
            }

            if a == true && b == true {
                apiRegisterUserByEmail()
            }
            
              }
            else
            {
                self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
            }
        }
    }
    
    func DesignStatusBar()
    {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.white
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
    }
    func DesignNavBar()
    {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setNavigationBar()
    }
    func setNavigationBar()
    {
        
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))
        
        if let imgBackArrow = UIImage(named: "Shape")
        {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)
        
        let leftBarButtonItem = UIBarButtonItem(customView: view )
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func backToMain()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func resetErrors()
    {
        self.mainError.isHidden = true
    }
    
    
    //MARK: check field are not empty
    func checkFields()-> Bool
    {
        resetErrors()
        if ( txtPassword.text!.isEmpty )
        {
            mainError.isHidden =  false
            mainError.text = "Field can't Be Empty"
            return false
        }
        if ( txtEmail.text!.isEmpty )
        {
            mainError.isHidden =  false
            mainError.text = "Field can't Be Empty"
            return false
        }
        if ( txtName.text!.isEmpty )
        {
            mainError.isHidden =  false
            mainError.text = "Field can't Be Empty"
            return false
        }
      
        return true
    }
    
    
  
    func NavToConfCode() {
//        let VC = storyboard?.instantiateViewController(identifier: "VC_OTPCheck")
//        self.present(VC!, animated: true, completion: nil)
        self.openVC("VC_OTPCheck")
    }
    
    //MARK:- SignUp With Email
    @available(iOS 13.0, *)
    func apiRegisterUserByEmail()
    {
        
     //   disableSignup()
        print("Log In Called")
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
            "full_name"  :  txtName.text!  ,
            "email"      :  txtEmail.text! ,
            "password"   :  txtPassword.text!
        ]
     //   self.enableSignup()
        print("SignupParams***** \(apiParams)")
        
            SignUpService.shared.getSignUpData(apiParams: apiParams)
            { (result, error) in
                
                if result?.status == "success"
                {
                    print(result?.verificationCode ?? 0)
                    DispatchQueue.main.async
                    {
                        self.alertMsg = result?.message
                        self.showAlertView(message: self.alertMsg!, title: "Alert", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.NavToConfCode()
                        }))
                        
//                    self.openVC("VC_OTPCheck")
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                    self.mainError.isHidden =  false
                    self.mainError.text! = result?.errors?[0] ?? result?.message ?? error!
                    }
                }
         //       self.disableSignup()
            }
     
    }//LoginByUserEmail
}


////MARK:FPNTextFieldDelegate
//extension VC_SignUp: FPNTextFieldDelegate
//{
//    func fpnDisplayCountryList()
//    {
//        print("function for displaying country list")
//    }
//
//    func fpnDidSelectCountry(name: String, dialCode: String, code: String)
//    {
//        print(name, dialCode, code)
//    }
//
//    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool)
//    {
//        if isValid
//        {
//            txtnumber.text =  textField.getRawPhoneNumber()
//        }
//        else
//        {
//        }
//    }
//}


