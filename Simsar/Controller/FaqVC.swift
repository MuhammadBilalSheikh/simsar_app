//
//  FaqVC.swift
//  Simsar
//
//  Created by NxGeN on 1/15/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class FaqVC: UIViewController {
    @IBOutlet var tblView: UITableView!
    var globalIndicator : UIViewController?
    let viewModel = FaqViewModel()


    override func viewDidLoad() {
        super.viewDidLoad()
        seupUi()
 
    }
    
    private func seupUi(){
        setBackNavButton()
        viewModel.getFaqDataFromAPi { (success, error) in
            if success {
                
                self.tblView.reloadData()
            }
            
        }
        tblView.delegate = self
        tblView.dataSource = self
        self.tblView.register(QuestionAnswerTableCell.nib(), forCellReuseIdentifier:  QuestionAnswerTableCell.identifier)
    }
    
}


//MARK: tableView Delegate
extension FaqVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getFaqSectionCount()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section =  viewModel.getFaqSection(section: section) //sections[section]
        if section.isOpened   {
            return section.answer!.count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let FaqArray = viewModel.getFaqArray(section: indexPath.section)
        let sectionTitle  = viewModel.getFaqSectionTitle(section: indexPath.section)
        
        
        if indexPath.row == 0 {
            let Cell = tableView.dequeueReusableCell(withIdentifier: "FaqTblCell", for: indexPath) as! FaqTblCell
            Cell.lblText.text = sectionTitle
            Cell.imgView.isHidden = false
            Cell.viewDivider.isHidden = false
            Cell.imgDropdown.isHidden = false
            return Cell
            
        } else {
            let Cell = tableView.dequeueReusableCell(withIdentifier: QuestionAnswerTableCell.identifier, for: indexPath) as! QuestionAnswerTableCell
            Cell.question.text = (FaqArray[indexPath.row - 1].question!)
            Cell.answer.text =  (FaqArray[indexPath.row - 1].answer!)
//            Cell.viewDivider.isHidden = true
//            Cell.imgDropdown.isHidden = true
            
            return Cell
        }
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = viewModel.getFaqSection(section: indexPath.section)
        section.isOpened = !section.isOpened
        tblView.reloadSections([indexPath.section], with: .none)
        
    }
}
