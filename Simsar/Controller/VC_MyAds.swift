//
//  VC_MyAds.swift
//  Simsar
//
//  Created by NxGeN on 2/6/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_MyAds: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DesignNavBar()
        
    }
    
    
    
    
    func DesignNavBar(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         self.navigationController?.navigationBar.backgroundColor = UIColor.white
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //setNavigationBar()
     }
    
    

    

}

extension VC_MyAds: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myAdsCollectionVC", for: indexPath) as! myAdsCollectionVC
        
        return cell
    }
    
    
    //SIZE FOR ITEM AT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let padding: CGFloat =  30
           let cvSize = collectionView.frame.size.width - padding
           
        
          //let cellWidth =  140  //cvSize/2    //140 Earlier
           let cellHeight = 166  //176 -> earlier
        return CGSize(width: Int(cvSize)/1 , height: cellHeight)
           
       }//
    
    //MINIMUM LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 30
       }//
    
    //ITEM SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }//
    
    //INSET (UIEDGEINSETS)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    }//
    
    
    
}
