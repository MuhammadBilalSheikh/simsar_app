//
//  NotificationVC.swift
//  Simsar
//
//  Created by NxGeN on 2/20/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class NotificationVC: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    var globalIndicator : UIViewController?
    var userId: Int?
    var collectionOfBrandPlan : [ VC_NotiModel ] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = true
        apiCalledForNoti()
        collectionView.delegate = self
        collectionView.dataSource = self

        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
       
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
    
       self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func disableCV(){
        self.collectionView.isScrollEnabled = false
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func enableCV(){
        self.collectionView.isScrollEnabled = true
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    
    
    //MARK:- API CALL FOR WIDHLIST
    func apiCalledForNoti(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  : 461  //self.userId!
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/my-notification" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_NotiModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                            obj.adTitle = jsonObj["content"].stringValue
                            obj.time = jsonObj["date"].stringValue
                    
                            
                            self.collectionOfBrandPlan.append(obj)
                            
                        }
                        
                        self.collectionView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
}

extension NotificationVC : UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfBrandPlan.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "notificationCollectionCell", for: indexPath) as! notificationCollectionCell
        
        let currentBrandPlan = collectionOfBrandPlan[indexPath.row]
     
        cell.lblNoti.text = currentBrandPlan.adTitle
        cell.lblTimeAgo.text = currentBrandPlan.time
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 378, height: 100)
        }
    
}


class VC_NotiModel {
    var id: Int?
    var  adTitle, time : String?
}


