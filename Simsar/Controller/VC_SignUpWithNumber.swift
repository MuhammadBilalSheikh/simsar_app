//
//  VC_SignUpWithNumber.swift
//  Simsar
//
//  Created by NxGeN on 2/5/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class VC_SignUpWithNumber: UIViewController, UITextFieldDelegate {

    @IBOutlet var contentView: UIView!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnView: UIView!
    @IBOutlet var txtConfrmPass: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtMobNum: FPNTextField!
    @IBOutlet var txtName: UITextField!
    
    var alertMsg: String!
    var a = false
    var b = false
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtMobNum.delegate = self
        txtMobNum.setFlag(key: .KW)
        
        
        self.makeSimsarThemedButton(outerView: btnView , button: btnLogin, needGradient: true )
        
        txtName.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        txtPassword.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        txtMobNum.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        txtConfrmPass.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        self.contentView.layer.cornerRadius = self.contentView.frame.height * 0.005

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3 )
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.55) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
        }
    }
    
    
    func closeAnimation() {
        UIView.animate(withDuration: 0.55 , animations:
                        {
                            self.view.alpha = 0
                            self.view.transform = CGAffineTransform (scaleX: 1.3 , y: 1.3 )
                        }) { (done) in
            self.view.removeFromSuperview()
        }
    }
    

    @IBAction func btnLoginAct(_ sender: UIButton) {
        if checkFields() == true {
            if Connectivity.isConnectedToInternet {
                if txtPassword.text! == txtConfrmPass.text! {
                a = true
                }else {
                    showAlertView(message: "Please Enter Your Password Again", title: "Password Do Not Match")
                }
                if(txtPassword.text! == "" || txtConfrmPass.text! == "") {
                    showAlertView(message: "Password Fields Are Empty", title: "Error")
                } else {
                    b = true
                }
                if a == true && b == true {
                    apiForSignUpWithNumber()
                }
                
        }
        
           
        }else{
            self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
        }
    }
    
    @IBAction func btnCloseAct(_ sender: Any) {
        closeAnimation()
    }
    
    
    //MARK: check field are not empty
    func checkFields()-> Bool
    {
       
        if ( txtName.text!.isEmpty )
        {
            self.showAlertView(message: "Please filled name field", title: "Please Fill All Field")
            return false
        }
        if ( txtPassword.text!.isEmpty )
        {
            self.showAlertView(message: "Please filled password field", title: "Please Fill All Field")
            return false
        }
        if ( txtMobNum.text!.isEmpty )
        {
            self.showAlertView(message: "Please filled mobile field", title: "Please Fill All Field")
            return false
        }
        if ( txtConfrmPass.text!.isEmpty) {
            self.showAlertView(message: "Please filled confirm-password field", title: "Please Fill All Field")
            return false
        }
      
        return true
    }
    
    func NavToVerfPopUp() {
        let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_OtpForNumVerf" )
        self.addChild( ppWithNumber! )
        guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
        vc.view.frame = self.view.frame
        self.view.addSubview( vc.view )
        
        vc.didMove(toParent: self )
    }
    
  
    
    
    func apiForSignUpWithNumber() {
        let apiParams = Parameter()
        apiParams.dictionary = [
            "name"  :  txtName.text!,
            "phone"      :  txtMobNum.text! ,
            "password"   :  txtPassword.text!
        ]
     //   self.enableSignup()
        print("SignupParams***** \(apiParams)")
        
        SignUpServicesWithNum.shared.getSignUpWithNumber(apiParams: apiParams)
            { (result, error) in
                
                if result?.status == "success"
                {
                    print(result?.verificationCode ?? 0)
                    DispatchQueue.main.async
                    {
                        self.alertMsg = result?.message
                       // self.closeAnimation()
                        self.showAlertView(message: self.alertMsg!, title: "Alert", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.NavToVerfPopUp()
                        }))
                      //  self.closeAnimation()
                                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        self.showAlertView(message: result?.message ?? "", title: result?.errors?[0] ?? "")
                   
                    
                    }
                }
         //       self.disableSignup()
            }
     
    }
    
    
    
    
}

extension VC_SignUpWithNumber : FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        print("sadasd")
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            txtMobNum.text =  txtMobNum.getRawPhoneNumber()
        }else{
        }
    }
}
