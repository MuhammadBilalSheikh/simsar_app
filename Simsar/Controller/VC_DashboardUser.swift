//
//  VC_DashboardUser.swift
//  Simsar
//
//  Created by NxGeN on 11/12/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class  VC_DashboardUser : UIViewController{
    
    @IBOutlet var lblExpAds: UILabel!
    @IBOutlet var lblTotalAds: UILabel!
    @IBOutlet var lblAds: UILabel!
    @IBOutlet var stackViewAd: UIStackView!
    @IBOutlet var lblUserEmail: UILabel!
    @IBOutlet var lblBio: UILabel!
    @IBOutlet var lblCity: UILabel!
    @IBOutlet var lblState: UILabel!
    @IBOutlet var lblCountry: UILabel!
    @IBOutlet var lblGender: UILabel!
    @IBOutlet var lblEmpStatus: UILabel!
    @IBOutlet var lblPOB: UILabel!
    @IBOutlet var lblDOB: UILabel!
    @IBOutlet var lblCnic: UILabel!
    @IBOutlet var lblFatherName: UILabel!
  
    @IBOutlet var viewOne: UIView!
    
    @IBOutlet var viewThree: UIView!
    @IBOutlet var viewTwo: UIView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgMain: UIImageView!
    
    @IBOutlet var lblAddress: UILabel!

    
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblPhone: UILabel!
    @IBOutlet var lblName: UILabel!
    var userProfileData:UserProfileData?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUI()
        stackViewAd.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getUserProfileData()
    }
  
    func updateUI(){
        viewOne.dropCardView()
        viewTwo.dropCardView()
        viewThree.dropCardView()
        
    }
    func getUserProfileData(){
        GetUserProfileService.shared.getUserProfileDataResponse { (responce, error) in
            if responce != nil{
                self.userProfileData = responce!
                print("DATA********* \(self.userProfileData!)")
                DispatchQueue.main.async {
                    self.updateContent()
                }
                
            }
        }
       
    }
    func updateContent(){
     
        lblName.text = userProfileData?.name ?? "No Name Found"
        lblPhone.text = userProfileData?.phone ?? "No phone Found"
        lblEmail.text = userProfileData?.email ?? "No email Found"
        lblAddress.text =  userProfileData?.address ?? "No address Found"
        lblFatherName.text =  userProfileData?.fatherName ?? "No address Found"
        lblAds.text = userProfileData?.activeSellNowCount ?? "0"
        lblExpAds.text = userProfileData?.expiredSellNowCount ?? "0"
        lblTotalAds.text = userProfileData?.totalSellNowCount ?? "0"
        lblCnic.text =  userProfileData?.cnic ?? "No address Found"
        lblDOB.text =  userProfileData?.dateOfBirth ?? "No address Found"
        lblPOB.text =  userProfileData?.placeOfBirth ?? "No address Found"
        lblEmpStatus.text =  userProfileData?.employmentStatus ?? "No address Found"
        lblGender.text =  userProfileData?.gender ?? "No Gender Found"
        lblCountry.text =  userProfileData?.country ?? "No address Found"
        lblCity.text =  userProfileData?.city ?? "No address Found"
        lblState.text =  userProfileData?.state ?? "No address Found"
        lblBio.text =  userProfileData?.bio ?? "No address Found"
        
        if ((userProfileData?.profilePic?.isEmpty) != nil){
            print("image hai")
            
            let activityINdicator = UIActivityIndicatorView()
            ServiceManager.shared.downloadImageWithName(Constant.baserUrlImage, (userProfileData?.profilePic)!, activityINdicator, imgMain)
        }
        lblUserName.text = userProfileData?.name
        lblUserEmail.text = userProfileData?.email
 
    }
 
}


//MARK: Ibactions
extension VC_DashboardUser {
    
    
    
    
    @IBAction func viewAll(_ sender: Any) {
        openVC(VcIdentifier.viewAll.rawValue)
    }
    
    
    
    @IBAction func viewAds(_ sender: Any) {
        openVC(VcIdentifier.MyAdds.rawValue)
        
    }
    @IBAction func viewServices(_ sender: Any) {
        openVC(VcIdentifier.services.rawValue)
        
    }
    @IBAction func viewRating(_ sender: Any) {
        openVC(VcIdentifier.rating.rawValue)
    }
    
    
}


