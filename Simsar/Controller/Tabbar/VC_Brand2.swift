//
//  VC_Brand2.swift
//  Simsar
//
//  Created by NxGeN on 11/7/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class VC_Brand2 : UIViewController {
    
    
        
    @IBOutlet var cViewTop: UICollectionView!
    @IBOutlet var cView: UICollectionView!
    
    var idOF : Int!
    var priceOnCall : String!
    
    let constUrl = "https://simsar.com/public/asset/images/sell-now/"
    
    var  globalIndicator : UIViewController? = nil
    
    var dataFromServer : [sellNowSubBrands] = []
    
    var dataFromServerTop : [SellNowCategoryDataTop] = []
    
    override func viewDidLoad() {
            super.viewDidLoad()
        
        
        callApi()
        callApiTop()
        
        
        cView.dataSource = self
        cView.delegate = self
        cViewTop.delegate = self
        cViewTop.dataSource = self
        
        cViewTop.register( brandCardClass.nib(), forCellWithReuseIdentifier: brandCardClass.identifier )
        cView.register( AllAdsCollectionViewCell.nib() , forCellWithReuseIdentifier: AllAdsCollectionViewCell.identifier )
        
        if let layout = cView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 10
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10) }
    
    }//VIEW_DID_Load
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    




func callApi(){
    
    if globalIndicator == nil {
        globalIndicator = createGlobalIndicator()
    }
    showGlobalIndicator(self.globalIndicator , self )
    
    
    let apiParams = Parameter()
    apiParams.dictionary = ["client_key" : Constant.client_id,
                            "brand_id" : idOF!]
    
    let serviceObj = Service(url: "https://simsar.com/api/v1/brand-product" , parameters: apiParams )
   
    serviceObj.headers = Constant.default_header
    
    ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
        if (result != nil ){
            
            self.stopGlobalIndicator( self.globalIndicator!  )
           // let decoder = JSONDecoder()
            
            do {
                
               // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                let jObj : JSON = try JSON(data: (result?.data)! )
                print("JSON___________+++++++ \(jObj)")
                let subBrands = jObj["sellNow"]
                print("categories ==== \(subBrands)" )

                for countOfObjectsInArray in 0..<subBrands.count {

                    print ("Loop For Time === \(countOfObjectsInArray)")


                    let singleObj  = subBrands[countOfObjectsInArray]// may be error in this
                    let obj = sellNowSubBrands(
                        id: singleObj["id"].intValue,
                        title: singleObj["ad_title"].stringValue,
                        datumDescription: singleObj["description"].stringValue,
                        cityname: singleObj["cityname"].stringValue,
                        long_description: singleObj["long_description"].stringValue,
                        price: singleObj["price"].stringValue,
                        del_price: singleObj["dis_price"].stringValue,
                        state: singleObj["statename"].stringValue,
                        createdDate: singleObj["created_date"].stringValue,
                        priceOnCall: singleObj["is_call_price"].stringValue,
                        wishlist: singleObj["is_wishlist"].stringValue,
                        auction: singleObj["is_auction"].stringValue,
                        discPrice: singleObj["is_offer"].stringValue,
                        sparkLight:  singleObj["checkToday"].stringValue,
                        iconImage: singleObj["main_image"].stringValue,
                    expiryDate: singleObj["expiry_date"].stringValue)
                        
                        
                        
                        
                        
        
                        

                    self.dataFromServer.append( obj )


                }
                //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                self.cView.reloadData()
                
            }catch let error {
                debugPrint("error== \(error.localizedDescription)")
            }
            
        }else {
            // no data found
        }
        
    }
    
    }
    
    
    func callApiTop(){
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "brand_id" : 50]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/brand-product" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
                self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let subBrands = jObj["SubBrand"]
                    print("categories ==== \(subBrands)" )

                    for countOfObjectsInArray in 0..<subBrands.count {

                        print ("Loop For Time === \(countOfObjectsInArray)")


                        let singleObj  = subBrands[countOfObjectsInArray]// may be error in this
                        let obj = SellNowCategoryDataTop(id: singleObj["id"].intValue, title: singleObj["title"].stringValue, datumDescription: singleObj["title"].stringValue, iconImage: singleObj["icon_image"].stringValue)
                            
                            
                            
                        self.dataFromServerTop.append( obj )
                       

                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.cViewTop.reloadData()
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
        }
    
    
    
    
    
}




    extension VC_Brand2 : UICollectionViewDelegate , UICollectionViewDataSource
    ,UICollectionViewDelegateFlowLayout
    {
        
//        ///DID SELECT ITEM AT
//        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        openVC("ProducBrandVC")
//
//        }
        
        ///No Of Cells
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if collectionView == cView {
                return dataFromServer.count
            }else{
                return dataFromServerTop.count
            }
            
        }
        
        ///CELL For Item At
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            if collectionView == cViewTop {
                let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: brandCardClass.identifier , for: indexPath ) as! brandCardClass
                cell.lblTitle.text = dataFromServerTop[indexPath.row].title
                if( dataFromServerTop[indexPath.row].iconImage != nil ){
                    ServiceManager.shared.downloadImageWithName(
                        constUrl
                        , dataFromServerTop[indexPath.row].iconImage ?? ""
                   //    , cell.activityIndicator
                        , cell.imgView )
                }
                return cell
                
            } else {
                
                let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier:
                AllAdsCollectionViewCell.identifier , for: indexPath ) as! AllAdsCollectionViewCell
                
                let priceOnCall = dataFromServer[indexPath.row].priceOnCall as? String
                if priceOnCall == "0" {
                    cell2.productPriceLabel.text = dataFromServer[indexPath.row].price
                    cell2.previousPriceLabel.text = dataFromServer[indexPath.row].discPrice
                }else {
                    cell2.lblKd.text = "Price On Call"
                    cell2.previousPriceLabel.isHidden = true
                    cell2.productPriceLabel.isHidden = true
                     cell2.lblKDDisc.isHidden = true
                    
                }
                
//                let wishlist = dataFromServer[indexPath.row].wishlist as? String
//                if wishlist == "0" {
//                    cell2.favouriteButton.backgroundColor = .white
//
//                    //cell2.favouriteButton.setImage(UIImage(named: "heart-icon-red"), for: .normal)
//                }else {
//                    cell2.favouriteButton.backgroundColor = .systemRed
//
//                   // cell2.favouriteButton.setImage(UIImage(named: "heart-icon"), for: .normal)
//                }
                
                
                let auction = dataFromServer[indexPath.row].auction as? String
                if auction == "0" {
                    cell2.betButton.isHidden = true
                }else {
                    cell2.betButton.isHidden = false
                }
                
                let disPrice = dataFromServer[indexPath.row].discPrice as? String
                if disPrice == "0" {
                    cell2.previousPriceLabel.isHidden = false
                    cell2.previousPriceLabel.text = dataFromServer[indexPath.row].del_price as? String
                    cell2.percentButton.isHidden = true
                    
                }else {
                    cell2.previousPriceLabel.text = dataFromServer[indexPath.row].del_price as? String
                    cell2.percentButton.isHidden = false
                }
                
                let spark = dataFromServer[indexPath.row].sparkLight as? String
                if spark == "0" {
                    cell2.boostButton.isHidden = true
                }else {
                    cell2.boostButton.isHidden = false
                }
                
                
                
                
                
                cell2.productTitleLabel.text = dataFromServer[indexPath.row].title
                cell2.productDetailLabel.text = dataFromServer[indexPath.row].long_description
                cell2.expiryDate = dataFromServer[indexPath.row].expiryDate
              
                cell2.lblState.text = dataFromServer[indexPath.row].state
                cell2.remainingTimeLabel.text = dataFromServer[indexPath.row].createdDate
                
                
                cell2.productAddressLabel.text = dataFromServer[indexPath.row].cityname
                
                if( dataFromServer[indexPath.row].iconImage != nil ){
                    ServiceManager.shared.downloadImageWithName(
                        constUrl
                        , dataFromServer[indexPath.row].iconImage ?? ""
                       , cell2.activityIndicator
                        , cell2.productImageView )
                }
                
                
                return cell2
                
                
            }
          
            
            
            
          
            
        }
        
        
        ///CEll Size
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            if collectionView == cView {
                let padding: CGFloat =  30
                let cvSize = collectionView.frame.size.width - padding
    //            print("Colection View Frame == \(collectionView.frame.width)")
    //            print("CView.frame = \(cvSize/2)")
    //            //print("collectionViewLayout == \(collectionViewLayout.collectionViewContentSize.width)")
    //            print("Cell Size -> Foe Item")
                return CGSize(width: cvSize/2 , height: 270 )
            }else {
            let padding: CGFloat =  30
            let cvSize = collectionView.frame.size.width - padding
//            print("Colection View Frame == \(collectionView.frame.width)")
//            print("CView.frame = \(cvSize/2)")
//            //print("collectionViewLayout == \(collectionViewLayout.collectionViewContentSize.width)")
//            print("Cell Size -> Foe Item")
            return CGSize(width: cvSize/2 , height: 102 )
        
        }
        }
        
        ///LINE SPACING
        func collectionView(_ collectionView: UICollectionView, layout
               collectionViewLayout: UICollectionViewLayout,
                               minimumLineSpacingForSectionAt section: Int) -> CGFloat {
                return 20
           }
        
        ///INTER CELL SPACING
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
                return 10
        }
        
        ///EDGE INSETS
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        }
    

    }


extension UIViewController{

func showToast(message : String, seconds: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = .black
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 15
       //self.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
 }




