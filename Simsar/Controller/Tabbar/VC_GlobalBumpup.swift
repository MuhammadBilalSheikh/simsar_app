//
//  VC_GlobalBumpup.swift
//  Simsar
//
//  Created by NxGeN on 11/27/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

//Home Page bumpup Slot(DONE)
//page_id === 1
//
//
//
//Brand Page bumpup Slot
//page_id === 2
//
//
//
//Category Page bumpup Slot
//page_id === 3
//category_id == 56
//
//
//
//Product Detail Page bumpup Slot
//page_id === 4
//
//
//
//My Profile Page bumpup Slot
//page_id === 5
//
//
//
//Search Page bumpup Slot
//page_id === 6




import UIKit
import SwiftyJSON
import ImageSlideshow

class VC_GlobalBumpup: UIViewController {
    
    var mainImage = UIImage()

    var pageId : Int = 1
    var parentController : UIViewController?
    
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var slideImageView : ImageSlideshow!
    @IBOutlet var button : UIButton!
    @IBOutlet weak var imageIndicator : UIActivityIndicatorView!
    @IBOutlet weak var contentView : UIView!
    
    var imageSliderDataSource : [ImageSource]?
    
    let constUrl = "https://simsar.com/public/asset/images/sell-now/"
    
    let adImages : [String] = []
    
    enum bumpUpPageId : Int {
        case homePage  = 1
        case brandPage = 2
        case categoryPage = 3
        case productdetailPage = 4
        case myProfilePage = 5
        
    }
    
    
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        updateUI()
        
    }
    
    func updateUI(){
        
        button.layer.cornerRadius = button.frame.height * 0.05
        self.contentView.layer.cornerRadius = contentView.frame.height * 0.05
        
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
           
           super.viewWillAppear( true )
           //self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3 )
            self.view.transform  = CGAffineTransform (translationX: 0 , y: self.view.bounds.height )
            self.view.alpha = 0
            
            updateView()
            
           UIView.animate(withDuration: 0.55) {
               
               self.view.alpha = 1
               self.view.transform  = CGAffineTransform (translationX: 0 , y: 0 )
               self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
           }
        
       }
    
    func updateView(){
        
        if self.imageSliderDataSource != nil && self.imageSliderDataSource!.count > 0 {
            
            self.slideImageView.isHidden = false
            self.imageView.isHidden = true
            setUpSlider()
        }else {
            self.imageView.isHidden = false
            self.slideImageView.isHidden = true
            self.imageView.image = mainImage
        }
        
    }
    
    
    func closingAnimation(){
    
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0 )
        
        UIView.animate(withDuration: 0.55 , animations:{
                self.view.alpha = 0
                //self.view.transform = CGAffineTransform (scaleX: 0.3 , y: 0.3 ) } )
            self.view.transform   = CGAffineTransform (translationX: 0 , y: self.view.bounds.height ) }) // FOR Moving to bottom
            { (done) in
            self.view.removeFromSuperview()
            }
        
    }
    
    @IBAction func closebutton() {
            
        closingAnimation()
            
    }
    
    func showBumpup(_ Parent : UIViewController ) {
        
        if self.parent == nil {
            self.parentController = Parent
        }
        self.parentController!.addChild( self )
       
        self.view.frame = Parent.view.frame
        self.parentController!.view.addSubview( self.view )
        
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.55 , animations:{ self.view.alpha = 1 } ) { (done) in self.didMove(toParent: Parent )}
    
    }
    
    // SETING UP SLIDER
    func setUpSlider(){
        if (self.imageSliderDataSource!.count > 0 ){
            self.imageView.isHidden =  true
            self.slideImageView.isHidden = false
            
            slideImageView.setImageInputs( self.imageSliderDataSource! )
            
            slideImageView.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center , vertical: .bottom)
            slideImageView.slideshowInterval = 2.0
            slideImageView.contentScaleMode = .scaleToFill
        }else {
            self.imageView.isHidden = false
            self.slideImageView.isHidden = true
        }

    }//SLIDER
    
    
  
}


  extension VC_GlobalBumpup{
    
    public func fetchBannerdata(  _ PageID : bumpUpPageId , _ additionalparam : Int? ){
             
//        case homePage  = 1
//        case brandPage = 2
//        case categoryPage = 3
//        case productdetailPage = 4
//        case myProfilePage = 5
        
        switch PageID {
        case .homePage :
            self.pageId = 1
            
        case .categoryPage :
            self.pageId = 3
            
        case .brandPage :
            self.pageId = 2
            
        case .myProfilePage:
            self.pageId = 5
            
        case .productdetailPage:
            self.pageId = 4
            
        default:
            break
            
        
        }
        
        
        print("FETCH BUMPUD FETCHEDD FOR \(self.pageId)")
                   
                   
                   
                   let apiParams = Parameter()
                   
        if (additionalparam == nil ){
                apiParams.dictionary = [
                    "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                    "page_id"    :  self.pageId]
        }else {
            apiParams.dictionary = [
            "client_key"  : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
            "page_id"     :  self.pageId ,
            "category_id" :  additionalparam! ]
        }
            print("Done with Setting Params \(apiParams)")
        
                   var serviceObj = Service(url: "https://simsar.com/api/v1/bumpup-slot" , parameters: apiParams )
                  
                   serviceObj.headers = ["Content-Type" : "application/json"]
               
                   
                   ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                       if (result != nil ){
                           
                        
                        
                           
                           print ("result is not nil")
                           do {

                               let jObj : JSON = try JSON(data: (result?.data)! )
                               
                            if(jObj["status"]  == "success"){
                            let bumpupSetting  = jObj["bumpupsetting"]
//                                self.imageIndicator.isHidden = false
//                                self.imageIndicator.startAnimating()
                                ServiceManager.shared.downloadImageWithName(self.constUrl
                                , bumpupSetting["banner_image"].stringValue) { image in
                                    if image != nil {
                                        self.mainImage  = image!
//                                        self.imageView.image = image!
//                                        self.imageIndicator.isHidden = true
                                    }else {
                                            print("No Image Found For bumup.")
//                                        self.imageIndicator.isHidden = true
                                    }
                                }
                                
                                
                            let bumpupAds      = jObj["bumupads"]
                                if  ( bumpupAds != nil && bumpupAds.count > 0 ){
                                        for count in 0...bumpupAds.count {
                                            
                                            let ad = bumpupAds[count]
                                            ServiceManager.shared.downloadImageWithName(self.constUrl
                                            , ad["bannerimage"].stringValue) { image in
                                                if image != nil {
                                                    self.imageSliderDataSource?.append(ImageSource(image: image! ) )
                                                }//IF
                                            
                                            }//CLOSURE
                                            
                                        }//FOR
                                }//BUMUP-COUNT-LOOP
                                if self.slideImageView != nil {
                                    self.setUpSlider()
                                }
                                
                            }else {
                                print(" \(jObj["status"]) ")
                            }
                            
                           }catch let error {
                               debugPrint("error== \(error.localizedDescription)")
                           }
                           
                       }else {
                           print ("An Error")
                       }
                       
                   }
                   
               }//FETCH BANNER DATA

}//EXTENSION




  class Bumpup_Settings{
    
    var id: Int?
    var pageID, bannerImage, status, createdAt: String?
    var updatedAt: String?
    var deletedAt: String?
    
}
