//
//  VC_SideMenu.swift
//  Simsar
//
//  Created by Sarmad Malik on 29/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_SideMenu: UIViewController {

    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var wallet: UIView!
    @IBOutlet var viewMyAds: UIView!
    @IBOutlet var myProfile: UIView!
    @IBOutlet var viewSetting: UIView!
    @IBOutlet var MyBrand: UIView!
    @IBOutlet var viewChats: UIView!
    @IBOutlet var viewAboutKnowMore: UIView!
    @IBOutlet var viewWishlist: UIView!
    @IBOutlet weak var viewLogout: UIView!
   
    
    @IBOutlet weak var viewCategories: UIView!
    @IBOutlet weak var viewHome: UIView!
    
    @IBOutlet weak var viewSellNow: UIView!
  
   
    @IBOutlet var boostAds: UIView!
   
    var userProfileData:UserProfileData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.applyGradient(colors: [Constant.themeColor_Light.cgColor , Constant.themeColor.cgColor])
        
        
        //CHANGING VIEW NAMES
        
        //
        
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.backgroundColor = UIColor.clear

        setNavigationBar()
        UpdateUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        getUserProfileData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
     func setNavigationBar() {
        
         self.navigationItem.setHidesBackButton(true, animated:false)

         //your custom view for back image with custom size
         let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
         let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

         if let imgBackArrow = UIImage(named: "back-white") {
             imageView.image = imgBackArrow
         }
         view.addSubview(imageView)

         let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
         view.addGestureRecognizer(backTap)

         let leftBarButtonItem = UIBarButtonItem(customView: view )
         self.navigationItem.rightBarButtonItem = leftBarButtonItem
     }

     @objc func backToMain() {
         self.navigationController?.popViewController(animated: true)
     }
    
    
    
    func getUserProfileData(){
        GetUserProfileService.shared.getUserProfileDataResponse { (responce, error) in
            if responce != nil{
                self.userProfileData = responce!
                print("DATA********* \(self.userProfileData!)")
                DispatchQueue.main.async {
                    self.updateContent()
                }
                
            }
        }
       
    }
    
    func updateContent(){
    
        lblUserName.text = userProfileData?.name
        lblEmail.text = userProfileData?.email
 
    }
 
    
    
    
    func UpdateUI(){
        
        //DISABLING SIGNUP VIEW
      //  viewLogout.alpha = 0
        //viewLogout.isHidden = true
        
        //BOOST ADS
        let boostAdsGest = UITapGestureRecognizer(target: self, action: #selector(openBoostAd))
        boostAds.isUserInteractionEnabled = true
        boostAds.addGestureRecognizer(boostAdsGest)
        
        
        
        let home = UITapGestureRecognizer(target: self, action: #selector(openHome))
        viewHome.isUserInteractionEnabled = true
        viewHome.addGestureRecognizer(home)
        
        let Brands = UITapGestureRecognizer(target: self, action: #selector(myBrands))
        MyBrand.isUserInteractionEnabled = true
        MyBrand.addGestureRecognizer(Brands)
        
        
        
        let categories = UITapGestureRecognizer(target: self, action: #selector(openCategories))
        viewCategories.isUserInteractionEnabled = true
        viewCategories.addGestureRecognizer(categories)
        
        

        let setting = UITapGestureRecognizer(target: self, action: #selector(openSetting))
        viewSetting.isUserInteractionEnabled = true
        viewSetting.addGestureRecognizer(setting)
        
        
        let walllet = UITapGestureRecognizer(target: self, action: #selector(openWallet))
        wallet.isUserInteractionEnabled = true
        wallet.addGestureRecognizer(walllet)
        
        
        let Profile = UITapGestureRecognizer(target: self, action: #selector(MyProfile))
        myProfile.isUserInteractionEnabled = true
        myProfile.addGestureRecognizer(Profile)



        
        
        let aboutKnow = UITapGestureRecognizer(target: self, action: #selector(openAboutKnowMore))
        viewAboutKnowMore.isUserInteractionEnabled = true
        viewAboutKnowMore.addGestureRecognizer(aboutKnow)
        
        let Chats = UITapGestureRecognizer(target: self, action: #selector(chat))
        viewChats.isUserInteractionEnabled = true
        viewChats.addGestureRecognizer(Chats)
        
//        let myProfile = UITapGestureRecognizer(target: self, action: #selector(openContactUs))
//        viewContactUs.isUserInteractionEnabled = true
//        viewContactUs.addGestureRecognizer(myProfile)
//
//        let settings = UITapGestureRecognizer(target: self, action: #selector(openFaq))
//        viewFaq.isUserInteractionEnabled = true
//        viewFaq.addGestureRecognizer(settings)
        
        let logout = UITapGestureRecognizer (target: self , action: #selector(funcLogout))
        viewLogout.isUserInteractionEnabled = true
        viewLogout.addGestureRecognizer(logout)
        
        
        let wishList = UITapGestureRecognizer (target: self , action: #selector(funcWishlist))
        viewWishlist.isUserInteractionEnabled = true
        viewWishlist.addGestureRecognizer(wishList)
        
        
        
        
        
        let sellNow = UITapGestureRecognizer (target: self , action: #selector(funcSellNow))
        viewSellNow.isUserInteractionEnabled = true
        viewSellNow.addGestureRecognizer(sellNow)
        
        
        let myAds = UITapGestureRecognizer (target: self , action: #selector(openMyAds))
        viewMyAds.isUserInteractionEnabled = true
        viewMyAds.addGestureRecognizer(myAds)
        
        
        
        
        //TAP Gestures
//        let tapTOU = UITapGestureRecognizer(target: self, action: #selector( termsOfUse ))
//        viewTermsofuse.addGestureRecognizer( tapTOU )
        
//        let Login = UITapGestureRecognizer (target: self , action: #selector( login ))
//        viewPrivacyPolicy.addGestureRecognizer(Login)
        
//        let tapPaymentMethod = UITapGestureRecognizer (target: self , action: #selector( openPaymentMethod ) )
//        paymentMethod.addGestureRecognizer(tapPaymentMethod)
        
    }
     
    @objc func openPaymentMethod(){
        openVC(VcIdentifier.paymentMethod.rawValue)
    }
    
    @objc func myBrands(){
        openVC(VcIdentifier.myBrands.rawValue)
    }
    
    @objc func openCategories(){
        openVC(VcIdentifier.categories.rawValue)
    }
    
    @objc func openSetting(){
        openVC(VcIdentifier.setting.rawValue)
    }
    
    @objc func openWallet(){
        openVC(VcIdentifier.Wallett.rawValue)
    }
    
    @objc func login(){
        openVC(VcIdentifier.login.rawValue)
    }
    
    @objc func MyProfile(){
        openVC(VcIdentifier.viewAll.rawValue)
    }
    
    @objc func openAboutSimsar(){
        openVC(VcIdentifier.aboutSimsar.rawValue)
    }
    
    @objc func openAboutKnowMore(){
        openVC(VcIdentifier.aboutKnow.rawValue)
    }
    
    @objc func openTermsAndCond(){
        openVC(VcIdentifier.termsAndConditions.rawValue)
    }
    @objc func chat(){
        openVC(VcIdentifier.chat.rawValue)
    }
    @objc func openContactUs(){
        openVC(VcIdentifier.contactUs.rawValue)
    }
    @objc func openFaq(){
        openVC(VcIdentifier.faq.rawValue)
    }
    
    @objc func funcLogout(){
        UserDefaults.standard.removeObject(forKey: Constant.userDefaultUserId)
        backToMain()
    }
    
    @objc func funcWishlist(){
        openVC(VcIdentifier.wishlist.rawValue)
    }
    
    @objc func openHome(){
        openVC(VcIdentifier.home.rawValue)
    }
    

        
    
    @objc func funcSellNow(){
        openVC(VcIdentifier.selectCategories.rawValue)
    }
    
    @objc func termsOfUse(){
        openVC(VcIdentifier.termsOfUse.rawValue)
    }
    
    @objc func privacyPolicy(){
        openVC(VcIdentifier.privacyPolicy.rawValue)
    }
    
    @objc func openBoostAd(){
        self.openVC(VcIdentifier.boostAds.rawValue)
    }
    
    
    @objc func openMyAds(){
        self.openVC(VcIdentifier.MyAdds.rawValue)
    }
}
