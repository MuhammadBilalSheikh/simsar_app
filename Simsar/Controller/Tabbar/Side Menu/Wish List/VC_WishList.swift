//
//  VC_WishList.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView


class VC_WishList: UIViewController {

    @IBOutlet weak var collectionView_Dashboard: UICollectionView!
    var globalIndicator : UIViewController?
    var userId = 471 //WIL BE CHANGED IF USER LOGGED IN
    
    var collectionOfWishListCards : [ VC_WishList_CardModel ] = []
    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        collectionView_Dashboard.delegate = self
        collectionView_Dashboard.dataSource = self
        print("viewDidLoad")
        //DesignNavBar()   // JUST TO TEST
        self.setNavBarWithNotificationIcon()
        
        //Registering Cell -> Featured Collecton View Cell
        collectionView_Dashboard.register(CardWishlistCell.nib() , forCellWithReuseIdentifier: CardWishlistCell.identifier )
        fetchWishListFromServer()
        
    }
    
    
    func DesignNavBar(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         self.navigationController?.navigationBar.backgroundColor = UIColor.white
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setNavigationBar()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        print("View Will Appear")
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        print("viewWillDisappear")
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    
    
    
    
    
    //MARK:- API CALL FOR WIDHLIST
    func fetchWishListFromServer(){
            
            print("API CALLED")
            
            if (globalIndicator == nil ) {
                //globalIndicator = self.storyboard?.instantiateViewController(withIdentifier: "VC_CustomIndicator")
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
            //self.stopGlobalIndicator( self.globalIndicator!  )
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  : self.userId
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/my-wishlist" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_WishList_CardModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                            obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.longDescription  = jsonObj["long_description"].stringValue
                            obj.price  = jsonObj["price"].stringValue
                            obj.disPrice  = jsonObj["dis_price"].stringValue
                            obj.id   = jsonObj["id"].intValue
                            obj.postID   = jsonObj["post_id"].stringValue
                            obj.userID   = jsonObj["user_id"].stringValue
                            obj.cityname = jsonObj["cityname"].stringValue
                            obj.statename = jsonObj["statename"].stringValue
                            obj.isCallPrice = jsonObj["is_call_price"].stringValue
                            obj.isOffer = jsonObj["is_offer"].stringValue
                            obj.isAuction = jsonObj["is_auction"].stringValue
                            obj.checkTodAY = jObj["checkToday"].stringValue
                            obj.Wishlist = jsonObj["is_wishlist"].stringValue
                            obj.expiryDate = jsonObj["expiry_date"].stringValue
                            
                            
                            
                            self.collectionOfWishListCards.append(obj)
                            
                        }
                        
                        self.collectionView_Dashboard.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
            
        }
    
   func setNavigationBar(){
       self.navigationController?.navigationBar.backgroundColor = UIColor.clear
       self.navigationItem.setHidesBackButton(true, animated:false)

       //your custom view for back image with custom size
       let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
       let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

       if let imgBackArrow = UIImage(named: "Shape") {
           imageView.image = imgBackArrow
       }
       view.addSubview(imageView)

       let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
       view.addGestureRecognizer(backTap)

       let leftBarButtonItem = UIBarButtonItem(customView: view)
       self.navigationItem.leftBarButtonItem = leftBarButtonItem
    
           let notification_btn = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(openNotification))
           notification_btn.setBackgroundImage(UIImage(named: "notification"), for: .normal, barMetrics: .default)
       self.navigationItem.rightBarButtonItem = notification_btn
    
    }
    @objc func backToMain(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func openNotification(){
     openVC("VC_Notifications")
    }
}
extension VC_WishList : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
 
    //NUMBER OF ITEMS IN SECTION
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfWishListCards.count
    }//
    
//
//    //CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding: CGFloat = 10
        let cvSize = collectionView.frame.size.width - padding
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")

        return CGSize(width: cvSize/2, height: 215 )

    }

    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20

    }
//
//    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
//
//    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    
    
    
//
    //SIZE FOR ITEM AT
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//           let padding: CGFloat =  30
//           let cvSize = collectionView.frame.size.width - padding
//
//
//          //let cellWidth =  140  //cvSize/2    //140 Earlier
//           let cellHeight = 215  //176 -> earlier
//        return CGSize(width: Int(cvSize)/2 , height: cellHeight)
//
//       }//
//
//    //MINIMUM LINE SPACING
//    func collectionView(_ collectionView: UICollectionView, layout
//           collectionViewLayout: UICollectionViewLayout,
//                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//           return 20
//       }//
//
//    //ITEM SPACING
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }//
//
//    //INSET (UIEDGEINSETS)
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
//    }//

    
    //CELL FOR ITEM AT
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DashBoardCell
//
//       cell.contentView.layer.cornerRadius = 10
//        cell.contentView.layer.borderWidth = 1.0
//
//        cell.contentView.layer.borderColor = UIColor.clear.cgColor
//        cell.contentView.layer.masksToBounds = true
//
//        cell.layer.shadowColor = UIColor.gray.cgColor
//        cell.layer.shadowOffset = CGSize(width: 6, height: 6)
//        cell.layer.shadowRadius = 10 / 2.0
//        cell.layer.shadowOpacity = 0.3
//        cell.layer.masksToBounds = false
//        //cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        //OVERRIDEING CELL -> FEATURED COLLECTION VIEW CELL
        let featuredCell  = collectionView.dequeueReusableCell(withReuseIdentifier: CardWishlistCell.identifier , for: indexPath ) as! CardWishlistCell
        let currentWishlistCard = collectionOfWishListCards[indexPath.row]
        
        
        if (featuredCell.lblDay.text == "00") && (featuredCell.lblHour.text == "00") && (featuredCell.lblMin.text == "00") {
            featuredCell.timmerStackView.isHidden = true
        }else {
            
        }
        
        
        let Auction = currentWishlistCard.isAuction
        if Auction == "1" {
            featuredCell.btnHammer.isHidden = false
        }else {
            featuredCell.btnHammer.isHidden = true
        }
        
        
        let check = currentWishlistCard.checkTodAY
        if check == "1" {
            featuredCell.btnSpark.isHidden = false
        }else {
            featuredCell.btnSpark.isHidden = true
        }
        
        
        let offer = currentWishlistCard.isOffer
        if offer == "1" {
            featuredCell.btnPecent.isHidden = false
            
        }else {
            featuredCell.btnPecent
                .isHidden = true
        }
        
        
        let feature = currentWishlistCard.isFeatured
        if feature == "1" {
            featuredCell.featuredLabel.isHidden = false
        }else{
            featuredCell.featuredLabel.isHidden = true
        }
        
        let isCall = currentWishlistCard.isCallPrice
        if isCall == "1" {
            featuredCell.lblPrice.text = "Is Call Price"
            featuredCell.lblCutPrice.isHidden = true
            featuredCell.btnPecent.isHidden = true
           // cell.productPriceLabel.isHidden = true
           // cell.lblKDDisc.isHidden = true
        }else {
            featuredCell.lblPrice.text = "KD \(currentWishlistCard.price)"
            featuredCell.lblCutPrice.text = "KD \(currentWishlistCard.disPrice)"
            featuredCell.btnPecent.isHidden = false
        }
        
        
        
        
        featuredCell.lblPrice.text   = currentWishlistCard.price
        featuredCell.lblCutPrice.text  = currentWishlistCard.disPrice
        featuredCell.lblTitle.text   = currentWishlistCard.adTitle
        featuredCell.lblLongDesc.text  = currentWishlistCard.longDescription
        featuredCell.lblCity.text = currentWishlistCard.cityname  ?? "" + " " + currentWishlistCard.statename! ?? ""
        if(currentWishlistCard.mainImage != nil ){
        
            ServiceManager.shared.downloadImageWithName(constUrlOfWishListCard
                , currentWishlistCard.mainImage!
                , featuredCell.imgLoader
                , featuredCell.imgViewMain)
            
        } else { print (" Main Image is Nill ")}
        
        
        
        
        return featuredCell
    
    
}



//extension VC_WishList : SkeletonCollectionViewDataSource  {
//
//    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
//
//        return FeaturedCollectionViewCell.identifier
//
//    }
//}


}


class VC_WishList_CardModel {
    
    
    
    
    
    var id: Int?
    var categoryID, postID, userID, subCategoryID: String?
    var brandID, subBrandID, adType, adTitle: String?
    var price, disPrice, disPercentage, isFeatured: String?
    var isBumpupFeatured, isSearchFeatured, isCategoryFeatured, isBrandFeatured: String?
    var isShopFeatured, isHotFeatured, isLatest, isOffer: String?
    var isCallPrice, mapID, isAuction, checkTodAY, Wishlist,likeCount: String?
    var viewCount: String?
    var offerStartDate, offerEndDate, shortDescription: String?
    var longDescription, mainImage, hoverImage, countryID: String?
    var stateID, cityID, latitude, longitude: String?
    var countryIP, sellerName, sellerNumber, isNumber: String?
    var isApprove, statusID, expiryDate, createdAt: String?
    var updatedAt: String?
    var deletedAt: String?
    var subCategory, category, statename, cityname: String?
    
    
}
