//
//  VC_MyAdds.swift
//  Simsar
//
//  Created by NxGeN on 11/14/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import SkeletonView

class VC_MyAdds : UIViewController {
    
    @IBOutlet var filterPopUp: UIView!
    @IBOutlet var btnPopUpSold: UIButton!
    @IBOutlet var btnPopUpAll: UIButton!
    @IBOutlet var btnPopUpPending: UIButton!
    @IBOutlet var btnPopUpExp: UIButton!
    @IBOutlet var btnPopUpDeact: UIButton!
    
    @IBOutlet var btnPopUpAct: UIButton!
    

    @IBOutlet var cView : UICollectionView!
    
    var globalIndicator : UIViewController?
    var userId: Int?
    var statusChanged: Int!
    
    var collectionOfMyAddsCards : [ VC_MyAddsModel ] = []
    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"
    
    
    override func viewDidLoad() {
            super.viewDidLoad()
        //DesignNavItemBarButton()
        filterPopUp.layer.cornerRadius = 15
        filterPopUp.isHidden = true
        
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
          fetchWishListFromServer()
        
        cView.dataSource = self
        cView.delegate = self
        
        
      //  cView.register( brandCardClass.nib(), forCellWithReuseIdentifier: //brandCardClass.identifier )
        cView.register( MyAddsCollectonViewCell.nib() , forCellWithReuseIdentifier: MyAddsCollectonViewCell.identifier )
        
//        if let layout = cView?.collectionViewLayout as? UICollectionViewFlowLayout{
//            layout.minimumLineSpacing = 10
//            layout.minimumInteritemSpacing = 10
//            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10) }
    
    }//VIEW_DID_Load
    
    override func viewWillAppear(_ animated: Bool) {
       // Indicator.sharedInstance.showIndicator()
        UIView.animate(withDuration: 0.55) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
        }
        super.viewWillAppear(true)
     
       self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    
    @IBAction func btnOpenPopUpFilter(_ sender: UIButton) {
        filterPopUp.isHidden = false
    }
    
    private func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        
        ///navigation item 2
        let btn2 = UIButton(type: .custom)
        let view2 = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView2 = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow2 = UIImage(named: "filter-icon") {
            imageView2.image = imgBackArrow2
        }
        
        view2.addSubview(imageView2)
        btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
       

        
        self.navigationItem.setLeftBarButton(item1, animated: true)
        self.navigationItem.setRightBarButton(item2, animated: true)
    
    }
    @objc func Action() {
        filterPopUp.isHidden = false
    }
    
    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
    
    
    func closingAnimation(){
        UIView.animate(withDuration: 0.55 , animations:
                        {
                            self.view.alpha = 0
                            self.view.transform = CGAffineTransform (scaleX: 1.3 , y: 1.3 )
                        }) { (done) in
            self.view.removeFromSuperview()
        }
    }
    
    
    func loadinHubShow() {
                    let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
                    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                    loadingIndicator.hidesWhenStopped = true
                    loadingIndicator.style = UIActivityIndicatorView.Style.gray
                    loadingIndicator.startAnimating();
                    alert.view.addSubview(loadingIndicator)
                    present(alert, animated: true, completion: nil)
        }

    func loadinHubDismiss() {
         dismiss(animated: false, completion: nil)
    }

    
    
    
    //MARK:- RADIO BUTTON POPUP
    
    
    @IBAction func btnRadioPopUpAvt(_ sender: UIButton) {
        if sender.tag == 1 {
            print("OnePressed")
            btnPopUpAll.isSelected = false
                btnPopUpExp.isSelected = false
            btnPopUpAct.isSelected = true
            btnPopUpSold.isSelected = false
            btnPopUpDeact.isSelected = false
            btnPopUpPending.isSelected = false
            statusChanged = 1
            print("SC \(self.statusChanged)")
            fetchApiForActive()
            
            filterPopUp.isHidden = true
            
        }else {
            if sender.tag == 2 {
                print("OnePressed")
                btnPopUpAll.isSelected = true
                    btnPopUpExp.isSelected = false
                btnPopUpAct.isSelected = false
                btnPopUpSold.isSelected = false
                btnPopUpDeact.isSelected = false
                btnPopUpPending.isSelected = false
                fetchWishListFromServer()
                
                filterPopUp.isHidden = true
            }else{
                if sender.tag == 3 {
                    print("OnePressed")
                    btnPopUpAll.isSelected = false
                        btnPopUpExp.isSelected = false
                    btnPopUpAct.isSelected = false
                    btnPopUpSold.isSelected = true
                    btnPopUpDeact.isSelected = false
                    btnPopUpPending.isSelected = false
                }else{
                    if sender.tag == 4 {
                        print("OnePressed")
                        btnPopUpAll.isSelected = false
                            btnPopUpExp.isSelected = false
                        btnPopUpAct.isSelected = false
                        btnPopUpSold.isSelected = false
                        btnPopUpDeact.isSelected = true
                        btnPopUpPending.isSelected = false
                    }else{
                        if sender.tag == 5 {
                            print("OnePressed")
                            btnPopUpAll.isSelected = false
                                btnPopUpExp.isSelected = true
                            btnPopUpAct.isSelected = false
                            btnPopUpSold.isSelected = false
                            btnPopUpDeact.isSelected = false
                            btnPopUpPending.isSelected = false
                        }else{
                            if sender.tag == 6 {
                                print("OnePressed")
                                btnPopUpAll.isSelected = false
                                    btnPopUpExp.isSelected = false
                                btnPopUpAct.isSelected = false
                                btnPopUpSold.isSelected = false
                                btnPopUpDeact.isSelected = false
                                btnPopUpPending.isSelected = true
                                fetchApiForPending()
                                
                                filterPopUp.isHidden = true
                            }else {
                                self.showAlertView(message: "Please Select Any Filter", title: "please Select")
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    //MARK:- API CALL FOR WIDHLIST
    func fetchApiForActive(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  :  471,   //self.userId!,
                                     "status_id" : 1
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/my-ads-status" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        Indicator.sharedInstance.hideIndicator()
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_MyAddsModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                            obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.longDescription  = jsonObj["long_description"].stringValue
                            obj.price  = jsonObj["price"].stringValue
                            obj.disPrice  = jsonObj["dis_price"].stringValue
                            obj.id   = jsonObj["id"].intValue
                            obj.postID   = jsonObj["post_id"].stringValue
                            obj.userID   = jsonObj["user_id"].stringValue
                            obj.cityname = jsonObj["cityname"].stringValue
                            obj.statename = jsonObj["statename"].stringValue
                            
                            
                            self.collectionOfMyAddsCards.append(obj)
                            
                        }
                        
                        self.cView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    
    //MARK:- API CALL FOR WIDHLIST
    func fetchApiForPending(){
        self.showAlertView(message: "Pending ADs", title: "")
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  :  471,   //self.userId!,
                                     "status_id" : 0
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/my-ads-status" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        Indicator.sharedInstance.hideIndicator()
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_MyAddsModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                            obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.longDescription  = jsonObj["long_description"].stringValue
                            obj.price  = jsonObj["price"].stringValue
                            obj.disPrice  = jsonObj["dis_price"].stringValue
                            obj.id   = jsonObj["id"].intValue
                            obj.postID   = jsonObj["post_id"].stringValue
                            obj.userID   = jsonObj["user_id"].stringValue
                            obj.cityname = jsonObj["cityname"].stringValue
                            obj.statename = jsonObj["statename"].stringValue
                            
                            
                            self.collectionOfMyAddsCards.append(obj)
                            
                        }
                        
                        self.cView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    //MARK:- API CALL FOR WIDHLIST
    func fetchWishListFromServer(){
            statusChanged = 2
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  :  461  //self.userId!
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/my-ads" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_MyAddsModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                            obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.longDescription  = jsonObj["long_description"].stringValue
                            obj.price  = jsonObj["price"].stringValue
                            obj.disPrice  = jsonObj["dis_price"].stringValue
                            obj.id   = jsonObj["id"].intValue
                            obj.postID   = jsonObj["post_id"].stringValue
                            obj.userID   = jsonObj["user_id"].stringValue
                            obj.cityname = jsonObj["cityname"].stringValue
                            obj.statename = jsonObj["statename"].stringValue
                            obj.isCallPrice = jsonObj["is_call_price"].stringValue
                            obj.isOffer = jsonObj["is_offer"].stringValue
                            obj.isAuction = jsonObj["is_auction"].stringValue
                            obj.checkTodAY = jObj["checkToday"].stringValue
                            obj.Wishlist = jsonObj["is_wishlist"].stringValue
                            obj.expiryDate = jsonObj["expiry_date"].stringValue
                            obj.isFeatured = jsonObj["is_featured"].stringValue
                            
                            self.collectionOfMyAddsCards.append(obj)
                            
                        }
                        
                        self.cView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    
    
    
    
    
}




    extension VC_MyAdds : UICollectionViewDelegate , UICollectionViewDataSource
    ,UICollectionViewDelegateFlowLayout
    {
        
        ///DID SELECT ITEM AT
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        }
        
        ///No Of Cells
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return collectionOfMyAddsCards.count
        }
        
        ///CELL For Item At
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
//            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: brandCardClass.identifier , for: indexPath )
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:
            MyAddsCollectonViewCell.identifier , for: indexPath ) as! MyAddsCollectonViewCell
            
            let currentAddsCard = collectionOfMyAddsCards[indexPath.row]
            
            if (cell.lblDays.text == "00") && (cell.lblHours.text == "00") && (cell.lblMinutes.text == "00") {
                cell.timmerViewMain.isHidden = true
            }else {
                
            }
            
            
            let Auction = currentAddsCard.isAuction
            if Auction == "1" {
                cell.btnHammer.isHidden = false
            }else {
                cell.btnHammer.isHidden = true
            }
            
            
            let check = currentAddsCard.checkTodAY
            if check == "1" {
                cell.btnSpark.isHidden = false
            }else {
                cell.btnSpark.isHidden = true
            }
            
            
            let offer = currentAddsCard.isOffer
            if offer == "1" {
                cell.btnPercent.isHidden = false
                
            }else {
                cell.btnPercent
                    .isHidden = true
            }
            
            
            let feature = currentAddsCard.isFeatured
            if feature == "1" {
                cell.featureViewMain.isHidden = false
            }else{
                cell.featureViewMain.isHidden = true
            }
            
            let isCall = currentAddsCard.isCallPrice
            if isCall == "1" {
                cell.lblPrice.text = "Is Call Price"
                cell.lblDiscPrice.isHidden = true
               
            }else {
                cell.lblPrice.text = "KD \(currentAddsCard.price)"
                cell.lblDiscPrice.text = "KD \(currentAddsCard.disPrice)"
            }
            
            
            //cell.btnActiveOrDeactive.setTitle("Pending", for: .normal)
            cell.lblPrice.text   = currentAddsCard.price
            cell.lblDiscPrice.text  = currentAddsCard.disPrice
            cell.lblTitle.text   = currentAddsCard.adTitle
            cell.lblDesc.text  = currentAddsCard.longDescription
            cell.lblAddres.text = currentAddsCard.cityname  ?? "" + " " + currentAddsCard.statename! ?? ""
             
            if statusChanged == 1 {
                cell.btnActForUpateAndActive.isHidden = false
                cell.btnActForUpateAndActive.setTitle("Active", for: .normal)
            } else {
                cell.btnActForUpateAndActive.isHidden = true
            }
            
            if statusChanged == 2 {
                cell.btnActForUpateAndActive.isHidden = false
                cell.btnActForUpateAndActive.setTitle("Pending", for: .normal)
            } else {
                cell.btnActForUpateAndActive.isHidden = true
            }
        
            if(currentAddsCard.mainImage != nil ){
            
                ServiceManager.shared.downloadImageWithName(constUrlOfWishListCard
                    , currentAddsCard.mainImage!
                    , cell.imgMain)
                
            } else { print (" Main Image is Nill ")}
            
            
            
            return cell
            
            
        }
        
        
        ///CEll Size
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let padding: CGFloat =  30
            let cvSize = collectionView.frame.size.width - padding
            print("Colection View Frame == \(collectionView.frame.width)")
            print("CView.frame = \(cvSize/2)")
        
            return CGSize(width: cvSize/2, height: 215 )
        
        }
        
        ///LINE SPACING
        func collectionView(_ collectionView: UICollectionView, layout
               collectionViewLayout: UICollectionViewLayout,
                               minimumLineSpacingForSectionAt section: Int) -> CGFloat {
                return 20
           }
        
        ///INTER CELL SPACING
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
                return 10
        }
        
        ///EDGE INSETS
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 5)
        }
    

    }


class VC_MyAddsModel {
    
    
    
    
    
    var id: Int?
    var categoryID, postID, userID, subCategoryID: String?
    var brandID, subBrandID, adType, adTitle: String?
    var price, disPrice, disPercentage, isFeatured: String?
    var isBumpupFeatured, isSearchFeatured, isCategoryFeatured, isBrandFeatured: String?
    var isShopFeatured, isHotFeatured, isLatest, isOffer: String?
    var isCallPrice, mapID, isAuction, likeCount: String?
    var viewCount: String?
    var offerStartDate, offerEndDate, checkTodAY, Wishlist, shortDescription: String?
    var longDescription, mainImage, hoverImage, countryID: String?
    var stateID, cityID, latitude, longitude: String?
    var countryIP, sellerName, sellerNumber, isNumber: String?
    var isApprove, statusID, expiryDate, createdAt: String?
    var updatedAt: String?
    var deletedAt: String?
    var subCategory, category, statename, cityname: String?
    
    
}





extension VC_MyAdds : SkeletonCollectionViewDataSource  {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        
        return "MyAddsCollectonViewCell"
    
    }
}




public class Indicator {

    public static let sharedInstance = Indicator()
    var blurImg = UIImageView()
    var indicator = UIActivityIndicatorView()

    private init()
    {
        blurImg.frame = UIScreen.main.bounds
        blurImg.backgroundColor = UIColor.black
        blurImg.isUserInteractionEnabled = true
        blurImg.alpha = 0
        indicator.style = .whiteLarge
        indicator.center = blurImg.center
        indicator.startAnimating()
        indicator.color = .systemGray    }

    func showIndicator(){
        DispatchQueue.main.async( execute: {

            UIApplication.shared.keyWindow?.addSubview(self.blurImg)
            UIApplication.shared.keyWindow?.addSubview(self.indicator)
        })
    }
    func hideIndicator(){

        DispatchQueue.main.async( execute:
            {
                self.blurImg.removeFromSuperview()
                self.indicator.removeFromSuperview()
        })
    }
}
