//
//  VC_DepositFunds.swift
//  Simsar
//
//  Created by NxGeN on 11/21/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON


class VC_DepositFunds: UIViewController {

    @IBOutlet var tfSelectBank: UITextField!
    @IBOutlet var tfWithdrawAmount: UITextField!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var outerViewSubmitButton: UIView!
    
    var constUrlToFetchImage = "https://simsar.com/public/images/common-pages/"
    var globalIndicator : UIViewController?
    var bankDD = DropDown ()
    var banks:[String] = []
    private let viewModel = updateLocModel()
    var bankArray:[DataForBankModel] = []
    var bankId = 0
    
    //DROP_DOWNS
//    var selectBankDD : DropDown = {
//        let dd = DropDown()
//        dd.dataSource = ["category 1 " , "Category 2" , "Category 3"]
//        return dd
//    }()
//
//    var withdrawAmountDD : DropDown = {
//        let dd = DropDown()
//        dd.dataSource = ["Explore Mode1","Explore Mode 2","Explore Mode3"]
//        return dd
//    }()
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBank()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func updateUI(){
        
        self.makeSimsarThemedButton(outerView: outerViewSubmitButton
                                    , button: submitButton, needGradient: true )
        setupDropDown()
        
        self.setBackNavButton()
        setTitle(title: "Deposit Funds")
        
    }
    
    func setTitle (title : String ){
        
        //self.navigationController?.navigationBar.largeContentTitle = title
        self.navigationController?.title = title
        
    }
    
    
    private func getBank() {
        viewModel.getBank { (Arr, error) in
            if Arr?.count != 0 {
                self.bankArray = (Arr)!
                var bankNameArr:[String] = []
                for n in Arr ?? []{
                    bankNameArr.append(n.ac_title!)
                }
                self.bankDD.dataSource = bankNameArr
            }else {
                
            }
        }
    }
    
    
    
    
//
//
//    func apiCalledForWithdraw(){
//
//
//      // startAnimatingWhileLoadingData()
//        let apiParams = Parameter()
//        apiParams.dictionary = [
//            "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
//            "user_id"    : 461
//        ]
//        let serviceObj = Service(url: "https://simsar.com/api/v1/walletfunds" , parameters: apiParams )
//
//        serviceObj.headers = ["Content-Type" : "application/json"]
//
//
//        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
//
//
//            if (result != nil ){
//
//                //  let decoder = JSONDecoder()
//               // self.stopAnimationgAsDataLoaded()
//                do {
//                    let jObj : JSON = try JSON(data: (result?.data)! )
//                    print("\(jObj)")
//                    let response = jObj["my_banks"]
//                    print("Responce**** \(response[0])")
//                    if jObj["status"].stringValue == "success"  {
//                        self.lblPrice.text = jObj["userWallet"].stringValue
//
//                        for countOfObjectInData in 0..<response.count {
//                            //let obj =  VC_WalletFundModel()
//                            let jj = response[countOfObjectInData]
//
////                            obj.title = jj["ac_title"].stringValue
////                            obj.id = jj["id"].intValue
//
//                            self.lblBank1.text = jj["ac_title"].stringValue
//                            self.lblBank2.text = jj["ac_title"].stringValue
//                            //self.collectionOfFunds.append(obj)
//                        }
//
//
//                    } else {  print("Error in response");   return }
//
//                }catch let error {
//                    debugPrint("error== \(error.localizedDescription)")
//                }
//
//            }else {
//                print ("An Error")
//            }
//
//        }//CLOSURE
//
//    }//LoginByUserEmail
//
}







extension VC_DepositFunds{
    
    func setupDropDown(){
        
        bankDD.anchorView = tfSelectBank
            
           
           // withdrawAmountDD.anchorView = tfWithdrawAmount
            
            
            //SELECTION_ACTION
            bankDD.selectionAction = {index , title in
                self.tfSelectBank.text = title
                self.bankId = Int(self.bankArray[index].id!)
              //  self.tfSelectBank.text = ""
                print("ID****DropDow*** \(self.bankId)")
                print("Name****DropDow*** \(self.title)")
            }
//            withdrawAmountDD.selectionAction = {index , title in
//                self.tfWithdrawAmount.text = title
//            }
//
            //SELECTION_ACTION
            
            //Tap_Gestures
            let selectBankGest = UITapGestureRecognizer(target: self , action: #selector( bankSelected ) )
            tfSelectBank.addGestureRecognizer(selectBankGest)
//
//            let amountGest = UITapGestureRecognizer(target: self , action: #selector( amountSelected ) )
//            tfWithdrawAmount.addGestureRecognizer(amountGest)
//
            
            
        }
        
//        @objc func amountSelected(){
//            //NOT A DROPDOWN .
//            //withdrawAmountDD.show()
//        }
//
        @objc func bankSelected(){
            
            bankDD.show()
        }
    
}
