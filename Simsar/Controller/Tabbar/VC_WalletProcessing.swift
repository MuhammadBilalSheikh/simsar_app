//
//  VC_WalletProcessing.swift
//  Simsar
//
//  Created by NxGeN on 11/21/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON

class VC_WalletProcessing: UIViewController {
       
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var outerViewDepositButton: UIView!
       @IBOutlet var outerViewWithdrawButton: UIView!
    
    var constUrlToFetchImage = "https://simsar.com/public/images/common-pages/"
    var globalIndicator: UIViewController?
       
       
       
       override func viewDidLoad() {
           super.viewDidLoad()
        apiFetchWalletData()
           updateUI()
       }
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
    
    
       
       func updateUI(){
           
        let deposit = UITapGestureRecognizer(target: self, action: #selector(openDeposit))
        outerViewDepositButton.isUserInteractionEnabled = true
        outerViewDepositButton.addGestureRecognizer(deposit)
           
        
        let withDraw = UITapGestureRecognizer(target: self, action: #selector(openWithDraw))
        outerViewWithdrawButton.isUserInteractionEnabled = true
        outerViewWithdrawButton.addGestureRecognizer(withDraw)
        
           reshapeButton(outerView: outerViewWithdrawButton)
           reshapeButton(outerView: outerViewDepositButton)
           self.setBackNavButton()
           setTitle(title: "Wallet")
           
       }
    
    @objc func openDeposit() {
        openVC("VC_WalletFunds")
    }
    
    @objc func openWithDraw() {
        openVC("VC_DepositFunds")
    }
       
       func setTitle (title : String ){
           
           //self.navigationController?.navigationBar.largeContentTitle = title
           self.navigationController?.title = title
           
       }
    
    func reshapeButton(outerView : UIView ){
        
        
        outerView.layer.masksToBounds = false
        outerView.layer.cornerRadius = outerView.frame.height/2
        outerView.layer.shadowOpacity = 0.7
        outerView.layer.shadowRadius = 3
        outerView.layer.shadowOffset = CGSize( width: 0, height: 3 )
        
    }

}



extension VC_WalletProcessing {
    
    func apiFetchWalletData(){
        
        
       startAnimatingWhileLoadingData()
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
            "user_id"    : 461
        ]
        let serviceObj = Service(url: "https://simsar.com/api/v1/mywallet" , parameters: apiParams )
        
        serviceObj.headers = ["Content-Type" : "application/json"]
        
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            
            
            if (result != nil ){
                
                //  let decoder = JSONDecoder()
                self.stopAnimationgAsDataLoaded()
                do {
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("\(jObj)")
                    //let response = jObj["data"]
                    
                    if jObj != nil {
                        
                        DispatchQueue.main.async {
                            self.lblPrice.text = jObj["data"].stringValue
                          
                            
                        }
                        
                    } else {  print("Error in response");   return }
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }//CLOSURE
        
    }//LoginByUserEmail
}

