//
//  VC_Chats.swift
//  Simsar
//
//  Created by NxGeN on 11/14/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit


class VC_Chats : UIViewController {
    
    @IBOutlet var collectionView : UICollectionView!
    
    override func viewDidLoad() {
            super.viewDidLoad()
            initiateCollectioView()
            self.setNavBarWithNotificationIcon()
    }
        
    
        func initiateCollectioView(){
            collectionView.dataSource = self
            collectionView.delegate   = self
        }
        
        
    }



    extension VC_Chats : UICollectionViewDelegate ,
    UICollectionViewDelegateFlowLayout , UICollectionViewDataSource {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return 20
        
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath ) as! Cell_Chat
            if(indexPath.row == 2 || indexPath.row == 3 ){
                cell.recentMessages( "3" )
            }
            return cell
        
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let cvSize = collectionView.frame.width
            return CGSize (width: cvSize , height: 100 )
            
        }
        
        
        
        
    }
