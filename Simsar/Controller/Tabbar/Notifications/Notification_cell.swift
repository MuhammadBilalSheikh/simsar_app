//
//  Notification_cell.swift
//  Simsar
//
//  Created by NxGeN on 11/10/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class Notification_Cell: UITableViewCell {

    @IBOutlet var cornerView: UIView!
    @IBOutlet var endImage: UIImageView!
    @IBOutlet var startImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setImageBorder()
        
    }

    
    func setImageBorder(){
    
        endImage.layer.borderWidth = 0.7
        endImage.layer.borderColor = UIColor.black.cgColor
        endImage.layer.cornerRadius = endImage.frame.height * 0.20
        
    }
    
    ///Wil set the corner red icon
    func isCurrent(_ val : Bool ){
        if(val == true){
            cornerView.layer.cornerRadius = cornerView.frame.height/2
            cornerView.layer.backgroundColor = UIColor.red.cgColor
        } else {
            cornerView.layer.backgroundColor = UIColor.white.cgColor
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

