//
//  VC_Notifications.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_Notifications: UIViewController {

    @IBOutlet weak var tableView_Notification: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView_Notification.delegate = self
        tableView_Notification.dataSource = self
        tableView_Notification.tableFooterView = UIView()
        tableView_Notification.separatorStyle = .none
        self.setBackNavButton()
        //setLeftNavigationBarItem() //ONLY ICON SHOWING LARGE
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        
        self.tabBarController?.tabBar.isHidden = true 
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(  true )
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    ///SETING LEFT NAV BAR ICON
    func setLeftNavigationBarItem(){
        
        let notification_btn = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(leftNavItemClick))
            notification_btn.setBackgroundImage(UIImage(named: "dotIcon"), for: .normal, barMetrics: .default)
        self.navigationItem.rightBarButtonItem = notification_btn
        
    }
    
    ///FUNCTIONALITY ON LEFT NAV ITEM CLICKED
    @objc func leftNavItemClick(){
        
    }

   

}
extension VC_Notifications : UITableViewDelegate , UITableViewDataSource {
    
    //ROWS IN SECTION
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
        return 3
        }else { return 4 }
    }
    
    //TITLE FOR HEADER ...
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(section == 0 ){
            return "    New"
        }else {
            return "    One Day Ago"
        }
        
    }
    //TO Customize Header view
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        view.tintColor = UIColor.white
        let header = view as! UITableViewHeaderFooterView
        //No Of Notifications
        let textNo = UILabel()
        textNo.text = "2"
        textNo.layer.cornerRadius = textNo.frame.height/2
        textNo.layer.backgroundColor = UIColor.red.cgColor
        textNo.font = UIFont (name: "System" , size: 13 )
        //
        
        if(section == 0 ){
            print("header view For Section Header being Called")
            header.addSubview( textNo )
            header.contentView.addSubview( textNo )
        }
        
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.layoutMargins = UIEdgeInsets(top: 0, left: 40 , bottom: 0 , right: 0 )
        
    }
    
    //CELL FOR ROW
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Notification_Cell
        if(indexPath.section == 0 ){
            Cell.isCurrent( true )// Will Set the corner red icon 
        }
        return Cell
    }
    
    //HEIGHT FOR ROW
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    //NUMBER OF SECTIONS IN TABLE VIEW
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
