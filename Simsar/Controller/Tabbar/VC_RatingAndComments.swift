//
//  VC_RatingAndComments.swift
//  Simsar
//
//  Created by NxGeN on 11/13/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class VC_RatingAndComments : UIViewController {
    
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiateCollectioView()
    }
    
    func initiateCollectioView(){
        collectionView.dataSource = self
        collectionView.delegate   = self 
    }
    
    
}



extension VC_RatingAndComments : UICollectionViewDelegate ,
UICollectionViewDelegateFlowLayout , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return 20
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath )
        //cell.contentView.addBottomBorderWithColor(color: UIColor.gray , width: 1 )
        cell.addBottomBorderWithColor(color: UIColor.gray , width: 1 )
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cvSize = collectionView.frame.width
        return CGSize (width: cvSize , height: 100 )
        
    }
  
}
