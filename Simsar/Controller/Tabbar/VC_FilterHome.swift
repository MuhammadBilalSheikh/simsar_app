//
//  VC_FilterHome.swift
//  Simsar
//
//  Created by NxGeN on 11/14/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class VC_FilterHome : UIViewController {

 //et -> Edit Text
    
    @IBOutlet var typeOfSearch: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var etFrom: UITextField!
    @IBOutlet var etTo: UITextField!

    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var stateTextField: UITextField!
    @IBOutlet var countryTextField: UITextField!
    @IBOutlet var exploreTextField: UITextField!
    @IBOutlet var categoryTextField: UITextField!
    
    @IBOutlet var rBtnFeatured : UIButton!
    @IBOutlet var rBtnLatest: UIButton!
    @IBOutlet var rBtnOffered: UIButton!
    
    @IBOutlet var closeLabel :UILabel!
    //
    let additionalView = UIView( frame: CGRect(x: 0, y: 0, width: 200, height: 100 ))
    var viewConstraints = [NSLayoutConstraint]()
    //
    var startDatePicker = UIDatePicker()
    var endDatePicker = UIDatePicker()
    
    //DROP_DOWNS
    var categoryDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["Cars" , "Real State" , "Bikes"]
        return dd
    }()
    
    var exploreMode : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["Yes","No"]
        return dd
    }()
    
    var countryDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["Pakistan", "India" , "UAE" , "Kuwait" , "Germony"]
        return dd
    }()
    
    var stateDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["sindh", "Punjab" , "KPK" , "Balochistan"]
        return dd
    }()
    
    var cityDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["Karachi", "Lahore" , "Islamabad"]
        return dd
    }()
    //DDS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        aligningEditTexts()
            
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.setupDropDown()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
   
    
}




///DATE PICKERS
extension VC_FilterHome{
    
    ///etTo
    func createStartDatePicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: nil , action: #selector( startDatePicked ) )
        
        toolBar.setItems([doneButton], animated: true)

        startDatePicker.datePickerMode = .date
        etTo.inputAccessoryView = toolBar
        etTo.inputView = self.startDatePicker
        
    }
    
    
    //etFrom
    func createEndDatePicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: nil , action: #selector( endDatePicked ) )
        toolBar.setItems([doneButton], animated: true)
        
        startDatePicker.datePickerMode = .date
        etFrom.inputAccessoryView = toolBar
        etFrom.inputView = self.startDatePicker
        
    }
    
    //etTo
    @objc func startDatePicked(){
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        etTo.text = formatter.string(from: startDatePicker.date)
        self.view.endEditing(true)
    }
    
    //etTo
    @objc func endDatePicked(){
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        etFrom.text = formatter.string(from: startDatePicker.date)
        self.view.endEditing(true)
        
    }
    
    ///will Add Icons To TextFields
    func addIconsToTextFields(){
       
        
    }
    
    
}






//----------------------------------
extension VC_FilterHome {
    
    
    func aligningEditTexts() {
        
        typeOfSearch.borderStyle = .none
        typeOfSearch.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        etFrom.borderStyle = .none
        etFrom.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        etTo .borderStyle = .none
        etTo.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        
    }
    
    func addScroll(){
    
    }
    
    func applyConstraints(){
        
    }
    
}



//DROPDOWNS

extension VC_FilterHome {
    
    func setupDropDown(){
        
        categoryDD.anchorView =  categoryTextField
        exploreMode.anchorView = exploreTextField
        countryDD.anchorView = countryTextField
        stateDD.anchorView = stateTextField
        cityDD.anchorView = cityTextField
        
        //SELECTION_ACTION
        cityDD.selectionAction = {index , title in
            self.cityTextField.text = title
        }
        stateDD.selectionAction = {index , title in
            self.stateTextField.text = title
        }
        countryDD.selectionAction = {index , title in
            self.countryTextField.text = title
        }
        exploreMode.selectionAction = {index , title in
            self.exploreTextField.text = title
        }
        categoryDD.selectionAction = {index , title in
            self.categoryTextField.text = title
        }
        //SELECTION_ACTION
        
        //Tap_Gestures
        let cityGest = UITapGestureRecognizer(target: self , action: #selector( cityTapped ) )
        cityTextField.addGestureRecognizer(cityGest)
        
        let stateGest = UITapGestureRecognizer(target: self , action: #selector( stateTapped ) )
        stateTextField.addGestureRecognizer(stateGest)
        
        let countryGest = UITapGestureRecognizer(target: self , action: #selector( countryTapped ) )
        countryTextField.addGestureRecognizer(countryGest)
        
        let exploreGest = UITapGestureRecognizer(target: self , action: #selector( exploreTapped ) )
        exploreTextField.addGestureRecognizer(exploreGest)
        
        let categoryGest = UITapGestureRecognizer(target: self , action: #selector( categoryTapped ) )
        categoryTextField.addGestureRecognizer(categoryGest)
        //------------TAP GESTURES
        
    }
    
    @objc func categoryTapped(){
        
        categoryDD.show()
    }
    
    @objc func exploreTapped(){
        
        exploreMode.show()
    }
    
    @objc func countryTapped(){
        
        countryDD.show()
    }
    
    @objc func stateTapped(){
        
        stateDD.show()
    }
    
    @objc func cityTapped(){
        
        cityDD.show()
    }
    
    
}


//DROPDOWNS


///UI THINGS
extension VC_FilterHome {

       func updateUI(){
           
        let gestCloseView = UITapGestureRecognizer(target: self  , action: #selector(closePage) )
        closeLabel.addGestureRecognizer( gestCloseView )
           searchButton.layer.cornerRadius = searchButton.frame.height/2
           searchButton.clipsToBounds = true
           
           searchButton.dropbtnShadow()
           searchButton.applyGradient()
           
           createStartDatePicker()
           createEndDatePicker()
           
           let buttonTap = UITapGestureRecognizer (target: self , action: #selector(searchButtonTapped))
           let viewTap = UITapGestureRecognizer (target: self , action: #selector(onViewTapped))
           
           searchButton.addGestureRecognizer( buttonTap )
           self.view.addGestureRecognizer( viewTap )
    }
        
       @objc func closePage(){
            self.dismiss(animated: true , completion: nil )
       }
       
       @objc func searchButtonTapped(){
           self.dismiss(animated: true , completion: nil )
       }
       
       @objc func onViewTapped(){
           print("View Tapped")
            
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil);

       }
    
    //RADIO BUTTONS
    @IBAction func radioButtons( _ sender : UIButton ){
        if sender.tag == 0{ //Featured
            
            rBtnFeatured.isSelected = true
            rBtnLatest.isSelected = false
            rBtnOffered.isSelected = false
        }
        else if sender.tag == 1 {//latest
            
            
            rBtnFeatured.isSelected = false
            rBtnLatest.isSelected = true
            rBtnOffered.isSelected = false
        }
        else if sender.tag == 2 {//offered

                rBtnFeatured.isSelected = false
                rBtnLatest.isSelected = false
                rBtnOffered.isSelected = true
        }
        
    }//radioButtons

}


