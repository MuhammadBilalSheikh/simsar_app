//
//  VC_SellNowAttribute.swift
//  Simsar
//
//  Created by Hamza Khan on 04.01.21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import DropDown

class VC_SellNowAttribute: UIViewController {
    
    var idOfCat: Int!
    var idOfSubCat: Int!
    var idOfBrand: Int!
    var idOfSubBrand: Int?
    var titleOfSelectedCat: String!
   
    
    let viewModel = SellNowAttributeViewModel()
    let dropDown = DropDown()
    @IBOutlet weak var mainTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
      
        getAttributeData()
        DesignNavItemBarButton()
        setupUi()
       // setBackNavButton()
    }
    
    
    
    func getAttributeData(){
        viewModel.getData(categoryId: idOfCat, subCategoryId: idOfSubCat, brandId: idOfBrand, subBrandId:  idOfSubBrand ?? 0){ complete,error in
            if complete{
                self.mainTableView.delegate = self
                self.mainTableView.dataSource = self
                self.mainTableView.reloadData()
            }else{
                
            }
        }
    }
    
    func setupUi(){
        mainTableView.estimatedRowHeight = 100
        mainTableView.rowHeight = UITableView.automaticDimension
        self.mainTableView.register(DropdownTableViewCell.nib(), forCellReuseIdentifier:  DropdownTableViewCell.identifier)
        self.mainTableView.register(RadioTableViewCell.nib(), forCellReuseIdentifier:  RadioTableViewCell.identifier)
        self.mainTableView.register(TextFieldTableViewCell.nib(), forCellReuseIdentifier:  TextFieldTableViewCell.identifier)
        self.mainTableView.register(NextCancelTableViewCell.nib(), forCellReuseIdentifier:  NextCancelTableViewCell.identifier)
        
    }
    
    //MARK:- BARBUTTONITEM
    func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        

        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: 60, y: 40, width: 10, height: 10)
      //  let  btn3Title = (selectedAdType == 0) ? "Normal" : "Auction"
        btn.cutomizingButtonNavigation(btnTitle: "Attributes", xAxis: 60, yAxis: 40 )
        btn.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn)


        self.navigationItem.setLeftBarButtonItems([item1, item2], animated: true)

        
        
        
        
       // self.navigationItem.setLeftBarButtonItems([item1], animated: true)
    }
    
    @objc func Action() {
        print("Favourite Tap")
    }
    
    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    func goToNext(selectedAttribute:[Int:String]?){
        let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.productDetail.rawValue) as! VC_ProductDetail
        VC.idOfCat = self.idOfCat
        VC.idOfSubCat = self.idOfSubCat
        VC.idOfBrand = self.idOfBrand
        VC.idOfSubBrand = self.idOfSubBrand
        VC.selectedAttribute = selectedAttribute!
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
}


extension VC_SellNowAttribute:UITableViewDelegate,UITableViewDataSource, RadioTableViewCellProtocol, NextCancelTableViewCellProtocol, DropdownTableViewCellProtocol,TextFieldTableViewCellProtocol{
    func didTapOnDropDown(row: Int, section: Int, cell: DropdownTableViewCell) {
        var nameArray : [String] = []
        for data in cell.cellViewModel.property{
            nameArray.append(data.title!)
        }
        dropDown.dataSource = nameArray
        dropDown.anchorView =  cell.txtDropDown
        dropDown.selectionAction = {index , title in
            cell.txtDropDown.text = title
            cell.delegate.didSelectValueFromDropDown(attributeIndex: row, section: section, isSelected: true, propertyRow: index)
        }
        dropDown.show()
    }
    
    func didWriteOnTextField(selectedText: String, section: Int, attributeId: Int) {
        self.viewModel.didWriteTextForAttribute(section: section, attributeId: attributeId, selectedText: selectedText)
    }
    
    func didSelectValueFromDropDown(attributeIndex: Int, section: Int, isSelected: Bool, propertyRow: Int) {
        self.viewModel.didSelectValueFromDropDown(section: section, row: attributeIndex, isSelected: isSelected, propertyRow: propertyRow)
    }
    
    func sendSelectionToSellNowViewModel(attributeIndex: Int, section: Int, isSelected: Bool, propertyRow: Int) {
        self.viewModel.didSelectRadioCell(section: section, row: attributeIndex, isSelected: isSelected, propertyRow: propertyRow)
        self.mainTableView.reloadData()
    }
    func didTapOnNext() {
         self.viewModel.getDataForDetail(completionHandler: { (success, message, data) in
            if success{
                print("Validated")
                goToNext(selectedAttribute: data)
            }
            else{
                showAlertView(message:message, title: "Error")

            }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.getSectionCount() + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == self.viewModel.getSectionCount(){
            return 1
        }
        return self.viewModel.getRowForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if self.viewModel.getSectionCount() == indexPath.section{
            let cell = tableView.dequeueReusableCell(withIdentifier: NextCancelTableViewCell.identifier, for: indexPath) as! NextCancelTableViewCell
//                cell.nextButton.addTarget(self, action:#selector(nextBtnAction), for: .touchUpInside)
                cell.nextButton.makeViewRoundCorner()
            cell.delegate = self
                return cell
        }
        else{
            
            let cellViewModel = self.viewModel.cellViewModelForRow(section: indexPath.section, row: indexPath.row)
            
            if cellViewModel.cellTypeID == "1"{
                let cell = tableView.dequeueReusableCell(withIdentifier: DropdownTableViewCell.identifier, for: indexPath) as! DropdownTableViewCell
                cell.cellViewModel = cellViewModel
                cell.delegate = self
                cell.tag = indexPath.row
                cell.section = indexPath.section
                

                return cell
            }else if cellViewModel.cellTypeID == "2"{
                let cell = tableView.dequeueReusableCell(withIdentifier: RadioTableViewCell.identifier, for: indexPath) as! RadioTableViewCell
                cell.cellViewModel = cellViewModel
                cell.delegate = self
                cell.section = indexPath.section
                cell.tag = indexPath.row
                cell.radioButtonTable.reloadData()
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier:TextFieldTableViewCell.identifier, for: indexPath) as! TextFieldTableViewCell
                cell.cellViewModel = cellViewModel
                cell.tag = indexPath.row
                cell.section = indexPath.section
                cell.delegate = self
                return cell
            }
            
            
        }
    }
}

