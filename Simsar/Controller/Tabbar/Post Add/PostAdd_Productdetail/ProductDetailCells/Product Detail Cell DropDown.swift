//
//  Product Detail Cell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 27/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import DropDown

class Product_Detail_Cell_DropDown: UICollectionViewCell {
    
    @IBOutlet weak var txtDropDown: UITextField!
    @IBOutlet weak var lblDropDown: UILabel!
    
    
    
    
    override func awakeFromNib() {
        self.setupDropDown()
       
    }
    
    
    
    //DROP_DOWNS
    var ddDataSource : DropDown = {
        let dd = DropDown()
        dd.dataSource = []
        return dd
    }()
    
    
    func setupDropDown(){
        
        ddDataSource.anchorView =  txtDropDown
        ddDataSource.selectionAction = {index , title in
            self.txtDropDown.text = title
        }
        
        
        // SET ACTION
        let categorySet = UITapGestureRecognizer(target: self , action: #selector( categoryTapped ) )
        txtDropDown.addGestureRecognizer(categorySet)
}

    @objc func categoryTapped(){
        
        print("Show Drop Down .")
        ddDataSource.show()
    }

    
    
    
    
    
}
