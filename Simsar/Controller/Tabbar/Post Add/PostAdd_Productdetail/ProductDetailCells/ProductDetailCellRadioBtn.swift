//
//  ProductDetailCellRadioBtn.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 27/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class ProductDetailCellRadioBtn: UICollectionViewCell {
    
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var lblRadioBtnThird: UILabel!
    @IBOutlet weak var lblRadioBtnSecond: UILabel!
    @IBOutlet weak var radioBtnThird: UIButton!
    @IBOutlet weak var radioBtnSecond: UIButton!
    @IBOutlet weak var lblRadioBtnFirst: UILabel!
    @IBOutlet weak var radioBtnFirst: UIButton!
    
    
    
    
    
    @IBAction func radioBtnAction(_ sender: UIButton) {
        if sender.tag == 0{ //Featured
            
            radioBtnFirst.isSelected = true
            radioBtnSecond.isSelected = false
            radioBtnThird.isSelected = false
        }
        else
        {
            if sender.tag == 1 {//latest
            
            
            radioBtnFirst.isSelected = false
            radioBtnSecond.isSelected = true
            radioBtnThird.isSelected = false
        }
            else{
                
            
            
                //if sender.tag == 2 {//offered

                radioBtnFirst.isSelected = false
                radioBtnSecond.isSelected = false
                radioBtnThird.isSelected = true
            }
            
            }
        
        
    } 
    
    
}
