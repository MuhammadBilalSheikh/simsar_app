//
//  ProductDetailCellPrice.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 27/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class ProductDetailCellPrice: UICollectionViewCell {
    
    @IBOutlet weak var txtfieldOffPrice: UITextField!
    @IBOutlet weak var txtFiledPrice: UITextField!
    @IBOutlet weak var lblOffPrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
}
