//
//  ProductDetailCellDiscription.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 27/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class ProductDetailCellDiscription: UICollectionViewCell {
    
    @IBOutlet weak var txtViewForDesc: UITextView!
    @IBOutlet weak var lbl: UILabel!
}
