//
//  ProductDetailCellDates.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 27/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class ProductDetailCellDates: UICollectionViewCell {
    
    @IBOutlet weak var lblStDate: UILabel!
    
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var txtStDate: UITextField!
    @IBOutlet weak var lblEndDate: UILabel!
    
    
    var startDatePicker = UIDatePicker()
    var endDatePicker = UIDatePicker()
    
    override func awakeFromNib() {
        createStartDatePicker()
        createEndDatePicker()
       
    }
    
    
   
    
    
    
    
    ///etTo
    func createStartDatePicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: self , action: #selector(startDatePicked))
        
        toolBar.setItems([doneButton], animated: true)

        startDatePicker.datePickerMode = .date
        txtEndDate.inputAccessoryView = toolBar
        txtEndDate.inputView = self.startDatePicker
        
    }
    
    
    //etFrom
    func createEndDatePicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: self , action: #selector( endDatePicked ) )
        toolBar.setItems([doneButton], animated: true)
        
        startDatePicker.datePickerMode = .date
        txtStDate.inputAccessoryView = toolBar
        txtStDate.inputView = self.startDatePicker
        
    }
    
    //etTo
    @objc func startDatePicked(){
       
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        txtEndDate.text = formatter.string(from: startDatePicker.date)
        print("Start tapped \(formatter.string(from: startDatePicker.date))")
    self.contentView.endEditing(true)
    }
    
    //etTo
    @objc func endDatePicked(){
        print("End tapped")
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        txtStDate.text = formatter.string(from: startDatePicker.date)
      self.contentView.endEditing(true)
        
    }
    
}
