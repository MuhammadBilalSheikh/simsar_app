//
//  ProductDetailCellTextField.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 27/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class ProductDetailCellTextField: UICollectionViewCell {
    
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var lbl: UILabel!
}
