//
//  VC_ProductDetail.swift
//  Simsar
//
//  Created by Sarmad Malik on 01/07/2020.
//  Copyright Â 2020 Sarmad Malik. All rights reserved.
//

import UIKit


class VC_ProductDetail: UIViewController {
    
//    @IBOutlet weak var collectionViewWithAllCells: UICollectionView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var btnClear: UIButton!
//    @IBOutlet weak var btnNew: UIButton!
    @IBOutlet weak var viewBtn: UIView!
//    @IBOutlet weak var btnUsed: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtViewDescription: UITextView!
//    @IBOutlet weak var txtBrand: UITextField!
//    @IBOutlet weak var txtModel: UITextField!
    @IBOutlet weak var switchOnCall: UISwitch!
    @IBOutlet weak var txtOfferPrice: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtPercentOff: UITextField!
    @IBOutlet weak var txtOfferStartDate: UITextField!
    @IBOutlet weak var txtOfferEndDate: UITextField!
    @IBOutlet weak var onCallVIew: UIView!
    @IBOutlet weak var ConstraintnxtButtonStack: NSLayoutConstraint!
    @IBOutlet weak var constraintMainViewHeight: NSLayoutConstraint!
   
    //var artist = [[String: Any]]()
    
    private var productDetail:SellNowProductDetailModel!
    var idOfCat: Int!
    var idOfSubCat: Int!
    var idOfBrand: Int!
    var idOfSubBrand: Int?
    var selectedAttribute:[Int:Any]?
    
    private var cellId = 0
   // var brandData : [String] = []
    private let dateFormatter = DateFormatter()
    private var datePickerStartdate = UIDatePicker()
    private var datePickerEnddate = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPrice.delegate = self
        txtOfferPrice.delegate = self
        
        DesignNavItemBarButton()
        updateUI()
        addDatePickerToTextField()
    }
    private func addDatePickerToTextField(){
        datePickerStartdate.addTarget(self, action: #selector(self.startDateChanged), for: .allEvents)
        datePickerEnddate.addTarget(self, action: #selector(self.EndDateChanged), for: .allEvents)
        updateDatePickerUi(textField: txtOfferStartDate, datePicker: datePickerStartdate)
        updateDatePickerUi(textField: txtOfferEndDate, datePicker: datePickerEnddate)

    }
    
    private func updateDatePickerUi(textField:UITextField,datePicker:UIDatePicker){
        datePicker.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200)
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.datePickerDone))
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        textField.inputAccessoryView = toolBar
        textField.inputView = datePicker
        textField.inputView = datePicker
        datePicker.datePickerMode = UIDatePicker.Mode.date
        dateFormatter.dateFormat = "yyyy-MM-dd"
    }
    @objc private func datePickerDone() {
        txtOfferStartDate.resignFirstResponder()
        txtOfferEndDate.resignFirstResponder()
    }

    @objc  private func startDateChanged() {
        txtOfferStartDate.text = dateFormatter.string(from: datePickerStartdate.date)
    }
    
    @objc private func EndDateChanged() {
        txtOfferEndDate.text = dateFormatter.string(from: datePickerEnddate.date)
    }
    
    private func validateData() -> Bool{
        if txtTitle.text == "" ||
            txtViewDescription.text == ""
        {
            return false
        }else if switchOnCall.isOn {
            return true
        }
     
        if  txtPrice.text == "" ||
                txtOfferPrice.text == "" ||
                txtPercentOff.text == "" ||
                txtOfferStartDate.text == "" ||
                txtOfferEndDate.text == ""
        {
            return false
        }else{
            return true
        }
    }
    
    private func updateUI(){
            txtTitle.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
            txtPrice.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
            txtOfferPrice.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
            txtPercentOff.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)

          self.makeSimsarThemedButton(outerView: viewBtn, button: btnNext, needGradient: true)

            btnClear.makeRound(radius: 20)
            btnClear.layer.borderColor = Constant.themeColor.cgColor
            btnClear.layer.borderWidth = 1
            
            txtViewDescription.makeBorder(size: 1)
            txtViewDescription.makeRound(radius: 10)
        }
    
    //MARK:- BARBUTTONITEM
    private func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        
        
        let btn2 = UIButton(type: .custom)
        btn2.cutomizingButtonNavigation(btnTitle: "Details" ,xAxis:60,yAxis:40)
        btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        
        self.navigationItem.setLeftBarButtonItems([item1 , item2 ], animated: true)
    }
    
    @objc private func Action() {
        print("Favourite Tap")
    }
    
    @objc private func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func OnCallShow(show:Bool){
        if show{
            /// onCall selected true
            onCallVIew.isHidden = false
            ConstraintnxtButtonStack.constant = 413
            //constraintMainViewHeight.constant = 790
        }else {
            /// onCall selected false
            onCallVIew.isHidden = true
            ConstraintnxtButtonStack.constant = 413 - 310
           // constraintMainViewHeight.constant = 790 - 300
        }
    }
    @IBAction func OnCall(_ sender: Any) {
          if ((sender as AnyObject).isOn == true) {
         OnCallShow(show: false)
          } else {
            OnCallShow(show: true)
          }
    }
    @IBAction func clear(_ sender: Any) {
        
    }
    @IBAction func next(_ sender: Any) {
        
        if validateData(){
          
            if switchOnCall.isOn == false{
                let price = Double(txtPrice.text!)
                let offerPrice = Double(txtOfferPrice.text!)
                let percentOff = Int(txtPercentOff.text!)
                productDetail =  SellNowProductDetailModel(title: txtTitle.text!, description: txtViewDescription.text!, onCall: switchOnCall.isOn, price: price, offerPrice: offerPrice, percentOff: percentOff, offerStartDate: txtOfferStartDate.text, offerEndDate: txtOfferEndDate.text)
            }else{
                productDetail =  SellNowProductDetailModel(title: txtTitle.text!, description: txtViewDescription.text!, onCall: switchOnCall.isOn)
            }
            
            let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.sellNowUploadPhoto.rawValue) as! VC_UploadPhoto
            VC.idOfCat = self.idOfCat
            VC.idOfSubCat = self.idOfSubCat
            VC.idOfBrand = self.idOfBrand
            VC.idOfSubBrand = self.idOfSubBrand
            VC.selectedAttribute = self.selectedAttribute
            VC.productDetail = self.productDetail
            
            self.navigationController?.pushViewController(VC, animated: true)
            

        }else {
            showAlertView(message: "All the fields should be filled", title:"Field Empty")
        }
    }

}

extension VC_ProductDetail:UITextFieldDelegate{
  
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtOfferPrice || textField == txtPrice {
            
            let Price = Double(txtPrice.text ?? "0") ?? 0.0
            var offerPrice = Double(txtOfferPrice.text ?? "0") ?? 0.0
            if offerPrice > Price {
                offerPrice = Price
                txtOfferPrice.text = txtPrice.text
                self.showAlertView(message: "", title: "Offer price can not be greater then orignal price")

            }
            if Price == 0 && offerPrice == 0 {
                txtPercentOff.text = "0"
            }else{
                let percentage = ((Price - offerPrice ) / Price)*100
                let percentageRounded = (percentage*100).rounded()/100
                txtPercentOff.text = String(percentageRounded)
            }
            
        }
    }
    
    
}

struct SellNowProductDetailModel {
    var title:String
    var description:String
    var onCall:Bool
    var price:Double? = 0.0
    var offerPrice:Double? = 0.0
    var percentOff:Int? = 0
    var offerStartDate:String? = ""
    var offerEndDate:String? = ""
}
