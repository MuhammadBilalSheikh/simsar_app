//
//  VC_AddType.swift
//  Simsar
//
//  Created by Sarmad Malik on 01/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
enum addType:Int{
    case normaAd = 0
    case auction = 1
}

class VC_AddType: UIViewController {
    @IBOutlet weak var viewAuction: UIView!
    @IBOutlet weak var viewNormalAdd: UIView!
    var idOfSelectedSubCat: Int!
    var idOfSelectedCat: Int!
    var selectedAdType: Int?
    var titleOfSelectedCat:String!
    var titleOfSelectedSubCat:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewAuction.dropCardView()
        viewNormalAdd.dropCardView()
       DesignNavItemBarButton()
    }

    //MARK:- NAVIGATION
    @objc func Navigation(){
        let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.chooseBrands.rawValue) as! VC_ChooseBrands
        VC.idOfSelectedSubCat = self.idOfSelectedSubCat
        VC.idOfSelectedCat = self.idOfSelectedCat
        VC.selectedAdType = self.selectedAdType
        VC.titleOfSelectedCat = self.titleOfSelectedCat
        self.navigationController?.pushViewController(VC, animated: true)
    }

    // MARK:- BARBUTTONITEM
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
        btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
        btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnBack)
        
//        let btn2 = UIButton(type: .custom)
//        btn2.cutomizingButtonNavigation(btnTitle: "Category",xAxis:60,yAxis:40)
//        btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
//        let item2 = UIBarButtonItem(customView: btn2)
        
//        let btn3 = UIButton(type: .custom)
//        btn3.cutomizingButtonNavigation(btnTitle:titleOfSelectedSubCat )
//        btn3.addTarget (self, action: #selector(Action), for: .touchUpInside)
//        let item3 = UIBarButtonItem(customView: btn3)
        
        self.navigationItem.setLeftBarButtonItems([item1  ], animated: true)
    }
    
    @objc func Action() {
        print("Favourite Tap")
    }

    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func auctionAdBtnAction(_ sender: Any) {
        selectedAdType = addType.auction.rawValue
        Navigation()
    }
    @IBAction func normalAdBtnAction(_ sender: Any) {
        selectedAdType = addType.normaAd.rawValue
        Navigation()
    }
 
}


