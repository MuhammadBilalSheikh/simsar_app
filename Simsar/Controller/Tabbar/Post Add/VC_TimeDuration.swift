//
//  VC_TimeDuration.swift
//  Simsar
//
//  Created by Sarmad Malik on 01/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_TimeDuration: UIViewController {

    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtEnd: UITextField!
    @IBOutlet weak var txtStart: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.applyGradient()
        btnNext.makeRound(radius: 20)
        viewBtn.clipsToBounds = false
        viewBtn.layer.cornerRadius = 20
        viewBtn.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        
        txtEnd.layer.cornerRadius = 15
        txtEnd.layer.borderColor = Constant.themeColor.cgColor
        txtEnd.layer.borderWidth = 1
        
        txtStart.layer.cornerRadius = 15
               txtStart.layer.borderColor = Constant.themeColor.cgColor
        txtStart.layer.borderWidth = 1
        
        txtStart.setLeftPaddingPoints(10)
        txtEnd.setLeftPaddingPoints(10)
        setNavigationBar()
        
    }
    
    func setNavigationBar() {

         self.navigationItem.setHidesBackButton(true, animated:false)

         //your custom view for back image with custom size
         let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
         let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

         if let imgBackArrow = UIImage(named: "Shape") {
             imageView.image = imgBackArrow
         }
         view.addSubview(imageView)

         let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
         view.addGestureRecognizer(backTap)

         let leftBarButtonItem = UIBarButtonItem(customView: view )
         self.navigationItem.leftBarButtonItem = leftBarButtonItem
     }

     @objc func backToMain() {
         self.navigationController?.popViewController(animated: true)
     }

    @IBAction func next(_ sender: Any) {
        openVC("VC_ContactDetail")
    }
    

}
