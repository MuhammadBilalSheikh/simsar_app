//
//  VC_BannerAdvertisement.swift
//  Simsar
//
//  Created by Sarmad Malik on 02/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_BannerAdvertisement: UIViewController {

    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var adview4: UIView!
    @IBOutlet weak var adview3: UIView!
    @IBOutlet weak var adview2: UIView!
    @IBOutlet weak var adView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNext.applyGradient()
        btnNext.makeRound(radius: 20)
        viewBtn.clipsToBounds = false
        viewBtn.layer.cornerRadius = 20
        viewBtn.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        
        adView.makeRound(radius: 8)
        adView.layer.borderColor = UIColor.lightGray.cgColor
        adView.layer.borderWidth = 1.5
        
        adview2.makeRound(radius: 8)
        adview2.layer.borderColor = UIColor.lightGray.cgColor
        adview2.layer.borderWidth = 1.5
        
        adview3.makeRound(radius: 8)
        adview3.layer.borderColor = UIColor.lightGray.cgColor
        adview3.layer.borderWidth = 1.5
        
        adview4.makeRound(radius: 8)
        adview4.layer.borderColor = UIColor.lightGray.cgColor
        adview4.layer.borderWidth = 1.5
        
        DesignNavBar()
        
    }
     func DesignNavBar(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         self.navigationController?.navigationBar.backgroundColor = UIColor.white
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setNavigationBar()
     }
    
    func setNavigationBar() {

          self.navigationItem.setHidesBackButton(true, animated:false)

          //your custom view for back image with custom size
          let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
          let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

          if let imgBackArrow = UIImage(named: "Shape") {
              imageView.image = imgBackArrow
          }
          view.addSubview(imageView)

          let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
          view.addGestureRecognizer(backTap)

          let leftBarButtonItem = UIBarButtonItem(customView: view )
          self.navigationItem.leftBarButtonItem = leftBarButtonItem
      }

      @objc func backToMain() {
          self.navigationController?.popViewController(animated: true)
      }
    
    @IBAction func next(_ sender: Any) {
        openVC("VC_Services")
    }
    


}
