//
//  VC_UpdateLocation.swift
//  Simsar
//
//  Created by Sarmad Malik on 01/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import GoogleMaps
import DropDown

class VC_UpdateLocation: UIViewController,CLLocationManagerDelegate, UITextViewDelegate {

    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var addressDetailView: UIStackView!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var parentViewSelectAddress: UIView!
    @IBOutlet var selectAddressView: UIView!
   // @IBOutlet weak var txtDropDown: UITextField!
    
    @IBOutlet weak var txtDropDown: UITextView!
    @IBOutlet weak var lblDropDown: UILabel!
    
    var arraData:[String] = []
    private var addressArray:[String] = []
    private var lastKnownLocation: CLLocationCoordinate2D?
    private var userLocation:CLLocationCoordinate2D?
  
    var countryDD = DropDown ()
    private let viewModel = UpdateSavedLocModel()
    var userSavedData:[UserSavedData] = []
    ///data to be forward
    var idOfCat: Int!
    var idOfSubCat: Int!
    var idOfBrand: Int!
    var idOfSubBrand: Int?
    var productDetail:SellNowProductDetailModel!
    var selectedAttribute:[Int:Any]!
    var imageData:SellNowProductImageModel!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDropDownView()
        setBackNavButton()
        setupUI()
        getSavedData()
       // DropDown.appearance().cellHeight = 80
        txtDropDown.delegate = self
        txtDropDown.text = "Choose Saved Address"
        txtDropDown.textColor = UIColor.darkGray
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("didbegin")
        if self.txtDropDown.textColor == UIColor.lightGray {
            self.txtDropDown.text = nil
            self.txtDropDown.textColor = UIColor.black
        }
    }
    
    

    func setupUI(){
        
        selectAddressView.frame.size.width = parentViewSelectAddress.bounds.size.width
       
        selectAddressView.frame.size.height = parentViewSelectAddress.bounds.size.height
        
//        selectAddressView.center = CGPoint(x: parentViewSelectAddress.frame.size.width  / 2,y: parentViewSelectAddress.frame.size.height / 2)
        parentViewSelectAddress.addSubview(selectAddressView)
        
        
        addressDetailView.isHidden = true
        map.isHidden = true
        txtCity.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtState.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtCountry.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        btnNext.applyGradient()
        btnNext.makeRound(radius: 20)
        viewNext.clipsToBounds = false
        viewNext.layer.cornerRadius = 20
        viewNext.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
    }
    
    @IBAction func next(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.sellNowContactDetail.rawValue) as! VC_ContactDetail
        VC.idOfCat = self.idOfCat
        VC.idOfSubCat = self.idOfSubCat
        VC.idOfBrand = self.idOfBrand
        VC.idOfSubBrand = self.idOfSubBrand
        VC.selectedAttribute = self.selectedAttribute
        VC.productDetail = self.productDetail
        VC.imageData = self.imageData
        VC.longi = lastKnownLocation?.longitude
        VC.lati = lastKnownLocation?.latitude
        self.navigationController?.pushViewController(VC, animated: true)
        

    }
    
    func useMyLocation(enable:Bool){
        if enable{
            addressDetailView.isHidden = false
            map.isHidden = false
            parentViewSelectAddress.isHidden = true
            
        }else {
            addressDetailView.isHidden = true
            map.isHidden = true
            parentViewSelectAddress.isHidden = false
        }
    }
    
    
  
    
    func setupDropDown(){
//        for data in arraData{
//            addressArray.append(data.title!)
//        }
//        ddDataSource.dataSource = nameArray
//        ddDataSource.anchorView =  txtDropDown
//        ddDataSource.selectionAction = {index , title in
//            self.txtDropDown.text = title
//        }
//
//
//        // SET ACTION
//        let categorySet = UITapGestureRecognizer(target: self , action: #selector( categoryTapped ) )
//        txtDropDown.addGestureRecognizer(categorySet)
    }
    //DROP_DOWNS
//    var ddDataSource : DropDown = {
//        let dd = DropDown()
//
//        return dd
//    }()
//
//    @objc func categoryTapped(){
//
//        print("Show Drop Down .")
//        ddDataSource.show()
//    }
//
    
    
    
    
/*
    // MARK:- IEMBARBUTTON
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
           btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
           btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
           btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
           let item1 = UIBarButtonItem(customView: btnBack)
        
/*
           let btn2 = UIButton(type: .custom)
          // btn2.setImage(UIImage(named: "imagename"), for: .normal)
           btn2.frame = CGRect(x: 60, y: 40, width: 70, height: 40)
        //btn2.setImage(UIImage(named: "back-icon"), for: .normal)
        btn2.setTitle("ADD", for: .normal)
        btn2.applyGradient()
        btn2.roundCorners(corners: .allCorners, radius: 15)
        btn2.setTitleColor(.white, for: .normal)
           btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
           let item2 = UIBarButtonItem(customView: btn2)
        
        let btn3 = UIButton(type: .custom)
       // btn2.setImage(UIImage(named: "imagename"), for: .normal)
        btn3.frame = CGRect(x: 60, y: 40, width: 70, height: 40)
     //btn2.setImage(UIImage(named: "back-icon"), for: .normal)
     btn3.setTitle("ADD", for: .normal)
        btn3.applyGradient()
     btn3.roundCorners(corners: .allCorners, radius: 15)
     btn3.setTitleColor(.white, for: .normal)
        btn3.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item3 = UIBarButtonItem(customView: btn3)
        
        let btn4 = UIButton(type: .custom)
       // btn2.setImage(UIImage(named: "imagename"), for: .normal)
        btn4.frame = CGRect(x: 60, y: 40, width: 70, height: 40)
     //btn2.setImage(UIImage(named: "back-icon"), for: .normal)
     btn4.setTitle("ADD", for: .normal)
        btn4.applyGradient()
     btn4.roundCorners(corners: .allCorners, radius: 15)
     btn4.setTitleColor(.white, for: .normal)
        btn4.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item4 = UIBarButtonItem(customView: btn4)
*/
        self.navigationItem.setLeftBarButtonItems([item1 ], animated: true)
    }
    
    @objc func Action() {
        print("Favourite Tap")
    }

    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
*/

   
}


extension VC_UpdateLocation{
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = pdblLatitude
            //21.228124
            let lon: Double = pdblLongitude
            //72.833770
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
             print(" =====================loc \(loc)")

            ceo.reverseGeocodeLocation(loc,  preferredLocale: Locale(identifier: "en"),completionHandler:
                {(placemarks, error) in
                    guard let placemark = placemarks?.first else {
                        return
                    }
                    print("placeMArk subThoroughfare\(placemark.subThoroughfare)")
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    print("placeMark ======================\(placemarks)")
                    let pm = placemarks! as [CLPlacemark]
                  print("pm ======================\(pm)")
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        print(pm.country)
                        print(pm.locality)
                        print(pm.subLocality)
                        print(pm.thoroughfare)
                        print(pm.postalCode)
                        print(pm.subThoroughfare)
                        print(pm.administrativeArea)
                        print(pm.areasOfInterest)
                        print(pm.inlandWater)
                        print(pm.isoCountryCode)
                        print(pm.location)
                        print(pm.name)
                        print(pm.ocean)
                        print(pm.region)
                       self.txtCountry.text = pm.country
                        self.txtCity.text = pm.locality
                        self.txtState.text = pm.subAdministrativeArea
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }


                        print("addressString ================\(addressString)")
                  }
            })

        }
    @IBAction func getMyLocation(_ sender: Any) {
        if ((sender as AnyObject).isOn == true) {
            if LocationManager.shared.isLocationPermissionGranted(){
                useMyLocation(enable:true)
                LocationManager.shared.startUpdatingLocation()
                lastKnownLocation = LocationManager.shared.lastReceivedLocation()
                LocationManager.shared.stopUpdatingLocation()
                guard lastKnownLocation != nil else {
                    return
                }
                map.animate(toLocation: lastKnownLocation!)
                map.animate(toZoom: 10)
                map.settings.myLocationButton = true
                map.isMyLocationEnabled = true
                getAddressFromLatLon(pdblLatitude: lastKnownLocation!.latitude, withLongitude: lastKnownLocation!.longitude)
                print("================================lastLat \(lastKnownLocation!.latitude) and lastLon \(lastKnownLocation!.longitude)")

                
            }else{
                if LocationManager.shared.ifUserDenied(){
                    self.showAlertView(message: "No Permission", title: "Please go to Settings and turn on the permissions")
                }
                LocationManager.shared.requestForLocationPermission()
                useMyLocation(enable:true)
                
            }
        } else {
            useMyLocation(enable:false)
            
        }
    }
}



extension VC_UpdateLocation {
    
    func setupDropDownView(){
        
        countryDD.anchorView = txtDropDown
//        stateDD.anchorView = txtState
//        cityDD.anchorView = txtCity
        
        //SELECTION_ACTION
//        cityDD.selectionAction = {index , title in
//            self.txtCity.text = title
//            self.cityLati = Double(self.cityArr[index].latitude!)
//            self.cityLong = Double(self.cityArr[index].longitude!)
//            self.cityId = Int(self.cityArr[index].id!)
//        }
        
//        stateDD.selectionAction = {index , title in
//            self.txtState.text = title
//            self.stateId = Int(self.statesArray[index].id!)
//            self.getCity(stateId: self.stateId)
//            self.txtCity.text = ""
//        }
        
        countryDD.selectionAction = {index , title in
            self.txtDropDown.text = title
          //  self.countryId = Int(self.countryArray[index].id!) ?? 0
            //self.getState()
            
        }
        
        //SELECTION_ACTION
        
        //Tap_Gestures
//        let cityGest = UITapGestureRecognizer(target: self , action: #selector( cityTapped ) )
//        txtCity.addGestureRecognizer(cityGest)
//
//        let stateGest = UITapGestureRecognizer(target: self , action: #selector( stateTapped ) )
//        txtState.addGestureRecognizer(stateGest)
        
        let countryGest = UITapGestureRecognizer(target: self , action: #selector( countryTapped ) )
        txtDropDown.addGestureRecognizer(countryGest)
        
        
    }
    
    @objc func countryTapped(){
        
        countryDD.show()
    }
    
//    @objc func stateTapped(){
//
//        stateDD.show()
//    }
//
//    @objc func cityTapped(){
//
//        cityDD.show()
//    }
    
}



//get address
extension VC_UpdateLocation {
    
    private func getSavedData() {
        viewModel.getSavedData(id: 471) { (savedLocArray, error) in
            if savedLocArray.count != 0{
                //  self.countries = countryArry
                self.userSavedData = savedLocArray
                var countries:[String] = []
                for c in savedLocArray {
                countries.append(c.address ?? "")
          
                }
                print("here is data =========\(self.userSavedData)")
                self.countryDD.dataSource = countries
                
            }else {
                ///show error message no country found
            }
        }
    }
}
