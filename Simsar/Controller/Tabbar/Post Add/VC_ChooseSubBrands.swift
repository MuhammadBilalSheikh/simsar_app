//
//  VC_ChooseSubBrands.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 18/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SkeletonView

class VC_ChooseSubBrands: UIViewController {
    
    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var collectionSubBrand: UICollectionView!
    @IBOutlet weak var btnNext: UIButton!
    
    var titleOfSelectedCat:String!
    var subBrandArray = [Subbrand]()
    var idOfSelectedBrand: Int!
    private var idOfSelectedSubBrandthis: Int!
    var idOfCategoryToSave: Int!
    var idOfSubCategoryToSave: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    //    updateUI()
        DesignNavItemBarButton()
      //  setBackNavButton()
    }
    
    private func updateUI(){
       // self.makeSimsarThemedButton(outerView: viewNext, button: btnNext, needGradient: true)
    }
    
    private func addButtonBorders(v : UIView ){
        v.layer.borderWidth = 1
        v.layer.borderColor = UIColor.black.cgColor
        v.layer.cornerRadius = v.frame.height/2
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
//        if idOfSelectedSubBrandthis == nil {
//            showAlertView(message: "Please Select Some Sub-Brand", title: "Please Select ?")
//        }else{
//            navigation()
//        }
    }
    
    // MARK:- BARBUTTONITEM
    private func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        
                let btn2 = UIButton(type: .custom)
                btn2.cutomizingButtonNavigation(btnTitle: titleOfSelectedCat ,xAxis:60,yAxis:40)
                btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
                let item2 = UIBarButtonItem(customView: btn2)
        
        //        let btn3 = UIButton(type: .custom)
        //        btn3.frame = CGRect(x: 60, y: 40, width: 70, height: 40)
        //        let  btn3Title = (selectedAdType == 0) ? "Normal" : "Auction"
        //        btn3.cutomizingButtonNavigation(btnTitle:btn3Title )
        //        btn3.addTarget (self, action: #selector(Action), for: .touchUpInside)
        //        let item3 = UIBarButtonItem(customView: btn3)
        self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
    }
    
    @objc private func Action() {
        print("Favourite Tap")
    }
    
    @objc private func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
 
    // MARK: - NAVIGATE
    private func navigation(){
        let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.sellNowAttribute.rawValue) as! VC_SellNowAttribute
        VC.idOfCat = self.idOfCategoryToSave
        VC.idOfSubCat = self.idOfSubCategoryToSave
        VC.idOfBrand = self.idOfSelectedBrand
        VC.idOfSubBrand = self.idOfSelectedSubBrandthis
        self.navigationController?.pushViewController(VC, animated: true)
    }
}


extension VC_ChooseSubBrands : UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        subBrandArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseSubBrandCell", for: indexPath) as! ChooseSubBrandCell
        let artData = subBrandArray[indexPath.row]
        cell.btnSubBrand.setTitle(artData.title, for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let artData = subBrandArray[indexPath.row]
        self.idOfSelectedSubBrandthis = artData.id
        print("This is subBrand Id \(idOfSelectedSubBrandthis)")
        let cell = collectionView.cellForItem(at: indexPath) as? ChooseSubBrandCell
        cell?.isSelected(true)
        navigation()
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ChooseSubBrandCell
        cell?.isSelected(false)
    }
}

extension VC_ChooseSubBrands : SkeletonCollectionViewDataSource {
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ChooseSubBrandCell"
    }
}
