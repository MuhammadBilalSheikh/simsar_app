//
//  VC_UploadPhoto.swift
//  Simsar
//
//  Created by Sarmad Malik on 01/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_UploadPhoto: UIViewController {
    
    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var btnNext: UIButton!
    // @IBOutlet weak var imageCollectionView:UICollectionView!
    private var imagePicker : ImagePicker!
    private var imageArray: [String:String] = [:]
    private var viewModel = SellNowUploadImageViewModel()
    private var imageCurrentIndex = 0
    private var imageDic: [String:String] = [:]
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img7: UIImageView!
    @IBOutlet weak var img8: UIImageView!
    @IBOutlet weak var img9: UIImageView!
    @IBOutlet weak var img10: UIImageView!
    @IBOutlet weak var img11: UIImageView!
    @IBOutlet weak var img12: UIImageView!
    
    
    ///data to be forward
    var idOfCat: Int!
    var idOfSubCat: Int!
    var idOfBrand: Int!
    var idOfSubBrand: Int?
    var productDetail: SellNowProductDetailModel!
    var selectedAttribute:[Int:Any]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        makeSimsarThemedButton(outerView: viewBtn, button: btnNext, needGradient: true)
        //DesignNavBar()
        DesignNavItemBarButton()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    func DesignNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
      //  navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //setNavigationBar()
    }
    
    //MARK:- BARBUTTONITEM
    func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        
        
        let btn2 = UIButton(type: .custom)
        // btn2.setImage(UIImage(named: "imagename"), for: .normal)
        btn2.frame = CGRect(x: 60, y: 40, width: 70, height: 40)
        //btn2.setImage(UIImage(named: "back-icon"), for: .normal)
        btn2.setTitle("ADD", for: .normal)
        btn2.applyGradient()
        btn2.roundCorners(corners: .allCorners, radius: 15)
        btn2.setTitleColor(.white, for: .normal)
        btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setLeftBarButtonItems([item1,item2 ], animated: true)
    }
    
    @objc private func Action() {
        print("Favourite Tap")
    }
    
    @objc private func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func addBorder(image : UIImageView){
        
        image.layer.borderWidth = 0.5
        image.layer.borderColor = Constant.themeColor.cgColor
        image.makeRound(radius: 10)
    }
    
}

//MARK: Button actions
extension VC_UploadPhoto{
    @IBAction func next(_ sender: Any) {
        
        if validateImage() {
            let imageData = SellNowProductImageModel(mainImage: imageDic["1"], coverImage: imageDic["2"], otherImages: imageArray)

            let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.sellNowUpdateLocForKuw.rawValue) as! UpdateLocVCForKuw
            print("ResponceOfImage** \(imageDic)")
            print("ResponceOfImage2888 \(imageArray)")
            VC.idOfCat = self.idOfCat
            VC.idOfSubCat = self.idOfSubCat
            VC.idOfBrand = self.idOfBrand
            VC.idOfSubBrand = self.idOfSubBrand
            VC.selectedAttribute = self.selectedAttribute
            VC.productDetail = self.productDetail
            VC.imageData = imageData
            self.navigationController?.pushViewController(VC, animated: true)

        }else{
            showAlertView(message: "You have to add first and second photo of product", title: "Image missing")
        }
        
        
        //not for kuw
//        if validateImage() {
//            let imageData = SellNowProductImageModel(mainImage: imageDic["1"], coverImage: imageDic["2"], otherImages: imageArray)
//
//            let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.sellNowUpdateLocation.rawValue) as! VC_UpdateLocation
//            print("ResponceOfImage** \(imageDic)")
//            print("ResponceOfImage2888 \(imageArray)")
//            VC.idOfCat = self.idOfCat
//            VC.idOfSubCat = self.idOfSubCat
//            VC.idOfBrand = self.idOfBrand
//            VC.idOfSubBrand = self.idOfSubBrand
//            VC.selectedAttribute = self.selectedAttribute
//            VC.productDetail = self.productDetail
//            VC.imageData = imageData
//            self.navigationController?.pushViewController(VC, animated: true)
//
//        }else{
//            showAlertView(message: "You have to add first and second photo of product", title: "Image missing")
//        }
    }
    
    @IBAction func imgOne(_ sender: Any) {
        didTapOnEditImage(imageNumber: 1)
    }
    @IBAction func imgTwo(_ sender: Any) {
        didTapOnEditImage(imageNumber: 2)
        
    }
    @IBAction func imgThree(_ sender: Any) {
        didTapOnEditImage(imageNumber: 3)
        
    }
    @IBAction func imgFour(_ sender: Any) {
        didTapOnEditImage(imageNumber: 4)
        
    }
    @IBAction func imgFive(_ sender: Any) {
        didTapOnEditImage(imageNumber:5)
        
    }
    @IBAction func imgsix(_ sender: Any) {
        didTapOnEditImage(imageNumber: 6)
        
    }
    @IBAction func imgSeven(_ sender: Any) {
        didTapOnEditImage(imageNumber: 7)
        
    }
    @IBAction func imgEight(_ sender: Any) {
        didTapOnEditImage(imageNumber: 8)
        
    }
    @IBAction func imgNine(_ sender: Any) {
        didTapOnEditImage(imageNumber: 9)
        
    }
    @IBAction func imgTen(_ sender: Any) {
        didTapOnEditImage(imageNumber: 10)
        
    }
    @IBAction func imgEleven(_ sender: Any) {
        didTapOnEditImage(imageNumber: 11)
        
    }
    @IBAction func imgTweleve(_ sender: Any) {
        didTapOnEditImage(imageNumber: 12)
        
    }
}


extension VC_UploadPhoto: ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        if image != nil{
            
            switch imageCurrentIndex {
            case 1:
                img1.image = image
                
            case 2:
                img2.image = image
            case 3:
                img3.image = image
            case 4:
                img4.image = image
            case 5:
                img5.image = image
            case 6:
                img6.image = image
            case 7:
                img7.image = image
            case 8:
                img8.image = image
            case 9:
                img9.image = image
            case 10:
                img10.image = image
            case 11:
                img1.image = image
            case 12:
                img12.image = image
            default:break
                
            }
            let imageKey = String(imageCurrentIndex)
            // self.imageArray.append(image!)
            guard let imgData = image?.jpegData(compressionQuality: 0.75) else { return }
            var mainImageType = 0
            if imageCurrentIndex == 1{
                mainImageType = 1
            }else{
                mainImageType = 2
            }
            self.viewModel.updatePicture(imageData: imgData, mainImageType: mainImageType) { (success, imageName) in
                if success{
                    if self.imageDic[imageKey] != nil{
                        self.imageDic.removeValue(forKey: imageKey)
                    }
                    self.imageDic[imageKey] = imageName
                    print(imageName)
                }
            }
            // imageCollectionView.reloadData()
            print(imageDic)
        }
        
    }
    private func validateImage() -> Bool{
        if self.imageDic["1"] == nil  || self.imageDic["2"]  == nil{
            return false
        }else{
            imageArray = imageDic
            imageArray.removeValue(forKey: "1")
            imageArray.removeValue(forKey: "2")
//            for i in 3...10{
//                let key = String(i)
//                if self.imageDic[key] != nil{
//                    imageArray.append(self.imageDic[key]!)
//                }
//            }
            return true
        }
        
    }
    
    @objc private func didTapOnEditImage(imageNumber:Int){
        //self.imagePicker.present(from: self.btnEditDisplayImage)\
        imageCurrentIndex = imageNumber
        print("current idenx of image \(imageCurrentIndex)")
        self.imagePicker.present()
    }
}



struct SellNowProductImageModel {
    var mainImage:String!
    var coverImage:String!
    var otherImages:[String:String]?
}
