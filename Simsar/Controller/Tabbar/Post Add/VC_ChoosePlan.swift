//
//  VC_ChoosePlan.swift
//  Simsar
//
//  Created by Sarmad Malik on 02/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_ChoosePlan: UIViewController {

    @IBOutlet weak var btnPlatinium: UIButton!
    @IBOutlet weak var viewPlatinium: UIView!
    @IBOutlet weak var btnGold: UIButton!
    @IBOutlet weak var viewGold: UIView!
    @IBOutlet weak var btnSilver: UIButton!
    @IBOutlet weak var viewSilver: UIView!
    @IBOutlet weak var btnBronze: UIButton!
    @IBOutlet weak var viewBronze: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
     
        viewBronze.makeRound(radius: 20)
        viewSilver.makeRound(radius: 20)
        viewGold.makeRound(radius: 20)
        viewPlatinium.makeRound(radius: 20)
        
        btnGold.makeRound(radius: 13)
        btnSilver.makeRound(radius: 13)
        btnPlatinium.makeRound(radius: 13)
        btnBronze.makeRound(radius: 13)
        DesignNavBar()
        
    }
    

       func DesignNavBar(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         self.navigationController?.navigationBar.backgroundColor = UIColor.white
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setNavigationBar()
     }
    
    func setNavigationBar() {

          self.navigationItem.setHidesBackButton(true, animated:false)

          //your custom view for back image with custom size
          let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
          let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

          if let imgBackArrow = UIImage(named: "Shape") {
              imageView.image = imgBackArrow
          }
          view.addSubview(imageView)

          let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
          view.addGestureRecognizer(backTap)

          let leftBarButtonItem = UIBarButtonItem(customView: view )
          self.navigationItem.leftBarButtonItem = leftBarButtonItem
      }

      @objc func backToMain() {
          self.navigationController?.popViewController(animated: true)
      }
}
