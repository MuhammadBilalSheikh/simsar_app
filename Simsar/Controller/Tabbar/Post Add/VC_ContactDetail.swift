//
//  VC_ContactDetail.swift
//  Simsar
//
//  Created by Sarmad Malik on 02/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import GoogleMaps
import FlagPhoneNumber
import SwiftyJSON
import OTPFieldView



class VC_ContactDetail: UIViewController, UITextFieldDelegate {

    
    //MARK:- WRITE EMIAL POPUP OUTLETS
    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var viewEnterEmailPopUp: UIView!
    @IBOutlet var btnVerfEmail: UIButton!
    
    
    //MARK:- WRITE EMIAL VERF POPUP OUTLETS
    
    @IBOutlet var viewEmailVerfCode: UIView!
    @IBOutlet var btnVerfy: UIButton!
    @IBOutlet var otpMail: OTPFieldView!
    
    
    
    
    @IBOutlet var otpTextFieldView: OTPFieldView!
    @IBOutlet var btnVerfCode: UIButton!
    @IBOutlet var centerAlignForCodeVerf: NSLayoutConstraint!
    @IBOutlet var txtWriteNum: FPNTextField!
    @IBOutlet var btnVerNum: UIButton!
    @IBOutlet var backGroundBtn: UIButton!
    @IBOutlet var centerAlign: NSLayoutConstraint!
    @IBOutlet var viewMain: UIView!
    @IBOutlet var viewEnterVerCode: UIView!
    @IBOutlet var viewEnterMob: UIView!
    @IBOutlet var lblVerified: UILabel!
    @IBOutlet var btnForVerify: UIButton!
    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var btnPostAd: UIButton!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    
    ///data to be forward
    var idOfCat: Int!
    var idOfSubCat: Int!
    var idOfBrand: Int!
    var idOfSubBrand: Int?
    var productDetail:SellNowProductDetailModel!
    var selectedAttribute:[Int:Any]!
    var imageData:SellNowProductImageModel!
    var longi:Double!
    var lati:Double!
    var sellerName: String?
    var userId: Int!
    var addType: String?
    var stateId:Int?
    var countryId:Int?
    var cityId:Int?
    var sellerNumber:String!
    var NumberVerified = false
    var alertMsg: String?
    var phoneNumThis: Int!
    var verfCode: String!

    var globalIndicator : UIViewController?
  
    
    let viewModel = SellNowPostAdViewModel()
    var userProfileData:UserProfileData?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        
        txtWriteNum.delegate = self
        txtWriteNum.setFlag(key: .KW)
        print("CityID******* \(self.cityId)")
        self.lblVerified.isHidden = true
        self.btnForVerify.isHidden = true
        self.btnVerfEmail.isHidden = true
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        
        setupUi()
       // setBackNavButton()
        //getUserProfileData()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserProfileData()
    }
    
    
    private func setupOtpView(){
            self.otpTextFieldView.fieldsCount = 6
            self.otpTextFieldView.fieldBorderWidth = 2
            self.otpTextFieldView.defaultBorderColor = UIColor.black
            self.otpTextFieldView.filledBorderColor = UIColor.green
            self.otpTextFieldView.cursorColor = UIColor.red
            self.otpTextFieldView.displayType = .underlinedBottom
            self.otpTextFieldView.fieldSize = 40
            self.otpTextFieldView.separatorSpace = 8
            self.otpTextFieldView.shouldAllowIntermediateEditing = false
            self.otpTextFieldView.delegate = self
            self.otpTextFieldView.initializeUI()
        }

    func apiCalledForCodeVerf() {
        startAnimatingWhileLoadingData()
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
         var apiParams = Parameter()
         apiParams.dictionary = [
            "client_key": Constant.client_id,
            "user_id" : userId! ,
            "phone" :  phoneNumThis! ,
            "verification_code": verfCode! ,
            
            
      ]
        print("++Params Verificatiom \(apiParams)")
        print("+VERCODE***** \(self.verfCode!)")
         var serviceObj = Service(url: "https://simsar.com/api/v1/sellnow-number-verification" , parameters: apiParams )
        
         serviceObj.headers = ["Content-Type" : "application/json"]
         ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
          if (result != nil ){
             self.stopAnimationgAsDataLoaded()
              let decoder = JSONDecoder()
             
              do {
              
                  let jObj : JSON = try JSON(data: (result?.data)! )
                  print("\(jObj)")
               //  let response = jObj["data"]
                 
                 if jObj["status"].stringValue == "success"  {
                  DispatchQueue.main.async {
                   //  self.pageTitle.text = response["title"].stringValue
                     self.alertMsg = jObj["message"].string
                     //print("*********** \(self.alertMsg)")
                    self.showAlertView(message: self.alertMsg!, title: "Succesed", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                        self.closeCodeVerfPopUp()
                    }))
                    self.getUserProfileData()
                     //self.showAlertView(message: self.alertMsg!, title: "Successed!")
                   // self.openSellNowVC("VC_ContactDetail")
                     
                 }
             
                 } else {
                     print("Error in response");
                    self.showAlertView(message: self.alertMsg!, title: "Sorry!")
                     return
                     
                 }
              }catch let error {
                  debugPrint("error== \(error.localizedDescription)")
              }
                 
             }else {
                 print ("An Error")
             }
         }//CLOSURE
    }
    
    
//    func showNumberUI() {
//        viewEnterMob.makeRound(radius: self.selectionView.frame.height * 0.1)
//        
//        //add View to parent view
//        showViewInParentView(removeViewFromParent: UIView(), childView: selectionView, ParentView: self.commonView, childViewHeight: self.view.frame.size.height * 0.249)
//        
//    }
//    
//    func showCodeUI() {
//        viewEnterVerCode.makeRound(radius: self.recoveryCodeView.frame.height * 0.1)
//        //add View to parent view
//        showViewInParentView(removeViewFromParent: resetPasswordView, childView: recoveryCodeView, ParentView: self.commonView, childViewHeight:  self.view.frame.size.height * 0.249)
//    }
//    
    func showViewInParentView(removeViewFromParent:UIView, childView:UIView,ParentView:UIView,childViewHeight:CGFloat){
        removeViewFromParent.removeFromSuperview()
        childView.frame.size.width = ParentView.bounds.size.width - 40
        childView.frame.size.height = childViewHeight
        childView.center = CGPoint(x: ParentView.frame.size.width  / 2,
                                   y: ParentView.frame.size.height / 2)
        ParentView.addSubview(childView)
    }
    
    func startAnimatingWhileLoadingData(){
        showGlobalIndicator(globalIndicator , self )
        self.view.showAnimatedGradientSkeleton()
    }
    
    func stopAnimationgAsDataLoaded(){
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
    }
    
    
    func ApiForNumValidate() {
        
        startAnimatingWhileLoadingData()
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        var apiParams = Parameter()
        apiParams.dictionary = [
            "client_key": Constant.client_id,
            "user_id" : userId!,
            "phone" : txtWriteNum.text!
        ]
        print("++++Params \(apiParams)")
        var serviceObj = Service(url: "https://simsar.com/api/v1/sellnow-number-verify" , parameters: apiParams )
        
        serviceObj.headers = ["Content-Type" : "application/json"]
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in

            if (result != nil ){
                self.stopAnimationgAsDataLoaded()
               // let decoder = JSONDecoder()
                
                do {
                    
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("\(jObj)")
                    //  let response = jObj["data"]
                    
                    if jObj["status"].stringValue == "success"  {
                        DispatchQueue.main.async {
                            //  self.pageTitle.text = response["title"].stringValue
                            self.alertMsg = jObj["message"].string
                            //print("*********** \(self.alertMsg)")
                            
                            self.showAlertView(message: self.alertMsg ?? "", title: "Successful", action: UIAlertAction(title:"OK", style: .default, handler: { _ in
                                self.openCodeVerfPopUp()
                            }))
                          
                            self.closeNumVerfPopUp()
                        }
                        
                    } else {
                        print("Error in response");
                        self.showAlertView(message: "Something Is Wrong While Sending", title: "Sorry!")
                        return
                        
                    }
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print("An Error")
            }
        }//CLOSURE
    }//LoginByUserEmail

    func getUserProfileData(){
        GetUserProfileService.shared.getUserProfileDataResponse { (responce, error) in
            if responce != nil{
                self.userProfileData = responce
                print("DATA**********\(self.userProfileData)")
                DispatchQueue.main.async { [self] in
                   // self.updateContent()
                    let ver = self.userProfileData?.isPhoneVerified as! String
                    if ver == "0" {
                        self.NumberVerified = false
                        self.lblVerified.isHidden = false
                        self.lblVerified.text = "(Please Verify Your Number?)"
                        self.btnForVerify.isHidden = false
                    }else {
                        self.NumberVerified = true
                        self.lblVerified.isHidden = false
                        self.lblVerified.text = "(Verified)"
                        self.btnForVerify.isHidden = true
                        self.sellerNumber = self.userProfileData?.phone
                    }
                    self.txtName.text = self.userProfileData?.name
                    self.txtPhone.text = self.userProfileData?.phone
                    self.userName.text = self.userProfileData?.name
                    self.sellerName = self.userProfileData?.name
                    print("SellerNameAfterDidLoad********** \(self.sellerName!)")
                    
                }
                
            }
        }
       
    }
    
    private func setupUi(){
        btnVerfCode.applyGradient()
        btnVerfCode.makeRound(radius: 15)
        btnVerNum.applyGradient()
        btnVerNum.makeRound(radius: 15)
        btnPostAd.applyGradient()
        btnPostAd.makeRound(radius: 20)
        viewBtn.clipsToBounds = false
        viewBtn.layer.cornerRadius = 20
        viewBtn.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        
        txtName.layer.cornerRadius = 15
        txtName.layer.borderColor = UIColor.gray.cgColor
        txtName.layer.borderWidth = 1
        
        txtPhone.layer.cornerRadius = 15
        txtPhone.layer.borderColor = UIColor.gray.cgColor
        txtPhone.layer.borderWidth = 1
        
        txtName.setLeftPaddingPoints(10)
        txtPhone.setLeftPaddingPoints(10)
       
    }
    
    
    //MARK:-  WRITE EMAIL POPUP ACTIONS
    @IBAction func btnNextAct(_ sender: UIButton) {
    }
    
    
    @IBAction func btnClickToVerfEmail(_ sender: UIButton) {
    }
    
    
    
    //MARK:-  EMAIL CODE VERF POPUP ACTIONS
    
    
    @IBAction func btnVerfAct(_ sender: UIButton) {
    }
    
    @IBAction func bnVerfCodeAct(_ sender: UIButton) {
        print("CodeVERfBTNPRessed**")
        
        let string : String = verfCode
        let otpNumberArray = string.map { String($0) }
        if otpNumberArray.count == 6 {
            apiCalledForCodeVerf()
        }else{
            self.showAlertView(message: "Plese enter code", title: "Incomplete")
        }
       
        
    }
    
    
    func openCodeVerfPopUp() {
        centerAlignForCodeVerf.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            
            self.backGroundBtn.alpha = 0.9
        })
    }
    
    @IBAction func btnNavToVerfAt(_ sender: UIButton) {
        centerAlign.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.backGroundBtn.alpha = 0.9
        })

    }
    
    
    @IBAction func showNumberAds(_ sender: Any) {
    }
    
    
    // MARK:- ITEMBARBUTTON
    func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        

        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
    }
    
    @objc func Action() {
        print("Favourite Tap")
    }

    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    

    private func validate()-> Bool{
        
        return true
    }
    
    
    func closeNumVerfPopUp() {
        centerAlign.constant = -400
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
            self.backGroundBtn.alpha = 0
           // self.openCodeVerfPopUp()
        })
    }
    
    func closeCodeVerfPopUp() {
        centerAlignForCodeVerf.constant = 480
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            
            self.backGroundBtn.alpha = 0
        })
    }
    
    
    
    @IBAction func btnVerfNumAct(_ sender: UIButton) {
        print("PRESSEDAFFAN***")
        self.phoneNumThis = Int(txtWriteNum.text!)
       if numValidate() == true {
            ApiForNumValidate()
        
        }else {
            self.showAlertView(message: "Check Your Everything Again", title: "Format Error")
        }
        
        
    }
  
    
    // MARK:- CHECKFIELDS ARE NOT EMPTY
    func numValidate() -> Bool {
        //"The phone must be at least 11 characters."
        // print("Text No is === \(self.txtNum.text!) && TEXTCOUNT == \(txtNum.text?.count ?? 0)")
        if ( txtWriteNum.text!.isEmpty) {
            self.showAlertView(message: "Please Fill Number Field", title: "Field Can't Be Empty")
            return false
        }else if ( self.txtWriteNum.text!.count > 7 && self.txtWriteNum.text!.count < 9 )  {
            return true
        }else {
            showAlertView(message: "Numbers should be less than 8", title: "Number Can't Be Exceed")
            return false
        }
        return true
    }
    
    @IBAction func PostAdd(_ sender: Any) {
       // self.btnPostAd.isEnabled = false
        
        if validate(){
            if txtPhone.text == "" {
                self.showAlertView(message: "Please verify Your Cell-Number", title: "Please Verify")
            }else {
            
            let parameter:[String:Any] = ["client_key":Model_UserStatus.shared.clientKey,
                                        "user_id": Model_UserStatus.shared.getUserId(),
                                        "category_id":self.idOfCat!,
                                        "sub_category_id":self.idOfSubCat!,
                                        "brand_id":self.idOfBrand!,
                                        "sub_brand_id":self.idOfSubBrand ?? 0,
                                        "ad_type":"Normal",
                                        "ad_title":self.productDetail.title,
                                        "is_call_price":(self.productDetail.onCall == true) ? 1:0 ,
                                        "price": productDetail.price ?? 0,
                                        "dis_price":productDetail.offerPrice ?? 0,
                                        "dis_percentage":productDetail.percentOff ?? 0,
                                        "is_featured":0,
                                        "is_latest":0,
                                        "is_auction":0,
                                        "is_offer":0,
                                        "map_id":0,
                                        "offer_start_date":self.productDetail.offerStartDate ?? "",
                                        "offer_end_date": self.productDetail.offerEndDate ?? "",
                                        "short_description":"Shortdescription",
                                        "long_description":self.productDetail.description,
                                        "main_image":self.imageData.mainImage!,
                                        "hover_image": self.imageData.coverImage!,
                                        "country_id":self.countryId!,
                                        "state_id":self.stateId!,
                                        "city_id":self.cityId!,
                                        "latitude":self.lati!,
                                        "longitude":self.longi!,
                                        "country_ip":0,
                                        "seller_name":self.sellerName!,
                                        "seller_number":self.sellerNumber!,
                                        "is_number":1,
                                        "sellnow_attributes": self.selectedAttribute!,
                                        "sellnow_gallery":self.imageData.otherImages ?? [:]]
            
            print("PostParams************ \(parameter)")
            viewModel.postAd(apiParams: parameter) { (success, message,adId, error) in
                if success!{
                    print("SUCCESSD**POST***")
                    
                    let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.sellNowPostAnotherAdd.rawValue) as! VC_PostAnotherAdd
                    VC.adId = adId
                    //self.present(VC, animated: true, completion: nil)
                    self.navigationController?.pushViewController(VC, animated: true)
                  //  self.btnPostAd.isEnabled = true
                }else{
                    print("MES \(message ?? "")")
                    print("ERR \(error ?? "")")
                    self.showAlertView(message: "Posting Failed", title: "Sorry due to some issue Ad failed to post")
                }
            }
                
            }
        }
    }
    
}



extension VC_ContactDetail : FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        print("sadasd")
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            txtWriteNum.text =  txtWriteNum.getRawPhoneNumber()
        }else{
        }
    }
}



extension VC_ContactDetail: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
       
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        print(index)
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        verfCode = otpString
        
    }
}



        
    
 
        
     
