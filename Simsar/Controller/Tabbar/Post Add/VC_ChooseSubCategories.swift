//
//  VC_ChooseProductType.swift
//  Simsar
//
//  Created by NxGeN on 11/12/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import SkeletonView
import UIKit

class VC_ChooseSubCategories : UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnNextView: UIView!
    @IBOutlet weak var cardView: UIView!
    
    private var idOfSelectedSubCatThis: Int!
    private var titleForSelectedSubCat:String!
//    var count = 0
    var strTitle: String!
    var strDesc: String!
    var imageToBeShown : UIImage!
    var idOfSelectedCatProduct : Int!
    var artist = [SubCategoriesData]()
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgView.image = imageToBeShown
        lblTitle.text = strTitle
        setupPostMethod()
        DesignNavItemBarButton()
       // setBackNavButton()
        updateUI()
    }
    
    
    @IBAction func btnNextAction(_ sender: UIButton) {
//        if idOfSelectedSubCatThis == nil {
//            showAlertView(message: "Please Select Some Sub-Category", title: "Please Select ?")
//        }else{
//            Navigation()
//        }
    }
    
    private func setupPostMethod(){
        self.view.startSkeletonAnimation()
        self.view.showAnimatedGradientSkeleton()
        if Connectivity.isConnectedToInternet{
            let apiParams:[String:Any]
            apiParams = [
                "client_key" : Constant.client_id,
                "category_id": idOfSelectedCatProduct!
            ]
            let urlString = Constant.base_url + Constant.eSubCategories
            let parameters = Parameter()
            parameters.dictionary = apiParams
            let serviceObj = Service(url: urlString, parameters: parameters, method: .post)
            serviceObj.headers =  Constant.default_header
            ServiceManager.shared.getRequestForResponse(service: serviceObj) {[weak self] (result, error) in
            
                
                if (result != nil){
                    let decoder = JSONDecoder()
                    print ("RecoveryCode result is not nil")
                    do {
                        let data = result?.data
                        let Responce =  try decoder.decode(SubCategoriesStruct.self, from: data!)
                        if Responce.status == "success"{
                            
                            self?.artist = Responce.data!
                            self?.view.hideSkeleton()
                            self?.view.stopSkeletonAnimation()
                            print("here is \(Responce.data!)")
                            self?.collectionView.reloadData()
                            
                        }else {
                            //  (nil , Responce?.message ?? "")
                        }
                    } catch let error {
                        // (nil , error.localizedDescription )
                        print(error.localizedDescription)
                    }
                }else {
                    //(nil , error?.description)
                    print(error?.description)
                }
            }
        }else{
            self.showAlertView(message: "", title: Constant.internetError)
        }
    }
    
    //MARK:- BARBUTTONITEM
    private func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        
        ///navigation item 2
        let btn2 = UIButton(type: .custom)
        btn2.cutomizingButtonNavigation(btnTitle: strTitle  ,xAxis:60,yAxis:40)
        btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
       

        print("Title****** \(strTitle)")
//        let btn3 = UIButton(type: .custom)
//        btn3.cutomizingButtonNavigation(btnTitle: "Category",xAxis:60,yAxis:40)
//        btn3.addTarget (self, action: #selector(Action), for: .touchUpInside)
//        let item3 = UIBarButtonItem(customView: btn2)
//
        self.navigationItem.setLeftBarButtonItems([item1, item2], animated: true)
    }
    
    @objc func Action() {
        print("Favourite Tap")
    }
    
    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    

    private func updateUI(){
        cardView.layer.shadowColor = UIColor.lightGray.cgColor
        cardView.layer.shadowOpacity = 0.8
        cardView.layer.shadowOffset = CGSize(width: 1, height: 3)
        cardView.layer.cornerRadius = 15
        cardView.DropCardView()
      //  self.makeSimsarThemedButton(outerView: btnNextView, button: btnNext, needGradient: true)
    }
    
    //MARK:- NAVIGATION
    private func Navigation(){
        let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.chooseBrands.rawValue) as! VC_ChooseBrands
        VC.idOfSelectedSubCat = self.idOfSelectedSubCatThis
        VC.idOfSelectedCat = self.idOfSelectedCatProduct
        VC.titleOfSelectedCat = self.strTitle
        VC.selectedAdType = 0 // normal add
        VC.titleOfSelectedSubCate = self.titleForSelectedSubCat
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

extension VC_ChooseSubCategories: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        artist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VC_ChooseProductTypeCell", for: indexPath) as! VC_ChooseProductTypeCell
        let artData = artist[indexPath.row]
        cell.btn.setTitle(artData.title ?? "", for: .normal)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? VC_ChooseProductTypeCell
        let artData = artist[indexPath.row]
        self.idOfSelectedSubCatThis = artData.id
        titleForSelectedSubCat = artData.title
        print("SubCategoryID \(String(idOfSelectedSubCatThis))")
        cell?.isSelected(true)
        Navigation()

    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? VC_ChooseProductTypeCell
        cell?.isSelected(false)
       
    }
}

extension VC_ChooseSubCategories: SkeletonCollectionViewDataSource {
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "VC_ChooseProductTypeCell"
    }
}
