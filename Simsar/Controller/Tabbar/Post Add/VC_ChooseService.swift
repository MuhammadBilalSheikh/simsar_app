//
//  VC_ChooseService.swift
//  Simsar
//
//  Created by NxGeN on 11/12/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class VC_ChooseService : UIViewController {
    
    @IBOutlet var service1View: UIView!
    @IBOutlet var service1Button: UIButton!
    @IBOutlet var service2View: UIView!
    @IBOutlet var service2Button: UIButton!
    @IBOutlet var service3View: UIView!
    @IBOutlet var service3Button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeViewsRound()
    }
    
    
}


extension VC_ChooseService {
    
    private func makeViewsRound(){
        
        makeRound(v: self.service1Button )
        makeRound(v: self.service1View )
        makeRound(v: self.service2Button )
        makeRound(v: self.service2View )
        makeRound(v: self.service3Button )
        makeRound(v: self.service3View )
        
        
    }
    
    private func makeRound( v  : UIView ){
        v.layer.cornerRadius = v.frame.height/2
    }
    
}
