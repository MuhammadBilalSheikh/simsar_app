//
//  VC_PostAnotherAdd.swift
//  Simsar
//
//  Created by Sarmad Malik on 01/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_PostAnotherAdd: UIViewController {
    
    
    @IBOutlet weak var viewViewAdd: UIView!
    @IBOutlet weak var btnviewAdd: UIButton!
    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var btnPostAd: UIButton!
    
    var adId: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ADID******* \(self.adId)")
        btnPostAd.applyGradient()
        btnPostAd.makeRound(radius: 20)
        viewBtn.clipsToBounds = false
        viewBtn.layer.cornerRadius = 20
        viewBtn.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        
        btnviewAdd.layer.borderWidth = 0.5
        btnviewAdd.layer.borderColor = UIColor.lightGray.cgColor
        btnviewAdd.makeRound(radius: 20)
        viewViewAdd.clipsToBounds = false
        viewViewAdd.layer.cornerRadius = 20
        viewViewAdd.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        
        //setNavigationBar()
    }
    
//    func setNavigationBar() {
//
//         self.navigationItem.setHidesBackButton(true, animated:false)
//
//         //your custom view for back image with custom size
//         let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
//         let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))
//
//         if let imgBackArrow = UIImage(named: "Shape") {
//             imageView.image = imgBackArrow
//         }
//         view.addSubview(imageView)
//
//         let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
//         view.addGestureRecognizer(backTap)
//
//         let leftBarButtonItem = UIBarButtonItem(customView: view )
//         self.navigationItem.leftBarButtonItem = leftBarButtonItem
//     }
//
//     @objc func backToMain() {
//         self.navigationController?.popViewController(animated: true)
//     }
    
    @IBAction func postAnother(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @available(iOS 13.0, *)
    @IBAction func viewAdd(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let VC  = storyboard.instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC
        VC?.productId = self.adId!
        self.navigationController?.pushViewController(VC!, animated: true)
        
      // openVC("ProductDetailVC")
    
    }
}
