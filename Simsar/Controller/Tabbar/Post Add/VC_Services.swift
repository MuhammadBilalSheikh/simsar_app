//
//  VC_Services.swift
//  Simsar
//
//  Created by Sarmad Malik on 02/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Foundation

class VC_Services: UIViewController {

    @IBOutlet weak var collectionView_Services: UICollectionView!
    
    var  globalIndicator : UIViewController? = nil
    var dataFromServer : [servicePlan] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Api()
        collectionView_Services.delegate = self
        collectionView_Services.dataSource = self
        DesignNavBar()
    }
    
    func DesignNavBar(){
   self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
       navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       setNavigationBar()
    }
   
   func setNavigationBar() {

         self.navigationItem.setHidesBackButton(true, animated:false)

         //your custom view for back image with custom size
         let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
         let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

         if let imgBackArrow = UIImage(named: "Shape") {
             imageView.image = imgBackArrow
         }
         view.addSubview(imageView)

         let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
         view.addGestureRecognizer(backTap)

         let leftBarButtonItem = UIBarButtonItem(customView: view )
         self.navigationItem.leftBarButtonItem = leftBarButtonItem
     }

     @objc func backToMain() {
         self.navigationController?.popViewController(animated: true)
     }
    
    
    
    
    func Api() {
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "user_id" : 471]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/service-ads" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
             //   self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let Data = jObj["data"]
                    print("categories ==== \(Data)" )

                    for countOfObjectsInArray in 0..<Data.count {

                        print ("Loop For Time === \(countOfObjectsInArray)")


                        let singleObj  = Data[countOfObjectsInArray]// may be error in this
                        let obj = servicePlan(id: singleObj["id"].intValue , title: singleObj["title"].stringValue, price: singleObj["price"].stringValue, isBuy: singleObj["is_buy"].stringValue)
                            
                        
                            
                            

                        self.dataFromServer.append( obj )


                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.collectionView_Services.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    @objc func navToNext() {
        openVC("PaymentVC")

    }
    
    
    
    


}
extension VC_Services : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataFromServer.count
    }
   

   
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchFeatureCell", for: indexPath) as! searchFeatureCell
        
        
        cell.lblPrice.text = dataFromServer[indexPath.row].title
        cell.lblSearchPlan.text = dataFromServer[indexPath.row].title
         
        let status = dataFromServer[indexPath.row].isBuy
        if status == "10" {
            cell.btnBuyNow.isHidden = false
            cell.btnBuyNow.setTitle("Buy Now", for: .normal)
        }
        
        cell.btnBuyNow.addTarget(self, action: #selector(navToNext), for: .touchUpInside)
        
        
        
        
        
        return cell
        
    }
    
    
    //LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 5
        
       }

    //INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 5
    }

    //EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 20, right: 5)
    }

    
    
    
    
//
//    ///CEll Size
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let padding: CGFloat =  30
//        let cvSize = collectionView.frame.size.width - padding
//
//        print("Colection View Frame == \(collectionView.frame.width)")
//        print("CView.frame = \(cvSize/2)")
//
//        return CGSize(width: cvSize/2, height: 198 )
//
//    }
//
//
//    ///LINE SPACING
//    func collectionView(_ collectionView: UICollectionView, layout
//           collectionViewLayout: UICollectionViewLayout,
//                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//            return 15
//       }
//
//    ///INTER CELL SPACING
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//            return 5
//    }
//
//    ///EDGE INSETS
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 5, left: 10, bottom: 0, right: 10)
//    }

    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openVC("VC_SubService")
    }
    
    
    
}
