//
//  VC_ChooseBrands.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 18/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SkeletonView

class VC_ChooseBrands: UIViewController {
    
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var collectionViewAtt: UICollectionView!
    @IBOutlet weak var btnNext: UIButton!
    
    var titleOfSelectedCat:String!
    var idOfSelectedSubCat: Int!
    var titleOfSelectedSubCate: String!
    private var idOfSelectedBrand: Int!
    var idOfSelectedCat: Int!
    var selectedAdType: Int!    //  value: 1 for normalAdd and 2 for auction 
    
    var brandArray = [Adbrand]()
    var subBrandArray = [Subbrand]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPostMethod()
    //    setButtonsBackground()
        DesignNavItemBarButton()
       // setBackNavButton()
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
//        if idOfSelectedBrand == nil {
//            showAlertView(message: "Please Select Some Brand", title: "Please Select ?")
//        }else{
//            navigation()
//        }
    }
    
    private func setButtonsBackground(){
      //  self.makeSimsarThemedButton(outerView: btnView, button: btnNext, needGradient: false)
    }
    
    private func setupPostMethod(){
   
        self.view.startSkeletonAnimation()
        self.view.showAnimatedGradientSkeleton()
        if Connectivity.isConnectedToInternet{
            let apiParams:[String:Any]
            apiParams = [
                "client_key": Constant.client_id
                , "category_id": idOfSelectedCat!
                ,"sub_category_id": idOfSelectedSubCat!
            ]
            
            SubCategoriesBrandService.shared.getSubCategoriesBrandResponse(apiParams: apiParams) { [weak self](response, error) in
                if  response == nil {
                    let getError = (error ?? "Can not able to get Categories")
                    print(getError)
                    self?.showAlertView(message: "", title: getError)
                    return
                }
                DispatchQueue.main.async {
                    self?.brandArray = response!
                    self?.view.hideSkeleton()
                    self?.view.stopSkeletonAnimation()
                    self?.collectionViewAtt.reloadData()
                }
            }
            
        }else{
            let action = UIAlertAction(title: "Retry", style: UIAlertAction.Style.destructive,handler: { action in
                self.setupPostMethod()
            })
            self.showAlertView(message: "", title: Constant.internetError, action : action )
        }
    }
    
    
    
    // MARK:- BARBUTTONITEM
    private func DesignNavItemBarButton(){
        self.navigationItem.setHidesBackButton(true, animated:false)
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 27, height: 15))
        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        view.addGestureRecognizer(backTap)
        let item1 = UIBarButtonItem(customView: view )
        
        
        
        let btn2 = UIButton(type: .custom)
        btn2.cutomizingButtonNavigation(btnTitle: titleOfSelectedCat ,xAxis:30,yAxis:40)
        btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        let btn3 = UIButton(type: .custom)
        btn3.frame = CGRect(x: 60, y: 40, width: 10, height: 10)
      //  let  btn3Title = (selectedAdType == 0) ? "Normal" : "Auction"
        btn3.cutomizingButtonNavigation(btnTitle: titleOfSelectedSubCate, xAxis: 60, yAxis: 65 )
        btn3.addTarget (self, action: #selector(Action), for: .touchUpInside)
        let item3 = UIBarButtonItem(customView: btn3)
        self.navigationItem.setLeftBarButtonItems([item1,item2, item3 ], animated: true)
    }
    
    @objc private func Action() {
        print("Favourite Tap")
    }
    
    @objc private func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
 
    
    //  MARK:- NAVIGATE
    private func navigation(){
        let parameters = Parameter()
        parameters.dictionary = [
            "client_key": Constant.client_id
            ,"brand_id": idOfSelectedBrand!
        ]
        let serviceObj =  Service(url: Constant.base_url + Constant.eSubBrand, parameters: parameters, method: .post)
        ServiceManager.shared.request(service:serviceObj , model: CategoriesSubBrandStruct.self) { (result, error) in
            if result?.subbrand?.count != 0  && ((result?.subbrand?.isEmpty) == false){
                self.subBrandArray = (result?.subbrand)!
                let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.chooseSubBrands.rawValue) as! VC_ChooseSubBrands
                // VC.idOfSelectedBrand = self.idOfSelectedBrand
                VC.titleOfSelectedCat = self.titleOfSelectedCat
                VC.idOfCategoryToSave = self.idOfSelectedCat
                VC.idOfSubCategoryToSave = self.idOfSelectedSubCat
                VC.idOfSelectedBrand = self.idOfSelectedBrand
                VC.subBrandArray = self.subBrandArray
                self.navigationController?.pushViewController(VC, animated: true)
            }else{
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "VC_SellNowAttribute") as! VC_SellNowAttribute
                VC.idOfCat = self.idOfSelectedCat
                VC.idOfSubCat = self.idOfSelectedSubCat
                VC.idOfBrand = self.idOfSelectedBrand
                VC.titleOfSelectedCat = self.titleOfSelectedCat
                VC.idOfSubBrand = 0
                self.navigationController?.pushViewController(VC, animated: true)
               
            }
        }
    }
}

extension VC_ChooseBrands : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        brandArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseBrandCell", for: indexPath) as! ChooseBrandCell
        let arrData = brandArray[indexPath.row]
        cell.btn.setTitle(arrData.brandtitle ?? "", for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ChooseBrandCell
        let artData = brandArray[indexPath.row]
        self.idOfSelectedBrand = Int(artData.brandid ?? "0")
        print("This is brand id \(artData)")
        print("This is brand id \(idOfSelectedBrand)")
        cell?.isSelected(true)
        navigation()
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ChooseBrandCell
        cell?.isSelected(false)
    }
}

extension VC_ChooseBrands: SkeletonCollectionViewDataSource {
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ChooseBrandCell"
    }
}
