//
//  VC_CotegoriesPage2.swift
//  Simsar
//
//  Created by NxGeN on 11/7/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import SwiftyJSON
import SkeletonView

import UIKit

class VC_CategoriesPage2 : UIViewController {
    
    @IBOutlet var cView: UICollectionView!
    
    var collectionOfProductDetail: [VC_Product] = []
    var globalIndicator : UIViewController?
    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCalledForProduct()
        
        cView.delegate = self
        cView.dataSource = self

        cView.register( AllAdsCollectionViewCell.nib() , forCellWithReuseIdentifier: AllAdsCollectionViewCell.identifier )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    func apiCalledForProduct(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "plan_id"  : 27
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/myshopplansdetail" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_Product()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                           // obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.shortDescription = jsonObj["long_description"].stringValue
                            obj.isFeatured = jsonObj["is_featured"].stringValue
                            obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.longDescription  = jsonObj["long_description"].stringValue
                            obj.price  = jsonObj["price"].stringValue
                            obj.disPrice  = jsonObj["dis_price"].stringValue
                            obj.id   = jsonObj["id"].intValue
                            obj.offerStartDate = jsonObj["offer_start_date"].stringValue
                            obj.offerEndDate = jsonObj["offer_end_date"].stringValue
                            obj.statename = jsonObj["state_id"].stringValue
                            obj.cityname = jsonObj["city_id"].stringValue
                            obj.isCallPrice = jsonObj["is_call_price"].stringValue
                            obj.isOffer = jsonObj["is_offer"].stringValue
                            obj.isAuction = jsonObj["is_auction"].stringValue
                            obj.checkTodAY = jObj["checkToday"].stringValue
                            obj.Wishlist = jsonObj["is_wishlist"].stringValue
                            obj.expiryDate = jsonObj["expiry_date"].stringValue
                            self.collectionOfProductDetail.append(obj)
                            
                        }
                        
                        self.cView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    
}







extension VC_CategoriesPage2 : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout   {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfProductDetail.count
    }
    
    

    //FOR CELL AT ROW
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AllAdsCollectionViewCell.identifier , for: indexPath) as! AllAdsCollectionViewCell
            
            let currentProduct = collectionOfProductDetail[indexPath.row]
            
            if (cell.dayLabel.text == "00") && (cell.hourLabel.text == "00") && (cell.mintLabel.text == "00") {
                cell.timerStackView.isHidden = true
            }else {
                
            }
            
            
            let Auction = currentProduct.isAuction
            if Auction == "1" {
                cell.betButton.isHidden = false
            }else {
                cell.betButton.isHidden = true
            }
            
            
            let check = currentProduct.checkTodAY
            if check == "1" {
                cell.boostButton.isHidden = false
            }else {
                cell.boostButton.isHidden = true
            }
            
            
            let offer = currentProduct.isOffer
            if offer == "1" {
                cell.percentButton.isHidden = false
                
            }else {
                cell.percentButton
                    .isHidden = true
            }
            
            
            let feature = currentProduct.isFeatured
            if feature == "1" {
                cell.featuredLabel.isHidden = false
            }else{
                cell.featuredLabel.isHidden = true
            }
            
            let isCall = currentProduct.isCallPrice
            if isCall == "1" {
                cell.lblKd.text = "Is Call Price"
                cell.lblKDDisc.isHidden = true
                cell.previousPriceLabel.isHidden = true
                cell.productPriceLabel.isHidden = true
            }else {
                cell.productPriceLabel.text = currentProduct.price
                cell.previousPriceLabel.text = currentProduct.disPrice
            }
           
            cell.productTitleLabel.text = currentProduct.adTitle
            cell.productDetailLabel.text = currentProduct.longDescription
            cell.expiryDate = currentProduct.expiryDate
            cell.productAddressLabel.text = currentProduct.statename! ?? "" + " " + currentProduct.cityname!
            cell.lblState.text = currentProduct.cityname
            if(currentProduct.mainImage != nil ){
            
                ServiceManager.shared.downloadImageWithName(constUrlOfWishListCard
                    , currentProduct.mainImage!
                                                            , cell.productImageView)
                
            } else { print (" Main Image is Nill ")}
            
            
            let width = collectionView.frame.width - 40
            cell.contentView.frame.size.width = width / 2
            cell.contentView.frame.size.height = width / 2
            print ("returned cell")
            print("cell Size ==\(cell.frame.width) ++ Height == \(cell.frame.height )")
            //cell.backgroundView!.frame.size.width = width/2
            //cell.backgroundView!.frame.size.height = width / 2
            
            return cell
        
        }
    
    //SIZE OF CELL
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding : CGFloat = 20
            print("Collection View Lower Cell Size")
            let width  = collectionView.frame.width - padding - 10

        return CGSize( width : width/1 , height: 270 )

       }
   
    
    //LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
       }
    
    //INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 5
    }
    
    //EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }

}




class VC_Product {
    
    
    
    
    
    var id: Int?
    var categoryID, postID, userID, subCategoryID: String?
    var brandID, subBrandID, adType, adTitle: String?
    var price, disPrice, disPercentage, isFeatured: String?
    var isBumpupFeatured, isSearchFeatured, isCategoryFeatured, isBrandFeatured: String?
    var isShopFeatured, isHotFeatured, isLatest, isOffer: String?
    var isCallPrice, mapID, isAuction, checkTodAY, Wishlist, likeCount: String?
    var viewCount: String?
    var offerStartDate, offerEndDate, shortDescription: String?
    var longDescription, mainImage, hoverImage, countryID: String?
    var stateID, cityID, latitude, longitude: String?
    var countryIP, sellerName, sellerNumber, isNumber: String?
    var isApprove, statusID, expiryDate, createdAt: String?
    var updatedAt: String?
    var deletedAt: String?
    var subCategory, category, statename, cityname: String?
    
    
}
