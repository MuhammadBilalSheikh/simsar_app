//
//  VC_PrivacyPolicy.swift
//  Simsar
//
//  Created by NxGeN on 11/7/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON



class VC_PrivacyPolicy: UIViewController {
    
    @IBOutlet weak var mainImageView : UIImageView!
    @IBOutlet weak var imageLoader : UIActivityIndicatorView!
    @IBOutlet weak var pageTitle : UILabel!
    @IBOutlet weak var titleDescription : UITextView!
    
    var constUrlToFetchImage = "https://simsar.com/public/images/common-pages/"
    
    var globalIndicator : UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageLoader.isHidden = true
        apiFetchContactUsData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
    
}




extension VC_PrivacyPolicy {

func apiFetchContactUsData(){
            
            
         //   print("fetching Contact Us data")
    startAnimatingWhileLoadingData()
            let apiParams = Parameter()
            apiParams.dictionary = [
             "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
             "page_id"    : 2
         ]
            let serviceObj = Service(url: "https://simsar.com/api/v1/common-pages" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                
             
             if (result != nil ){
                    
                // let decoder = JSONDecoder()
                // print ("result is not nil")
                self.stopAnimationgAsDataLoaded()
                 do {
                 
                     let jObj : JSON = try JSON(data: (result?.data)! )
                     print("\(jObj)")
                    let response = jObj["data"]
                    
                    if jObj["status"].stringValue == "success"  {
                    ServiceManager.shared.downloadImageWithName(
                        self.constUrlToFetchImage
                        , response["image"].stringValue
                        , self.imageLoader
                        , self.mainImageView
                    )
                    
                    DispatchQueue.main.async {
                        self.pageTitle.text = response["title"].stringValue
                        let htmlText = response["content"].stringValue
                        self.titleDescription.attributedText = htmlText.htmlToAttributedString
                    }
                
                    } else {  print("Error in response");   return }
               
                 }catch let error {
                     debugPrint("error== \(error.localizedDescription)")
                 }
                    
                }else {
                    print ("An Error")
                }
                
            }//CLOSURE
            
        }//LoginByUserEmail
    
}
//
//extension VC_PrivacyPolicy : SkeletonCollectionViewDataSource {
//
//    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
//
//        return AllAdsCollectionViewCell.identifier
//
//    }
//
//}
