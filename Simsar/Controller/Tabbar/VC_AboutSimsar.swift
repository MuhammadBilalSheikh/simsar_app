//
//  VC_AboutSimsar.swift
//  Simsar
//
//  Created by NxGeN on 11/4/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class VC_AboutSimsar : UIViewController {
    
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblNum: UILabel!
    @IBOutlet weak var mainImageView : UIImageView!
    @IBOutlet weak var imageLoader : UIActivityIndicatorView!
    @IBOutlet weak var pageTitle : UILabel!
    @IBOutlet weak var titleDescription : UITextView!
    
    var constUrlToFetchImage = "https://simsar.com/public/images/common-pages/"
    
    var globalIndicator : UIViewController?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.setBackNavButton()
        imageLoader.isHidden =  true
        self.tabBarController?.tabBar.isHidden = true
        apiFetchContactUsData()
    }
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
}


extension VC_AboutSimsar {

func apiFetchContactUsData(){
    
           startAnimatingWhileLoadingData()
    let apiParams = Parameter()
            apiParams.dictionary = [
             "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
             "page_id"    : 1
         ]
             let serviceObj = Service(url: "https://simsar.com/api/v1/common-pages" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                
             
             if (result != nil ){
                    
               //  let decoder = JSONDecoder()
                self.stopAnimationgAsDataLoaded()
                 do {
                 
                     let jObj : JSON = try JSON(data: (result?.data)! )
                     print("\(jObj)")
                    let response = jObj["data"]
                    
                    if jObj["status"].stringValue == "success"  {
                    ServiceManager.shared.downloadImageWithName(
                        self.constUrlToFetchImage
                        , response["image"].stringValue
                        , self.imageLoader
                        , self.mainImageView
                    )
                    
                    DispatchQueue.main.async {
                        self.pageTitle.text = response["title"].stringValue
                        self.lblNum.text = response["reach_us_number"].stringValue
                        self.lblEmail.text = response["reach_us_email"].stringValue
                        
                        let htmlString = response["content"].stringValue
                        self.titleDescription.attributedText = htmlString.htmlToAttributedString
                    }
                
                    } else {  print("Error in response");   return }
                    
                      
                 }catch let error {
                     debugPrint("error== \(error.localizedDescription)")
                 }
                    
                }else {
                    print ("An Error")
                }
                
            }//CLOSURE
            
        }//LoginByUserEmail
}
