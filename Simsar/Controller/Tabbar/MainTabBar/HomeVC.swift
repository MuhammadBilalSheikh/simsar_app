//
//  HomeVC.swift
//  Simsar
//
//  Created by NxGeN on 10/19/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import ImageSlideshow
import SwiftyJSON
import SkeletonView


class HomeVC: UIViewController {
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet var slideImageView: ImageSlideshow!
    @IBOutlet var sliderHeight: NSLayoutConstraint!
    @IBOutlet var collectionView: UICollectionView!
    
    
    let constUrl = "https://simsar.com/public/asset/images/sell-now/"
   //https://simsar.com/public/assets/media/applicationslider/15973164011594191241.png FORSlider URL
    let constUrlForBanner = "https://simsar.com/public/assets/media/buybannersplans/"

    var dataToBeShownInCells : [AdsApiData]?
    var countOfMainCellInCV = 0
    var countOfBannerAdsInCV = 0
    var collectionOfHomeAds : [AdsApiData] = []
    var collectionOfFeaturedAdds : [AdsApiData] = []
    var collectionOfOfferedAdds : [AdsApiData] = []
    var collectionOfLatestAdds : [AdsApiData] = []
    var bumpupCell : VC_GlobalBumpup!
    var collectionOfBanners : [VC_Home_BannerClass ] = []
    var sliderData:[slidesData]?
    
    var viewModel = HomeViewModel()
    @IBOutlet weak var cvIndicator : UIActivityIndicatorView!
    
    var globalIndicator : UIViewController?
    var adTypeToBeShown : AdTypes = .homeAds
    
    enum AdTypes {
        case homeAds
        case featuredAds
        case offeredAds
        case latestAds
    }
    
    var timer = Timer()
    var counter = 0
    var models = [Model]()
    var isVisible = true
    let sectionText = UILabel()
    var buttonTitle = "Featured Ads."
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        //MODELS
//        models.append(Model(text: "Dakota Johnson", imageName: "Dakota Johnson"))
//        models.append(Model(text: "Angelina Jolie", imageName: "Angelina Jolie"))
//        models.append(Model(text: "Anne Hathaway", imageName: "Anne Hathaway"))
       
        //setTableView()
        setCollecionView()
        self.tabBarController?.tabBar.isHidden = false
        callApiForHome()
        callApiForBannerSlots()
        callApiForSliderImgaes()
        //disableCV()//For Loading data From Server ....
        setupUI()
        //
        self.bumpupCell = createGlobalBumpup()
        self.bumpupCell.fetchBannerdata(.homePage , nil )
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openPopUpSlider()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        isVisible = true
        self.tabBarController?.tabBar.isHidden = false
        //tableView.reloadData()
    }
    
    func disableCV(){
        self.collectionView.isScrollEnabled = false
        self.cvIndicator.isHidden = false
        self.cvIndicator.startAnimating()
    }
    
    func enableCV(){
        self.collectionView.isScrollEnabled = true
        self.cvIndicator.isHidden = true
        self.cvIndicator.stopAnimating()
    }
    
    
    func startAnimatingWhileLoadingData(){
        showGlobalIndicator(globalIndicator , self )
        print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
    }
    
    func stopAnimationgAsDataLoaded(){
        //self.stopGlobalIndicator( self.globalIndicator!  )
        print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        //self.bumpupCell.showBumpup( self )
    }
    
    
    func isCellFeatured(data: AdsApiData) -> Bool {
        //var counter = 0
        //print ( "is Cell featureed" )
        let arrayOfVals = [data.isFeatured , data.isBumpupFeatured , data.isSearchFeatured , data.isCategoryFeatured , data.isBrandFeatured
                           ,data.isShopFeatured, data.isHotFeatured]
        //print ("arrayOfVar Count == \(arrayOfVals.count) // ")
        for val in 0...arrayOfVals.count {
            
            //print ( "For loop of is cell featured \( val ) for counter ==  \( counter )" )
            //counter += 1
            if  ( val < arrayOfVals.count && arrayOfVals[val] != nil && arrayOfVals[val]! == "1" ){
                //print ("value of ==  \(val)")
                return true
            }
        }
        //print ("out of for loop ")
        return false
        
    }
    
    func isWishlisted(data: AdsApiData) -> Bool {
        if ( data.isWishlist != nil && data.isWishlist == 1 ) {
            return true
        }else { return false }
        
    }
    
    //MARK:-Api For Banner Slots
    func callApiForBannerSlots(){
        
        print("API CALLED For Banners")
        
        startAnimatingWhileLoadingData()
        
        let apiParams = Parameter()
        
        if (Model_UserStatus.shared.isUserIdThere()){
            
            apiParams.dictionary = ["client_key" : Model_UserStatus.shared.clientKey , "user_id" : Model_UserStatus.shared.getUserId() ]
            
        } else {
            apiParams.dictionary = ["client_key" : Model_UserStatus.shared.clientKey ]
        }
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/banner-slot" , parameters: apiParams )
        
        serviceObj.headers = Constant.default_header
        
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
                self.stopAnimationgAsDataLoaded()
                
                
                
                do {
                    
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    
                    if(jObj["status"] == "success"){
                        
                        let slotA = jObj["slotA"]
                        let slotABanner = VC_Home_BannerClass()
                        slotABanner.mainImage = slotA["banner_img"].stringValue
                        self.collectionOfBanners.append(slotABanner)
                        print("slotA ==== \(slotA)" )
                        let slotB = jObj["slotB"]
                        let slotBBanner = VC_Home_BannerClass()
                        slotBBanner.mainImage = slotB["banner_img"].stringValue
                        self.collectionOfBanners.append(slotBBanner)
                        let slotC = jObj["slotC"]
                        let slotCBanner = VC_Home_BannerClass()
                        slotCBanner.mainImage = slotC["banner_img"].stringValue
                        self.collectionOfBanners.append(slotCBanner)
                        let slotD = jObj["slotD"]
                        let slotDBanner = VC_Home_BannerClass()
                        slotDBanner.mainImage = slotD["defaultbanner"].stringValue
                        self.collectionOfBanners.append(slotDBanner)
                        let slotE = jObj["slotE"]
                        let slotEBanner = VC_Home_BannerClass()
                        slotEBanner.defaultBanner = slotE["defaultbanner"].stringValue
                        self.collectionOfBanners.append(slotEBanner)
                        let slotF = jObj["slotF"]
                        let slotFBanner = VC_Home_BannerClass()
                        slotFBanner.mainImage = slotF["defaultbanner"].stringValue
                        self.collectionOfBanners.append(slotFBanner)
                        
                        let slotG = jObj["slotG"]
                        let slotGBanner = VC_Home_BannerClass()
                        slotGBanner.mainImage = slotG["defaultbanner"].stringValue
                        self.collectionOfBanners.append(slotGBanner)
                        
                        let slotH = jObj["slotH"]
                        let slotHBanner = VC_Home_BannerClass()
                        slotHBanner.mainImage = slotH["defaultbanner"].stringValue
                        self.collectionOfBanners.append(slotHBanner)
                        
                        let slotI = jObj["slotI"]
                        let slotIBanner = VC_Home_BannerClass()
                        slotIBanner.mainImage = slotI["defaultbanner"].stringValue
                        self.collectionOfBanners.append(slotIBanner)
                        //let slotC = lObj["slotj"]
                        //                           let slotC = lObj["slotC"]
                        //                           let slotC = lObj["slotC"]
                    }else {
                        print(" \(jObj["status"]) ")
                    }
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    
    func openPopUpSlider() {
        let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "PopUpHomeSLider" )
        self.addChild( ppWithNumber! )
        guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
        vc.view.frame = self.view.frame
        self.view.addSubview( vc.view )
        
        vc.didMove(toParent: self )
     
    }

    //MARK:-  Api For Latest Ads
    func callApiForLatestAds(){
        print("Featured API CALLED")
         self.viewModel.getAds(urlExtention: Constant.eLatestAds) { (success,adsData, error) in
             if success{
               //  data = self.adsData ?? []
                 self.collectionOfLatestAdds = adsData //(result?.data)!
                 self.enableCV()
                 self.collectionView.reloadData()
                 
             }else{
                 print ("An Error")
             }
         }
    }
    
    //MARK:- Api For Offered Ads
    //    https://simsar.com/testsimsar/api/v1/offered_ads
    
    func callApiForOfferedAds(){
        print("Featured API CALLED")
         self.viewModel.getAds(urlExtention: Constant.eOfferedAds) { (success,adsData, error) in
             if success{
               //  data = self.adsData ?? []
                 self.collectionOfOfferedAdds = adsData //(result?.data)!
                 self.enableCV()
                 self.collectionView.reloadData()
                 
             }else{
                 print ("An Error")
             }
         }
   }
    
    //MARK:- Api For featured Ads
    //SAME FOR GUEST AND LOGINNED USER..
    func callApiForFeaturedAds(){
        print("Featured API CALLED")
         self.viewModel.getAds(urlExtention: Constant.eFeaturedAds) { (success,adsData, error) in
             if success{
               //  data = self.adsData ?? []
                 self.collectionOfFeaturedAdds = adsData //(result?.data)!
                 self.enableCV()
                 self.collectionView.reloadData()
                 
             }else{
                 print ("An Error")
             }
         }
    }
    
    //MARK:- Api for Home
    //USER ID I OPTIONAL -- IF USER LOGGED IN
    //THEN WISHLIST BE COMMING
    //is wishlist or not
    func callApiForHome(){
       print("Home API CALLED")
        self.viewModel.getAds(urlExtention: Constant.eHomeAds) { (success,adsData, error) in
            if success{
              //  data = self.adsData ?? []
                self.collectionOfHomeAds = adsData //(result?.data)!
                self.enableCV()
                self.collectionView.reloadData()
                
            }else{
                print ("An Error")
            }
        }
    }
 
    //MARK: Api for slider Imgages
    func callApiForSliderImgaes(){
        print("Home API CALLED")
         self.viewModel.getSliderData{ (success,slidesData, error) in
             if success{
               //  data = self.adsData ?? []
                 self.sliderData = slidesData //(result?.data)!
                self.setUpSlider(data: self.sliderData ?? [])
//                 self.enableCV()
//                 self.collectionView.reloadData()
                
             }else{
                 print ("An Error")
             }
         }
    }
    // SETING UP SLIDER
    func setUpSlider(data:[slidesData]){
        var imagesArray:[InputSource] = []
        let myGroup = DispatchGroup()
        for index in 0 ... data.count - 1{
            myGroup.enter()

            ServiceManager.shared.downloadImageWithName(Constant.baseUrlImageForSlider  , data[index].slide!) { (image) in
                
                if image != nil{
                    imagesArray.append(ImageSource(image:image!))
                }else{
                    imagesArray.append(ImageSource(image:UIImage(named:"Angelina Jolie")!))
                }
                myGroup.leave()
            }
        }
        myGroup.notify(queue: .main) {
            self.slideImageView.setImageInputs(imagesArray)
            self.slideImageView.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .bottom)
            self.slideImageView.slideshowInterval = 2.0
            self.slideImageView.contentScaleMode = .scaleToFill
         }
    }//SLIDER
    
    func setCollecionView(){
        
        //REGISTRATION OF CELLS
        
        collectionView.register(FeaturedTableViewCell.nib(), forCellWithReuseIdentifier: FeaturedTableViewCell.identifier)
        collectionView.register(CategoryButtonsTableViewCell.nib(), forCellWithReuseIdentifier: CategoryButtonsTableViewCell.identifier)
        collectionView.register(AllAdsCollectionViewCell.nib(), forCellWithReuseIdentifier: AllAdsCollectionViewCell.identifier)
        collectionView.register(addBanner2Class.nib() , forCellWithReuseIdentifier: addBanner2Class.Identifier )
        //REGISTERD - PLACE ADS .
        collectionView.register(PlaceAdsCollectionViewCell.nib() , forCellWithReuseIdentifier: PlaceAdsCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    func setupUI(){
       // setUpSlider(Images: <#[String]#>)
        setUpNavigationTab()
        //        let loaderView = UIView()
        //        loaderView.frame = view.bounds
        //        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.prominent)
        //        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //        blurEffectView.backgroundColor = AppTheme.colorPrimary
        //        blurEffectView.frame = view.bounds
        //        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        loaderView.addSubview(blurEffectView)
        //
        //        let jeremyGif = UIImage.gifImageWithName("loader")
        //        let imageView = UIImageView(image: jeremyGif)
        //        imageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 300, height: 80.0)
        //        imageView.center = self.view.center
        //        loaderView.addSubview(imageView)
        //
        //        self.view.addSubview(loaderView)
        
    }
    

    
    func setUpNavigationTab (){
        let logoImage = UIImage.init(named: "logo")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x: -40, y: 0, width: 100, height: 20)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -25
        navigationItem.leftBarButtonItems = [negativeSpacer, imageItem]
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "sidemenu-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapSideMenu))
        let rightSearchBarButtonItem = UIBarButtonItem(image: UIImage(named: "search-icons.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapSearchButton))
        let rightNotificationBarButtonItem = UIBarButtonItem(image: UIImage(named: "notification-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapNotificationButton))
        
        navigationItem.leftBarButtonItems = [leftBarButtonItem, negativeSpacer, imageItem]
       // navigationItem.rightBarButtonItems = [rightNotificationBarButtonItem, rightSearchBarButtonItem]
        
        
    }// Set up Navigation Tab
    
    func showActivityIndicator(view: UIView, withOpaqueOverlay: Bool) {
        
        // this will be the alignment view for the activity indicator
        var superView: UIView = view
        
        // if we want an opaque overlay, do that work first then put the activity indicator within that view; else just use the passed UIView to center it
        if withOpaqueOverlay {
            let overlay = UIView()
            overlay.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.width, height: view.frame.height)
            overlay.layer.backgroundColor = UIColor.black.cgColor
            overlay.alpha = 0.7
            overlay.tag = 1
            
            overlay.center = superView.center
            overlay.isHidden = false
            superView.addSubview(overlay)
            superView.bringSubviewToFront(overlay)
            
            // now we'll work on adding the indicator to the overlay (now superView)
            superView = overlay
        }
        
        let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
        
        indicator.center = superView.center
        indicator.tag = 2
        indicator.isHidden = false
        
        superView.addSubview(indicator)
        superView.bringSubviewToFront(indicator)
        
        indicator.startAnimating()
        
        // also indicate network activity in the status bar
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    @objc func didTapSearchButton() {
        print("Search Tap")
        openVC("VC_Search")
    }
    
    //MARK: NotificationButton
    @objc func didTapNotificationButton() {
        print("Notification Tap")
        //openVC("VC_AddType") //Error in Story Board id
        openVC("VC_Notifications")
    }
    
    @objc func didTapSideMenu() {
        openVC("VC_SideMenu")
    }
    
    func handleCategoryButton(index: Int){
        if index == 1 {
            
            isVisible = false
            buttonTitle = "Featured Ads."
            self.adTypeToBeShown = .featuredAds
            callApiForFeaturedAds()
            
            
        } else if index == 2{
            
            isVisible = false
            buttonTitle = "Offered Ads."
            self.adTypeToBeShown = .offeredAds
            callApiForOfferedAds()
            
            
            
        } else if index == 3{
            
            isVisible = false
            buttonTitle = "Latest Ads."
            self.adTypeToBeShown = .latestAds
            callApiForLatestAds()
            
        } else if index == 4{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier: "VC_FilterHome")
            vc.modalPresentationStyle = .formSheet
            self.present( vc , animated: true , completion: nil )
            
        } else {
            isVisible = true
            buttonTitle = "Featured Ads."
            //self.tableView.reloadData()
            self.collectionView.reloadData()
        }
    }
    
}


//MARK:- Collection View
//--> Collection View
extension HomeVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func addBannerAdsInNumberOfItems(_ count : Int ) -> Int{
        if ( self.collectionOfBanners != nil ){
            return count + self.collectionOfBanners.count
        }else {
            return count
        }
    }
    
 
    func setDataInDataVarForCell(){
        switch (self.adTypeToBeShown){
        case .featuredAds:
            dataToBeShownInCells = []
            dataToBeShownInCells = collectionOfFeaturedAdds
            print("Showing Featured Ads")
        case .homeAds    :
            dataToBeShownInCells = []
            dataToBeShownInCells = collectionOfHomeAds
            print("Showing Home Ads")
        case .latestAds  :
            dataToBeShownInCells = []
            dataToBeShownInCells = collectionOfLatestAdds
            print("Showing Latest Ads")
        case .offeredAds :
            dataToBeShownInCells = []
            dataToBeShownInCells = collectionOfOfferedAdds
            print("Showing Offered")
        }
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
  
    //Number Of Items
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (section == 1){
            self.countOfMainCellInCV = 0
            self.countOfBannerAdsInCV = 0
            setDataInDataVarForCell()
            
            switch (self.adTypeToBeShown){
            case .featuredAds:
                print("retturned No Of Featured Adds == \( collectionOfFeaturedAdds.count )")
                return collectionOfFeaturedAdds.count //addBannerAdsInNumberOfItems( collectionOfFeaturedAdds.count )
            case .homeAds    :
                print("retturned No Of Home Adds")
                return  collectionOfHomeAds.count
            case .latestAds  :
                print("retturned No Of Latest Adds")
                return  collectionOfLatestAdds.count
            case .offeredAds :
                print("retturned No Of Offered Adds")
                return  collectionOfOfferedAdds.count
            }
        }else {
            return 3
        }
    }
    
    
    //CELLS AT RAW
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //HORIZONTAL
        if (indexPath.section == 0) {
            if (indexPath.row == 0){
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CellHomeHeader
                
                cell.title.text = self.buttonTitle
                
                return cell
                
            } else if (indexPath.row == 1){
                let horizontalCell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedTableViewCell.identifier, for: indexPath) as! FeaturedTableViewCell
                
                horizontalCell.delegate = self
                
                horizontalCell.configure(with:collectionOfHomeAds )
                
                return horizontalCell
                //CHIPS
            }else {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryButtonsTableViewCell", for: indexPath) as! CategoryButtonsTableViewCell
                cell.delegate = self
                return cell
                
            }
        }  else if (indexPath.section == 1 ){
            
            //CELL -> (PLACE YOUR ADD HERE )
//            if indexPath.row == 6  || indexPath.row == 10  || indexPath.row == 18 {
//                //NOT USING THIS
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlaceAdsCollectionViewCell.identifier, for: indexPath ) as! PlaceAdsCollectionViewCell
//                cell.clickDelegate = self
//                return cell
//            }
            
   /*         //CELL -> AddBanner cell 2
            if (indexPath.row == 1 || indexPath.row == 16 ){
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addBanner2Class.Identifier , for: indexPath ) as! addBanner2Class
                
                if (countOfBannerAdsInCV < self.collectionOfBanners.count ){
                    
                    if (collectionOfBanners[countOfBannerAdsInCV].getImageString() != nil ){
                        ServiceManager.shared.downloadImageWithName(constUrlForBanner
                                                                    , collectionOfBanners[countOfBannerAdsInCV].getImageString()!
                                                                    , cell.imageIndicator
                                                                    , cell.imageView)
                    }
                    
                    countOfBannerAdsInCV = countOfBannerAdsInCV + 1
                }else {
                    print(" countcountOfBannerAdsInCV :  \(countOfBannerAdsInCV) == collectionOfBanners.count : \(collectionOfBanners.count)")
                }
                
                return cell
                
            } */
            
            //MAIN CELL
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AllAdsCollectionViewCell.identifier, for: indexPath) as! AllAdsCollectionViewCell
            
       //     if (self.countOfMainCellInCV < dataToBeShownInCells?.count ?? 0){
            countOfMainCellInCV = indexPath.row
                //IF ANY OF THIS IS 1 THEN Featured Tag Will e Shown
                
                ///IF USER LOGGED IN THEN
                //IS WISHLIST BE SHOWN .

                ServiceManager.shared.downloadImageWithName(Constant.baserUrlImage , dataToBeShownInCells![self.countOfMainCellInCV].mainImage! , cell.activityIndicator , cell.productImageView )
                print(Constant.baserUrlImage + dataToBeShownInCells![self.countOfMainCellInCV].mainImage!)
                
            
            let feature = dataToBeShownInCells?[self.countOfMainCellInCV].isFeatured
            if feature == "1" {
                cell.featuredLabel.isHidden = false
            }else{
                cell.featuredLabel.isHidden = true
            }
            
            
            let isCall = dataToBeShownInCells?[self.countOfMainCellInCV].isCallPrice
            if isCall == "1" {
                cell.productPriceLabel.text = "Is Call Price"
                cell.previousPriceLabel.isHidden = true
                cell.percentButton.isHidden = true
                cell.lblKd.isHidden = true
                cell.lblKDDisc.isHidden = true
               
            }else {
                cell.productPriceLabel.text = dataToBeShownInCells?[self.countOfMainCellInCV].price
                cell.previousPriceLabel.text = dataToBeShownInCells?[self.countOfMainCellInCV].disPrice
                cell.percentButton.isHidden = false
                cell.lblKd.isHidden = false
                cell.lblKDDisc.isHidden = false
               
                
            }
            
            
            
            let Auction = dataToBeShownInCells?[self.countOfMainCellInCV].isAuction
            if Auction == "1" {
                cell.betButton.isHidden = false
            }else {
                cell.betButton.isHidden = true
            }
            
            
            let check = dataToBeShownInCells?[self.countOfMainCellInCV].checkFor
            if check == "1" {
                cell.boostButton.isHidden = false
            }else {
                cell.boostButton.isHidden = true
            }
            
            
            let offer = dataToBeShownInCells?[self.countOfMainCellInCV].isOffer
            if offer == "1" {
                cell.percentButton.isHidden = false
                
            }else {
                cell.percentButton
                    .isHidden = true
            }
            cell.productAddressLabel.text = dataToBeShownInCells?[self.countOfMainCellInCV].cityname
            cell.lblState.text = dataToBeShownInCells?[self.countOfMainCellInCV].statename
            cell.remainingTimeLabel.text = dataToBeShownInCells?[self.countOfMainCellInCV].createddate  
                cell.productTitleLabel.text = dataToBeShownInCells?[ self.countOfMainCellInCV].adTitle
               // cell.productPriceLabel.text = dataToBeShownInCells?[self.countOfMainCellInCV].price
                cell.idUsedInServer = dataToBeShownInCells?[self.countOfMainCellInCV ].id
                cell.expiryDate = dataToBeShownInCells?[self.countOfMainCellInCV ].expiryDate
            cell.productDetailLabel.text = dataToBeShownInCells?[self.countOfMainCellInCV ].longDescription
                //cell.isFeatured(isCellFeatured(data: dataToBeShownInCells![self.countOfMainCellInCV])).
                //cell.isWishListed( isWishlisted(data: dataToBeShownInCells![self.countOfMainCellInCV]))
           // cell.previousPriceLabel.text = dataToBeShownInCells?[self.countOfMainCellInCV].disPrice
                
               // self.countOfMainCellInCV = self.countOfMainCellInCV + 1
//            } else {
//                print(" countcountOfBannerAdsInCV :  \(countOfMainCellInCV) == collectionOfBanners.count : \(dataToBeShownInCells?.count)")
//            }
            return cell
        }//ELSE
        //EXTRAVAGANT WONT GONNA RUN
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AllAdsCollectionViewCell.identifier, for: indexPath) as! AllAdsCollectionViewCell
        
        return cell
    }
    
    //SIZE FOR ITEM AT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cvSize = collectionView.frame.width
        let paddedWidth = cvSize - 30
        if (indexPath.section == 0 ) {
            
            if (indexPath.row == 1 )
            
            {
                if isVisible {
                    return CGSize(width: cvSize , height: 300 )
                } else {
                    
                    return CGSize(width: cvSize , height: 0 )
                    
                    //return CGSize(width: cvSize , height: 0 )
                }
                
            }
            else if (indexPath.row == 2 )
            {
                return CGSize(width: cvSize , height: 50 )
            }else if (indexPath.row == 0 )
            {
                return CGSize(width: cvSize , height: 30 )
            }
            
        } else if (indexPath.section ==  1){
            return CGSize(width: paddedWidth/2, height: 212)
        }
        else {
            return CGSize (width: 50 , height: 50 )
        }
        return CGSize (width: 50 , height: 50 )
    }//SIZE FOR ITEM
    
    
    //EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if (section == 0 ){
            return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        }else {
            return UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        }
        
    }//EDGE INSETS
    
    //LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if (section == 1){
            return 15
        }else {
            return 0
        }
        
    }//LINE SPACING
    
    //INTER ITEM SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if (section == 1){
            return 10
        }else {
            return 0
        }
    }//INTER ITEM SPACING
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 6  || indexPath.row == 10  || indexPath.row == 18{
            
        } else if indexPath.section == 1{
            
            didTapAd(with: dataToBeShownInCells![indexPath.row])
        }
    }
}

//ENDS
//MARK:- Collection View Ends


//MARK:- SKELTON VIEW DELEGATE
extension HomeVC : SkeletonCollectionViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return AllAdsCollectionViewCell.identifier
    }
    
}

//HORIZONTAL TABLE VIEW
extension HomeVC: FeaturedTableViewCellDelegate{
   
    func didTapAd(with model: AdsApiData) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProductDetailVC") as? ProductDetailVC
        vc?.productId = model.id!
        print("ID**ID**ID \(model.id!)")
        self.navigationController?.pushViewController(vc!, animated: true)
    }

}

//CATEGORY BUTTONS
extension HomeVC: CategoryButtonsTableViewCellDelegate{
    
    func didTapButton(with index: Int) {
        self.handleCategoryButton(index: index)
    }
    
}


//MARK: PlaceAdDelegate -> CALLED WHEN CLICKED ON PLACE YOUR AD HERE
extension HomeVC : PlaceAdsCellDelegate {
    //USED_ON PLACE_AD_HERE
    func itemClicked() {
        self.openVC("VC_AddType")
    }
}

struct Model {
    let text: String
    let imageName: String
    
    init(text: String, imageName: String) {
        self.text = text
        self.imageName = imageName
    }
}




//
//class VC_Home_AddCell {
//
//
//    var id: Int?
//    var categoryID, postID, userID, subCategoryID: String?
//    var brandID, subBrandID, adType, adTitle: String?
//    var price, disPrice, disPercentage, isFeatured: String?
//    var isBumpupFeatured, isSearchFeatured, isCategoryFeatured, isBrandFeatured: String?
//    var isShopFeatured, isHotFeatured, isLatest, isOffer: String?
//    var isCallPrice, mapID, isAuction, likeCount: String?
//    var viewCount, offerStartDate, offerEndDate: String?
//    var shortDescription: String?
//    var longDescription, mainImage, hoverImage, countryID: String?
//    var stateID, cityID, latitude, longitude: String?
//    var countryIP, sellerName, sellerNumber, isNumber: String?
//    var isApprove, statusID, expiryDate, createdAt: String?
//    var updatedAt: String?
//    var deletedAt: String?
//    var subCategory, category, statename, cityname: String?
//    var isWishlist: String?
//
//
//    func isCellFeatured() -> Bool {
//
//        //var counter = 0
//        //print ( "is Cell featureed" )
//        let arrayOfVals = [self.isFeatured , self.isBumpupFeatured , self.isSearchFeatured , self.isCategoryFeatured , self.isBrandFeatured
//            ,self.isShopFeatured, self.isHotFeatured]
//        //print ("arrayOfVar Count == \(arrayOfVals.count) // ")
//        for val in 0...arrayOfVals.count {
//
//            //print ( "For loop of is cell featured \( val ) for counter ==  \( counter )" )
//            //counter += 1
//            if  ( val < arrayOfVals.count && arrayOfVals[val] != nil && arrayOfVals[val]! == "1" ){
//                //print ("value of ==  \(val)")
//                return true
//            }
//        }
//        //print ("out of for loop ")
//        return false
//
//    }
//
//    func isWishlisted() -> Bool {
//
//        if ( self.isWishlist != nil && self.isWishlist == "1" ) {
//            return true
//        }else { return false }
//
//    }
//
//}




class VC_Home_BannerClass{
    
    var id: Int?
    var categoryID, postID, userID, subCategoryID: String?
    var brandID, subBrandID, adType, adTitle: String?
    var price, disPrice, disPercentage, isFeatured: String?
    var isBumpupFeatured, isSearchFeatured, isCategoryFeatured, isBrandFeatured: String?
    var isShopFeatured, isHotFeatured, isLatest, isOffer: String?
    var isCallPrice, mapID, isAuction, likeCount: String?
    var viewCount, offerStartDate, offerEndDate: String?
    var shortDescription: String?
    var longDescription, mainImage, hoverImage, countryID: String?
    var stateID, cityID, latitude, longitude: String?
    var countryIP, sellerName, sellerNumber, isNumber: String?
    var isApprove, statusID, expiryDate, createdAt: String?
    var updatedAt: String?
    var deletedAt: String?
    var subCategory, category, statename, cityname: String?
    var isWishlist: Int?
    var defaultBanner : String?
    
    init () {
        
    }
    
    func getImageString () -> String? {
        if self.mainImage != nil {
            return mainImage
        }else if self.defaultBanner != nil {
            return  defaultBanner
        }else { return nil }
    }
}
