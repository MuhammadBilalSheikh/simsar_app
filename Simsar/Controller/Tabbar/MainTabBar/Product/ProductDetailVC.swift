//
//  ProductDetailVC.swift
//  Simsar
//
//  Created by NxGeN on 10/20/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import GoogleMaps
import ImageSlideshow
import SwiftyJSON



class related_Ads {
    var id: Int?
    var title, mainImg, timeLbl, desc, price, disPrice, state, address, isCallPrice, isOffer, isAuction, expiryDate, isCheck, Wishlist, isFeature : String?

}



class ProductDetailVC: UIViewController {
    
    @IBOutlet var attValueFirst: UILabel!
    @IBOutlet var attributeFirst: UILabel!
    @IBOutlet var likesLbl: UILabel!
    @IBOutlet var sharesLbl: UILabel!
    @IBOutlet var mainSliderView: UIView!
    @IBOutlet var bidStackView: UIStackView!
    @IBOutlet var slideImageView: ImageSlideshow!
    @IBOutlet var sliderView: UIView!
    @IBOutlet var detailView: UIView!
    @IBOutlet var productTitleLabel: UILabel!
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var activeSignLabel: UILabel!
    @IBOutlet var shortDescLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var timeAgoLabel: UILabel!
    @IBOutlet var bidView: UIView!
    @IBOutlet var bidStatButton: UIButton!
    @IBOutlet var bidStatView: UIView!
    @IBOutlet var topBidderButton: UIButton!
    @IBOutlet var topBidderView: UIView!
    @IBOutlet var bidStatsContainerView: UIView!
    @IBOutlet var topBidderContainerView: UIView!
    @IBOutlet var bannerImageView: UIImageView!
    @IBOutlet var callButton: UIButton!
    @IBOutlet var smsButton: UIButton!
    @IBOutlet var safePayButton: UIButton!
    @IBOutlet var emailButton: UIButton!
    @IBOutlet var ChatButton: UIButton!
    @IBOutlet var detailDescLabel: UILabel!
    @IBOutlet var detailDescView: UIView!
    @IBOutlet var sellerDetailView: UIView!
    @IBOutlet var sellerNameLabel: UILabel!
    @IBOutlet var sellerTimeAgoLabel: UILabel!
    @IBOutlet var sellerImageView: UIImageView!
    @IBOutlet var viewProfileButton: UIButton!
    @IBOutlet var sellerAddressLabel: UILabel!
    @IBOutlet var relatedAdsCollectionView: UICollectionView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var discountPriceLabel: UILabel!
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var mintLabel: UILabel!
    @IBOutlet var secLabel: UILabel!
    @IBOutlet var remainingTimeLabel: UILabel!
    @IBOutlet var timerView: UIView!
    
    var productId:Int = 0
    var lat: String!
    var lon: String!
    var arrForApi: [related_Ads] = []
    var constUrlOfRelatedAds = "https://simsar.com/public/asset/images/sell-now/"
    var globalIndicator : UIViewController?
    var constUrlToFetchImage = "https://simsar.com/public/assets/media/contact/banner/"
    var counter = 0
    private let location = LocationManager.shared
    private var pickupMarker = GMSMarker()
    var locationMarker: GMSMarker!
    var relatedAdsCount = 5
    var sliderImagesArray:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bidStackView.isHidden = true
        
        self.sellerAddressLabel.isHidden = true
       
      
        
        relatedAdsCollectionView.delegate = self
        relatedAdsCollectionView.dataSource = self
        
        topBidderContainerView.isHidden = true
        
        relatedAdsCollectionView.register(FeaturedCollectionViewCell.nib(), forCellWithReuseIdentifier: FeaturedCollectionViewCell.identifier)
        relatedAdsCollectionView.register(PlaceAdsCollectionViewCell.nib(), forCellWithReuseIdentifier: PlaceAdsCollectionViewCell.identifier)
        
        setupUI()
        configureMapView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callApiForProductDetail()
        callApiForProductUses()
        callApiForProductAttributes()
        relatedAdsApi()
    }
    
    func startAnimatingWhileLoadingData(){
       
//        if (globalIndicator == nil ) {
//            globalIndicator = createGlobalIndicator()
//        }
        showGlobalIndicator(globalIndicator , self )
        
        print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
    
    }
    
func stopAnimationgAsDataLoaded(){
        
    //self.stopGlobalIndicator( self.globalIndicator!  )
    print ("stop Animating While Loading ")
    self.view.stopSkeletonAnimation()
    self.view.hideSkeleton()
        
    //self.bumpupCell.showBumpup( self )
        
}

    
    func mapRelocate(lat:Double,long:Double){
        
        var location =  CLLocationCoordinate2D()
        location.latitude = lat
        location.longitude = long
        mapView.animate(toLocation: location)
        self.setuplocationMarker(coordinate: location)
        mapView.animate(toZoom: 20)

        
    }
    
    
    func callApiForProductDetail(){
            
            
            print("fetching Contact Us data")
            var apiParams = Parameter()
            apiParams.dictionary = [
             "product_id" : productId
         ]
        print("ProductDetailPARAMS**** \(apiParams.dictionary)")
            var serviceObj = Service(url: "https://simsar.com/api/v1/productdetail" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                
             
             if (result != nil ){
                    
                 let decoder = JSONDecoder()
                 print ("result is not nil")
                 do {
                 
                     let jObj : JSON = try JSON(data: (result?.data)! )
                     print("\(jObj)")
                    let response = jObj["sellnow"]
                    
                    if jObj["status"].stringValue == "success"  {
//                    ServiceManager.shared.downloadImageWithName(
//                        self.constUrlToFetchImage
//                        , response["banner"].stringValue
//                        , self.
//                    )
                    
                        DispatchQueue.main.async { [self] in
                        self.productTitleLabel.text = response["ad_title"].stringValue
                        self.productPriceLabel.text = response["price"].stringValue
                        self.discountPriceLabel.text = response["dis_price"].stringValue
                        self.remainingTimeLabel.text = response["created_date"].stringValue
                        self.shortDescLabel.text = response["short_description"].stringValue
                            self.addressLabel.text = response["statename"].stringValue + " " + response["cityname"].stringValue
                        self.timeAgoLabel.text = response["created_date"].stringValue
                            self.sharesLbl.text = ("Shares \(response["view_count"])")
                        self.likesLbl.text = ("Likes \(response["like_count"])")
                        self.detailDescLabel.text = response["long_description"].stringValue
                        self.detailDescLabel.text = response["long_description"].stringValue
                        self.sliderImagesArray.append( response["main_image"].stringValue)
                        self.sliderImagesArray.append( response["hover_image"].stringValue)
                            let gallery:[Any] =  response["sellnowgallery"].arrayObject ?? []
                            for image in gallery {
                                self.sliderImagesArray.append(image as! String)
                            }

                        self.lat = response["latitude"].stringValue
                        self.lon = response["longitude"].stringValue
                            self.sellerAddressLabel.text = response["statename"].stringValue + " " + response["cityname"].stringValue
                
                            print("Address ********* \(self.addressLabel)")
                        
                        self.mapRelocate(lat: Double(self.lat)!, long: Double(self.lon)!)
                        self.sliderViewSetup()
                       

                    }
                
                    } else {  print("Error in response");   return }
    
                    
                    
                 }catch let error {
                     debugPrint("error== \(error.localizedDescription)")
                 }
                    
                }else {
                    print ("An Error")
                }
                
            }//CLOSURE
            
        }//LoginByUserEmail
    
    
    
    
    
    func callApiForProductUses(){
            
            
            print("fetching Contact Us data")
            var apiParams = Parameter()
            apiParams.dictionary = [
             "product_id" : productId
         ]
            var serviceObj = Service(url: "https://simsar.com/api/v1/productdetail" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                
             
             if (result != nil ){
                    
                 let decoder = JSONDecoder()
                 print ("result is not nil")
                 do {
                 
                     let jObj : JSON = try JSON(data: (result?.data)! )
                    // print("\(jObj)")
                    let response = jObj["productuser"]
                    
                    if jObj["status"].stringValue == "success"  {
//                    ServiceManager.shared.downloadImageWithName(
//                        self.constUrlToFetchImage
//                        , response["banner"].stringValue
//                        , self.
//                    )
                        
                    DispatchQueue.main.async {
                        self.sellerNameLabel.text = response["name"].stringValue
                   //     self.sellerImageView.image = response["avatar"]
                        self.sellerTimeAgoLabel.text = response["created_at"].stringValue
                        
                        
                        
                       

                    }
                
                    } else {  print("Error in response");   return }
    
                    
                    
                 }catch let error {
                     debugPrint("error== \(error.localizedDescription)")
                 }
                    
                }else {
                    print ("An Error")
                }
                
            }//CLOSURE
            
        }//LoginByUserEmail
    
    
    
    func callApiForProductAttributes(){
        
            var apiParams = Parameter()
            apiParams.dictionary = [
             "product_id" : productId
         ]
            var serviceObj = Service(url: "https://simsar.com/api/v1/productdetail" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                
             
             if (result != nil ){
                    
                 let decoder = JSONDecoder()
                 print ("result is not nil")
                 do {
                 
                     let jObj : JSON = try JSON(data: (result?.data)! )
                  
                    let response = jObj["adAttribute"]
                    print("ATTT*************Responce \(response)")
                    
                    
                    
                    if jObj["status"].stringValue == "success"  {
//                    ServiceManager.shared.downloadImageWithName(
//                        self.constUrlToFetchImage
//                        , response["banner"].stringValue
//                        , self.
//                    )
                        
                        for attributeData in 0..<response.count {
                            let obj = response[attributeData]
                            
                            self.attributeFirst.text = obj["attributesTitle"].stringValue
                            self.attValueFirst.text = obj["propertiesTitle"].stringValue
                            
                        }
                    
                    DispatchQueue.main.async {
                      
                        
                        print("ATributeeeee \(self.attributeFirst)")

                    }
                
                    } else {  print("Error in response");   return }
    
                    
                    
                 }catch let error {
                     debugPrint("error== \(error.localizedDescription)")
                 }
                    
                }else {
                    print ("An Error")
                }
                
            }//CLOSURE
            
        }//LoginByUserEmail
    
    
    func relatedAdsApi(){
            
            print("API CALLED")
            
            if (globalIndicator == nil ) {
                //globalIndicator = self.storyboard?.instantiateViewController(withIdentifier: "VC_CustomIndicator")
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
            //self.stopGlobalIndicator( self.globalIndicator!  )
            
            var apiParams = Parameter()
            apiParams.dictionary = ["product_id"  :  productId // self.userId
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/relatedproductdetail" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let relatedApiData = jObj["sellnow"]
                        print("RealtedAdsData***** \(relatedApiData)")
                       
                        
//
                        
                        for countOfObjectsInData in 0..<relatedApiData.count {
                            print("DataFromAPI********** \(relatedApiData)")
                            
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = related_Ads()
                            
                            let jsonObj = relatedApiData[countOfObjectsInData]
 
                            
                            obj.mainImg = jsonObj["main_image"].stringValue
                            obj.address = jsonObj["cityname"].stringValue + " " + jsonObj["statename"].stringValue
                            obj.desc = jsonObj["long_description"].stringValue
                            obj.title = jsonObj["ad_title"].stringValue
                            obj.price = jsonObj["price"].stringValue
                            obj.disPrice = jsonObj["dis_price"].stringValue
                            obj.timeLbl = jsonObj["created_date"].stringValue
                            obj.isCallPrice = jsonObj["is_call_price"].stringValue
                            obj.isOffer = jsonObj["is_offer"].stringValue
                            obj.isAuction = jsonObj["is_auction"].stringValue
                            obj.isCheck = jObj["checkToday"].stringValue
                            obj.Wishlist = jsonObj["is_wishlist"].stringValue
                            obj.expiryDate = jsonObj["expiry_date"].stringValue
                            obj.isFeature = jsonObj["is_featured"].stringValue
                            
            
                            self.arrForApi.append(obj)
                        
                            
                            
                            //self.related_Ads.append(obj)
                            
                        }
                        
                        self.relatedAdsCollectionView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
            
        }
    
    
    
    private func configureMapView(){
        mapView.isMyLocationEnabled = true
        self.mapView.delegate = self
        // pickup marker
        //        pickupMarker.map = self.mapView
        //        pickupMarker.icon = UIImage(named: "drop-marker")
        
      //  navigateToCurrentLocation()  // (latitude: (self.lat as? Double)!, longitude: (self.lon as? Double)!)
        addLocationNotificationObserver()
    }
    
    private func addLocationNotificationObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCurrentLocation), name: .receivedLocationFirstTime, object: nil)
    }
    
    @objc private func navigateToCurrentLocation(){
        
        if let currentLocation = location.lastReceivedLocation(){
            self.mapView.animate(to: GMSCameraPosition(latitude: currentLocation.latitude , longitude: currentLocation.longitude, zoom: 15.0))
            self.setuplocationMarker(coordinate: currentLocation)
            print(currentLocation.latitude)
            // removing the current location blue icon of google maps
            self.mapView.isMyLocationEnabled = false
            // adding pickup marker to current location if pikcup location is not set yet
            // removing observer because it has got the user location.
            // why removing?
            // Well i guess i can save memory by doing that 😇 , maybe 🙄
            NotificationCenter.default.removeObserver(self, name: .receivedLocationFirstTime, object: nil)
        }
    }
    
    func showLocationAlertView(){
        let alert = UIAlertController(title: NSLocalizedString("Allow Location Access", comment: ""), message: NSLocalizedString("Simsar needs access to your location. Turn on Location Services in your device settings.", comment: ""), preferredStyle: UIAlertController.Style.alert)
        
        // Button to Open Settings
        alert.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setuplocationMarker(coordinate: CLLocationCoordinate2D) {
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
    }
    func sliderViewSetup(){
        
        var imagesArray:[InputSource] = []
        let myGroup = DispatchGroup()
        for image in sliderImagesArray{
            myGroup.enter()

            ServiceManager.shared.downloadImageWithName(Constant.productDetailSlider  , image ) { (downloadedImage) in
                
                if downloadedImage != nil{
                    imagesArray.append(ImageSource(image:downloadedImage!))
                }else{
                    imagesArray.append(ImageSource(image:UIImage(named:"Angelina Jolie")!))
                }
                myGroup.leave()
            }
        }
        myGroup.notify(queue: .main) {
            self.slideImageView.setImageInputs(imagesArray)
            self.slideImageView.pageIndicatorPosition = PageIndicatorPosition(horizontal:.center , vertical: .bottom)
            self.slideImageView.slideshowInterval = 2.0
            self.slideImageView.contentScaleMode = .scaleToFill
         }
    }
    
    func setupUI(){
        
      
        
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapBackButton))
        let rightSearchBarButtonItem = UIBarButtonItem(image: UIImage(named: "share-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapShareMenu))
        let rightNotificationBarButtonItem = UIBarButtonItem(image: UIImage(named: "heart-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapFavouriteButton))
        
        navigationItem.leftBarButtonItem = leftBarButtonItem
       // navigationItem.rightBarButtonItems = [rightNotificationBarButtonItem, rightSearchBarButtonItem]
        
        [sliderView, mainSliderView, detailView].forEach { (view) in
            view?.roundCorners(corners: [.topLeft, .topRight], radius: 25.0)
        }
        
        productTitleLabel.textColor = AppTheme.colorPrimary
        
        [hourLabel, mintLabel, secLabel].forEach { (label) in
            label?.backgroundColor = AppTheme.colorPrimary
        }
        
        [bidView, detailDescView, sellerDetailView].forEach { (view) in
            view?.layer.cornerRadius = 25
            view?.layer.borderColor = UIColor.black.cgColor
            view?.layer.borderWidth = 1
        }
        
        [sellerImageView, hourLabel, mintLabel, secLabel].forEach { (view) in
            view?.makeRound(radius: (0.5 * ((view?.layer.bounds.size.width)!)))
        }
        
        sellerImageView.layer.cornerRadius = 0.5 * (sellerImageView.layer.bounds.size.width)
        sellerImageView.clipsToBounds = true
        
        viewProfileButton.setTitleColor(AppTheme.colorPrimary, for: .normal)
        
        [callButton, smsButton, safePayButton, emailButton, ChatButton, timerView, activeSignLabel].forEach { (button) in
            button?.layer.cornerRadius = 10
            button?.layer.masksToBounds = true
        }
        
        [callButton, smsButton, safePayButton, emailButton, ChatButton].forEach { (button) in
            button?.backgroundColor = AppTheme.colorPrimary
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "Rs 15000")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        discountPriceLabel.attributedText = attributeString
    }
    
    @objc func didTapBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapFavouriteButton() {
        print("Favourite Tap")
    }
    
    @objc func didTapShareMenu() {
        print("Share Tap")
    }
    
    @IBAction func didTapBidStatsButton(_ sender: Any) {
        bidStatsContainerView.isHidden = false
        topBidderContainerView.isHidden = true
        bidStatButton.setTitleColor(AppTheme.colorPrimary, for: .normal)
        bidStatView.backgroundColor = AppTheme.colorPrimary
        topBidderButton.setTitleColor(#colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1), for: .normal)
        topBidderView.backgroundColor = #colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1)
    }
    
    @IBAction func didTapTopBidderButton(_ sender: Any) {
        bidStatsContainerView.isHidden = true
        topBidderContainerView.isHidden = false
        bidStatButton.setTitleColor(#colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1), for: .normal)
        bidStatView.backgroundColor = #colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1)
        topBidderButton.setTitleColor(AppTheme.colorPrimary, for: .normal)
        topBidderView.backgroundColor = AppTheme.colorPrimary
    }
}

extension ProductDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrForApi.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == relatedAdsCount {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlaceAdsCollectionViewCell.identifier, for: indexPath) as! PlaceAdsCollectionViewCell
            
             return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedCollectionViewCell.identifier, for: indexPath) as! FeaturedCollectionViewCell
            
            let currentArr = arrForApi[indexPath.row]
            
            
            let Auction = currentArr.isAuction
            if Auction == "1" {
                cell.betButton .isHidden = false
            }else {
                cell.betButton.isHidden = true
            }
            
            
            let check = currentArr.isCheck
            if check == "1" {
                cell.boostButton.isHidden = false
            }else {
                cell.boostButton.isHidden = true
            }
            
            
            let offer = currentArr.isOffer
            if offer == "1" {
                cell.percentButton.isHidden = false
                
            }else {
                cell.percentButton
                    .isHidden = true
            }
            
            
            let feature = currentArr.isFeature
            if feature == "1" {
                cell.featuredLabel.isHidden = false
            }else{
                cell.featuredLabel.isHidden = true
            }
            
            let isCall = currentArr.isCallPrice
            if isCall == "1" {
                cell.productPriceLabel.text = "Is Call Price"
                cell.previousPriceLabel.isHidden = true
               
            }else {
                cell.productPriceLabel.text = "KD \(currentArr.price)"
                cell.previousPriceLabel.text = "KD \(currentArr.disPrice)"
            }
            
            
            
            
            
            
            cell.productTitleLabel.text = currentArr.title
            cell.productAddressLabel.text = currentArr.address ?? "Sorry" + "" + currentArr.state! ?? "Sorry"
            cell.productDetailLabel.text = currentArr.desc
            cell.productPriceLabel.text = currentArr.disPrice
            cell.remainingTimeLabel.text = currentArr.timeLbl
            cell.previousPriceLabel.text = currentArr.disPrice
            if (currentArr.mainImg != nil) {
                ServiceManager.shared.downloadImageWithName(constUrlOfRelatedAds
                                                            , currentArr.mainImg!
                  , cell.imageIndicator
            , cell.productImageView)
            } else {
                print("Main Image is Nill")
            }
          
            
             return cell
        }
    }
}

extension ProductDetailVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  30
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 270)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}

//MARK:- Map view delegate
extension ProductDetailVC: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        self.mapView.clear()
        
        //            GMSGeocoder().reverseGeocodeCoordinate(marker.position) { (response, error) in
        //                if markerIndex == 0{
        //                    self.pickupLocationTitleLabel.text = response?.results()?.first?.thoroughfare ?? NSLocalizedString(UserDefaultConstants.pinLocation, comment: "")
        //                    self.pickupLocationDescLabel.text = response?.results()?.first?.lines?.first ?? "\(marker.position.latitude),\(marker.position.longitude)"
        //                    MapViewController.addressForCurrentLocation = self.pickupLocationDescLabel.text ?? ""
        //                }else if markerIndex == 1{
        //                    self.dropLocationTitleLabel.text = response?.results()?.first?.thoroughfare ?? NSLocalizedString(UserDefaultConstants.pinLocation, comment: "")
        //                    self.DropLocationDescLabel.text = response?.results()?.first?.lines?.first ?? "\(marker.position.latitude),\(marker.position.longitude)"
        //                    self.viewModel.setDropLocation(location: SearchPlace(name: self.DropLocationDescLabel.text ?? NSLocalizedString(UserDefaultConstants.pinLocation, comment: ""), latitude: marker.position.latitude, longitude: marker.position.longitude))
        //                }
        //            }
    }
}



