//
//  VC_ProductDetail2.swift
//  Simsar
//
//  Created by NxGeN on 11/20/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import GoogleMaps
import ImageSlideshow


class VC_ProductDetail2: UIViewController {

    @IBOutlet var mainSliderView: UIView!
    @IBOutlet var bidStackView: UIStackView!
    @IBOutlet var slideImageView: ImageSlideshow!
    @IBOutlet var sliderView: UIView!
    @IBOutlet var detailView: UIView!
    @IBOutlet var productTitleLabel: UILabel!
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var activeSignLabel: UILabel!
    @IBOutlet var shortDescLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var timeAgoLabel: UILabel!
    @IBOutlet var bidView: UIView!
    @IBOutlet var bidStatButton: UIButton!
    @IBOutlet var bidStatView: UIView!
    @IBOutlet var topBidderButton: UIButton!
    @IBOutlet var topBidderView: UIView!
    @IBOutlet var bidStatsContainerView: UIView!
    @IBOutlet var topBidderContainerView: UIView!
    @IBOutlet var bannerImageView: UIImageView!
    @IBOutlet var callButton: UIButton!
    @IBOutlet var smsButton: UIButton!
    @IBOutlet var safePayButton: UIButton!
    @IBOutlet var emailButton: UIButton!
    @IBOutlet var ChatButton: UIButton!
    @IBOutlet var detailDescLabel: UILabel!
    @IBOutlet var detailDescView: UIView!
    @IBOutlet var sellerDetailView: UIView!
    @IBOutlet var sellerNameLabel: UILabel!
    @IBOutlet var sellerTimeAgoLabel: UILabel!
    @IBOutlet var sellerImageView: UIImageView!
    @IBOutlet var viewProfileButton: UIButton!
    @IBOutlet var sellerAddressLabel: UILabel!
    @IBOutlet var relatedAdsCollectionView: UICollectionView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var discountPriceLabel: UILabel!
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var mintLabel: UILabel!
    @IBOutlet var secLabel: UILabel!
    @IBOutlet var remainingTimeLabel: UILabel!
    @IBOutlet var timerView: UIView!
  
    var counter = 0
    private let location = LocationManager.shared
    private var pickupMarker = GMSMarker()
    var locationMarker: GMSMarker!
    var relatedAdsCount = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        relatedAdsCollectionView.delegate = self
        relatedAdsCollectionView.dataSource = self
        
        topBidderContainerView.isHidden = true
        
        relatedAdsCollectionView.register(FeaturedCollectionViewCell.nib(), forCellWithReuseIdentifier: FeaturedCollectionViewCell.identifier)
        relatedAdsCollectionView.register(PlaceAdsCollectionViewCell.nib(), forCellWithReuseIdentifier: PlaceAdsCollectionViewCell.identifier)
        
        setupUI()
        configureMapView()
    }
    
    private func configureMapView(){
        mapView.isMyLocationEnabled = true
        self.mapView.delegate = self
        // pickup marker
        //        pickupMarker.map = self.mapView
        //        pickupMarker.icon = UIImage(named: "drop-marker")
        
        navigateToCurrentLocation()
        addLocationNotificationObserver()
    }
    
    private func addLocationNotificationObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCurrentLocation), name: .receivedLocationFirstTime, object: nil)
    }
    
    @objc private func navigateToCurrentLocation(){
        
        if let currentLocation = location.lastReceivedLocation(){
            self.mapView.animate(to: GMSCameraPosition(latitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 15.0))
            self.setuplocationMarker(coordinate: currentLocation)
            print(currentLocation.latitude)
            // removing the current location blue icon of google maps
            self.mapView.isMyLocationEnabled = false
            // adding pickup marker to current location if pikcup location is not set yet
            // removing observer because it has got the user location.
            // why removing?
            // Well i guess i can save memory by doing that 😇 , maybe 🙄
            NotificationCenter.default.removeObserver(self, name: .receivedLocationFirstTime, object: nil)
        }
    }
    
    func showLocationAlertView(){
        let alert = UIAlertController(title: NSLocalizedString("Allow Location Access", comment: ""), message: NSLocalizedString("Simsar needs access to your location. Turn on Location Services in your device settings.", comment: ""), preferredStyle: UIAlertController.Style.alert)
        
        // Button to Open Settings
        alert.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setuplocationMarker(coordinate: CLLocationCoordinate2D) {
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
    }
    
    func setupUI(){
        
        slideImageView.setImageInputs([
            ImageSource(image: UIImage(named:"Angelina Jolie")!),
            ImageSource(image: UIImage(named:"Emma Stone")!),
            ImageSource(image: UIImage(named:"Anne Hathaway")!),
            ImageSource(image: UIImage(named:"Dakota Johnson")!)
        ])
        
        slideImageView.pageIndicatorPosition = PageIndicatorPosition(horizontal: .right(padding: 20), vertical: .bottom)
        slideImageView.slideshowInterval = 2.0
        slideImageView.contentScaleMode = .scaleToFill
        
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapBackButton))
        let rightSearchBarButtonItem = UIBarButtonItem(image: UIImage(named: "share-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapShareMenu))
        let rightNotificationBarButtonItem = UIBarButtonItem(image: UIImage(named: "heart-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapFavouriteButton))
        
        navigationItem.leftBarButtonItem = leftBarButtonItem
        navigationItem.rightBarButtonItems = [rightNotificationBarButtonItem, rightSearchBarButtonItem]
        
        [sliderView, mainSliderView, detailView].forEach { (view) in
            view?.roundCorners(corners: [.topLeft, .topRight], radius: 25.0)
        }
        
        productTitleLabel.textColor = AppTheme.colorPrimary
        
        [hourLabel, mintLabel, secLabel].forEach { (label) in
            label?.backgroundColor = AppTheme.colorPrimary
        }
        
        [bidView, detailDescView, sellerDetailView].forEach { (view) in
            view?.layer.cornerRadius = 25
            view?.layer.borderColor = UIColor.black.cgColor
            view?.layer.borderWidth = 1
        }
        
        [sellerImageView, hourLabel, mintLabel, secLabel].forEach { (view) in
            view?.makeRound(radius: (0.5 * ((view?.layer.bounds.size.width)!)))
        }
        
        sellerImageView.layer.cornerRadius = 0.5 * (sellerImageView.layer.bounds.size.width)
        sellerImageView.clipsToBounds = true
        
        viewProfileButton.setTitleColor(AppTheme.colorPrimary, for: .normal)
        
        [callButton, smsButton, safePayButton, emailButton, ChatButton, timerView, activeSignLabel].forEach { (button) in
            button?.layer.cornerRadius = 10
            button?.layer.masksToBounds = true
        }
        
        [callButton, smsButton, safePayButton, emailButton, ChatButton].forEach { (button) in
            button?.backgroundColor = AppTheme.colorPrimary
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "Rs 15000")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        discountPriceLabel.attributedText = attributeString
    }
    
    @objc func didTapBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapFavouriteButton() {
        print("Favourite Tap")
    }
    
    @objc func didTapShareMenu() {
        print("Share Tap")
    }
    
    @IBAction func didTapBidStatsButton(_ sender: Any) {
        bidStatsContainerView.isHidden = false
        topBidderContainerView.isHidden = true
        bidStatButton.setTitleColor(AppTheme.colorPrimary, for: .normal)
        bidStatView.backgroundColor = AppTheme.colorPrimary
        topBidderButton.setTitleColor(#colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1), for: .normal)
        topBidderView.backgroundColor = #colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1)
    }
    
    @IBAction func didTapTopBidderButton(_ sender: Any) {
        bidStatsContainerView.isHidden = true
        topBidderContainerView.isHidden = false
        bidStatButton.setTitleColor(#colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1), for: .normal)
        bidStatView.backgroundColor = #colorLiteral(red: 0.8505099416, green: 0.8505299091, blue: 0.8505191207, alpha: 1)
        topBidderButton.setTitleColor(AppTheme.colorPrimary, for: .normal)
        topBidderView.backgroundColor = AppTheme.colorPrimary
    }
}

extension VC_ProductDetail2 : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  5 + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == relatedAdsCount {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlaceAdsCollectionViewCell.identifier, for: indexPath) as! PlaceAdsCollectionViewCell
            
             return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedCollectionViewCell.identifier, for: indexPath) as! FeaturedCollectionViewCell
            
             return cell
        }
    }
}

extension VC_ProductDetail2 : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  30
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 270)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}

//MARK:- Map view delegate
extension VC_ProductDetail2 : GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        self.mapView.clear()
        
        //            GMSGeocoder().reverseGeocodeCoordinate(marker.position) { (response, error) in
        //                if markerIndex == 0{
        //                    self.pickupLocationTitleLabel.text = response?.results()?.first?.thoroughfare ?? NSLocalizedString(UserDefaultConstants.pinLocation, comment: "")
        //                    self.pickupLocationDescLabel.text = response?.results()?.first?.lines?.first ?? "\(marker.position.latitude),\(marker.position.longitude)"
        //                    MapViewController.addressForCurrentLocation = self.pickupLocationDescLabel.text ?? ""
        //                }else if markerIndex == 1{
        //                    self.dropLocationTitleLabel.text = response?.results()?.first?.thoroughfare ?? NSLocalizedString(UserDefaultConstants.pinLocation, comment: "")
        //                    self.DropLocationDescLabel.text = response?.results()?.first?.lines?.first ?? "\(marker.position.latitude),\(marker.position.longitude)"
        //                    self.viewModel.setDropLocation(location: SearchPlace(name: self.DropLocationDescLabel.text ?? NSLocalizedString(UserDefaultConstants.pinLocation, comment: ""), latitude: marker.position.latitude, longitude: marker.position.longitude))
        //                }
        //            }
    }
}

