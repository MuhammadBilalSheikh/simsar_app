//
//  HomeTabBarController.swift
//  Simsar
//
//  Created by NxGeN on 10/19/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
    
    let button = UIButton.init(type: .custom)
    
   @IBOutlet weak var tabbar: UITabBar!
   
    override func viewDidLoad() {
        super.viewDidLoad()
            
        setupUI()
      
    }

    func setupUI(){
        
        if let newButtonImage = UIImage(named: "home-Add") {
            self.addCenterButton(withImage: newButtonImage, highlightImage: newButtonImage)
        }
    }
    
    
    @objc func handleTouchTabbarCenter(sender : UIButton)
    {
        if let count = self.tabBar.items?.count
        {
            let i = floor(Double(count / 2))
            self.selectedViewController = self.viewControllers?[Int(i)]
        }
    }
    
    func addCenterButton(withImage buttonImage : UIImage, highlightImage: UIImage) {
        
        let paddingBottom : CGFloat = 0.0
        
        let button = UIButton(type: .custom)
        button.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin]
        button.frame = CGRect(x: 0.0, y: 0.0, width: buttonImage.size.width / 1.15, height: buttonImage.size.height / 1.15)
        button.setBackgroundImage(buttonImage, for: .normal)
        button.setBackgroundImage(highlightImage, for: .highlighted)
        
        let rectBoundTabbar = self.tabBar.bounds
        let xx = rectBoundTabbar.midX
        let yy = rectBoundTabbar.midY - paddingBottom
        button.center = CGPoint(x: xx, y: yy)
        
        self.tabBar.addSubview(button)
        self.tabBar.bringSubviewToFront(button)
        
        button.addTarget(self, action: #selector(handleTouchTabbarCenter), for: .touchUpInside)
        
        if let count = self.tabBar.items?.count
        {
            let i = floor(Double(count / 2))
            let item = self.tabBar.items![Int(i)]
            item.title = ""
        }
    }
    
}
