//
//  BidStatsVC.swift
//  Simsar
//
//  Created by NxGeN on 10/20/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class BidStatsVC: UIViewController {

    @IBOutlet var totalView: UIView!
    @IBOutlet var highestBidView: UIView!
    @IBOutlet var lowestBidView: UIView!
    @IBOutlet var bidButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        [totalView, highestBidView, lowestBidView, bidButton].forEach { (button) in
            button?.layer.cornerRadius = 10
        }
        
        [totalView, highestBidView, lowestBidView].forEach { (button) in
            button?.layer.borderWidth = 1
            button?.layer.borderColor = UIColor.lightGray.cgColor
            
        }
        
    }

}
