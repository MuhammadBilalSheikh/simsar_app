//
//  VC_faq.swift
//  Simsar
//
//  Created by NxGeN on 11/4/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class VC_faq : UIViewController {
    
    
    @IBOutlet var view_faqs: UIView!
    @IBOutlet var firstLine: UIView!
    @IBOutlet var secondLine: UIView!
    @IBOutlet var thirdLine: UIView!
    @IBOutlet var fourthLine: UIView!
    @IBOutlet var sixthLine: UIView!
    @IBOutlet var fifthLine: UIView!
    @IBOutlet var eighthLine: UIView!
    @IBOutlet var seventhLine: UIView!
    @IBOutlet var ninethLine: UIView!
    @IBOutlet var tenthLine: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        addBorders()
        self.setNavBarWithNotificationIcon()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func addBorders(){
        view_faqs.layer.borderWidth = 1
        view_faqs.layer.borderColor = UIColor.black.cgColor
        addBlackBorder(v: firstLine)
        addBlackBorder(v: secondLine)
        addBlackBorder(v: thirdLine)
        addBlackBorder(v: fourthLine)
        addBlackBorder(v: fifthLine)
        addBlackBorder(v: sixthLine)
        addBlackBorder(v: seventhLine)
        addBlackBorder(v: eighthLine)
        addBlackBorder(v: ninethLine)
        addBlackBorder(v: tenthLine)
    }
    
    func addBlackBorder( v : UIView){
        v.layer.borderColor = UIColor.black.cgColor
        v.layer.borderWidth = 0.5
        
    }
}
