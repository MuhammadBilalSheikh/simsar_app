//
//  VC_Test.swift
//  Simsar
//
//  Created by NxGeN on 11/17/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import ImageSlideshow

class VC_Test: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet var activeView: UIView!
    @IBOutlet var detailsView: UIView!
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        setViews()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Helper Functions
    func setData() {
        slideshow.setImageInputs([
            ImageSource(image: UIImage(named: "Alexandra Daddario")!),
            ImageSource(image: UIImage(named: "Angelina Jolie")!),
            ImageSource(image: UIImage(named: "Anne Hathaway")! )])
    }
    
    func setViews() {
        activeView.layer.cornerRadius = 8
        activeView.clipsToBounds = true
        
        detailsView.roundCorners(corners: .topLeft, radius: 25)
        detailsView.roundCorners(corners: .topRight, radius: 25)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
