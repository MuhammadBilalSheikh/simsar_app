//
//  VC_Home.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_Home: UIViewController {

    let searchBar = UISearchBar()
    let constUrl = "https://simsar.com/public/asset/images/sell-now/"
    //https://simsar.com/testsimsar/public/assets/media/category/icon/
    //https://simsar.com/testsimsar/public/asset/images/sell-now/
    
    @IBOutlet weak var btnFeature: UIButton!
    @IBOutlet weak var btnOffered: UIButton!
    @IBOutlet weak var btnLatest: UIButton!
    @IBOutlet weak var lblfilterName: UILabel!
    @IBOutlet weak var filterNameHeight: NSLayoutConstraint! // by default 18
    @IBOutlet weak var collectionView_Dashboard: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DesignNavBar()
        addLeftNavButton()
        ConfigureSearchBar()
        collectionView_Dashboard.delegate = self
        collectionView_Dashboard.dataSource = self
        btnFeature.alpha = 0.5
        btnOffered.alpha = 0.5
        btnLatest.alpha = 0.5
        btnFeature.makeRound(radius: 10)
        btnOffered.makeRound(radius: 10)
        btnLatest.makeRound(radius: 10)
        hideFeautureName()
    }
    
    @IBAction func feature(_ sender: Any) {
        lblfilterName.text = "Featured Ads."
        if btnFeature.alpha == 1{
            btnFeature.alpha = 0.5
            hideFeautureName()
        }
        else{
        btnFeature.alpha = 1
        showfeatureName()
        }
        btnOffered.alpha = 0.5
        btnLatest.alpha = 0.5
    }
    
    @IBAction func offered(_ sender: Any) {
        lblfilterName.text = "Offered Ads."
        if btnOffered.alpha == 1{
            btnOffered.alpha = 0.5
            hideFeautureName()
        }
        else{
        btnOffered.alpha = 1
        showfeatureName()
        }
        btnFeature.alpha = 0.5
        btnLatest.alpha = 0.5
    }
    
    @IBAction func latest(_ sender: Any) {
        lblfilterName.text = "Latest Ads."
        if btnLatest.alpha == 1{
            btnLatest.alpha = 0.5
            hideFeautureName()
        }
        else{
        btnLatest.alpha = 1
        showfeatureName()
        }
        btnFeature.alpha = 0.5
        btnOffered.alpha = 0.5
        
    }
    
    func showfeatureName(){
        UIView.animate(withDuration: 0.2) {
            self.lblfilterName.isHidden = false
            self.filterNameHeight.constant = 18
            self.view.superview?.layoutIfNeeded()

            
        }
     
    }
    func hideFeautureName(){
        UIView.animate(withDuration: 0.2) {
            self.lblfilterName.isHidden = true
            self.filterNameHeight.constant = 0
            self.view.superview?.layoutIfNeeded()
           
        }
        
     
    }
    
    
    
      //MARK:- SearchBar UI
      func ConfigureSearchBar(){
          searchBar.sizeToFit()
          searchBar.delegate = self
        searchBar.placeholder = "Search Keyword"
        searchBar.searchBarStyle = .minimal
        
          navigationItem.title = "Simsar"
          showSearchBarButton(shouldShow: true)
    
      }
    func showSearchBarButton(shouldShow: Bool) {
        if shouldShow {
            
        let searchBtn = UIBarButtonItem(barButtonSystemItem: .search ,
        target: self,
        action: #selector(handleShowSearchBar))
        
            let notification_btn = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(openNotification))
            notification_btn.setBackgroundImage(UIImage(named: "notification"), for: .normal, barMetrics: .default)
        self.navigationItem.rightBarButtonItem = notification_btn
            
            navigationItem.rightBarButtonItems = [notification_btn , searchBtn]
            
        } else {
            navigationItem.rightBarButtonItems = []
        }
    }
    func search(shouldShow: Bool) {
          showSearchBarButton(shouldShow: !shouldShow)
          searchBar.showsCancelButton = shouldShow
          navigationItem.titleView = shouldShow ? searchBar : nil
      }
    @objc func handleShowSearchBar() {
         searchBar.becomeFirstResponder()
         self.search(shouldShow: true)
     }
    
     func DesignNavBar(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         self.navigationController?.navigationBar.backgroundColor = UIColor.white
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
     }
    func addLeftNavButton(){
        let newBtn = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(LeftMenu))
        self.navigationItem.hidesBackButton = true
        newBtn.setBackgroundImage(UIImage(named: "menu"), for: .normal, barMetrics: .default)
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItem = newBtn
    }

   @objc func LeftMenu(){
        openVC("VC_SideMenu")
       }
    @objc func openNotification(){
     openVC("VC_Notifications")
    }

}

extension VC_Home: UISearchBarDelegate {
func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    print("Search bar editing did begin..")
    
}

func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    print("Search bar editing did end..")
   
    
}

func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    search(shouldShow: false)
}

func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    print("Search text is \(searchText)")
    
}
}
extension VC_Home : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let padding: CGFloat =  30
           let collectionViewSize = collectionView.frame.size.width - padding
           
           let cellWidth = 140
           let cellHeight = 176
           return CGSize(width: cellWidth, height: cellHeight)
           
       }
    
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 30
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DashBoardCell
        
       cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.borderWidth = 1.0

        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true

        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 6, height: 6)
        cell.layer.shadowRadius = 10 / 2.0
        cell.layer.shadowOpacity = 0.3
        cell.layer.masksToBounds = false
        //cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        return cell
    }
    
    
}
