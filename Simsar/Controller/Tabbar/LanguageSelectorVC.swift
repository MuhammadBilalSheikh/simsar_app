//
//  LanguageSelectorVC.swift
//  Simsar
//
//  Created by NxGeN on 10/31/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class LanguageSelectorVC : UIViewController {
    
    
    
    @IBOutlet var englishButton: UIView!
    @IBOutlet var arabicButton: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer( target: self, action: #selector( languageSelected ))
        setUpButtons()
        
        let tap2 = UITapGestureRecognizer( target: self, action: #selector( languageSelected ))
        setUpButtons()
        
        englishButton.addGestureRecognizer( tap2 )
        arabicButton.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        setUpButtons()
    }
    
    @objc func languageSelected () {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "home") as! HomeTabBarController
        let secondVC          = storyBoard.instantiateViewController(withIdentifier: "VC_CustomIndicator") as! VC_CustomIndicator
        newViewController.modalPresentationStyle = .fullScreen
        self.present( newViewController , animated: true, completion: nil)
        
    }
    
    func setUpButtons(){
        
        print(" Set Up Buttons ")
        //ENGLISH BUTTON
        englishButton.layer.borderWidth = 2
        englishButton.layer.borderColor = UIColor.white.cgColor
        englishButton.layer.cornerRadius = 15
        //ARABIC BUTTON
        arabicButton.layer.borderWidth = 2
        arabicButton.layer.borderColor = UIColor.white.cgColor
        arabicButton.layer.cornerRadius = 15
        
    }
    
    
}
