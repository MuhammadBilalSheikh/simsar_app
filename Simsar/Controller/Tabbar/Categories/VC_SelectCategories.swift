//
//  VC_SelectCategories.swift
//  Simsar
//
//  Created by NxGeN on 11/12/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

class VC_SelectCategories : UIViewController {
    var _selectedIndexPath : IndexPath? = nil
   // var selectedIndexPath: IndexPath?
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet var  collectionView : UICollectionView!
    @IBOutlet var buttonView: UIView!
    @IBOutlet var button: UIView!
    
    var strTitleThis: String!
    var strDescThis: String!
    var imageToBeShownThis : UIImage!
    var idOfSelectedProductThis: Int!
    var artist = [SellNowCategoryData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnNext.isHidden = true
        self.buttonView.isHidden = true
     //   if Model_UserStatus.shared.isUserIdThere(){
            collectionView.allowsMultipleSelection = false
            setButtonsBackground()
            setupPostMethod()
            collectionView.delegate = self
            collectionView.dataSource = self
      //  }
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if Model_UserStatus.shared.isUserIdThere(){
            self.tabBarController?.tabBar.isHidden = false
        }else {
            self.tabBarController?.selectedIndex =  4
        }
    }
   
    func setupPostMethod(){
        self.view.showAnimatedGradientSkeleton()
        self.view.startSkeletonAnimation()
        if Connectivity.isConnectedToInternet{
            SellNowCategoriesService.shared.getSellNowCategoriesResponse{[weak self](response, error) in
                if  response == nil {
                    let getError = (error ?? "Can not able to get Categories")
                    print(getError)
                    self?.showAlertView(message: "", title: getError)
                    return
                }
                DispatchQueue.main.async {
                    self?.artist = response!
                    self?.view.hideSkeleton()
                    self?.view.stopSkeletonAnimation()
                    self?.collectionView.reloadData()
                }
            }
        }else{
            self.showAlertView(message: "", title: Constant.internetError)
        }
    }
    
    func setButtonsBackground(){
        makeSimsarThemedButton(outerView: buttonView, button: btnNext, needGradient: true)
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
//        if idOfSelectedProductThis == nil {
//            showAlertView(message: "Please Select Some Category", title: "Please Select ?")
//        }else{
//            Navigation()
//        }
    }
}

//MARK: UiCollectionView Delegates
extension VC_SelectCategories : UICollectionViewDelegate , UICollectionViewDataSource ,
UICollectionViewDelegateFlowLayout {
    
    //Number Of Items
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artist.count
    }
    
    //RETURN CELL
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectCategoryCell", for: indexPath ) as! SelectCategoryCell
        cell.toggleSelected(isSelected: false)
        cell.layer.masksToBounds = false
        cell.clipsToBounds = false
        cell.layer.shadowOpacity = 0.07
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 1, height: 2)
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = cell.frame.height * 0.05
        if(artist.count) > 0 {
            let artistData = artist[indexPath.row]
            print(artistData)
            print(indexPath.row)
            print(artist.count)
            cell.checkMarkImg.isHidden = true
           // cell.checkMarkImg.image = mark.init(ismMarked: true) ? UIImage(named: "done")
          //  cell.lblDesc.text = artistData["description"] as! String
            cell.lblTitle.text = artistData.title
            ServiceManager.shared.downloadImageWithName(
                Constant.baseUrlImageForCategories
                , artistData.iconImage!
                , cell.imgView)
    }
        return cell
    }
    
    // MARK:- NAVIGATE
    func Navigation(){
        let storyboard = UIStoryboard(name: "SellNow", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: VcIdentifier.subCategories.rawValue) as! VC_ChooseSubCategories
        VC.strTitle = self.strTitleThis
     //   VC.strDesc = self.strDescThis
        VC.imageToBeShown = self.imageToBeShownThis
        VC.idOfSelectedCatProduct = self.idOfSelectedProductThis
        self.navigationController?.pushViewController(VC, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //SIZE OF CELLS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cvSize = collectionView.frame.width - 50
        return CGSize(width: cvSize/3 , height: cvSize/3 )
    }
    
    //item spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? SelectCategoryCell
        cell!.toggleSelected(isSelected: false)
    }
    
    @available(iOS 12.0, *)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
               print("Select")
        guard let cell = collectionView.cellForItem(at: indexPath) as? SelectCategoryCell else {return}
        
       
        if(cell.checkMarkImg.isHidden){
            cell.checkMarkImg.isHidden = false
                    cell.checkMarkImg.image = UIImage(named: "done")
                } else {
                       cell.checkMarkImg.isHidden = true
                    [cell.isSelected : true];
                }
//        let sb = UIStoryboard(name: "Main", bundle: nil )
//        let detail: VC_SelectCategories = sb.instantiat eViewController(withIdentifier: "VC_SelectCategories") as! VC_SelectCategoriess
        let artistData = artist[indexPath.row]
        self.strTitleThis = artistData.title
      //  self.strDescThis = artistData["description"] as? String
        self.imageToBeShownThis = cell.imgView.image
       
        self.idOfSelectedProductThis = artistData.id
        print("This is checking category Id \(String(idOfSelectedProductThis))")
        cell.toggleSelected(isSelected: true)
        
        Navigation()

    }
    
    //inter line spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension VC_SelectCategories : SkeletonCollectionViewDataSource {
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "SelectCategoryCell"
    }
}
