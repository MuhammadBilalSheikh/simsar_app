//
//  VC_TabbarCategories.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class VC_TabbarCategories: UIViewController {

    @IBOutlet weak var AllCategories_CollectionView: UICollectionView! //Last One
    @IBOutlet weak var CV_Horizontal: UICollectionView! //Horizontal
    
    let constUrl = "https://simsar.com/public/assets/media/category/icon/"
    //https://simsar.com/testsimsar/public/assets/media/category/icon/
    //https://simsar.com/testsimsar/public/asset/images/sell-now/
    
    var  globalIndicator : UIViewController? = nil
    
    var dataFromServer : [SellNowCategoryData] = []
    
    var dataFromServerTop : [SellNowCategoryDataTop] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CV_Horizontal.delegate = self
        CV_Horizontal.dataSource = self
        //
        AllCategories_CollectionView.delegate = self
        AllCategories_CollectionView.dataSource = self
        //
        DesignNavBar()
        addLeftNavButton()
        callApi()
        callApiTop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear( true )
        
    }
    
    //
    
    func callApi(){
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/categories?" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
                self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let categories = jObj["categories"]
                    print("categories ==== \(categories)" )
                    
                    for countOfObjectsInArray in 0..<categories.count {
                        
                        print ("Loop For Time === \(countOfObjectsInArray)")
                        
                        
                        let singleObj  = categories[countOfObjectsInArray]// may be error in this
                        let obj = SellNowCategoryData(id: singleObj["id"].intValue
                                                      , title: singleObj["title"].stringValue
                                                      , datumDescription:  singleObj["description"].stringValue
                                                      , iconImage:  singleObj["icon_image"].stringValue )
                        
                        self.dataFromServer.append( obj )
                        
                        
                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.AllCategories_CollectionView.reloadData()
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    
    
    
    func callApiTop(){
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/categories?" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
                self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let categories = jObj["topcategories"]
                    print("categories ==== \(categories)" )
                    
                    for countOfObjectsInArray in 0..<categories.count {
                        
                        print ("Loop For Time === \(countOfObjectsInArray)")
                        
                        
                        let singleObj  = categories[countOfObjectsInArray]// may be error in this
                        let obj = SellNowCategoryDataTop(id: singleObj["id"].intValue
                                                      , title: singleObj["title"].stringValue
                                                      , datumDescription:  singleObj["description"].stringValue
                                                      , iconImage:  singleObj["icon_image"].stringValue )
                        
                        self.dataFromServerTop.append( obj )
                        
                        
                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.CV_Horizontal.reloadData()
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    
      func DesignNavBar(){
        
     self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
          self.navigationController?.navigationBar.backgroundColor = UIColor.white
          self.navigationController?.navigationBar.shadowImage = UIImage()
          self.navigationController?.navigationBar.layoutIfNeeded()
         navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
      
    }
    
     func addLeftNavButton(){
        
         let newBtn = UIBarButtonItem(image: UIImage(named: "sidemenu-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(LeftMenu))
         self.navigationItem.hidesBackButton = true
//         newBtn.setBackgroundImage(UIImage(named: "menu"), for: .normal, barMetrics: .default)
         self.navigationItem.leftItemsSupplementBackButton = true
         self.navigationItem.leftBarButtonItem = newBtn
     
    }

    @objc func LeftMenu(){
         openVC("VC_SideMenu")
        }


}

extension VC_TabbarCategories : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    //No Of Cells
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
       
        if collectionView == AllCategories_CollectionView {
            return dataFromServer.count
        }else{
            return dataFromServerTop.count
        }

    }
    
    //Item Select At
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openVC("VC_CategoriesPage2")
    }
    
    //SIZE OF CELL
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding : CGFloat = 30
        //POPULAR COLLECTION VIEW -> HORIZONTAL
        if collectionView == CV_Horizontal {
            
            let height = collectionView.frame.height - padding
            return CGSize ( width: height , height: height )
            
        }else {
            
            print("Collection View Lower Cell Size")
            let width  = collectionView.frame.width - padding - 10
            return CGSize( width : width/2  , height: width/2 )
        
            
        }
            //EARLIER CODE
        
           //let cellWidth = 80
           //let cellHeight = 100
           //return CGSize(width: cellWidth, height: cellHeight)
       }
    
    //LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 25
       }
    
    //INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    
    //EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    //FOR CELL AT ROW
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == CV_Horizontal{
        
        let cellTop = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PopularCategories_Cell
        
            cellTop.cellCustomization()
            
            cellTop.lblName.text = dataFromServerTop[indexPath.row].title
            cellTop.lblDes.text = dataFromServerTop[indexPath.row].datumDescription
            
            if( dataFromServerTop[indexPath.row].iconImage != nil ){
                ServiceManager.shared.downloadImageWithName(
                    constUrl
                    , dataFromServerTop[indexPath.row].iconImage ?? ""
                 //   , cell.activityIndicator
                    , cellTop.viewImage )
            }
            
        return cellTop
        }else{
           
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AllCategories_Cell
             
            //CELL VIEW
            cell.cellCustomization()
            //CELL DATA INTEGRATING
            
            cell.lblDes.text = dataFromServer[indexPath.row].datumDescription
            
            cell.lblName.text = dataFromServer[indexPath.row].title
            
            if( dataFromServer[indexPath.row].iconImage != nil ){
                ServiceManager.shared.downloadImageWithName(
                    constUrl
                    , dataFromServer[indexPath.row].iconImage ?? ""
                    , cell.activityIndicator
                    , cell.viewImage )
            }
             return cell
            
        }
    }
   
}

extension VC_TabbarCategories : SkeletonCollectionViewDataSource  {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        
        return "cell"
    
    }
}
