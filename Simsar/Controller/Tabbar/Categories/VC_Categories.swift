//
//  VC_Categories.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SkeletonView

class VC_Categories: UIViewController {
    
    var arrayPopularCategories : [String] = [""]
    
    @IBOutlet weak var AllCategories_CollectionView: UICollectionView!
    @IBOutlet weak var Popular_CollectionView: UICollectionView!
    var topCategories = [Cat]()
    var categories = [Cat]()
    override func viewDidLoad() {
        super.viewDidLoad()
        Popular_CollectionView.delegate = self
        Popular_CollectionView.dataSource = self
        AllCategories_CollectionView.delegate = self
        AllCategories_CollectionView.dataSource = self
        getCategories()
        //
        self.tabBarController?.tabBar.isHidden = true
        self.setBackNavButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.tabBarController?.tabBar.isHidden = true 
    }
    func getCategories(){
        if Connectivity.isConnectedToInternet{
            CategoriesService.shared.getCategoriesResponse { (responce, error) in
                if responce != nil  {
                    self.topCategories = responce?.topCategories ?? []
                    self.categories = responce?.categories ?? []
                    DispatchQueue.main.async {
                        self.AllCategories_CollectionView.reloadData()
                        self.Popular_CollectionView.reloadData()
                    }
                }else{
                    print(error ?? "")
                }
                
            }
        }
    }
    
}


extension VC_Categories : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == Popular_CollectionView{
            return topCategories.count
        }else{
            return categories.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = 80
        let cellHeight = 100
        return CGSize(width: cellWidth, height: cellHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == Popular_CollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PopularCategories_Cell
            
            cell.cellCustomization()
            let c = topCategories[indexPath.row]
            cell.lblName.text = c.title
            cell.lblDes.text  = c.categoryDescription
            ServiceManager.shared.downloadImageWithName(Constant.baseUrlImageForCategories, c.iconImage ?? "", cell.viewImage)
            
            return cell
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AllCategories_Cell
            cell.cellCustomization()
            let c = categories[indexPath.row]
            cell.lblName.text = c.title
            cell.lblDes.text  = c.categoryDescription
            ServiceManager.shared.downloadImageWithName(Constant.baseUrlImageForCategories, c.iconImage ?? "", cell.viewImage)
            return cell
            
        }
    }  
}
extension VC_Categories : SkeletonCollectionViewDataSource  {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        
        return "cell"
    
    }
}
