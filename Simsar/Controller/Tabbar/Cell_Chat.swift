//
//  Cell_ChatCollectionViewCell.swift
//  Simsar
//
//  Created by NxGeN on 11/14/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class Cell_Chat: UICollectionViewCell {
    
    @IBOutlet var productName : UILabel!
    @IBOutlet var userImage : UIImageView!
    @IBOutlet var userName  : UILabel!
    @IBOutlet var lastMessage  : UILabel!
    @IBOutlet var lastSeen  : UILabel!
    @IBOutlet var noOfMessages  : UILabel!
    @IBOutlet var noOfMessagesView: UIView!
    
    override func awakeFromNib() {
        
        updateUI()
    }
    
    func updateUI(){
        
        self.userImage.layer.cornerRadius = userImage.frame.height/2
        self.contentView.addBottomBorderWithColor(color: UIColor.lightGray , width: 0.5 )
        
       
    }
    
    func recentMessages(_ no : String ){
        
        if (no.isEmpty){
            noOfMessages .backgroundColor = UIColor.white
        }else {
            noOfMessagesView.backgroundColor = UIColor.red
            noOfMessages.text = no
            noOfMessages.textColor = UIColor.white
            noOfMessagesView.layer.cornerRadius = noOfMessagesView.frame.height/2
            noOfMessagesView.clipsToBounds = true 
        }
        
    }
    
}
