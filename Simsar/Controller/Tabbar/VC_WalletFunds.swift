//
//  VC_WalletFunds.swift
//  Simsar
//
//  Created by NxGeN on 11/21/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class VC_WalletFunds: UIViewController {

    @IBOutlet var lblBank3: UILabel!
    @IBOutlet var lblBank2: UILabel!
    @IBOutlet var lblBank1: UILabel!
    @IBOutlet var lblPrice: UILabel!
    
    @IBOutlet var tfDepositAmount: UITextField!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var outerViewSubmitButton: UIView!
    
    @IBOutlet var rBtnBank: UIButton!
    @IBOutlet var rBtnDebitCard: UIButton!
    @IBOutlet var rBtnCreditCard: UIButton!
    
    
    @IBOutlet var cvUpperScroll : UICollectionView!
    
    var constUrlToFetchImage = "https://simsar.com/public/images/common-pages/"
    var globalIndicator: UIViewController?
   
    var upperScrollTitles = ["Wallet","Deposit Funds" ,"Transactions","Withdraw"]
    
    var upperScrollImages = ["wallet_image" , "deposit_image" ,
    "transaction_Image" , "withdraw_image"
    ]
    
    var collectionOfFunds : [ VC_WalletFundModel ] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiCalledForFunds()
        updateUI()
        cvUpperScroll.delegate = self
        cvUpperScroll.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    func updateUI(){
        
        self.makeSimsarThemedButton(outerView: outerViewSubmitButton
                                    , button: submitButton, needGradient: true )
        
        
        self.setBackNavButton()
        setTitle(title: "Deposit Funds")
        
    }
    
    func setTitle (title : String ){
        
        //self.navigationController?.navigationBar.largeContentTitle = title
        self.navigationController?.title = title
        
    }
    
    //RADIO BUTTONS
    @IBAction func radioButtons( _ sender : UIButton ){
        if sender.tag == 0{ //Bank
            
            rBtnCreditCard.isSelected = false
            rBtnDebitCard.isSelected = false
            rBtnBank.isSelected = true
        }
        else if sender.tag == 1 {//Debit Card
            
            
            rBtnCreditCard.isSelected = false
            rBtnDebitCard.isSelected = true
            rBtnBank.isSelected = false
        }
        else if sender.tag == 2 {//CreditCard

                rBtnCreditCard.isSelected = true
                rBtnDebitCard.isSelected = false
                rBtnBank.isSelected = false
        }
        
    }//radioButtons
    
    
    func apiCalledForFunds(){
        
        
      // startAnimatingWhileLoadingData()
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
            "user_id"    : 461
        ]
        let serviceObj = Service(url: "https://simsar.com/api/v1/walletfunds" , parameters: apiParams )
        
        serviceObj.headers = ["Content-Type" : "application/json"]
        
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            
            
            if (result != nil ){
                
                //  let decoder = JSONDecoder()
               // self.stopAnimationgAsDataLoaded()
                do {
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("\(jObj)")
                    let response = jObj["my_banks"]
                    print("Responce**** \(response[0])")
                    if jObj["status"].stringValue == "success"  {
                        self.lblPrice.text = jObj["userWallet"].stringValue
                        
                        for countOfObjectInData in 0..<response.count {
                            //let obj =  VC_WalletFundModel()
                            let jj = response[countOfObjectInData]
                            
//                            obj.title = jj["ac_title"].stringValue
//                            obj.id = jj["id"].intValue
                            
                            self.lblBank1.text = jj["ac_title"].stringValue
                            self.lblBank2.text = jj["ac_title"].stringValue
                            //self.collectionOfFunds.append(obj)
                        }
                    
                        
                    } else {  print("Error in response");   return }
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }//CLOSURE
        
    }//LoginByUserEmail
    
}





extension VC_WalletFunds : UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UICollectionViewDataSource {
    
    //No Of Cells
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return upperScrollTitles.count
    }
    
    //Cell At Raw
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath ) as! Cell_WalletUpperScroll
        
        cell.title.text = upperScrollTitles[indexPath.row]
        cell.imageView.image = UIImage(named: upperScrollImages[indexPath.row] )
        
        return cell
        
    }
    
    //Size Of Cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cvWidth = collectionView.frame.width
        let cvHeght = collectionView.frame.height
        return CGSize( width: cvWidth/4 , height: cvHeght )
        
    }
    
    
}

class VC_WalletFundModel {
    
    var id: Int?
    var title: String?
}
