//
//  VC_BrandTab.swift
//  Simsar
//
//  Created by NxGeN on 11/5/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class VC_BrandTab : UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var topView: UIView! //For Giving Shadow --
    
    var dataFromServer : [sellNowBrands] = []
    
    var  globalIndicator : UIViewController? = nil
    
    var idOfThis: Int!
    
    let constUrl = "https://simsar.com/public/assets/media/brand/logo/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Api()
    
        collectionView.dataSource = self
        collectionView.delegate = self
        
        //self.topView.dropbtnShadow() // Working But Not Required

        collectionView.register( brandCardClass.nib(), forCellWithReuseIdentifier: brandCardClass.identifier )
        
        collectionView.register( AllAdsCollectionViewCell.nib() , forCellWithReuseIdentifier: AllAdsCollectionViewCell.identifier )
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 10
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        
        setUpNavBar()
    }
    
    
    
    func Api() {
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/allbrand" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
                self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let brands = jObj["brand"]
                    print("categories ==== \(brands)" )
                    
                    for countOfObjectsInArray in 0..<brands.count {

                        print ("Loop For Time === \(countOfObjectsInArray)")


                        let singleObj  = brands[countOfObjectsInArray]// may be error in this
                        let obj = sellNowBrands(id: singleObj["id"].intValue
                                                      , title: singleObj["title"].stringValue
                                                      , datumDescription:  singleObj["description"].stringValue
                                                      , iconImage:  singleObj["image_logo"].stringValue )

                        self.dataFromServer.append( obj )


                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.collectionView.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
        
    
    
    
    
    
    
    //MARK: NAVIGATION TA;B BUTTONS
    func setUpNavBar(){
        
        //
        let rightSearchBarButtonItem = UIBarButtonItem(image: UIImage(named: "search-icons.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapSearchButton))
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
         self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "sidemenu-icon.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(didTapSideMenu))
        
        navigationItem.leftBarButtonItem =  leftBarButtonItem
        //
        
    }
    
    @objc func didTapSideMenu() {
        openVC("VC_SideMenu")
    }
    
    @objc func didTapSearchButton(){
        openVC("VC_Search")
    }
    //MARK: NAV ENDS
}





extension VC_BrandTab : UICollectionViewDelegate , UICollectionViewDataSource
,UICollectionViewDelegateFlowLayout
{
    
    // MARK:- NAVIGATE
    func Navigation(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: VcIdentifier.brand2.rawValue) as! VC_Brand2
        VC.idOF = self.idOfThis
        self.navigationController?.pushViewController(VC, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: indexPath) as?
                brandCardClass else {return}
        self.idOfThis = dataFromServer[indexPath.row].id
        Navigation()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataFromServer.count
    }
    
    //CEll For Item At
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: brandCardClass.identifier , for: indexPath ) as! brandCardClass
        cell.lblTitle.text = dataFromServer[indexPath.row].title
       // cell.lblDetail.text = dataFromServer[indexPath.row].datumDescription
      //  cell.imgView.image = dataFromServer[indexPath.row].iconImage as? UIImage
        
        if( dataFromServer[indexPath.row].iconImage != nil ){
            ServiceManager.shared.downloadImageWithName(
                constUrl
                , dataFromServer[indexPath.row].iconImage ?? ""
                //, cell.activityIndicator
                , cell.imgView )
        }
        
        
    
        
        
        return cell
        
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  40
        let collectionViewSize = collectionView.frame.size.width - padding - 10
        return CGSize(width: collectionViewSize/3.5 , height: collectionViewSize/3 )
    
    }
    
    //LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 25
       }
    
    //INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    //EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
}


