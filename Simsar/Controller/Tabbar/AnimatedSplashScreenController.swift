//
//  AnimatedSplashScreenController.swift
//  Simsar
//
//  Created by NxGeN on 10/28/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import ImageIO


class AnimatedSplashScreenController
    : UIViewController
{

    @IBOutlet var gifImage: UIImageView!
    
    @IBOutlet var gifHolder: UIImageView!
    let dispatchTime = DispatchTime.now() + .seconds( 6 )
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        gifImage.image = UIImage.gifImageWithName("loader-750x750")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LanguageSelect") as! LanguageSelectorVC
        newViewController.modalPresentationStyle = .fullScreen
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
        
            //let varController = VC_Categories()
            //varController.modalPresentationStyle = .fullScreen
            //self.openVC("VC_Categories")
            //self.present( varController , animated: true , completion : nil )
            //self.openVC("VC_Search")
            self.present(newViewController, animated: true, completion: nil)
            
        }
    
    }//viewDidiLoad()
    
}
