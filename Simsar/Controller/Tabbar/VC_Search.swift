//
//  VC_Search.swift
//  Simsar
//
//  Created by NxGeN on 11/4/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit
import DropDown


class VC_Search : UIViewController {

    //et -> Edit Text
    
    @IBOutlet var typeOfSearch: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var etFrom: UITextField!
    @IBOutlet var etTo: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var stateTextField: UITextField!
    @IBOutlet var countryTextField: UITextField!
    @IBOutlet var exploreTextField: UITextField!
    @IBOutlet var categoryTextField: UITextField!
    
    @IBOutlet var seearchButtonView: UIView!
    
    @IBOutlet var rBtnRentOnly: UIButton!
    @IBOutlet var rBtnRent: UIButton!
    @IBOutlet var rBtnSell: UIButton!
    
    private let viewModel = HomeSearchViewModel()
    private var countries:[String] = []
   // private var states:[]
    
    
    
    let additionalView = UIView( frame: CGRect(x: 0, y: 0, width: 200, height: 100 ))
    var viewConstraints = [NSLayoutConstraint]()
    //
    var startDatePicker = UIDatePicker()
    var endDatePicker = UIDatePicker()
    
    //DROP_DOWNS
    var categoryDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["category 1 " , "Category 2" , "Category 3"]
        return dd
    }()
    
    var exploreMode : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["Explore Mode1","Explore Mode 2","Explore Mode3"]
        return dd
    }()
    
    var countryDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["Pakistan", "Uganda" , "UAE"]
        return dd
    }()
    
    var stateDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["sindh", "Punjab" , "KPK" , "Balochistan"]
        return dd
    }()
    
    var cityDD : DropDown = {
        let dd = DropDown()
        dd.dataSource = ["Karachi", "Lahore" , "Islamabad"]
        return dd
    }()
    //DDS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        getCountries()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        addScroll()
        aligningEditTexts()
        print("View Will Appear - VC - Search")
        self.setupDropDown()
        self.addIconsToTextFields()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        addScroll()
        applyConstraints()
    }

    private func getCountries(){
        viewModel.getCountries { (countryArry, error) in
            if countryArry.count != 0{
              //  self.countries = countryArry
               
                self.countryDD.dataSource = countryArry
                
            }else {
                ///show error message no country found
            }
        }
    }
    private func getStateByCountries(country:String){
        viewModel.getStateByCountry(country: country) { (stateArray, error) in
            if stateArray.count != 0{
              //  self.countries = countryArry
               // self.stateDD.dataSource.removeAll()
                self.stateDD.dataSource = stateArray
            }else {
                ///show error message no country found
            }
        }
    }
    
    private func getCityByState(state:String){
        
        viewModel.getCityByState(state: state) { (cityArray, error) in
            if cityArray.count != 0{
              //  self.countries = countryArry
                self.cityDD.dataSource = cityArray
            }else {
                ///show error message no country found
            }
        }
        
    }
    
}




///DATE PICKERS
extension VC_Search{
    
    ///etTo
    func createStartDatePicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: nil , action: #selector( startDatePicked ) )
        
        toolBar.setItems([doneButton], animated: true)

        startDatePicker.datePickerMode = .date
        etTo.inputAccessoryView = toolBar
        etTo.inputView = self.startDatePicker
        
    }
    
    
    //etFrom
    func createEndDatePicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: nil , action: #selector( endDatePicked ) )
        toolBar.setItems([doneButton], animated: true)
        
        startDatePicker.datePickerMode = .date
        etFrom.inputAccessoryView = toolBar
        etFrom.inputView = self.startDatePicker
        
    }
    
    //etTo
    @objc func startDatePicked(){
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        etTo.text = formatter.string(from: startDatePicker.date)
        self.view.endEditing(true)
    }
    
    //etTo
    @objc func endDatePicked(){
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        etFrom.text = formatter.string(from: startDatePicker.date)
        self.view.endEditing(true)
        
    }
    
    ///will Add Icons To TextFields
    func addIconsToTextFields(){
    
    }
    
    
}






//----------------------------------
extension VC_Search {
    
    
    func aligningEditTexts() {
        
        typeOfSearch.borderStyle = .none
        typeOfSearch.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        etFrom.borderStyle = .none
        etFrom.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        etTo .borderStyle = .none
        etTo.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        
    }
    
    func addScroll(){
    
        self.contentView.addSubview( additionalView )
        scrollView.isScrollEnabled = true
        self.scrollView.contentSize = CGSize .init( width: view.frame.width , height: contentView.frame.height + 20 )
        
    }
    
    func applyConstraints(){
        
        additionalView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = additionalView.topAnchor.constraint(equalTo: searchButton.bottomAnchor , constant: 20  )
        let leadingConstraint = additionalView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor , constant: 40 )
        let trailingConstance = additionalView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: 40 )
        let bottomConstraint = additionalView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor , constant: 30 )
        
        viewConstraints.append(contentsOf: [topConstraint , leadingConstraint , trailingConstance , bottomConstraint ] )
        
        NSLayoutConstraint.activate( viewConstraints )
        additionalView.backgroundColor = .white
        
    }
    
}



//DROPDOWNS

extension VC_Search {
    
    func setupDropDown(){
        
        categoryDD.anchorView =  categoryTextField
        exploreMode.anchorView = exploreTextField
        countryDD.anchorView = countryTextField
        stateDD.anchorView = stateTextField
        cityDD.anchorView = cityTextField
        
        //SELECTION_ACTION
        cityDD.selectionAction = {index , title in
            self.cityTextField.text = title
        }
        stateDD.selectionAction = {index , title in
            self.stateTextField.text = title
            self.getCityByState(state: title)
            self.cityTextField.text = ""
        }
        countryDD.selectionAction = {index , title in
            self.countryTextField.text = title
            self.getStateByCountries(country: title)
            self.stateTextField.text = ""
            self.cityTextField.text = ""
            
        }
        //SELECTION_ACTION
        
        //Tap_Gestures
        let cityGest = UITapGestureRecognizer(target: self , action: #selector( cityTapped ) )
        cityTextField.addGestureRecognizer(cityGest)
        
        let stateGest = UITapGestureRecognizer(target: self , action: #selector( stateTapped ) )
        stateTextField.addGestureRecognizer(stateGest)
        
        let countryGest = UITapGestureRecognizer(target: self , action: #selector( countryTapped ) )
        countryTextField.addGestureRecognizer(countryGest)
        
       
        
        let categoryGest = UITapGestureRecognizer(target: self , action: #selector( categoryTapped ) )
        categoryTextField.addGestureRecognizer(categoryGest)
        //------------TAP GESTURES
        
    }
    
    @objc func categoryTapped(){
        
        categoryDD.show()
    }
    
    
    
    @objc func countryTapped(){
        
        countryDD.show()
    }
    
    @objc func stateTapped(){
        
        stateDD.show()
    }
    
    @objc func cityTapped(){
        
        cityDD.show()
    }
    
    
}


//DROPDOWNS






extension VC_Search {
    
    func updateUI(){
        
        createStartDatePicker()
        createEndDatePicker()
        
        self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
        
        let viewTap = UITapGestureRecognizer (target: self , action: #selector(onViewTapped))
         
        self.contentView.addGestureRecognizer( viewTap )
        
        searchButton.layer.cornerRadius = searchButton.frame.height/2
        searchButton.clipsToBounds = true
        searchButton.dropbtnShadow()
        searchButton.applyGradient()
        
        seearchButtonView.layer.cornerRadius = seearchButtonView.frame.height/2
        seearchButtonView.layer.shadowOpacity = 0.8
        seearchButtonView.layer.shadowRadius = 5
        seearchButtonView.layer.shadowOffset = CGSize(width: 0, height: 3 )
        //seearchButtonView.clipsToBounds = false
        //seearchButtonView.layer.masksToBounds = false
        
    }
    
    @objc func onViewTapped(){
        print("View Tapped")
         
     UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil);

    }
    
    //RADIO BUTTONS
    @IBAction func radioButtons( _ sender : UIButton ){
        if sender.tag == 0{ //Sel
            
            rBtnSell.isSelected = true
            rBtnRent.isSelected = false
            rBtnRentOnly.isSelected = false
        }
        else if sender.tag == 1 {//Rent
            
            
            rBtnSell.isSelected = false
            rBtnRent.isSelected = true
            rBtnRentOnly.isSelected = false
        }
        else if sender.tag == 2 {//rent Only

                rBtnSell.isSelected = false
                rBtnRent.isSelected = false
                rBtnRentOnly.isSelected = true
        }
        
    }//radioButtons

}
