//
//  VC_ContactUS.swift
//  Simsar
//
//  Created by Sarmad Malik on 02/07/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import DCToastView

class VC_ContactUS: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var mainImageView : UIImageView!
    @IBOutlet weak var imageLoader : UIActivityIndicatorView!
    @IBOutlet weak var pageTitle : UILabel!
    @IBOutlet weak var titleDescription : UILabel!
    
    var constUrlToFetchImage = "https://simsar.com/public/assets/media/contact/banner/"
    var globalIndicator : UIViewController?
    var alertMsg: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageLoader.isHidden = true
        txtFirstName.borderStyle = .none
        txtLastName.borderStyle = .none
        txtEmail.borderStyle = .none
     txtFirstName.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
     txtLastName.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
     txtEmail.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        
        tvMessage.layer.borderWidth = 0.5
        tvMessage.layer.borderColor = UIColor.lightGray.cgColor
        tvMessage.makeRound(radius: 10)
     
     btnNext.applyGradient()
     btnNext.makeRound(radius: 20)
     viewBtn.clipsToBounds = false
     viewBtn.layer.cornerRadius = 20
     viewBtn.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
     setNavigationBar()
     apiFetchContactUsData()
    }
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
    
    func setNavigationBar() {

        self.navigationItem.setHidesBackButton(true, animated:false)

        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)

        let leftBarButtonItem = UIBarButtonItem(customView: view )
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    @objc func backToMain() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func resignFirst() {
        txtEmail.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtFirstName.resignFirstResponder()
        tvMessage.resignFirstResponder()
    }
    
    
    @IBAction func next(_ sender: Any) {
        sendPostApi()
    }
    

}






//MARK:- Calling Api
extension VC_ContactUS {
    
    func apiFetchContactUsData(){
            
            
           startAnimatingWhileLoadingData()
            var apiParams = Parameter()
            apiParams.dictionary = [
             "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy"
         ]
            var serviceObj = Service(url: "https://simsar.com/api/v1/contact-us" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                
             
             if (result != nil ){
                self.stopAnimationgAsDataLoaded()
                 let decoder = JSONDecoder()
                
                 do {
                 
                     let jObj : JSON = try JSON(data: (result?.data)! )
                     print("\(jObj)")
                    let response = jObj["data"]
                    
                    if jObj["status"].stringValue == "success"  {
                    ServiceManager.shared.downloadImageWithName(
                        self.constUrlToFetchImage
                        , response["banner"].stringValue
                        , self.imageLoader
                        , self.mainImageView
                    )
                    
                    DispatchQueue.main.async {
                        self.pageTitle.text = response["title"].stringValue
                        self.titleDescription.text = response["content"].stringValue
                    }
                
                    } else {  print("Error in response");   return }
                    
//                    {
//                        "status": "success",
//                        "data": {
//                            "id": "1",
//                            "status": "1",
//                            "title": "Contact Us",
//                            "slug": "contact-us",
//                            "content": "Please send email for: Info@simsar.com",
//                            "banner": "1602314698contact-us.jpg",
//                            "created_at": "2020-09-16 09:17:12",
//                            "updated_at": "2020-10-10 07:24:58"
//                        }
//                    }
                    
                    
                    
                 }catch let error {
                     debugPrint("error== \(error.localizedDescription)")
                 }
                    
                }else {
                    print ("An Error")
                }
                
            }//CLOSURE
            
        }//LoginByUserEmail
    
    
}
//ABOUT US
//https://simsar.com/testsimsar/api/v1/common-pages
//client_key === wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy
//page_id === 1

extension VC_ContactUS {
    
    func sendPostApi(){
            
            
           startAnimatingWhileLoadingData()
            var apiParams = Parameter()
            apiParams.dictionary = [
                "firstname" : txtFirstName.text ,
                "lastname" : txtLastName.text,
                "email" :  txtEmail.text,
                "message" : tvMessage.text
         ]
            var serviceObj = Service(url: "https://simsar.com/api/v1/add-contact-us" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                
             
             if (result != nil ){
                self.stopAnimationgAsDataLoaded()
                 let decoder = JSONDecoder()
                
                 do {
                 
                     let jObj : JSON = try JSON(data: (result?.data)! )
                     print("\(jObj)")
                  //  let response = jObj["data"]
                    
                    if jObj["status"].stringValue == "success"  {
                     DispatchQueue.main.async {
                      //  self.pageTitle.text = response["title"].stringValue
                        self.alertMsg = jObj["message"].string
                        //print("*********** \(self.alertMsg)")
                        self.showAlertView(message: self.alertMsg!, title: "Succesfully Added!")
                        
                    }
                
                    } else {
                        print("Error in response");
                        self.showAlertView(message: "Something Is Wrong While Sending", title: "Sorry!")
                        return
                        
                    }
                    

                    
                    
                    
                 }catch let error {
                     debugPrint("error== \(error.localizedDescription)")
                 }
                    
                }else {
                    print ("An Error")
                }
                
            }//CLOSURE
            
        }//LoginByUserEmail
    
    
}
