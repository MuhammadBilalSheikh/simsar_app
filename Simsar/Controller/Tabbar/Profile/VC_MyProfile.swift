//
//  VC_MyProfile.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class section {
    let title : String
    let options : [String]
    var isOpened: Bool = false
    
    init(title: String,
         options: [String],
         isOpened: Bool = false) {
        self.title = title
        self.isOpened = isOpened
        self.options = options
    }
}



class VC_MyProfile: UIViewController {
    
    @IBOutlet weak var tableview_Profile: UITableView!
    
    fileprivate var arrayMyProfile = [My_Profile]()
    
private var sections = [section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
//        sections = [
//            section(title: "Sectio", options: [1,2,3].compactMap({return "Cell \($0)"}))
//        ]
    //   menuArr = [cellData(opened: false, title: "Title", secionData: ["Cell1", "Cell2"])]
        
       
        
        fillProfileArray()
        
        tableview_Profile.delegate = self
        tableview_Profile.dataSource = self
        tableview_Profile.tableFooterView = UIView()
        
        let rightBtn = UIBarButtonItem(image: UIImage(named: "Edit Icon"), style: .plain, target: self, action: #selector(editProfile))
        navigationItem.rightBarButtonItem = rightBtn
        DesignNavBar()
        
    }
    
    @objc func editProfile(){
        print("Pressed")
//        self.performSegue(withIdentifier: "WelcomeFromProfileVC", sender: nil)
//        openVC("VC_EditProfile")
        //openVC("VC_EditProfile")
    }
    
    func DesignNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    
    private func fillProfileArray(){
        
        let a1 = My_Profile(img: "ios icons-2", nam: "My Ads")
        let a2 = My_Profile(img: "ios icons-4", nam: "Chats")
        let a3 = My_Profile(img: "ios icons-3", nam: "My Favourite Ads")
        let a4 = My_Profile(img: "ios icons-5", nam: " Notifications")
        
        let a6 = My_Profile(img: "ios icons-5", nam: " Transaction ")
        let a7 = My_Profile(img: "ios icons-7", nam: "My Bank Accounts ")

        let a9 = My_Profile(img: "ios icons-13", nam: "Buy Services")
        let a10 = My_Profile(img: "ios icons-8", nam: "Boost Your Ads")
       
        let a12 = My_Profile(img: "ios icons-13", nam: "My Coins")
        let a13 = My_Profile(img: "ios icons-15", nam: "Safe Pay")
        let a14 = My_Profile(img: "ios icons-14", nam: "Change Password")
        let a15 = My_Profile(img: "ios icons-9", nam: "Settings")
        let a16 = My_Profile(img: "ios icons-12", nam: "My Wallet")
        let a17 = My_Profile(img: "ios icons-13", nam: "My Deposits")
        let a18 = My_Profile(img: "ios icons-12", nam: "Withdraw")
        let a19 = My_Profile(img: "ios icons-2", nam: "Coin Transaction")
        let a20 = My_Profile(img: "ios icons-2", nam: "Wallet Transaction")
        let a21 = My_Profile(img: "ios icons-11", nam: "My Purchases")
       
       
       
        
        arrayMyProfile.append(a1)
        arrayMyProfile.append(a2)
        arrayMyProfile.append(a3)
        arrayMyProfile.append(a4)
        arrayMyProfile.append(a6)
        arrayMyProfile.append(a7)
        arrayMyProfile.append(a9)
        arrayMyProfile.append(a10)
        arrayMyProfile.append(a12)
        arrayMyProfile.append(a13)
        arrayMyProfile.append(a14)
        arrayMyProfile.append(a15)
        arrayMyProfile.append(a16)
        arrayMyProfile.append(a17)
        arrayMyProfile.append(a18)
        arrayMyProfile.append(a19)
        arrayMyProfile.append(a20)
        arrayMyProfile.append(a21)
        
    }
    
    
    
}
extension VC_MyProfile : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMyProfile.count
//        let section = sections[section]
//        if section.isOpened  {
//            return section.options.count + 1
//        } else {
//            return arrayMyProfile.count
//        }
        
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0 {
//            return UITableViewCell()
//
//        }
//
        let Cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyProfile_Cell
        
     //   Cell.lblName.text = sections[indexPath.row].title
        Cell.lblName.text = arrayMyProfile[indexPath.row].name
       Cell.viewImage.image = UIImage(named: arrayMyProfile[indexPath.row].image)
        return Cell
       
     
    }
      
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   sections[indexPath.section].isOpened = !sections[indexPath.section].isOpened
       //tableview_Profile.reloadSections([indexPath.section], with: .none)
       
        if indexPath.row == 2 {
            //openVC("TabbarController_Wallet")
            let nextVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarController_Wallet")
            nextVC.modalPresentationStyle = .overFullScreen
            present(nextVC, animated: true, completion: nil)
        }
        if indexPath.row == 0 {
            openVC("VC_ChoosePlan")
        }
        if indexPath.row == 1 {
            openVC("VC_ChatsCards")
        }

        if indexPath.row == 3 {
            openMyPurchasesVC("NotificationVC")
        }
        if indexPath.row == 5 {
           openSecondVC("VC_MyBankAccount")
        }
        if indexPath.row == 6 {
            openVC("VC_SearchFeatured")
        }
        if indexPath.row == 7 {
            openVC("VC_ShopFeatured")
        }
        if indexPath.row == 10 {
            openVC("VC_ChangePassword")
        }
        if indexPath.row == 12 {
            openVC("VC_WalletProcessing")
        }
        if indexPath.row == 9 {
            openVC("VC_CoinWallet")
        }
        if indexPath.row == 11 {
            openVC("SettingVCC")
        }
        if indexPath.row == 14 {
            openVC("VC_WalletFunds")
        }
        if indexPath.row == 13 {
            openVC("VC_DepositFunds")
        }
        if indexPath.row == 15 {
            openSecondVC("VC_MyClanWalletTransaction")
        }
        if indexPath.row == 16 {
            openSecondVC("MyWalletTransactionVC")
        }
        if indexPath.row == 17 {
            openMyPurchasesVC("MyPurchasesVC")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}

class My_Profile : NSObject {
    
    var image = ""
    var name  = ""
    
    init(img : String , nam : String) {
        self.image = img
        self.name = nam
    }
    
}
