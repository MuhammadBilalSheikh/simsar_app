//
//  VC_EditProfile.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_EditProfile: UIViewController {
    
    @IBOutlet var txtLinkedinLink: UITextField!
    @IBOutlet var txtLongitude: UITextField!
    @IBOutlet var txtLatitude: UITextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtTwitterLink: UITextField!
    @IBOutlet var txtEmployeementStatus: UITextField!
    @IBOutlet var txtCnic: UITextField!
    @IBOutlet var txtMap: UITextField!
    @IBOutlet var txtDateOfBirth: UITextField!
    @IBOutlet var txtFacebookLink: UITextField!
    @IBOutlet var txtPlaceOfBirth: UITextField!
    @IBOutlet var txtFatherName: UITextField!
    @IBOutlet var txtBio: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtUniqueID: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet var txtState: UITextField!
    
    @IBOutlet var imgMain: UIImageView!
    var userProfileData:UserProfileData?
    var userId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        imgMain.contentMode = .scaleAspectFill
        imgMain.layer.cornerRadius = 20
        
        setTextFields()

        setNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserProfileData()
    }
    
    
    func getUserProfileData(){
        GetUserProfileService.shared.getUserProfileDataResponse { (responce, error) in
            if responce != nil{
                self.userProfileData = responce!
                print("DATA********* \(self.userProfileData!)")
                DispatchQueue.main.async {
                    self.updateContent()
                }
                
            }
        }
       
    }
    
    
    
    func apiCalledForPostEditProfile(){
        
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
            "user_id"      :  self.userId!  ,
            ""   :    txtMap.text!  ?? "",
            "" : txtBio.text!  ?? "",
            "" : txtCnic.text! ?? "",
            "" : txtName.text!  ?? "",
            "" : txtCity.text!  ?? "",
            "" : txtEmail.text!  ?? "",
            "" : txtState.text!  ?? "",
            "" : txtGender.text!  ?? "",
            "" : txtCountry.text!  ?? "",
            "" : txtAddress.text!  ?? "",
            "" : txtLatitude.text!  ?? "",
            "" : txtLongitude.text!  ?? "",
            "" : txtFatherName.text! ?? "",
            "" : txtTwitterLink.text!  ?? "",
            "" : txtPhoneNumber.text!  ?? "",
            "" : txtLinkedinLink.text!  ?? "",
            "" : txtFacebookLink.text!  ?? "",
            "" : txtEmployeementStatus.text!  ?? "",
            "" : txtDateOfBirth.text!  ?? "",
            "" : txtPlaceOfBirth.text!  ?? ""
        ]
        print("PARAMS*** \(apiParams)")
        let serviceObj = Service(url: "\(Constant.base_url)\(Constant.eEdirProfilePost)" , parameters: apiParams )
        serviceObj.headers = Constant.default_header
    
        ServiceManager.shared.request(service: serviceObj, model: UserProfileStruct.self) { (result, error) in
            if result != nil
            {
                DispatchQueue.main.async {
                    let loginResult = result!
                    if result?.status == "success"{
//                        print(loginResult.user!.id)
//                        let userId = loginResult.user!.id
                        /*storing user id in userDefault*/
                        UserDefaults.standard.set(self.userId, forKey: Constant.userDefaultUserId)
                      //  self.closingAnimation()
                        self.showAlertView(message: (result?.message!)! , title: "Successed", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.openVC("HomeVC")
                        }))
                       // self.navigationController?.popViewController(animated: true)
                       // self.tabBarController?.selectedIndex =  0
                    }else{
                        self.showAlertView(message: result?.message ?? "Please Filled All fields" , title: "Error")
                        self.showAlertView(message: "", title: result?.status! ?? "")
                    }
                }
            
            }else{
             
                self.showAlertView(message: "", title: result?.status ?? error?.localizedDescription ?? "")
                print(error ?? "")
            
            }
          
        }
    }//LoginByUserEmail
    
    
    
    
    func updateContent(){
     
        txtName.text = userProfileData?.name  ?? "No Name Found"
        txtPhoneNumber.text = userProfileData?.phone ?? "No phone Found"
        txtEmail.text = userProfileData?.email ?? "No email Found"
        txtAddress.text =  userProfileData?.address ?? "No address Found"
        txtFatherName.text =  userProfileData?.fatherName ?? "No address Found"
        txtCnic.text =  userProfileData?.cnic ?? "No address Found"
        txtDateOfBirth.text =  userProfileData?.dateOfBirth ?? "No address Found"
        txtPlaceOfBirth.text =  userProfileData?.placeOfBirth ?? "No address Found"
        txtEmployeementStatus.text =  userProfileData?.employmentStatus ?? "No address Found"
        txtGender.text =  userProfileData?.gender ?? "No address Found"
        txtCountry.text =  userProfileData?.country ?? "No address Found"
        txtCity.text =  userProfileData?.city ?? "No address Found"
       txtState.text =  userProfileData?.state ?? "No address Found"
        txtBio.text =  userProfileData?.bio ?? "No address Found"
        txtFacebookLink.text =  userProfileData?.facebookLink ?? "No address Found"
        txtLinkedinLink.text =  userProfileData?.linkedInLink ?? "No address Found"
        txtTwitterLink.text = userProfileData?.twitterLink ?? "No Data Found"
        
        
        if ((userProfileData?.profilePic?.isEmpty) != nil){
            
            let activityINdicator = UIActivityIndicatorView()
            ServiceManager.shared.downloadImageWithName(Constant.baserUrlImage, (userProfileData?.profilePic)!, activityINdicator, imgMain )
        }
        txtName.text = userProfileData?.name
 
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        self.navigationItem.setHidesBackButton(true, animated:false)

        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)

        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        let rightBTn = UIBarButtonItem(image: UIImage(named: "Check Icon"), style: .plain, target: self, action: #selector(DoneEditing))
        self.navigationItem.rightBarButtonItem = rightBTn
        
    }

    @objc func backToMain() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func DoneEditing(){
         self.navigationController?.popViewController(animated: true)
    }

    @IBAction func imageUpload(_ sender: Any) {
        
        let imageController = UIImagePickerController()
        imageController.delegate = self
        imageController.allowsEditing = true
        imageController.sourceType = .photoLibrary
        present(imageController, animated: true, completion: nil)
//        self.showImagePickerControllerActionSheet()
    }
    
    
    func apiPostForEditProfile() {
        
    }
    
    
    func setTextFields() {
        
        txtName.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtUniqueID.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtCity.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtGender.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtEmail.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtPhoneNumber.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtAddress.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtLongitude.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtLatitude.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtCountry.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtTwitterLink.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtEmployeementStatus.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtCnic.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtMap.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtDateOfBirth.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtFacebookLink.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtPlaceOfBirth.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtFatherName.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtBio.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtLinkedinLink.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        txtState.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 0.5)
        
        
    }
    

    
}

extension VC_EditProfile: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imageController = UIImagePickerController()
        imageController.delegate = self
        imageController.allowsEditing = true
        imageController.sourceType = sourceType
        present(imageController, animated: true, completion: nil)
    }
    
    
    
    func showImagePickerControllerActionSheet() {
        let photoLibraryAction = UIAlertAction(title: "Choose From Library", style: .default) { (action) in
            self.showImagePickerController(sourceType: .photoLibrary)
        }
        
        let cameraAction = UIAlertAction(title: "Choose From Camera", style: .default) { (action) in
            self.showImagePickerController(sourceType: .camera  )
        }
        let cancelAction = UIAlertAction(title: "Cncel", style: .cancel, handler: nil)
        //self.showAlertView(message: "", title: "Choose Your Image", action: [photoLibraryAction, cameraAction, cancelAction])
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("Info***** \(info)")
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        imgMain.image = image
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
