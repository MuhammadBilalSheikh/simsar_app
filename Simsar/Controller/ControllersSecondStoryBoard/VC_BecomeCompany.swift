//
//  VC_BecomeCompany.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 19/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_BecomeCompany: UIViewController {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgViewSix: UIImageView!
    @IBOutlet weak var imgViewFive: UIImageView!
    @IBOutlet weak var imgViewFour: UIImageView!
    @IBOutlet weak var imgViewThree: UIImageView!
    @IBOutlet weak var imgViewTwo: UIImageView!
    @IBOutlet weak var imgViewOne: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        DesignNavItemBarButton()
        btnSubmit.applyGradient()
        btnSubmit.makeRound(radius: 20)
        //viewBtn.clipsToBounds = false
      //  viewBtn.layer.cornerRadius = 20
      //  viewBtn.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        
        addBorder(image: imgViewOne)
        addBorder(image: imgViewTwo)
        addBorder(image: imgViewThree)
        addBorder(image: imgViewFour)
        addBorder(image: imgViewFive)
        addBorder(image: imgViewSix)

        
    }
    
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
           btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
           btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
           btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButton(item1, animated: true)

    
}
    
    @IBAction func btnSubmitAction(_ sender: Any) {
    }
    

    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }


    func addBorder(image : UIImageView){
        
        image.layer.borderWidth = 0.5
        image.layer.borderColor = Constant.themeColor.cgColor
        image.makeRound(radius: 10)
        
    }

}
