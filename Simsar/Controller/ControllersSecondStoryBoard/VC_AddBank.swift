//
//  VC_AddBank.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 20/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON

class VC_AddBank: UIViewController {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtSwiftCode: UITextField!
    @IBOutlet weak var txtBnkAddress: UITextField!
    @IBOutlet weak var txtBnkName: UILabel!
    @IBOutlet weak var txtIBANNum: UITextField!
    @IBOutlet weak var txtBrCode: UITextField!
    @IBOutlet weak var txtAcntNum: UITextField!
    @IBOutlet weak var txtAcntTitle: UITextField!
    
    var globalIndicator : UIViewController?
    var userId = 471
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Api()
        DesignNavItemBarButton()
        btnSubmit.applyGradient()
        btnSubmit.makeRound(radius: 20)

        
    }
    
    
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
           btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
           btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
           btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
           let item1 = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButton(item1, animated: true)
    }
        
        @objc func actionBack() {
            self.navigationController?.popViewController(animated: true)
        }
    
    
    
    
    func Api() {
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "user_id" : userId,
                                "name" : txtBnkName.text ?? "",
                                "ac_number" : txtAcntNum.text ?? "",
                                "ac_title" : txtAcntTitle.text ?? "",
                                "branch_code" : txtBrCode.text ?? "",
                                "branch_address" : txtBnkAddress.text ?? "",
                                "swift_code" : txtSwiftCode.text ?? "",
                                "iban_no" : txtIBANNum.text ?? ""]
        
        var serviceObj = Service(url: "https://simsar.com/api/v1/add-bank" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
             //   self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let statusOfRequest = jObj["status"]
                    let messageFromServer = jObj["message"]
                    
                    if ( statusOfRequest == "success") {
                        print("Bank Added")
                    }else{
                        print("Request \(statusOfRequest)")
                    }
                    
                    

         //           }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                //    self.MyBankCollectionView.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    
    
    

    @IBAction func btnSubmitAction(_ sender: Any) {
        Api()
    }
    

}
