//
//  VC_LinkToSecondStoryboard.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 20/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_LinkToSecondStoryboard: UIViewController {

    @IBOutlet weak var btnLinked: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    @available(iOS 13.0, *)
    @IBAction func btnAction(_ sender: Any) {
        
//        print ("Main 2 Storyboard Called")
//        let sbXYZ = UIStoryboard(name: "mainTwo", bundle: nil)
//        let VC = sbXYZ.instantiateViewController(withIdentifier: "VC_SafePaymentRecieved") as! VC_SafePaymentRecieved
//        //self.present(VC, animated: true, completion: nil)
//        VC.modalPresentationStyle = .fullScreen
//        present(VC , animated: true , completion: nil )
        //self.navigationController?.pushViewController(VC, animated: true)
        
        
        let storyboard = UIStoryboard(name: "mainTwo", bundle: nil)
      let controller  = storyboard.instantiateViewController(withIdentifier: "VC_ContactDetail")
    // controller.modalPresentationStyle = .fullScreen
      //  self.present(controller, animated: true, completion: nil)
       // let VC = storyboard?.instantiateViewController(identifier: "VC_MyBankAccount") as! VC_MyBankAccount
        self.navigationController?.pushViewController(controller, animated: true)
      //  self.present(VC, animated: true, completion: nil)
    }
    

    

}
