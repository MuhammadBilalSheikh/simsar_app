//
//  VC_MyBankAccount.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 21/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON

class VC_MyBankAccount: UIViewController {
    
  
    var  globalIndicator : UIViewController? = nil
    
    var dataFromServer : [myBnkAccount] = []
    var arrImg = [UIImage]()

    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var MyBankCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Api()
        DesignNavItemBarButton()
        MyBankCollectionView.dataSource = self
        MyBankCollectionView.delegate = self
        DesignNavItemBarButton()
       

        
    }
    
    
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
           btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
           btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
           btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
           let item1 = UIBarButtonItem(customView: btnBack)

           let btn2 = UIButton(type: .custom)
           btn2.setImage(UIImage(named: "ad.-button"), for: .normal)
           btn2.frame = CGRect(x: 5, y: 5, width: 5, height: 5)
       // btn2.applyGradient()
      //  btn2.self.applyGradient()
       // btn2.roundCorners(corners: .allCorners, radius: 15)
       // btn2.setTitleColor(.white, for: .normal)
          btn2.addTarget (self, action: #selector(actionAddBank), for: .touchUpInside)
         let item2 = UIBarButtonItem(customView: btn2)

        self.navigationItem.setRightBarButton(item2, animated: true)
        self.navigationItem.setLeftBarButton(item1, animated: true)
    
    }
    
   
    
    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func actionAddBank() {
        print("Pressed")
        //self.openSecondVC("VC_AddBank")
    }
    
    
    func Api() {
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "user_id" : 471]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/my-bank" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
             //   self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let Data = jObj["data"]
                    print("categories ==== \(Data)" )

                    for countOfObjectsInArray in 0..<Data.count {

                        print ("Loop For Time === \(countOfObjectsInArray)")


                        let singleObj  = Data[countOfObjectsInArray]// may be error in this
                        let obj = myBnkAccount(id: singleObj["id"].intValue, accountTitle: singleObj["name"].stringValue, accountNo: singleObj["ac_number"].stringValue, bankName: singleObj["ac_title"].stringValue, bankAddress: singleObj["branch_address"].stringValue, ibanNo: singleObj["iban_no"].stringValue, branchCode: singleObj["branch_code"].stringValue, swiftCode: singleObj["swift_code"].stringValue)
                            
                            
                            
                     
            
                        
                            
                            

                        self.dataFromServer.append( obj )


                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.MyBankCollectionView.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    
    
   

}

extension VC_MyBankAccount: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    //No Of Cells
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataFromServer.count
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        //openVC("VC_MyLocation")
//    }
    
    //Item Select At
    
    //SIZE OF CELL
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let padding : CGFloat = 30
//        //POPULAR COLLECTION VIEW -> HORIZONTAL
//        if collectionView == MyBankCollectionView {
//
//            let height = collectionView.frame.height - padding
//            return CGSize ( width: height , height: height )
//
//        }else {
//
//            print("Collection View Lower Cell Size")
//            let width  = collectionView.frame.width - padding - 10
//            return CGSize( width : width/2  , height: width/2 )
//
//
//        }
//            //EARLIER CODE
//
//           //let cellWidth = 80
//           //let cellHeight = 100
//           //return CGSize(width: cellWidth, height: cellHeight)
//       }
       
       
    
    //LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 25
       }
    
    //INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    
    //EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    //FOR CELL AT ROW
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      //  if collectionView == myLocatioCollectionView{

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyBankAccountCell", for: indexPath) as! MyBankAccountCell

       cell.contentView.layer.cornerRadius = 15
       cell.contentView.layer.borderWidth = 1.0
       cell.contentView.layer.borderColor = UIColor.clear.cgColor
       cell.contentView.layer.masksToBounds = true

        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 10 / 2.0
        cell.layer.shadowOpacity = 0.3
        cell.layer.cornerRadius = 15
        cell.layer.masksToBounds = false
        
        cell.lblAcntTitle.text = dataFromServer[indexPath.row].accountTitle
        cell.lblIBANNo.text = dataFromServer[indexPath.row].ibanNo
        cell.lblAcntNo.text = dataFromServer[indexPath.row].accountNo
        cell.lblBankName.text =  dataFromServer[indexPath.row].bankName
        cell.lblBankAddress.text = dataFromServer[indexPath.row].bankAddress
        cell.lblSwiftCode.text = dataFromServer[indexPath.row].swiftCode
        cell.lblBranchCode.text = dataFromServer[indexPath.row].branchCode
        


        return cell
//        }else{
           
           
            //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyLocationCell", for: indexPath)
//
//            cell.contentView.layer.cornerRadius = 15
//            cell.contentView.layer.borderWidth = 1.0
//            cell.contentView.layer.borderColor = UIColor.clear.cgColor
//            cell.contentView.layer.masksToBounds = true
//
//            cell.layer.shadowColor = UIColor.gray.cgColor
//            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
//            cell.layer.shadowRadius = 10 / 2.0
//            cell.layer.shadowOpacity = 0.3
//            cell.layer.cornerRadius = 15
//            cell.layer.masksToBounds = false

             
           //  return cell
            
        }
    
   
    
    
   
}
    

