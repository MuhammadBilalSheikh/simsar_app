//
//  VC_SafePaymentRecieved.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 28/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_SafePaymentRecieved: UIViewController {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionViewRecieved: UICollectionView!
    
    var cellId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DesignNavItemBarButton()
       print("Called")
    }
    
    
    
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
           btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
           btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
           btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
           let item1 = UIBarButtonItem(customView: btnBack)
        
        
        let btnSearch = UIButton(type: .custom)
        btnSearch.setImage(UIImage(named: "search-icons"), for: .normal)
    
        btnSearch.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
        btnSearch.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
           let item2 = UIBarButtonItem(customView: btnSearch)

//           let btn2 = UIButton(type: .custom)
//          // btn2.setImage(UIImage(named: "imagename"), for: .normal)
//           btn2.frame = CGRect(x: 0, y: 0, width: 120, height: 40)
//        //btn2.setImage(UIImage(named: "back-icon"), for: .normal)
//        btn2.setTitle("ADD", for: .normal)
//        btn2.applyGradient()
//      //  btn2.self.applyGradient()
//        btn2.roundCorners(corners: .allCorners, radius: 15)
//        btn2.setTitleColor(.white, for: .normal)
//           btn2.addTarget (self, action: #selector(Action), for: .touchUpInside)
//           let item2 = UIBarButtonItem(customView: btn2)

        self.navigationItem.setLeftBarButton(item1, animated: true)
       self.navigationItem.setRightBarButton(item2, animated: true)
    
    }
    
    @objc func Action() {
        print("Favourite Tap")
    }

    @objc func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    

}

extension VC_SafePaymentRecieved: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3 * 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cellId == 0  {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MySafePayRecievedCell", for: indexPath) as! MySafePayRecievedCell
            cellId = 1
            return cell
        }else if cellId == 1 {
            let cellTwo = collectionView.dequeueReusableCell(withReuseIdentifier: "MySafePayRecievedCellTwo", for: indexPath) as? MySafePayRecievedCellTwo
            cellId = 2
            return cellTwo!
        }else if cellId == 2{

        let cellTwo = collectionView.dequeueReusableCell(withReuseIdentifier: "MySafePayRecievedCellDate", for: indexPath) as? MySafePayRecievedCellDate
        cellId = 0
        return cellTwo!
        }else{
            let cellTwo = collectionView.dequeueReusableCell(withReuseIdentifier: "MySafePayRecievedCell", for: indexPath) as? MySafePayRecievedCell
            cellId = 1
            return cellTwo!
        }
        
    }
    
    //LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10
       }
    
    //INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
        
}
