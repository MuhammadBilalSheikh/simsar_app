//
//  CodeVerfVCPopUp.swift
//  Simsar
//
//  Created by NxGeN on 1/29/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import OTPFieldView

class CodeVerfVCPopUp: UIViewController {

    @IBOutlet var backView: UIView!
//    @IBOutlet var txtField6: UITextField!
//    @IBOutlet var txtField5: UITextField!
//    @IBOutlet var txtField4: UITextField!
//    @IBOutlet var txtField3: UITextField!
//    @IBOutlet var txtField2: UITextField!
//    @IBOutlet var txtField1: UITextField!
    @IBOutlet var btnVerify: UIButton!
    
    var globalIndicator : UIViewController?
    var userId: Int!
    var alertMsg: String?
    var phoneNum: Int!
    var verfCode: String!
    var Count = 0
    
    @IBOutlet var otpTextFieldView: OTPFieldView!
    override func viewDidLoad() {
       // txtField1.textContentType = .oneTimeCode
//        self.txtField1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//        self.txtField1.becomeFirstResponder()
        
        
        print("++UserID***** \(self.userId)")
        super.viewDidLoad()
        self.userId = Model_UserStatus.shared.getUserId()
        self.backView.backgroundColor = UIColor.init(white:0.4, alpha: 0.8)
        btnVerify.applyGradient()
        btnVerify.makeRound(radius: 15)
        setupOtpView()
        print("++PhoneNum \(self.phoneNum)")

    }
    private func setupOtpView(){
            self.otpTextFieldView.fieldsCount = 6
            self.otpTextFieldView.fieldBorderWidth = 2
            self.otpTextFieldView.defaultBorderColor = UIColor.black
            self.otpTextFieldView.filledBorderColor = UIColor.green
            self.otpTextFieldView.cursorColor = UIColor.red
            self.otpTextFieldView.displayType = .underlinedBottom
            self.otpTextFieldView.fieldSize = 40
            self.otpTextFieldView.separatorSpace = 8
            self.otpTextFieldView.shouldAllowIntermediateEditing = false
            self.otpTextFieldView.delegate = self
            self.otpTextFieldView.initializeUI()
        }
//    @objc func textFieldDidChange(_ textField: UITextField) {
//       if #available(iOS 12.0, *) {
//           if textField.textContentType == UITextContentType.oneTimeCode{
//               //here split the text to your four text fields
//            if let otpCode = textField.text, otpCode.count >= 5 {
//                   txtField1.text = String(otpCode[otpCode.index(otpCode.startIndex, offsetBy: 0)])
//                   txtField2.text = String(otpCode[otpCode.index(otpCode.startIndex, offsetBy: 1)])
//                   txtField3.text = String(otpCode[otpCode.index(otpCode.startIndex, offsetBy: 2)])
//                   txtField4.text = String(otpCode[otpCode.index(otpCode.startIndex, offsetBy: 3)])
//                   txtField5.text = String(otpCode[otpCode.index(otpCode.startIndex, offsetBy: 4)])
//                   txtField6.text = String(otpCode[otpCode.index(otpCode.startIndex, offsetBy: 5)])
//               }
//           }
//        }
//     }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//          if (string.count == 1){
//              if textField == txtField1 {
//                txtField2?.becomeFirstResponder()
//              }
//              if textField == txtField2 {
//                txtField3?.becomeFirstResponder()
//              }
//              if textField == txtField3 {
//                txtField4?.becomeFirstResponder()
//              }
//              if textField == txtField4 {
//                txtField5?.resignFirstResponder()
//
//              }
//            if textField == txtField5 {
//                txtField6?.resignFirstResponder()
//            }
//            if textField == txtField6 {
//                txtField6.resignFirstResponder()
//                textField.text? = string
//              //  apiCalled()   //APICall Verify OTP
//                  //Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.VerifyOTPAPI), userInfo: nil, repeats: false)
//              }
//              textField.text? = string
//              return false
//          }else{
//              if textField == txtField1 {
//                txtField1?.becomeFirstResponder()
//              }
//              if textField == txtField2 {
//                txtField1?.becomeFirstResponder()
//              }
//              if textField == txtField3 {
//                txtField2?.becomeFirstResponder()
//              }
//              if textField == txtField4 {
//                txtField3?.becomeFirstResponder()
//              }
//
//            if textField == txtField5 {
//                txtField4?.resignFirstResponder()
//            }
//
//            if textField == txtField6 {
//                txtField5?.resignFirstResponder()
//            }
//             textField.text? = string
//              return false
//          }
//        return true
//
//      }
    
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
    
    //MARK:- CHECKING FIELDS IS EMPTY
//    func checkField() -> Bool {
//        if ( txtField1.text!.isEmpty) {
//            self.showAlertView(message: "Please Fill All Fields", title: "Field Can't be Empty")
//            return false
//        }
//
//        if ( txtField2.text!.isEmpty) {
//            self.showAlertView(message: "Please Fill All Fields", title: "Field Can't be Empty")
//            return false
//
//        }
//
//        if ( txtField3.text!.isEmpty) {
//            self.showAlertView(message: "Please Fill All Fields", title: "Field Can't be Empty")
//            return false
//
//        }
//
//        if ( txtField4.text!.isEmpty) {
//            self.showAlertView(message: "Please Fill All Fields", title: "Field Can't be Empty")
//            return false
//
//        }
//
//        if ( txtField5.text!.isEmpty) {
//            self.showAlertView(message: "Please Fill All Fields", title: "Field Can't be Empty")
//            return false
//
//        }
//
//        if ( txtField6.text!.isEmpty) {
//            self.showAlertView(message: "Please Fill All Fields", title: "Field Can't be Empty")
//            return false
//
//        }
//
//        return true
//    }
    
    @available(iOS 13.0, *)
    func apiCalled() {
        
        startAnimatingWhileLoadingData()
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
         var apiParams = Parameter()
         apiParams.dictionary = [
            "client_key": Constant.client_id,
            "user_id" : userId! ,
            "phone" :  phoneNum!   ,
            "verification_code": verfCode! ,
            
            
      ]
        print("++Params Verificatiom \(apiParams)")
        print("+VERCODE***** \(self.verfCode!)")
         var serviceObj = Service(url: "https://simsar.com/api/v1/sellnow-number-verification" , parameters: apiParams )
        
         serviceObj.headers = ["Content-Type" : "application/json"]
         ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
          if (result != nil ){
             self.stopAnimationgAsDataLoaded()
              let decoder = JSONDecoder()
             
              do {
              
                  let jObj : JSON = try JSON(data: (result?.data)! )
                  print("\(jObj)")
               //  let response = jObj["data"]
                 
                 if jObj["status"].stringValue == "success"  {
                  DispatchQueue.main.async {
                   //  self.pageTitle.text = response["title"].stringValue
                     self.alertMsg = jObj["message"].string
                     //print("*********** \(self.alertMsg)")
                    self.showAlertView(message: self.alertMsg!, title: "Succesed", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                        self.openSellNowVC("VC_ContactDetail")
                        //self.navigateToContactDetail()
                    }))
                     //self.showAlertView(message: self.alertMsg!, title: "Successed!")
                   // self.openSellNowVC("VC_ContactDetail")
                     
                 }
             
                 } else {
                     print("Error in response");
                    self.showAlertView(message: self.alertMsg!, title: "Sorry!")
                     return
                     
                 }
              }catch let error {
                  debugPrint("error== \(error.localizedDescription)")
              }
                 
             }else {
                 print ("An Error")
             }
         }//CLOSURE
     }//LoginByUserEmail
    
    
    
    @available(iOS 13.0, *)
    func navigateToContactDetail() {
    let VC = self.storyboard?.instantiateViewController(identifier: "VC_ContactDetail")
    
    
      self.navigationController?.pushViewController(VC!, animated: true)
    }
    
    

   
//    @available(iOS 13.0, *)
    @available(iOS 13.0, *)
    @IBAction func btnVerifyAct(_ sender: Any) {
        let string : String = verfCode
        let otpNumberArray = string.map { String($0) }
        if otpNumberArray.count == 6 {
    
            apiCalled()

        } else {
            self.showAlertView(message: "Plese enter code", title: "Incomplete")
        }
    }
}


extension CodeVerfVCPopUp: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
       
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        print(index)
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        verfCode = otpString
        
    }
}


    
    
    
