//
//  MyLocationCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 21/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class MyLocationCell: UICollectionViewCell {
    
    @IBOutlet weak var imgRightBottom: UIImageView!
    @IBOutlet weak var imgRightTop: UIImageView!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    
    
    override func awakeFromNib() {
    
        containerView.makeRound(radius: 10)
    }
}


