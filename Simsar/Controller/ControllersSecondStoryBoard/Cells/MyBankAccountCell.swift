//
//  MyBankAccountCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 21/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
class MyBankAccountCell: UICollectionViewCell {
    
    @IBOutlet var lblSwiftCode: UILabel!
    @IBOutlet var lblBranchCode: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgRightTop: UIImageView!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var lblIBANNo: UILabel!
    @IBOutlet weak var lblBankAddress: UILabel!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblAcntNo: UILabel!
    @IBOutlet weak var lblAcntTitle: UILabel!
    
    var  globalIndicator : UIViewController? = nil
    
    
    
    override func awakeFromNib() {
    
        containerView.makeRound(radius: 10)
    }
    
    func ApiDeleteBank() {
        var  globalIndicator : UIViewController? = nil
        
    
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "bank_id" : 64]
        
        var serviceObj = Service(url: "https://simsar.com/api/v1/delete-bank" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
             //   self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let statusOfRequest = jObj["status"]
                    let messageFromServer = jObj["message"]
                    
                    if ( statusOfRequest == "success") {
                        print("Bank Deleted")
                    }else{
                        print("Request \(statusOfRequest)")
                    }
                    
                    

         //           }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                //    self.MyBankCollectionView.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    @IBAction func btnEditAct(_ sender: Any) {
    
    }
    
    @IBAction func btnDelAct(_ sender: UIButton) {
        ApiDeleteBank()
    }
}


