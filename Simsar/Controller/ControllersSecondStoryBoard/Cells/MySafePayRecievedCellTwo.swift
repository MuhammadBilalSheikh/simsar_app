//
//  MySafePayRecievedCellTwo.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 28/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class MySafePayRecievedCellTwo: UICollectionViewCell {
    
    @IBOutlet weak var imgLblRight: UILabel!
    @IBOutlet weak var imgRight: UIImageView!
    @IBOutlet weak var imgViewRight: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblstatusField: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAdTitleField: UILabel!
    @IBOutlet weak var lblAdTitle: UILabel!
    @IBOutlet weak var lblAdIdField: UILabel!
    @IBOutlet weak var lblAdId: UILabel!
    @IBOutlet weak var lblUserNameField: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblNameUserIdField: UILabel!
    @IBOutlet weak var lblNameUserId: UILabel!
    @IBOutlet weak var lblNameTransField: UILabel!
    @IBOutlet weak var lblNameTrans: UILabel!
    @IBOutlet weak var imgLeft: UIImageView!
}
