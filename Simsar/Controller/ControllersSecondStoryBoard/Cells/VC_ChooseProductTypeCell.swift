//
//  VC_ChooseProductTypeCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 25/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_ChooseProductTypeCell: UICollectionViewCell {
    
   
    @IBOutlet weak var btn: UIButton!
  
    var label = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()

        updateButton()

    }
    
    func isSelected(_ selected:Bool){
        if selected{
            btn.backgroundColor = UIColor.PrimaryColor
        }else{
            btn.backgroundColor = UIColor.white
        }
    }
    func updateButton() {
       // addButtonBorders(v: btn)
        //btn.applyGradient()
        btn.makeRound(radius: 15)
        //btnView.makeRound(radius: 15)
      btn.makeBorder(size: 3)
    
        
    }
    

    
    
    
}
