//
//  SelectCategoryCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 23/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class SelectCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var checkMarkImg: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
 
    
    
    func toggleSelected(isSelected: Bool)
        {
        if (isSelected){
           // backgroundColor = UIColor.red
             checkMarkImg.image = UIImage(named: "done")
            }else {
                
                backgroundColor = UIColor.white
                checkMarkImg.isHidden = true
            }
        }

    
}



