//
//  ChooseBrandCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 25/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class ChooseBrandCell: UICollectionViewCell {
    
    @IBOutlet weak var btn: UIButton!
   
    
    override func awakeFromNib() {
        updateButton()
       
    }
    
    func isSelected(_ selected:Bool){
        if selected{
            btn.backgroundColor = UIColor.PrimaryColor
        }else{
            btn.backgroundColor = UIColor.white
        }
    }
    
    func updateButton() {
       // addButtonBorders(v: btn)
       // btn.applyGradient()
        btn.makeBorder(size: 3)
        btn.makeRound(radius: 15)
       
        
    }
    
    
}
