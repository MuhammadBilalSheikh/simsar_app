//
//  AllCategories_Cell.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class AllCategories_Cell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
}
