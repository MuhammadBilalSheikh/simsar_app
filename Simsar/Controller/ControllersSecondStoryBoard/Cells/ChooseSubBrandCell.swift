//
//  ChooseSubBrandCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 26/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class ChooseSubBrandCell: UICollectionViewCell {
    
    @IBOutlet weak var btnSubBrand: UIButton!
    
    
    
    override func awakeFromNib() {
        updateButton()
        
    }
    func isSelected(_ selected:Bool){
        if selected{
            btnSubBrand.backgroundColor = UIColor.PrimaryColor
        }else{
            btnSubBrand.backgroundColor = UIColor.white
        }
        
    }
    
    func updateButton() {
       // addButtonBorders(v: btn)
      //  btnSubBrand.applyGradient()
        btnSubBrand.makeRound(radius: 15)
        btnSubBrand.makeBorder(size: 3)
        
       
    
        
    }
    
    
    
}
