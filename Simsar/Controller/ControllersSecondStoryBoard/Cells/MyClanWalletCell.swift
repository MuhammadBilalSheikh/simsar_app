//
//  MyClanWalletCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 23/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class MyClanWalletCell: UICollectionViewCell {
    
    @IBOutlet weak var lblCountRight: UILabel!
    
    @IBOutlet weak var lblTypeField: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDealPlanField: UILabel!
    @IBOutlet weak var lblDealPlan: UILabel!
    @IBOutlet weak var lbltransIdField: UILabel!
    @IBOutlet weak var lbltransId: UILabel!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    
    
    override func awakeFromNib() {
        let radius: CGFloat = 10
        containerView.makeRound(radius: 10)
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
    }
    
    
    
}
