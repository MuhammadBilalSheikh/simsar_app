//
//  MySafePayRecievedCell.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 28/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class MySafePayRecievedCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var lblNameTrans: UILabel!
    @IBOutlet weak var lblTransField: UILabel!
    @IBOutlet weak var lblNameUsrIdField: UILabel!
    
    @IBOutlet weak var imgLblRightBottom: UILabel!
    @IBOutlet weak var imgRightBottom: UIImageView!
    @IBOutlet weak var imgViewRightBottom: UIView!
    @IBOutlet weak var imgLblRightTop: UILabel!
    @IBOutlet weak var imgRightTop: UIImageView!
    @IBOutlet weak var imgViewRightTop: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblNameStatusField: UILabel!
    @IBOutlet weak var lblNameStatus: UILabel!
    @IBOutlet weak var lblNameAdTitleField: UILabel!
    @IBOutlet weak var lblNameAdTitl: UILabel!
    @IBOutlet weak var lblNameAdIdField: UILabel!
    @IBOutlet weak var lblNameAdId: UILabel!
    @IBOutlet weak var lblNameUsrNameField: UILabel!
    @IBOutlet weak var lblNameUserName: UILabel!
    @IBOutlet weak var lblNameUserId: UILabel!
}
