//
//  MyWalletTransactionCell.swift
//  Simsar
//
//  Created by NxGeN on 2/15/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class MyWalletTransactionCell: UICollectionViewCell {
    @IBOutlet var lblWalletId: UILabel!
    @IBOutlet var lblType: UILabel!
    
    @IBOutlet var lblAmout: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblSafePayId: UILabel!
    
    
    override func awakeFromNib() {
        let radius: CGFloat = 10
       // containerView.makeRound(radius: 10)
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
        layer.cornerRadius = radius
    }
    
}
