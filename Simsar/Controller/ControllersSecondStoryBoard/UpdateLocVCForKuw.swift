//
//  UpdateLocVCForKuw.swift
//  Simsar
//
//  Created by NxGeN on 1/28/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import Foundation
import DropDown

class UpdateLocVCForKuw: UIViewController {
    
    private let viewModel = updateLocModel()
    private var state:[String] = []
    private var countries:[String] = []
    
    //DROP_DOWNS
    var countryDD = DropDown ()
    var stateDD = DropDown ()
    var cityDD = DropDown()
    
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var searchBtn: UIButton!
    
    
    var countryArray:[CountryForKuw] = []
    ///data to be forward
    var idOfCat: Int!
    var idOfSubCat: Int!
    var idOfBrand: Int!
    var idOfSubBrand: Int?
    var productDetail:SellNowProductDetailModel!
    var selectedAttribute:[Int:Any]!
    var imageData:SellNowProductImageModel!
    var statesArray:[DataStateForKuw] = []
    var cityArr:[DataCityForKuw] = []
    var stateId = 0
    var cityLati:Double?
    var cityLong:Double?
    var countryId = 0
    var cityId = 0
   // var userId: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCountries()
        getState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        
        self.setupDropDown()
        
    }
    
    func updateUI() {
        searchBtn.layer.cornerRadius = searchBtn.frame.height/2
        searchBtn.clipsToBounds = true
        searchBtn.dropbtnShadow()
        searchBtn.applyGradient()
    }
    
    private func getCountries(){
        viewModel.getCountries { (countryArry, error) in
            if countryArry.count != 0{
                //  self.countries = countryArry
                self.countryArray = countryArry
                var countries:[String] = []
                for c in countryArry {
                countries.append(c.country ?? "")
          
                }
                self.countryDD.dataSource = countries
                
            }else {
                ///show error message no country found
            }
        }
    }
    
    private func getState(){
        viewModel.getState { (stateArray, error) in
            if stateArray?.count != 0{
                self.statesArray = stateArray!
                var stateNameArray:[String] = []
                for n in stateArray ?? []{
                    stateNameArray.append(n.name!)
                }
                self.stateDD.dataSource = stateNameArray
            }else {
                ///show error message no country found
            }
        }
    }
    
    private func getCity(stateId:Int){
        viewModel.getCity(id: stateId) { (cityArray, error) in
            if cityArray.count != 0{
                self.cityArr = cityArray
                var cityNameArray:[String] = []
                for n in cityArray{
                    
                    cityNameArray.append(n.name!)
                }
                self.cityDD.dataSource = cityNameArray
                
                
            }else {
                ///show error message no country found
            }
        }
        
    }
    
    private func validate() -> Bool{
        if self.countryId == 0  || self.stateId == 0 || self.cityId == 0 {
            
            return false
        }
        return true
    }
    
    @IBAction func btnNext(_ sender: UIButton) {
        if validate(){
            let VC = self.storyboard?.instantiateViewController(withIdentifier: VcIdentifier.sellNowContactDetail.rawValue) as! VC_ContactDetail
            VC.idOfCat = self.idOfCat
            VC.idOfSubCat = self.idOfSubCat
            VC.idOfBrand = self.idOfBrand
            VC.idOfSubBrand = self.idOfSubBrand
            VC.selectedAttribute = self.selectedAttribute
            VC.productDetail = self.productDetail
            VC.imageData = self.imageData
            VC.longi = self.cityLong
            VC.lati = self.cityLati
            VC.countryId = self.countryId
            VC.stateId = self.stateId
            VC.cityId = self.cityId
            self.navigationController?.pushViewController(VC, animated: true)
        }
        else{
            self.showAlertView(message: "", title: "Please select all options")
        }
    }
    
}

extension UpdateLocVCForKuw {
    
    func setupDropDown(){
        
        countryDD.anchorView = txtCountry
        stateDD.anchorView = txtState
        cityDD.anchorView = txtCity
        
        //SELECTION_ACTION
        cityDD.selectionAction = {index , title in
            self.txtCity.text = title
            self.cityLati = Double(self.cityArr[index].latitude!)
            self.cityLong = Double(self.cityArr[index].longitude!)
            self.cityId = Int(self.cityArr[index].id!)
        }
        
        stateDD.selectionAction = {index , title in
            self.txtState.text = title
            self.stateId = Int(self.statesArray[index].id!)
            self.getCity(stateId: self.stateId)
            self.txtCity.text = ""
        }
        
        countryDD.selectionAction = {index , title in
            self.txtCountry.text = title
            self.countryId = Int(self.countryArray[index].id!) ?? 0
            //self.getState()
            
        }
        
        //SELECTION_ACTION
        
        //Tap_Gestures
        let cityGest = UITapGestureRecognizer(target: self , action: #selector( cityTapped ) )
        txtCity.addGestureRecognizer(cityGest)
        
        let stateGest = UITapGestureRecognizer(target: self , action: #selector( stateTapped ) )
        txtState.addGestureRecognizer(stateGest)
        
        let countryGest = UITapGestureRecognizer(target: self , action: #selector( countryTapped ) )
        txtCountry.addGestureRecognizer(countryGest)
        
        
    }
    
    @objc func countryTapped(){
        
        countryDD.show()
    }
    
    @objc func stateTapped(){
        
        stateDD.show()
    }
    
    @objc func cityTapped(){
        
        cityDD.show()
    }
    
}
