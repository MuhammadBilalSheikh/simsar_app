//
//  VC_CreatLocation.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 20/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import GooglePlaces

class VC_CreatLocation: UIViewController, UISearchControllerDelegate, UISearchBarDelegate , UITextFieldDelegate{

   
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtLongitude: UITextField!
    @IBOutlet weak var txtLatitude: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    var  globalIndicator : UIViewController? = nil
    
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var text : UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self

        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController

        // Put the search bar in the navigation bar.
       // searchController?.searchBar.sizeToFit()
       // navigationItem.titleView = searchController?.searchBar
//        view.addSubview(searchController!.searchBar)
        
        

        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true

        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
       
        DesignNavItemBarButton()
        btnSubmit.applyGradient()
        btnSubmit.makeRound(radius: 20)

        
    }
    
    

       
        

    
    
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
           btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
           btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
           btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
           let item1 = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButton(item1, animated: true)
    }
        
        @objc func actionBack() {
            self.navigationController?.popViewController(animated: true)
        }
    
    func Api() {
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "user_id" : 471,
                                "country" : txtCountry.text ?? "",
                                "state" : txtState.text ?? "",
                                "city" : txtCity.text ?? "",
                                "latitude" : txtLatitude.text ?? "",
                                "longitude" : txtLongitude.text ?? "",
                                "address" : txtAddress.text ?? ""]
        var serviceObj = Service(url: "https://simsar.com/api/v1/add-map" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
             //   self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let statusOfRequest = jObj["status"]
                    let messageFromServer = jObj["message"]
                    
                    if ( statusOfRequest == "success") {
                        print("Location Added")
                    }else{
                        print("Request \(statusOfRequest)")
                    }
                    
                    

         //           }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                //    self.MyBankCollectionView.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    
    

    @IBAction func btnSubmitAction(_ sender: Any) {
        
        Api()
    }
    
}




// Handle the user's selection.
extension VC_CreatLocation : GMSAutocompleteResultsViewControllerDelegate {
  func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                         didAutocompleteWith place: GMSPlace) {
    // Do something with the selected place.
    print("Place name: \(place.name)")
    print("Place address: \(place.formattedAddress)")
    print("Place attributions: \(place.attributions)")
    print("places \(place)")
  }

  func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                         didFailAutocompleteWithError error: Error){
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
}
