//
//  MyWalletTransactionVC.swift
//  Simsar
//
//  Created by NxGeN on 2/15/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class MyWalletTransactionVC: UIViewController {
    
    @IBOutlet var cView: UICollectionView!
    
    var globalIndicator : UIViewController?
    var userId: Int?
    
    var collectionOfMyWalletTrans : [ MyWalletTransaction ] = []
    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"

    override func viewDidLoad() {
        super.viewDidLoad()
        cView.delegate = self
        cView.dataSource = self
        
        apiCalledForTransacton()
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }

      
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
     
       self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK:- API CALL FOR WIDHLIST
    func apiCalledForTransacton() {
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  :  471  //self.userId!
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/wallettransections" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["transactions"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = MyWalletTransaction()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                        
                            obj.Amount = jsonObj["debit"].stringValue
                            obj.type = jsonObj["request_type"].stringValue
                            obj.SafePayId = jsonObj["safe_pay_id"].stringValue
                            obj.WalletId = jsonObj["tid"].stringValue
                            obj.Status = jsonObj["status"].stringValue
                            self.collectionOfMyWalletTrans.append(obj)
                            
                        }
                        
                        self.cView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    
    

  
}


extension MyWalletTransactionVC : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfMyWalletTrans.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyWalletTransactionCell", for: indexPath) as! MyWalletTransactionCell
        
        let currentTransactions = collectionOfMyWalletTrans[indexPath.row]
        
        
        let status = currentTransactions.Status
        if status == "0" {
            cell.lblStatus.text = "Pending"
        }else{
            cell.lblStatus.text = "Approved"
        }
        cell.lblAmout.text = currentTransactions.Amount
        cell.lblSafePayId.text = currentTransactions.SafePayId
        cell.lblWalletId.text = currentTransactions.WalletId
        cell.lblType.text = currentTransactions.type
        return cell
    }
    
    
    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")
    
        return CGSize(width: cvSize/1, height: 253 )
    
    }
    
    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
       }
    
    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
}



class MyWalletTransaction {
    var id: Int?
    var WalletId, SafePayId, Status, type , Amount : String?
}


extension MyWalletTransactionVC : SkeletonCollectionViewDataSource  {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        
        return "MyWalletTransactionCell"
    
    }
}
