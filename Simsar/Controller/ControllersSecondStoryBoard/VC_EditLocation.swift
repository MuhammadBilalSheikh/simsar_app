//
//  VC_EditLocation.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 20/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
class VC_EditLocation: UIViewController {

    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtLongitude: UITextField!
    @IBOutlet weak var txtLatitude: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    
    var  globalIndicator : UIViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DesignNavItemBarButton()
        btnUpdate.applyGradient()
        btnUpdate.makeRound(radius: 20)

      
    }
    
    func DesignNavItemBarButton(){
        let btnBack = UIButton(type: .custom)
           btnBack.setImage(UIImage(named: "back-icon"), for: .normal)
           btnBack.frame = CGRect(x: 10, y: 10, width: 60, height: 50)
           btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
           let item1 = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButton(item1, animated: true)
    }
        
        @objc func actionBack() {
            self.navigationController?.popViewController(animated: true)
        }
    
    func Api() {
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "map_id" : 191]
        
        var serviceObj = Service(url: "https://simsar.com/api/v1/edit-map" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
             //   self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let statusOfRequest = jObj["status"]
                    let messageFromServer = jObj["message"]
                    
                    if ( statusOfRequest == "success") {
                        print("Map Updated")
                    }else{
                        print("Request \(statusOfRequest)")
                    }
                    
                    

         //           }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                //    self.MyBankCollectionView.reloadData()

                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
    }
    
    

    @IBAction func btnUpdateAction(_ sender: Any) {
        Api()
    }
    

}
