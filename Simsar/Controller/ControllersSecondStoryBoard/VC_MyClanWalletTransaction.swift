//
//  VC_MyClanWalletTransaction.swift
//  Simsar
//
//  Created by Mohammad Affan Siddiqui on 23/11/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import BLTNBoard
import SwiftyJSON
import SkeletonView

class VC_MyClanWalletTransaction: UIViewController {
    

    @IBOutlet weak var collectionViewWallet: UICollectionView!
    
    var globalIndicator : UIViewController?
    var userId: Int?
    
    
    var collectionOfCoinsTransactions : [ VC_MyCoinWalletModel ] = []
    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"
    
    private lazy var boardManager: BLTNItemManager =  {
        let item = BLTNPageItem(title: "Push")
    
        
        return BLTNItemManager(rootItem: item)
        
        
    }()
        
        

    override func viewDidLoad() {
        super.viewDidLoad()
        apiCalledForTransacton()
        
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }

        
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    
    //MARK:- API CALL FOR WIDHLIST
    func apiCalledForTransacton() {
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  :  461  //self.userId!
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/coinwallettransaction" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["transactions"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_MyCoinWalletModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                        
                            obj.Plane = jsonObj["plan"].stringValue
                            obj.type = jsonObj["type"].stringValue
                            obj.transId = jsonObj["transaction_id"].stringValue
                            
                            self.collectionOfCoinsTransactions.append(obj)
                            
                        }
                        
                        self.collectionViewWallet.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    
    

}

extension VC_MyClanWalletTransaction : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfCoinsTransactions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyClanWalletCell", for: indexPath) as! MyClanWalletCell
        
        let curentTrnsactions = collectionOfCoinsTransactions[indexPath.row]
        
        cell.lbltransIdField.text = curentTrnsactions.transId
        cell.lblTypeField.text = curentTrnsactions.type
        cell.lblDealPlanField.text = curentTrnsactions.Plane
        
        
        return cell
    }
    
    
    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")
    
        return CGSize(width: cvSize/1, height: 76 )
    
    }
    
    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
       }
    
    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    
}


class VC_MyCoinWalletModel {
    var id: Int?
    var transId, type , Plane : String?
}


extension VC_MyClanWalletTransaction : SkeletonCollectionViewDataSource  {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        
        return "MyClanWalletCell"
    
    }
}

