//
//  ProducBrandVC.swift
//  Simsar
//
//  Created by NxGeN on 1/6/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProducBrandVC: UIViewController {

    @IBOutlet var cView: UICollectionView!
    var dataFromServer : [sellNowSubBrands] = []
    
    let constUrl = "https://simsar.com/public/asset/images/sell-now/"
    
    var  globalIndicator : UIViewController? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callApi()
        cView.dataSource = self
        cView.delegate = self
        
        cView.register( AllAdsCollectionViewCell.nib() , forCellWithReuseIdentifier: AllAdsCollectionViewCell.identifier )

        
    }
    
    
    func callApi(){
        
        if globalIndicator == nil {
            globalIndicator = createGlobalIndicator()
        }
        showGlobalIndicator(self.globalIndicator , self )
        
        
        let apiParams = Parameter()
        apiParams.dictionary = ["client_key" : Constant.client_id,
                                "brand_id" : 50]
        
        let serviceObj = Service(url: "https://simsar.com/api/v1/brand-product" , parameters: apiParams )
       
        serviceObj.headers = Constant.default_header
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            if (result != nil ){
                
                self.stopGlobalIndicator( self.globalIndicator!  )
               // let decoder = JSONDecoder()
                
                do {
                    
                   // print( "\(String(describing: result?.response?.value(forKey: "description")) )")
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("JSON___________+++++++ \(jObj)")
                    let subBrands = jObj["sellNow"]
                    print("categories ==== \(subBrands)" )

                    for countOfObjectsInArray in 0..<subBrands.count {

                        print ("Loop For Time === \(countOfObjectsInArray)")


                        let singleObj  = subBrands[countOfObjectsInArray]// may be error in this
                        let obj = sellNowSubBrands(
                            id: singleObj["id"].intValue,
                            title: singleObj["ad_title"].stringValue,
                            datumDescription: singleObj["description"].stringValue,
                            cityname: singleObj["cityname"].stringValue,
                            long_description: singleObj["long_description"].stringValue,
                            price: singleObj["price"].stringValue, del_price: singleObj["dis_price"].stringValue,
                            state: singleObj["statename"].stringValue,
                            createdDate: singleObj["created_date"].stringValue,
                            priceOnCall: singleObj["is_call_price"].stringValue,
                            wishlist: singleObj["is_wishlist"].stringValue,
                            auction: singleObj["is_auction"].stringValue,
                            discPrice: singleObj["is_offer"].stringValue,
                            sparkLight:  singleObj["checkToday"].stringValue,
                            iconImage: singleObj["main_image"].stringValue, expiryDate: singleObj["expiry_date"].stringValue)
                            
                            
                            
                            
                            
            
                            

                        self.dataFromServer.append( obj )


                    }
                    //self.dataFromServer = try decoder.decode([categoryDataFromServer].self , from: categories.rawData() )
                    self.cView.reloadData()
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }
        
        }
    

    
}


extension ProducBrandVC : UICollectionViewDelegate , UICollectionViewDataSource
                      ,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataFromServer.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier:
        AllAdsCollectionViewCell.identifier , for: indexPath ) as! AllAdsCollectionViewCell
        
        cell2.productTitleLabel.text = dataFromServer[indexPath.row].title
        cell2.productDetailLabel.text = dataFromServer[indexPath.row].long_description
       
      
        cell2.lblState.text = dataFromServer[indexPath.row].state
        cell2.remainingTimeLabel.text = dataFromServer[indexPath.row].createdDate
        
        
        cell2.productAddressLabel.text = dataFromServer[indexPath.row].cityname
        
        if( dataFromServer[indexPath.row].iconImage != nil ){
            ServiceManager.shared.downloadImageWithName(
                constUrl
                , dataFromServer[indexPath.row].iconImage ?? ""
               , cell2.activityIndicator
                , cell2.productImageView )
        }
        
        
        return cell2
        
    }
    
    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding - 10
//            print("Colection View Frame == \(collectionView.frame.width)")
//            print("CView.frame = \(cvSize/2)")
//            //print("collectionViewLayout == \(collectionViewLayout.collectionViewContentSize.width)")
//            print("Cell Size -> Foe Item")
        return CGSize(width: cvSize/2 , height: 270 )
    
    }
    
    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
       }
    
    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
        
    
    
    
    
}
