//
//  PopUpHomeSLider.swift
//  Simsar
//
//  Created by NxGeN on 2/20/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import ImageSlideshow
import SwiftyJSON
import SkeletonView

class PopUpHomeSLider: UIViewController {

    @IBOutlet var contentView: UIView!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var viewImageSlider: ImageSlideshow!
    
    var sliderData:[slidesData]?
    var viewModel = HomeViewModel()
    
    let constUrl = "https://simsar.com/public/asset/images/sell-now/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callApiForSliderImgaes()
        btnClose.applyGradient()
        btnClose.makeRound(radius: 10)
        //MAkING IT POPUP
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        self.contentView.layer.cornerRadius = self.contentView.frame.height * 0.005
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3 )
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.55) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
        }
    }
    
    
    func closingAnimation(){
        UIView.animate(withDuration: 0.55 , animations:
                        {
                            self.view.alpha = 0
                            self.view.transform = CGAffineTransform (scaleX: 1.3 , y: 1.3 )
                        }) { (done) in
            self.view.removeFromSuperview()
        }
    }
    
    
    //MARK: Api for slider Imgages
    func callApiForSliderImgaes(){
        print("Home API CALLED")
         self.viewModel.getSliderData{ (success,slidesData, error) in
             if success{
               //  data = self.adsData ?? []
                 self.sliderData = slidesData //(result?.data)!
                self.setUpSlider(data: self.sliderData ?? [])
//                 self.enableCV()
//                 self.collectionView.reloadData()
                
             }else{
                 print ("An Error")
             }
         }
    }
    
    
    
    // SETING UP SLIDER
    func setUpSlider(data:[slidesData]){
        var imagesArray:[InputSource] = []
        let myGroup = DispatchGroup()
        for index in 0 ... data.count - 1{
            myGroup.enter()

            ServiceManager.shared.downloadImageWithName(Constant.baseUrlImageForSlider  , data[index].slide!) { (image) in
                
                if image != nil{
                    imagesArray.append(ImageSource(image:image!))
                }else{
                    imagesArray.append(ImageSource(image:UIImage(named:"Angelina Jolie")!))
                }
                myGroup.leave()
            }
        }
        myGroup.notify(queue: .main) {
            self.viewImageSlider.setImageInputs(imagesArray)
            self.viewImageSlider.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .bottom)
            self.viewImageSlider.slideshowInterval = 2.0
            self.viewImageSlider.contentScaleMode = .scaleToFill
         }
    }//SLIDER
    
    

    @IBAction func btnCloseAct(_ sender: UIButton) {
        closingAnimation()
    }
    

}
