//
//  VC_OtpForNumVerf.swift
//  Simsar
//
//  Created by NxGeN on 2/5/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import OTPFieldView

class VC_OtpForNumVerf: UIViewController {

    @IBOutlet var btnVerf: UIButton!
    @IBOutlet var btnView: UIView!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var otpTextFiledView: OTPFieldView!
    
    
    var otpNumberArray:[String] = []
    var alertMsg: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.makeSimsarThemedButton(outerView: btnView , button: btnVerf, needGradient: true )
        
        setupOtpView()

        
       // self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        self.contentView.layer.cornerRadius = self.contentView.frame.height * 0.005
        
    }
    
    private func setupOtpView(){
            self.otpTextFiledView.fieldsCount = 6
            self.otpTextFiledView.fieldBorderWidth = 2
            self.otpTextFiledView.defaultBorderColor = UIColor.black
            self.otpTextFiledView.filledBorderColor = UIColor.green
            self.otpTextFiledView.cursorColor = UIColor.red
            self.otpTextFiledView.displayType = .underlinedBottom
            self.otpTextFiledView.fieldSize = 35
        self.otpTextFiledView.separatorSpace = 5
            self.otpTextFiledView.shouldAllowIntermediateEditing = false
            self.otpTextFiledView.delegate = self
            self.otpTextFiledView.initializeUI()
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3 )
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.55) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
        }
    }
    
    
    func closeAnimation() {
        UIView.animate(withDuration: 0.55 , animations:
                        {
                            self.view.alpha = 0
                            self.view.transform = CGAffineTransform (scaleX: 1.3 , y: 1.3 )
                        }) { (done) in
            self.view.removeFromSuperview()
        }
    }
    
    @IBAction func btnVerAct(_ sender: Any) {
        if Connectivity.isConnectedToInternet{
            if otpNumberArray.count == 6{
                otpCheck()
            }else{
                self.showAlertView(message: "Plese fill all spaces", title:"Incomplete" )
            }
            
        }else{
            self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
        }
                
        
    }
    
    
    
    
    
    
    @IBAction func btnCloseAct(_ sender: Any) {
        closeAnimation()
    }
    
    
    
     func otpCheck()
    {
        let apiParams = Parameter()
        apiParams.dictionary = [
           // "client_key" : Constant.client_id 
            "digit1"      : Int(otpNumberArray[0]) ?? 0, //Int(txt1.text!) ?? 0 ,
            "digit2"      : Int(otpNumberArray[1]) ?? 0,//Int(txt2.text!) ?? 0 ,
            "digit3"      :  Int(otpNumberArray[2]) ?? 0,//Int(txt3.text!) ?? 0 ,
            "digit4"      : Int(otpNumberArray[3]) ?? 0,// Int(txt4.text!) ?? 0,
            "digit5"      :  Int(otpNumberArray[4]) ?? 0,//Int(txt5.text!) ?? 0 ,
            "digit6"      :  Int(otpNumberArray[5]) ?? 0//Int(txt6.text!) ?? 0 ,
        ]
        
       // print("SIgnupCODEVER****** \()")
        DispatchQueue.main.async
        {
            OtpCheckServiceForNumber.shared.getOtpCheckForNumber(apiParams: apiParams) { (result, error) in
                if result != nil{
                    print(result?.userID ?? 0)
                    self.alertMsg = result?.status
                  self.showAlertView(message: self.alertMsg!, title: "Alert", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                    self.openVC("HomeVC")
                    }))
                   // self.navigationController!.popToRootViewController(animated: true)
                }else{
                    self.showAlertView(message: "Something Wrong while Getting Responce", title: error ?? "")
                }
            }
        }
    }
    

}


extension VC_OtpForNumVerf: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        if hasEntered == false{
            otpNumberArray.removeAll()
        }
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        print(index)
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        let string : String = otpString
        otpNumberArray = string.map { String($0) }
        btnVerf.isEnabled = true
        
    }
}

