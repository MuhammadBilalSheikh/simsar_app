//
//  VC_CoinWallet.swift
//  Simsar
//
//  Created by NxGeN on 2/15/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON

class VC_CoinWallet: UIViewController {

    @IBOutlet var vcIndicator: UIActivityIndicatorView!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblCoin: UILabel!
    
    
    var constUrlToFetchImage = "https://simsar.com/public/images/common-pages/"
    var globalIndicator : UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiFetchCoinData()
        vcIndicator.isHidden = true
        
    }
    
    func disableCV(){
        
        self.vcIndicator.isHidden = false
        self.vcIndicator.startAnimating()
    }
    
    func enableCV(){
       
        self.vcIndicator.isHidden = true
        self.vcIndicator.stopAnimating()
    }
    
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
    

  

}




extension VC_CoinWallet {
    
    func apiFetchCoinData(){
        
        
       startAnimatingWhileLoadingData()
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
            "user_id"    : 461
        ]
        let serviceObj = Service(url: "https://simsar.com/api/v1/mycoinwallet" , parameters: apiParams )
        
        serviceObj.headers = ["Content-Type" : "application/json"]
        
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            
            
            if (result != nil ){
                
                //  let decoder = JSONDecoder()
                self.stopAnimationgAsDataLoaded()
                do {
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("\(jObj)")
                    //let response = jObj["data"]
                    
                    if jObj != nil {
                        
                        DispatchQueue.main.async {
                            self.lblPrice.text = jObj["coinsamountInKD"].stringValue
                            self.lblCoin.text = jObj["coin"].stringValue
                          
                            
                        }
                        
                    } else {  print("Error in response");   return }
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }//CLOSURE
        
    }//LoginByUserEmail
}

