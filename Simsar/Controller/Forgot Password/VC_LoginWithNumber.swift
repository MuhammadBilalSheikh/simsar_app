//
//  Vc_LoginWithNumber.swift
//  Simsar
//
//  Created by NxGeN on 11/24/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class VC_LoginWithNumber: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var tfNumber : FPNTextField!
    @IBOutlet var tfPassword : UITextField!
    @IBOutlet var btnLognWithNumber : UIButton!
    @IBOutlet var viewOuterOfButton : UIView!
    @IBOutlet var contentView : UIView!
    @IBOutlet var btnLogin : UIButton!
    @IBOutlet var loginBtnIndicator : UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfNumber.delegate = self
        tfNumber.setFlag(key: .KW)
        
        tfNumber.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        tfPassword.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
        self.makeSimsarThemedButton(outerView: viewOuterOfButton , button: btnLognWithNumber, needGradient: true )
        //MAkING IT POPUP
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        //let gestViewTap = UITapGestureRecognizer (target: self , action: #selector(objcViewTapped) )
       // self.view.addGestureRecognizer( gestViewTap )
        self.contentView.layer.cornerRadius = self.contentView.frame.height * 0.005
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3 )
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.55) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
        }
    }
    
    func closingAnimation(){
        UIView.animate(withDuration: 0.55 , animations:
                        {
                            self.view.alpha = 0
                            self.view.transform = CGAffineTransform (scaleX: 1.3 , y: 1.3 )
                        }) { (done) in
            self.view.removeFromSuperview()
        }
    }
    
//    @objc func objcViewTapped(){
//        self.closingAnimation()
//    }
    
    
    @IBAction func closeView(_ sender: UIButton) {
        closingAnimation()
    }
    
    
    @IBAction func loginWithNumber(){
        
        if Connectivity.isConnectedToInternet{
            loginUserByPhone()
        }else{
            self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
        }
    }
    func loginUserByPhone(){
        disableLogin()
        print("Log In Called")
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
            "phone"      :  tfNumber.text ?? ""  ,
            "password"   :  tfPassword.text ?? ""
        ]
        let serviceObj = Service(url: "\(Constant.base_url)\(Constant.ePhoneLoginUrl)" , parameters: apiParams )
        serviceObj.headers = Constant.default_header
        enableLogin()
        ServiceManager.shared.request(service: serviceObj, model: LoginResponseModel.self) { (result, error) in
            if result != nil
            {
                DispatchQueue.main.async {
                    let loginResult = result!
                    if result?.status == "success"{
                        print(loginResult.user!.id)
                        let userId = loginResult.user!.id
                        /*storing user id in userDefault*/
                        UserDefaults.standard.set(userId, forKey: Constant.userDefaultUserId)
                      //  self.closingAnimation()
                        self.showAlertView(message: (result?.message!)! , title: "Successed", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.openVC("HomeVC")
                        }))
                       // self.navigationController?.popViewController(animated: true)
                       // self.tabBarController?.selectedIndex =  0
                    }else{
                        self.showAlertView(message: result?.message ?? "Please Filled All fields" , title: "Error")
                        self.showAlertView(message: "", title: result?.errors![0] ?? "")
                    }
                }
                self.disableLogin()
            }else{
             
                self.showAlertView(message: "", title: result?.errors?[0] ?? error?.localizedDescription ?? "")
                print(error ?? "")
                self.disableLogin()
            }
          
        }
    }//LoginByUserEmail
    
    func enableLogin(){
        self.startAnimatingButton(true , btnLogin , loginBtnIndicator )
    }
    
    func disableLogin(){
        self.startAnimatingButton(false , btnLogin , loginBtnIndicator )
    }
}



extension VC_LoginWithNumber : FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        print("sadasd")
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            tfNumber.text =  tfNumber.getRawPhoneNumber()
        }else{
        }
    }
}

