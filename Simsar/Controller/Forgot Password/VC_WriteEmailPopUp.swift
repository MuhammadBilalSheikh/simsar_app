//
//  VC_WriteEmailPopUp.swift
//  Simsar
//
//  Created by NxGeN on 2/12/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit



@available(iOS 13.0, *)
class VC_WriteEmailPopUp: UIViewController {

    @IBOutlet var btnSubmit: UIButton!

    @IBOutlet var mainView: UIView!
    @IBOutlet var actInd: UIActivityIndicatorView!
    @IBOutlet var txtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        actInd.isHidden = true
     //   self.mainView.layer.cornerRadius = self.mainView.frame.height * 0.005
        self.mainView.makeRound(radius: 15)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3 )
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.55) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
        }
    }
    
    
    @IBAction func btnCloseAct(_ sender: UIButton) {
       
      
    }
    
   
    @IBAction func btnSubmitAct(_ sender: UIButton) {
        if valiadteEmail() == true {
            apiCalledForEmail()
        } else {
            showAlertView(message: "Please Check Everything Properly", title: "Please Check")
        }
        
    }
    
    
    
    
    func valiadteEmail() -> Bool {
    if txtEmail.text!.isEmpty {
                
        showAlertView(message: "Email Field Cant be Empty", title: "Text field can't be empty")
                return false
                
            }else {
                return true
            }
        }
    
    @available(iOS 13.0, *)
    func NavvToVerfPopUp() {
        

        let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_WriteOtpPopUp" )
        self.addChild( ppWithNumber! )
        guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
        vc.view.frame = self.view.frame
        self.view.addSubview( vc.view )

        vc.didMove(toParent: self )
    }
    
    
    
    
    @available(iOS 13.0, *)
    func apiCalledForEmail() {
        self.startAnimatingButton(false , btnSubmit , actInd )
            if Connectivity.isConnectedToInternet {
                    let apiParams = Parameter()
                    apiParams.dictionary = [
                        "client_key" : Constant.client_id ,
                        "email"      :  txtEmail.text!
                    ]
                    self.startAnimatingButton(true , btnSubmit , actInd )
                    
                    ResetPasswordService.byEmail.getResetPasswordResponse(apiParams: apiParams) { (result, error) in
                        if result != nil {
                            print(result?.verificationcode ?? 0)
                            DispatchQueue.main.async {
            
                                self.showAlertView(message: "", title:result?.message ?? "" , action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                                    self.NavvToVerfPopUp()
                                }))
                               
                            
                            }
                        }else{
                            print ("error")
                            let anyErrorFromServer = error
                            DispatchQueue.main.async {
                                self.showAlertView(message: "", title: anyErrorFromServer ?? "")
                            }
                        }
                        self.startAnimatingButton(false , self.btnSubmit , self.actInd ) // for activity Indicator
                    }
                print("email")
                }
            }
    
    
    
}
