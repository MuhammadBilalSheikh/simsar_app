//
//  VC_ForgetPassword.swift
//  Simsar
//
//  Created by NxGeN on 11/26/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import OTPFieldView


@available(iOS 13.0, *)
class VC_ResetPassword: UIViewController {
    
    
    
    //MARK:- outlets or write email

    @IBOutlet weak var resetWithEmailView : UIView!
    @IBOutlet weak var resetWithNumberView : UIView!
    
    @IBOutlet var centerAlign: NSLayoutConstraint!
    
    @IBOutlet weak var commonView: UIView!  //main view

    var checkRestType:String?
    var alertMsg: String?
    
    enum checkRestPasswordType:String{
        case email
        case number
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSelectionUI()
//        showAnimate()
//        updateUI()
       
    }
    
   
    
    
    
    
    

    
   
    
    //MARK:- SHOW AND CLOSE VIEWS !!
    @available(iOS 13.0, *)
    func showSelectionUI(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        resetWithEmailView.makeRound(radius: self.resetWithEmailView.frame.height * 0.1)
        resetWithNumberView.makeRound(radius: self.resetWithNumberView.frame.height * 0.1)
        commonView.makeRound(radius: self.commonView.frame.height * 0.1)
       // selectionView.makeRound(radius: self.selectionView.frame.height * 0.1)
    //makeSimsarThemedButton(outerView: selectionView , buttonView: resetWithNumberView, needGradient: false )
   //makeSimsarThemedButton(outerView: selectionView , buttonView: resetWithEmailView, needGradient: false )
        
        //add View to parent view
       // showViewInParentView(removeViewFromParent: UIView(), childView: selectionView, ParentView: self.commonView, childViewHeight: self.view.frame.size.height * 0.20)
        //adding gesture for Numberbutton action
        let gestResetWithNumber = UITapGestureRecognizer (target: self , action: #selector( resetWithEmail ) )
        self.resetWithNumberView.addGestureRecognizer( gestResetWithNumber )
        //adding gesture for emailbutton action
        let gestResetWithEmail = UITapGestureRecognizer(target: self , action: #selector(  resetWithNumber) )
        self.resetWithEmailView.addGestureRecognizer( gestResetWithEmail )
    }
    
    
    @available(iOS 13.0, *)
    @objc func resetWithEmail(){
        let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_WriteEmailPopUp" )
        self.addChild( ppWithNumber! )
        guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
        vc.view.frame = self.view.frame
        self.view.addSubview( vc.view )
        
        vc.didMove(toParent: self )
    }
    @available(iOS 13.0, *)
    @objc func resetWithNumber(){
        let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_WriteNumPopUp" )
        self.addChild( ppWithNumber! )
        guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
        vc.view.frame = self.view.frame
        self.view.addSubview( vc.view )
        
        vc.didMove(toParent: self )
        
        }
    
    //MARK:- SELECTION UI CLOSE OUTLETS ACTIONS
    
    @IBAction func btnCloseView(_ sender: UIButton) {
        self.openVC("VC_Login")
    }
    
    func showResetPasswordNumberUI(){
        
        closeFirstPopUp()
      //  openWriteNumPopUp()
        
//        txtnumber.isHidden = true
//        tfEmail.isHidden = false
//        checkRestType = checkRestPasswordType.email.rawValue
//        resetPasswordView.makeRound(radius: self.resetPasswordView.frame.height * 0.1)
    
        //add View to parent view
       // showViewInParentView(removeViewFromParent: selectionView, childView: resetPasswordView, ParentView: self.commonView, childViewHeight:  self.view.frame.size.height * 0.319)
    }
    
    func closeFirstPopUp() {
        centerAlign.constant = -450
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
           // self.backGroundBtn.alpha = 0
           // self.openCodeVerfPopUp()
        })
    }
    
    
    func closeWriteEmailPopUp() {
      //  centerAlignOfEmail.constant = -450
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
            self.view .layoutIfNeeded()
           // self.backGroundBtn.alpha = 0
           // self.openCodeVerfPopUp()
        })
    }
    
    func openVerfCodePopUp() {
        centerAlign.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            
            //self.backGroundBtn.alpha = 0.9
        })
        
    }
   
    
    func closeVerfCodePopUp() {
        centerAlign.constant = -450
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            
            //self.backGroundBtn.alpha = 0.9
        })
        
    }
    
    func openEnterNumPopUp() {
        centerAlign.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            
            //self.backGroundBtn.alpha = 0.9
        })
        
    }
    
//    func openWriteEmailPopUp() {
//        centerAlignOfEmail.constant = 0
//        UIView.animate(withDuration: 0.3, animations: {
//            self.view.layoutIfNeeded()
//
//            //self.backGroundBtn.alpha = 0.9
//        })
//
//    }
//
  
    
    
//    func showResetPasswordEmailUI(){
//        closeFirstPopUp()
//        openWriteEmailPopUp()
//
        
        
        
//        txtnumber.isHidden = false
//        tfEmail.isHidden = true
//        checkRestType = checkRestPasswordType.number.rawValue
//        label.text = "Reset Password Via Number"
//        //add View to parent view
//       // showViewInParentView(removeViewFromParent: selectionView, childView: resetPasswordView, ParentView: self.commonView, childViewHeight:  self.view.frame.size.height * 0.319)
    }
    
//    func showRecoveryCodeUI(){
//        recoveryCodeView.makeRound(radius: self.recoveryCodeView.frame.height * 0.1)
//        makeSimsarThemedButton(outerView: viewcodeConfirmBtn, button: codeConfrim, needGradient: false)
//        //add View to parent view
//       // showViewInParentView(removeViewFromParent: resetPasswordView, childView: recoveryCodeView, ParentView: self.commonView, childViewHeight:  self.view.frame.size.height * 0.249)
//    }
    
//    func showViewInParentView(removeViewFromParent:UIView, childView:UIView,ParentView:UIView,childViewHeight:CGFloat){
//        removeViewFromParent.removeFromSuperview()
//        childView.frame.size.width = ParentView.bounds.size.width - 40
//        childView.frame.size.height = childViewHeight
//        childView.center = CGPoint(x: ParentView.frame.size.width  / 2,
//                                   y: ParentView.frame.size.height / 2)
//        ParentView.addSubview(childView)
//    }
    

   
    
    func updateUI(){
//        txt1.delegate = self
//        txt2.delegate = self
//        txt3.delegate = self
//        txt4.delegate = self
//        txt5.delegate = self
//        txt6.delegate = self
//        
//        txtnumber.delegate = self
//        txtnumber.setFlag(key: .PK)
//        txtnumber.placeholder = ""
        
//        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
//
//        self.indicatorButton.isHidden = true
////        self.tfError.isHidden = true
//
//        makeSimsarThemedButton(outerView: outerView , button: btnSendVerf, needGradient: true )
//        let gestViewTap = UITapGestureRecognizer (target: self , action: #selector(removeAnimate ) )
        //  let gesContentView = UITapGestureRecognizer (target: self , action: #selector( objcContentViewTapped ) )
        
        //        self.resetPasswordView.addGestureRecognizer( gesContentView )
       // self.view.addGestureRecognizer( gestViewTap )
    }
    
    
    
    
    
    
    
    
//MARK: open and close animation
@available(iOS 13.0, *)
extension VC_ResetPassword{
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @objc func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
}


////MARK: all ibAction of buttons
//extension VC_ResetPassword{
//
//    @IBAction func resetButton(_ sender: Any) {
////        self.startAnimatingButton(false , resetButton , indicatorButton )
////        if checkRestType == checkRestPasswordType.email.rawValue{
////            if Connectivity.isConnectedToInternet {
////                if valiadteTF(){
////                    let apiParams = Parameter()
////                    apiParams.dictionary = [
////                        "client_key" : Constant.client_id ,
////                        "email"      :  tfEmail.text!
////                    ]
////                    self.startAnimatingButton(true , resetButton , indicatorButton )
////
////                    ResetPasswordService.byEmail.getResetPasswordResponse(apiParams: apiParams) { (result, error) in
////                        if result != nil {
////                            print(result?.verificationcode ?? 0)
////                            DispatchQueue.main.async {
////                                self.showAlertView(message: "", title: result?.message ?? "")
////                                self.showRecoveryCodeUI()
////                            }
////                        }else{
////                            print ("error")
////                            let anyErrorFromServer = error
////                            DispatchQueue.main.async {
////                                self.tfError.isHidden =  false
////                                self.tfError.text! = anyErrorFromServer ?? ""
////                            }
////                        }
////                        self.startAnimatingButton(false , self.resetButton , self.indicatorButton ) // for activity Indicator
////                    }
////                }
////            }
////            print("email")
//        }else{
//            print("number")
//            // for activity Indicator
//            if Connectivity.isConnectedToInternet {
//
//                if valiadteTF() {
//                    let apiParams = Parameter()
//                    apiParams.dictionary = [
//                        "client_key" : Constant.client_id ,
//                        "phone"      :  "0" + txtnumber.text!
//                    ]
//                    self.startAnimatingButton(true , resetButton , indicatorButton )
//
//                    ResetPasswordService.byNumber.getResetPasswordResponse(apiParams: apiParams) { (result, error) in
//                        if result != nil {
//                            print(result?.verificationCode ?? 0)
//                            DispatchQueue.main.async {
//                                self.showAlertView(message: "", title: result?.message ?? "")
//                                self.showRecoveryCodeUI()
//
//                            }
//                            self.startAnimatingButton(false , self.resetButton , self.indicatorButton )
//                        }else{
//                            print ("error")
//                            let anyErrorFromServer = error
//                            DispatchQueue.main.async {
//                                self.tfError.isHidden =  false
//                                self.tfError.text! = anyErrorFromServer ?? ""
//                            }
//                            self.startAnimatingButton(false , self.resetButton , self.indicatorButton )
//                        }
//                    }
//                }
//            }
//        }
//    }
    
//    @IBAction func verifyCodeButtonAction(_ sender: Any) {
//        // removeAnimate()
//        print("number")
//        // for activity Indicator
//        if Connectivity.isConnectedToInternet {
//            //  self.startAnimatingButton(true , resetButton , indicatorButton )
//            if valiadteTF() {
//                let apiParams = Parameter()
//                apiParams.dictionary = [
//                    "client_key" : Constant.client_id ,
//                    "digit1"      :  Int(txt1.text!) ?? 0 ,
//                    "digit2"      :  Int(txt2.text!) ?? 0 ,
//                    "digit3"      :  Int(txt3.text!) ?? 0 ,
//                    "digit4"      :  Int(txt4.text!) ?? 0,
//                    "digit5"      :  Int(txt5.text!) ?? 0 ,
//                    "digit6"      :  Int(txt6.text!) ?? 0 ,
//                ]
//               var Service = ResetPasswordService()
//                if checkRestType == checkRestPasswordType.number.rawValue{
//               Service = ResetPasswordService.recoveryCodeByNumber
//                }else {
//                    Service = ResetPasswordService.recoveryCodeByEmail
//                }
//                DispatchQueue.main.async {
//                    Service.getRecoveryCodeResponse(apiParams: apiParams) { (result, error) in
//                        if result != nil {
//                            self.openVC(VcIdentifier.changePasswordConfirm.rawValue)
//                            //                            let controller  = VC_ChangePassword()
//                            //                            controller.UserId = 374
//                            //                            self.navigationController?.pushViewController(controller, animated: true)
//                            self.removeAnimate()
//                        }else{
//                            print ("error")
//                            let anyErrorFromServer = error
//                            self.showAlertView(message: "", title: anyErrorFromServer ?? "")
//                        }
//                    }
//                }
//            }
//        }else {
//            self.showAlertView(message: "", title: "Internet is not working properly")
//        }
//    }
//
//    @IBAction func sendVerifyCodeAgainButtonAction(_ sender: Any) {
//    }
//}



//
////MARK: textfield delegate
//extension VC_ResetPassword:UITextFieldDelegate {
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == txtnumber{
//            return true
//        }
//        let currentText = textField.text ?? ""
//        guard let stringRange = Range(range, in: currentText) else { return false }
//        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
//        return updatedText.count <= 1
//
//    }
//}





