//
//  VC_ChangePassword.swift
//  Simsar
//
//  Created by NxGeN on 11/26/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_ChangePassword: UIViewController {

    @IBOutlet weak var tfPreviousPas : UITextField!
    @IBOutlet weak var tfNewPas : UITextField!
    @IBOutlet weak var tfNewPasSecond : UITextField!
    @IBOutlet weak var btnChangePass : UIButton!
    @IBOutlet weak var outerViewBtn : UIView!
    @IBOutlet weak var indicatorButton : UIActivityIndicatorView!
    var userId:Int?
    var a = false
    var b = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        self.indicatorButton.isHidden =  true
        updateUI()
    }
    
    @IBAction func changePassPressed(){
        if checkNewPassMatches() == true {
            if Connectivity.isConnectedToInternet {
                if tfNewPas.text! == tfNewPasSecond.text! {
                  a = true
                }else {
                    showAlertView(message: "Please Enter Your Password Again", title: "Password Do Not Match")
                }
                
                if(tfNewPas.text! == "" || tfNewPasSecond.text! == "") {
                    showAlertView(message: "Password Fields Are Empty", title: "Error")
                }else{
                    b = true
                }
                if a == true && b == true {
                    apiCalledForChangePass()
                }
            }else{
                self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
            }
        }
        
    }
    
    
    
    
    func enableInd(){
        self.startAnimatingButton(true , btnChangePass , indicatorButton )
    }
    
    func disableInd(){
        self.startAnimatingButton(false , btnChangePass , indicatorButton )
    }
    
    
    
    
    func apiCalledForChangePass(){
        disableInd()
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
            "user_id"      :  userId!  ,
            "password"   :    tfNewPas.text!  ?? ""
        ]
        print("PARAMS*** \(apiParams)")
        let serviceObj = Service(url: "\(Constant.base_url)\(Constant.eChangePasswordUrl)" , parameters: apiParams )
        serviceObj.headers = Constant.default_header
        enableInd()
        ServiceManager.shared.request(service: serviceObj, model: ChangePasswordStructModel.self) { (result, error) in
            if result != nil
            {
                DispatchQueue.main.async {
                    let loginResult = result!
                    if result?.status == "success"{
//                        print(loginResult.user!.id)
//                        let userId = loginResult.user!.id
                        /*storing user id in userDefault*/
                        UserDefaults.standard.set(self.userId, forKey: Constant.userDefaultUserId)
                      //  self.closingAnimation()
                        self.showAlertView(message: (result?.message!)! , title: "Successed", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.openVC("HomeVC")
                        }))
                       // self.navigationController?.popViewController(animated: true)
                       // self.tabBarController?.selectedIndex =  0
                    }else{
                        self.showAlertView(message: result?.message ?? "Please Filled All fields" , title: "Error")
                        self.showAlertView(message: "", title: result?.status! ?? "")
                    }
                }
                self.disableInd()
            }else{
             
                self.showAlertView(message: "", title: result?.status ?? error?.localizedDescription ?? "")
                print(error ?? "")
                self.disableInd()
            }
          
        }
    }//LoginByUserEmail
    
    
    
    
    
    
    
    
    
    private func updateUI(){
        setBackNavButton()
        self.makeSimsarThemedButton(outerView: outerViewBtn , button: btnChangePass, needGradient: true )
        self.tfPreviousPas.isSecureTextEntry =  true
        self.tfNewPasSecond.isSecureTextEntry =  true
        self.tfNewPas.isSecureTextEntry =  true
    }

    private func checkNewPassMatches() -> Bool {
        if ( tfNewPas.text!.isEmpty )
        {
            showAlertView(message: "Your New Password Field Is Empty", title: "TextField Is Empty")
            return false
        }
        if ( tfPreviousPas.text!.isEmpty )
        {
            showAlertView(message: "Your Old Password Field Is Empty", title: "TextField Is Empty")
            return false
        }
        if ( tfNewPasSecond.text!.isEmpty )
        {
            showAlertView(message: "Your Confirm Password Field Is Empty", title: "TextField Is Empty")
            return false
        }
      
        return true
    }
    
    private func checkIfPreviouPassCorrect() -> Bool {
        return true
    }
}








// MARK: - changePasswordStruct
struct ChangePasswordStructModel: Codable {
    let status, message, user: String?
}


