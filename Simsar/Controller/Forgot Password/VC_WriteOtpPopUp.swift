//
//  VC_WriteOtpPopUp.swift
//  Simsar
//
//  Created by NxGeN on 2/12/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import OTPFieldView

class VC_WriteOtpPopUp: UIViewController {

    @IBOutlet var indAct: UIActivityIndicatorView!
    @IBOutlet var btnSubmit: UIButton!
    
    var otpNumberArray:[String] = []
    var alertMsg: String?


    @IBOutlet var viewMain: UIView!
    
    @IBOutlet var otpTextViewField: OTPFieldView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        indAct.isHidden = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        self.viewMain.layer.cornerRadius = self.viewMain.frame.height * 0.005
       
    }
    
    
    private func setupOtpView(){
      
            self.otpTextViewField.fieldsCount = 6
            self.otpTextViewField.fieldBorderWidth = 2
            self.otpTextViewField.defaultBorderColor = UIColor.black
            self.otpTextViewField.filledBorderColor = UIColor.green
            self.otpTextViewField.cursorColor = UIColor.red
            self.otpTextViewField.displayType = .underlinedBottom
            self.otpTextViewField.fieldSize = 35
        self.otpTextViewField.separatorSpace = 5
            self.otpTextViewField.shouldAllowIntermediateEditing = false
            self.otpTextViewField.delegate = self
            self.otpTextViewField.initializeUI()
        }
    
    
    
    
    @IBAction func btnSendAgainCode(_ sender: UIButton) {
      
                
    }
    
    @IBAction func btnCloseAct(_ sender: UIButton) {
    }
    

    @IBAction func btnSubmitAct(_ sender: UIButton) {
        
        if Connectivity.isConnectedToInternet{
            if otpNumberArray.count == 6{
                otpCheckForEmail()
            }else{
                self.showAlertView(message: "Plese fill all spaces", title:"Incomplete" )
            }
            
        }else{
            self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
        }
    }
    
    
    
    
    
    
    func otpCheckForEmail() {
        self.startAnimatingButton(false , btnSubmit , indAct )
        if Connectivity.isConnectedToInternet {
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
            "digit1"      : Int(otpNumberArray[0]) ?? 0,    //Int(txt1.text!) ?? 0 ,
            "digit2"      : Int(otpNumberArray[1]) ?? 0,    //Int(txt1.text!) ?? 0 ,
            "digit3"      :  Int(otpNumberArray[2]) ?? 0,    //Int(txt1.text!) ?? 0 ,
            "digit4"      :   Int(otpNumberArray[03]) ?? 0,    //Int(txt1.text!) ?? 0 ,
            "digit5"      :  Int(otpNumberArray[4]) ?? 0,      //Int(txt1.text!) ?? 0 ,
            "digit6"      :   Int(otpNumberArray[5]) ?? 0,      //Int(txt1.text!) ?? 0 ,
        ]
            self.startAnimatingButton(true , btnSubmit , indAct )
       
        DispatchQueue.main.async {
            let service = ResetPasswordService()
            service.getRecoveryCodeResponse(apiParams: apiParams) { (result, error) in
                if result != nil {
                
                    self.alertMsg = result?.status
                    self.showAlertView(message: self.alertMsg!, title: "Alert", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                        self.openVC("HomeVC")
                    }))
                }else {
                    self.showAlertView(message: "SomeThing Wrong While Getting Responce", title: error ?? "")
                }
                self.startAnimatingButton(false , self.btnSubmit , self.indAct ) // for activity Indicator
            }
            
        }
            
        }
    }
    
    
    
           
    
    
    
    
    
    
    
}



extension VC_WriteOtpPopUp: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        if hasEntered == false{
            otpNumberArray.removeAll()
        }
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        print(index)
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        let string : String = otpString
        otpNumberArray = string.map { String($0) }
        
        
    }
}

