//
//  VC_ForgotPassword.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
import OTPFieldView


@available(iOS 13.0, *)
class VC_OTPCheck: UIViewController {
    
    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var btnConfrim: UIButton!
//    @IBOutlet weak var txt6: UITextField!
//    @IBOutlet weak var txt5: UITextField!
//    @IBOutlet weak var txt4: UITextField!
//    @IBOutlet weak var txt3: UITextField!
//    @IBOutlet weak var txt2: UITextField!
//    @IBOutlet weak var txt1: UITextField!
    @IBOutlet var otpTextFieldView: OTPFieldView!
    var otpNumberArray:[String] = []
    var alertMsg: String!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupOtpView()
        setupUi()
    }
    private func setupOtpView(){
            self.otpTextFieldView.fieldsCount = 6
            self.otpTextFieldView.fieldBorderWidth = 2
            self.otpTextFieldView.defaultBorderColor = UIColor.black
            self.otpTextFieldView.filledBorderColor = UIColor.green
            self.otpTextFieldView.cursorColor = UIColor.red
            self.otpTextFieldView.displayType = .underlinedBottom
            self.otpTextFieldView.fieldSize = 40
            self.otpTextFieldView.separatorSpace = 8
            self.otpTextFieldView.shouldAllowIntermediateEditing = false
            self.otpTextFieldView.delegate = self
            self.otpTextFieldView.initializeUI()
        }
    private func setupUi() {
        
//        textfieldCustomization(textField:txt1)
//        textfieldCustomization(textField:txt2)
//        textfieldCustomization(textField:txt3)
//        textfieldCustomization(textField:txt4)
//        textfieldCustomization(textField:txt5)
//        textfieldCustomization(textField:txt6)
        btnConfrim.applyGradient()
        btnConfrim.makeRound(radius: 20)
        btnConfrim.isEnabled = false
        viewBtn.clipsToBounds = false
        viewBtn.layer.cornerRadius = 20
        viewBtn.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        setNavigationBar()
    }
//    private func textfieldCustomization(textField:UITextField){
//        textField.borderStyle = .none
//        textField.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
//        textField.delegate = self
//        
//    }
    
    ///TAB BAR WIL DISAPPEAR
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear( true )
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setNavigationBar(){
        
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))
        
        if let imgBackArrow = UIImage(named: "Shape")
        {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)
        
        let leftBarButtonItem = UIBarButtonItem(customView: view )
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func backToMain(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendCode(_ sender: Any){
        
    }
    
    @IBAction func confirm(_ sender: Any){
        if Connectivity.isConnectedToInternet{
            if otpNumberArray.count == 6{
                otpCheck()
                print("Button Pressed")
                openVC("HomeVC")
//                let VC = storyboard?.instantiateViewController(identifier: "HomeVC") as? HomeVC
//                self.navigationController?.pushViewController(VC!, animated: true)
            }else{
                self.showAlertView(message: "Plese fill all spaces", title:"Incomplete" )
            }
            
        }else{
            self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
        }
    }
//    func checkTextFieldNotEmpty(){
//        if txt1.text?.isEmpty == false && (txt2.text?.isEmpty == false)  && txt3.text?.isEmpty == false && txt4.text?.isEmpty == false && txt5.text?.isEmpty == false && txt6.text?.isEmpty == false {
//            btnConfrim.isEnabled = true
//        }else{
//            btnConfrim.isEnabled = false
//        }
//
//    }
    
    
   
    func navToHomeVc() {
        let VC = storyboard?.instantiateViewController(identifier: "HomeVC") as? HomeVC
        self.navigationController?.pushViewController(VC!, animated: true)
    }
    private func otpCheck()
    {
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
            "digit1"      : Int(otpNumberArray[0]) ?? 0, //Int(txt1.text!) ?? 0 ,
            "digit2"      : Int(otpNumberArray[1]) ?? 0,//Int(txt2.text!) ?? 0 ,
            "digit3"      :  Int(otpNumberArray[2]) ?? 0,//Int(txt3.text!) ?? 0 ,
            "digit4"      : Int(otpNumberArray[3]) ?? 0,// Int(txt4.text!) ?? 0,
            "digit5"      :  Int(otpNumberArray[4]) ?? 0,//Int(txt5.text!) ?? 0 ,
            "digit6"      :  Int(otpNumberArray[5]) ?? 0//Int(txt6.text!) ?? 0 ,
        ]
        
       // print("SIgnupCODEVER****** \()")
        DispatchQueue.main.async
        {
            OtpCheckService.shared.getOtpCheckResponceData(apiParams: apiParams) { (result, error) in
                if result != nil{
                    print(result?.userID ?? 0)
                    self.alertMsg = result?.message
//                  self.showAlertView(message: self.alertMsg!, title: "Alert", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
//                        self.navToHomeVc()
//                    }))
                   // self.navigationController!.popToRootViewController(animated: true)
                }else{
                    self.showAlertView(message: "Something Wrong while Getting Error", title: error ?? "")
                }
            }
        }
    }
}



//MARK: textfield delegate
//extension VC_OTPCheck:UITextFieldDelegate {
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        let currentText = textField.text ?? ""
//        guard let stringRange = Range(range, in: currentText) else { return false }
//        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
//        if textField == txt1{
//            if updatedText.count == 1{
//                txt2.becomeFirstResponder()
//            }
//        }
//        return updatedText.count <= 1
//
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.checkTextFieldNotEmpty()
//    }
//}

@available(iOS 13.0, *)
extension VC_OTPCheck: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        if hasEntered == false{
            otpNumberArray.removeAll()
        }
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        print(index)
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        let string : String = otpString
        otpNumberArray = string.map { String($0) }
        btnConfrim.isEnabled = true
        
    }
}
