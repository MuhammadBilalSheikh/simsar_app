//
//  VC_CustomIndicator.swift
//  Simsar
//
//  Created by NxGeN on 11/24/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_CustomIndicator: UIViewController {

    @IBOutlet var indicator : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicator.startAnimating()
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true )
        self.view.alpha = 0
        self.indicator.startAnimating()
        UIView.animate(withDuration: 0.55 )
                  {self.view.alpha = 1 }
    }
    
   override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.view.alpha = 0
        self.indicator.stopAnimating()
        UIView.animate(withDuration: 0.55 )
                  {self.view.alpha = 1 }
    }
}
