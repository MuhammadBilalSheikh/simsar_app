//
//  VC_WriteNumPopUp.swift
//  Simsar
//
//  Created by NxGeN on 2/12/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class VC_WriteNumPopUp: UIViewController {

    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var viewMain: UIView!
    @IBOutlet var actInd: UIActivityIndicatorView!
    @IBOutlet var txtNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        actInd.isHidden = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent( 0.5 )
        self.viewMain.layer.cornerRadius = self.viewMain.frame.height * 0.005

    }
    
    @available(iOS 13.0, *)
    @IBAction func btnSubmitAct(_ sender: UIButton) {

            apiCalledForNum()
      
        
      
    }
    
    @IBAction func btnCloseAct(_ sender: UIButton) {
    }
    
    
    @available(iOS 13.0, *)
    func NavvToVerfPopUp() {
        

        let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_WriteCodeForNumPopUp" )
        self.addChild( ppWithNumber! )
        guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
        vc.view.frame = self.view.frame
        self.view.addSubview( vc.view )

        vc.didMove(toParent: self )
    }
    
    
    
    
    
    @available(iOS 13.0, *)
    func apiCalledForNum() {
        self.startAnimatingButton(false , btnSubmit , actInd )
            if Connectivity.isConnectedToInternet {
                    let apiParams = Parameter()
                    apiParams.dictionary = [
                        "client_key" : Constant.client_id ,
                        "phone"      :  txtNumber.text!
                    ]
                    self.startAnimatingButton(true , btnSubmit , actInd )
                    
                    ResetPasswordService.byNumber.getResetPasswordResponse(apiParams: apiParams) { (result, error) in
                        if result != nil {
                            print("RESPONCR** \(result)")
                            print(result?.verificationcode ?? 0)
                            DispatchQueue.main.async {
                                
                                self.showAlertView(message: result?.status ?? "" , title: result?.message ?? "" , action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                                    self.NavvToVerfPopUp()
                                }))
                               
                            
                            }
                        }else{
                            print ("error")
                            let anyErrorFromServer = error
                            DispatchQueue.main.async {
                                self.showAlertView(message: "", title: anyErrorFromServer ?? "")
                            }
                        }
                        self.startAnimatingButton(false , self.btnSubmit , self.actInd ) // for activity Indicator
                    }
                print("email")
                }
            }
    
    
    
    
    
    func valiadteNum() -> Bool {
        if txtNumber.text!.isEmpty {
            showAlertView(message: "Number Field Cant be Empty", title: "Text field can't be empty")
            return false
        }else if ( txtNumber.text!.count > 9 && self.txtNumber.text!.count < 11 )  {
            return true
        }else {
            showAlertView(message: "Number Can't be less than 8", title: "InvalidNumber")
            return false
        }
    }
        
    

}


//MARK: using FlagPhoneNumber Delegate
extension VC_WriteNumPopUp : FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        print("sadasd")
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            txtNumber.text =  textField.getRawPhoneNumber()
        }else{
        }
    }
}

