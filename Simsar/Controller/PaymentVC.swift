//
//  PaymentVC.swift
//  Simsar
//
//  Created by NxGeN on 2/20/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class PaymentVC: UIViewController {

    @IBOutlet var submitBtnIndicator: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var txtField: UITextField!
    @IBOutlet var lblNameAndCost: UILabel!
    
    var collectionOfBrandPlan : [ VC_PaymentModel ] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtnIndicator.isHidden = true
       // btnSubmit.applyGradient()
      //  btnSubmit.makeRound(radius: 10)
        apiCalledForPaymentList()
        collectionView.delegate = self
        collectionView.dataSource = self
        apiFetchForGet()

       
    }
    
    
    func apiCalledForPaymentList(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
          
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "plan_id"  : 9  //self.userId!
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/getserviceadsplans" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                //    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["payment_list"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_PaymentModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                                // obj.adTitle = jsonObj["plan_title"].stringValue
                            obj.id = jsonObj["id"].intValue
                            obj.title =  jsonObj["payment_title"].stringValue
                      
                            
                            self.collectionOfBrandPlan.append(obj)
                            
                        }
                        
                        self.collectionView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    func apiFetchForGet(){
        
        
    //   startAnimatingWhileLoadingData()
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
            "plan_id"    : 9
        ]
        let serviceObj = Service(url: "https://simsar.com/api/v1/getserviceadsplans" , parameters: apiParams )
        
        serviceObj.headers = ["Content-Type" : "application/json"]
        
        
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
            
            
            if (result != nil ){
                
                //  let decoder = JSONDecoder()
               // self.stopAnimationgAsDataLoaded()
                do {
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("\(jObj)")
                    let response = jObj["plan"]
                    
                    if jObj["status"].stringValue == "success"  {
                    
                        
                        DispatchQueue.main.async {
                            self.lblNameAndCost.text = " \(response["title"]) | Per Day Cost: KD \(response["price"])"
                            self.txtField.text = response["price"].stringValue
                        
                        }
                        
                    } else {  print("Error in response");   return }
                    
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print ("An Error")
            }
            
        }//CLOSURE
        
    }//LoginByUserEmail
    
    
    
    
    func enableLogin(){
        self.startAnimatingButton(true , btnSubmit , submitBtnIndicator )
    }
    
    func disableLogin(){
        self.startAnimatingButton(false , btnSubmit , submitBtnIndicator )
    }
    
    
    
    
    func apiForPost(){
        disableLogin()
        print("Log In Called")
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
//            "plan_id" : portt,
//                "payment_type" : dcash,
//            "total_amount" : uiddd,
//            "user_id" : 66
        ]
        let serviceObj = Service(url: "\(Constant.base_url)\(Constant.eBuyServicePlan)" , parameters: apiParams )
        serviceObj.headers = Constant.default_header
        enableLogin()
        ServiceManager.shared.request(service: serviceObj, model: LoginResponseModel.self) { (result, error) in
            if result != nil
            {
                DispatchQueue.main.async {
                    let loginResult = result!
                    if result?.status == "success"{
                        print(loginResult.user!.id)
                        let userId = loginResult.user!.id
                        /*storing user id in userDefault*/
                        UserDefaults.standard.set(userId, forKey: Constant.userDefaultUserId)
                      //  self.closingAnimation()
                        self.showAlertView(message: (result?.message!)! , title: "Successed", action: UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.openVC("HomeVC")
                        }))
                       // self.navigationController?.popViewController(animated: true)
                       // self.tabBarController?.selectedIndex =  0
                    }else{
                        self.showAlertView(message: result?.message ?? "Please Filled All fields" , title: "Error")
                        self.showAlertView(message: "", title: result?.errors![0] ?? "")
                    }
                }
                self.disableLogin()
            }else{
             
                self.showAlertView(message: "", title: result?.errors?[0] ?? error?.localizedDescription ?? "")
                print(error ?? "")
                self.disableLogin()
            }
          
        }
    }//LoginByUserEmail
    
    
    
    
    
    
    
//    @IBAction func btnSubmitAct(_ sender: UIButton) {
//
//    }
//
    


}

extension PaymentVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfBrandPlan.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentVcCell", for: indexPath) as! PaymentVcCell
        
        let currentPayment = collectionOfBrandPlan[indexPath.row]
        cell.btnSubmit.setTitle(currentPayment.title, for: .normal)
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? PaymentVcCell else {return}
        
        let select = collectionOfBrandPlan[indexPath.row]
        let id = select.id
        print("ID** \(id)")
    }
    
    
//
//    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding 
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")

        return CGSize(width: cvSize/3, height: 41 )

    }

    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
       }

    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }

    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }

}





class VC_PaymentModel {
    var id: Int?
    var title: String?
}
