//
//  VC_CheckUserLoginViewController.swift
//  Simsar
//
//  Created by Hamza Khan on 07.12.20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_CheckUserLoginViewController: UIViewController {
    
    @IBOutlet weak var userDashBoardContainer: UIView!
    @IBOutlet weak var loginContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.string(forKey: Constant.userDefaultUserId) != nil{
            print(UserDefaults.standard.integer(forKey: Constant.userDefaultUserId))
            userDashBoardContainer.isHidden = false
            loginContainer.isHidden = true
          }else{
            
            
            
            userDashBoardContainer.isHidden = true
            loginContainer.isHidden = false
          }
    }
}
