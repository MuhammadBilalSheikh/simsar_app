//
//  KnowMoreVC.swift
//  Simsar
//
//  Created by NxGeN on 1/27/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class KnowMoreVC: UIViewController {

    @IBOutlet var viewTAC: UIView!
    @IBOutlet var viewAU: UIView!
    @IBOutlet var viewPM: UIView!
    @IBOutlet var viewFAQ: UIView!
    @IBOutlet var viewCU: UIView!
    @IBOutlet var viewAS: UIView!
    @IBOutlet var vireTOU: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewAU.DropCardView()
        viewPM.DropCardView()
        viewCU.DropCardView()
        viewAS.DropCardView()
        viewTAC.DropCardView()
        viewFAQ.DropCardView()
        vireTOU.DropCardView()

        
    }
    
    func updateUI() {
        
        let TAC = UITapGestureRecognizer (target: self , action: #selector(openTermsAndCond))
        viewTAC.isUserInteractionEnabled = true
        viewTAC.addGestureRecognizer(TAC)
        
        let Au = UITapGestureRecognizer (target: self , action: #selector(openAbouUs))
        viewAU.isUserInteractionEnabled = true
        viewAU.addGestureRecognizer(Au)
        
        let faq = UITapGestureRecognizer (target: self , action: #selector(openFaq))
        viewFAQ.isUserInteractionEnabled = true
        viewFAQ.addGestureRecognizer(faq)
        
        let TOU = UITapGestureRecognizer (target: self , action: #selector(openTermOfUse))
        vireTOU.isUserInteractionEnabled = true
        vireTOU.addGestureRecognizer(TOU)
        
        let CU = UITapGestureRecognizer (target: self , action: #selector(openContactUs))
        viewCU.isUserInteractionEnabled = true
        viewCU.addGestureRecognizer(CU)
        
        let PM = UITapGestureRecognizer (target: self , action: #selector(openPaymentMethod))
        viewPM.isUserInteractionEnabled = true
        viewPM.addGestureRecognizer(PM)
        
        let AS = UITapGestureRecognizer (target: self , action: #selector(openAboutSimsar))
        viewAS.isUserInteractionEnabled = true
        viewAS.addGestureRecognizer(AS)
        
    }
    
    
    @objc func openTermsAndCond(){
        openVC("VC_TermsAndConditions")
    }
    
    
    @objc func openAbouUs(){
        openVC("VC_AboutSimsar")
    }
    

    
    
    
    @objc func openFaq(){
        openVC("FaqVC")
    }
    

    
    
    
    @objc func openTermOfUse(){
        openVC("VC_TermsOfUse")
    }
    

    
    
    
    @objc func openContactUs(){
        openVC("VC_ContactUS")
    }
    

    
    
    
    @objc func openPaymentMethod(){
        openVC("VC_PaymentMethods")
    }
    

    
    
    
    @objc func openAboutSimsar(){
        openVC("VC_PrivacyPolicy")
    }
    

    @IBAction func btnFAQAct(_ sender: UIButton) {
        openVC("FaqVC")
    }
    
    @IBAction func btnAUAct(_ sender: UIButton) {
        openVC("VC_AboutSimsar")
    }
    

    @IBAction func btnTOUAct(_ sender: UIButton) {
        openVC("VC_TermsOfUse")
    }
    
    @IBAction func btnTACAct(_ sender: UIButton) {
        openVC("VC_TermsAndConditions")
    }
    
    
    
    @IBAction func btnCU(_ sender: UIButton) {
        openVC("VC_ContactUS")
    }
    
    
    @IBAction func btnPMAct(_ sender: UIButton) {
        openVC("VC_PaymentMethods")
    }
    
    
    @IBAction func btnPPAct(_ sender: UIButton) {
        openVC("VC_PrivacyPolicy")
    }
    
}
