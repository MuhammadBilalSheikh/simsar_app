//
//  BrandFeaProductDetPurc.swift
//  Simsar
//
//  Created by NxGeN on 2/22/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class BrandFeaProductDetPurc: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    
    var globalIndicator : UIViewController?

    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"
    
    var collectionOfProductDetail: [VC_MyBrandProduct] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(AllAdsCollectionViewCell.nib(), forCellWithReuseIdentifier: AllAdsCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        apiCalledForBrandProduct()
        
    }
    
    func apiCalledForBrandProduct(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "plan_id"  : 32
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/mybrandplansdetail" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_MyBrandProduct()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                           // obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.shortDescription = jsonObj["long_description"].stringValue
                            obj.isFeatured = jsonObj["is_featured"].stringValue
                            obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.longDescription  = jsonObj["long_description"].stringValue
                            obj.price  = jsonObj["price"].stringValue
                            obj.disPrice  = jsonObj["dis_price"].stringValue
                            obj.id   = jsonObj["id"].intValue
                            obj.offerStartDate = jsonObj["offer_start_date"].stringValue
                            obj.offerEndDate = jsonObj["offer_end_date"].stringValue
                            obj.statename = jsonObj["state_id"].stringValue
                            obj.cityname = jsonObj["city_id"].stringValue
                            obj.isCallPrice = jsonObj["is_call_price"].stringValue
                            obj.isOffer = jsonObj["is_offer"].stringValue
                            obj.isAuction = jsonObj["is_auction"].stringValue
                            obj.checkTodAY = jObj["checkToday"].stringValue
                            obj.Wishlist = jsonObj["is_wishlist"].stringValue
                            obj.expiryDate = jsonObj["expiry_date"].stringValue
                            self.collectionOfProductDetail.append(obj)
                            
                        }
                        
                        self.collectionView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    

  
}



extension BrandFeaProductDetPurc: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfProductDetail.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AllAdsCollectionViewCell.identifier, for: indexPath) as! AllAdsCollectionViewCell
        
        let currentProduct = collectionOfProductDetail[indexPath.row]
        
//        var date: String?
//        if date == currentProduct.expiryDate {
//            if date != nil {
//            cell.expiryDate = date
//            cell.timerStackView.isHidden = false
//            } else {
//                print("No Date Found")
//            }
//        }
        
        
        
       
        if (cell.dayLabel.text == "00") && (cell.hourLabel.text == "00") && (cell.mintLabel.text == "00") {
            cell.timerStackView.isHidden = true
        }else {
            
        }
        
        
        let Auction = currentProduct.isAuction
        if Auction == "1" {
            cell.betButton.isHidden = false
        }else {
            cell.betButton.isHidden = true
        }
        
        
        let check = currentProduct.checkTodAY
        if check == "1" {
            cell.boostButton.isHidden = false
        }else {
            cell.boostButton.isHidden = true
        }
        
        
        let offer = currentProduct.isOffer
        if offer == "1" {
            cell.percentButton.isHidden = false
            
        }else {
            cell.percentButton
                .isHidden = true
        }
        
        
        let feature = currentProduct.isFeatured
        if feature == "1" {
            cell.featuredLabel.isHidden = false
        }else{
            cell.featuredLabel.isHidden = true
        }
        
        let isCall = currentProduct.isCallPrice
        if isCall == "1" {
            cell.lblKd.text = "Is Call Price"
            cell.previousPriceLabel.isHidden = true
            cell.productPriceLabel.isHidden = true
            cell.lblKDDisc.isHidden = true
        }else {
            cell.productPriceLabel.text = currentProduct.price
            cell.previousPriceLabel.text = currentProduct.disPrice
        }
       
        cell.productTitleLabel.text = currentProduct.adTitle
        cell.productDetailLabel.text = currentProduct.longDescription
        cell.expiryDate = currentProduct.expiryDate
        cell.productAddressLabel.text = currentProduct.statename! ?? "" + " " + currentProduct.cityname!
        cell.lblState.text = currentProduct.cityname
        if(currentProduct.mainImage != nil ){
        
            ServiceManager.shared.downloadImageWithName(constUrlOfWishListCard
                , currentProduct.mainImage!
                                                        , cell.productImageView)
            
        } else { print (" Main Image is Nill ")}
        
        
        return cell
    }
    
    
    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding - 10
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")
    
        return CGSize(width: cvSize/1, height: 270 )
    
    }
    
    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
       }
    
    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    
   
    
    
}




class VC_MyBrandProduct {
    
    
    
    
    
    var id: Int?
    var categoryID, postID, userID, subCategoryID: String?
    var brandID, subBrandID, adType, adTitle: String?
    var price, disPrice, disPercentage, isFeatured: String?
    var isBumpupFeatured, isSearchFeatured, isCategoryFeatured, isBrandFeatured: String?
    var isShopFeatured, isHotFeatured, isLatest, isOffer: String?
    var isCallPrice, mapID, isAuction, checkTodAY, Wishlist, likeCount: String?
    var viewCount: String?
    var offerStartDate, offerEndDate, shortDescription: String?
    var longDescription, mainImage, hoverImage, countryID: String?
    var stateID, cityID, latitude, longitude: String?
    var countryIP, sellerName, sellerNumber, isNumber: String?
    var isApprove, statusID, expiryDate, createdAt: String?
    var updatedAt: String?
    var deletedAt: String?
    var subCategory, category, statename, cityname: String?
    
    
}
