//
//  MySearchPlansVC.swift
//  Simsar
//
//  Created by NxGeN on 2/18/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView


@available(iOS 13.0, *)
class MySearchPlansVC: UIViewController {

    @IBOutlet var activityIndictor: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    
    var globalIndicator : UIViewController?
    var userId: Int?
    var collectionOfSearchPlan : [ VC_SearchPlanModel ] = []
    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndictor.isHidden = true
        apiCalledFoSearchPlan()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        
        
        collectionView.register( MyPurchasesCell.nib() , forCellWithReuseIdentifier: MyPurchasesCell.identifier )

    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
    
       self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func disableCV(){
        self.collectionView.isScrollEnabled = false
        self.activityIndictor.isHidden = false
        self.activityIndictor.startAnimating()
    }
    
    func enableCV(){
        self.collectionView.isScrollEnabled = true
        self.activityIndictor.isHidden = true
        self.activityIndictor.stopAnimating()
    }
    
    
    
    
    
    //MARK:- API CALL FOR WIDHLIST
    func apiCalledFoSearchPlan(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  : 461  //self.userId!
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/mysearchplans" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_SearchPlanModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                            obj.adTitle = jsonObj["plan_title"].stringValue
                            obj.amout = jsonObj["total_amount"].stringValue
                            obj.stDate = jsonObj["start_date"].stringValue
                            obj.endDate = jsonObj["end_date"].stringValue
                            obj.status = jsonObj["status"].stringValue
                           // obj.mainImage = jsonObj["main_image"].stringValue
                      
                            
                            self.collectionOfSearchPlan.append(obj)
                            
                        }
                        
                        self.collectionView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    @available(iOS 13.0, *)
    @objc func NavToProduct() {
        let vc = storyboard?.instantiateViewController(identifier: "PurchasesProductDetail") as! PurchasesProductDetail
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    

  
}
@available(iOS 13.0, *)
extension MySearchPlansVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfSearchPlan.count
    }
    
    @available(iOS 13.0, *)
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyPurchasesCell", for: indexPath) as! MyPurchasesCell
        
        let currentSearchPlan = collectionOfSearchPlan[indexPath.row]
        
        let status = currentSearchPlan.status
        if status == "0" {
            cell.S.text = "Pending"
        }else{
            cell.S.text = "Active"
        }
        cell.lblPrice.text = currentSearchPlan.amout
        cell.lblStDate.text = currentSearchPlan.stDate
        cell.lblEndDate.text = currentSearchPlan.endDate
        cell.lblMAinTitle.text = currentSearchPlan.adTitle
        cell.btnViewDetails.addTarget(self, action: #selector(NavToProduct), for: .touchUpInside)
        return cell
    }
    
    
    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding
        
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")
    
        return CGSize(width: cvSize/2, height: 188 )
    
    }
    
    
    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 15
       }
    
    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 5
    }
    
    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 10, bottom: 0, right: 10)
    }

    
    
    
    
}


@available(iOS 13.0, *)
extension MySearchPlansVC: SkeletonCollectionViewDataSource  {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        
        return "MyPurchasesCell"
    
    }
}



class VC_SearchPlanModel {
    var id: Int?
    var userID, adTitle, amout, stDate, endDate, status : String?
}






