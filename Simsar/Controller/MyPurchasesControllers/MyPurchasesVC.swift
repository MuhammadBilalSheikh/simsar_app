//
//  MyPurchasesVC.swift
//  Simsar
//
//  Created by NxGeN on 2/18/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class MyPurchasesVC: UIViewController {
    @IBOutlet var viewBumpUp: UIView!
    
    @IBOutlet var btnBumpUp: UIButton!
    @IBOutlet var btnServices: UIButton!
    @IBOutlet var btnBannerFeat: UIButton!
    @IBOutlet var btnShop: UIButton!
    @IBOutlet var btnBrandFea: UIButton!
    @IBOutlet var btnCategoryPlan: UIButton!
    @IBOutlet var btnSearchPlan: UIButton!
    @IBOutlet var viewServices: UIView!
    @IBOutlet var viewBanner: UIView!
    @IBOutlet var viewShop: UIView!
    @IBOutlet var viewBrand: UIView!
    @IBOutlet var viewCategory: UIView!
    @IBOutlet var viewSearch: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiModified()

        
    }
    
    
    
    func uiModified() {
        self.setBackNavButton()
        btnSearchPlan.applyGradient()
        btnSearchPlan.makeRound(radius: 10)
        
        btnCategoryPlan.applyGradient()
        btnCategoryPlan.makeRound(radius: 10)
        
        btnServices.applyGradient()
        btnServices.makeRound(radius: 10)
        
        btnShop.applyGradient()
        btnShop.makeRound(radius: 10)
        
        btnBumpUp.applyGradient()
        btnBumpUp.makeRound(radius: 10)
        
        btnBannerFeat.applyGradient()
        btnBannerFeat.makeRound(radius: 10)
        
        btnBrandFea.applyGradient()
        btnBrandFea.makeRound(radius: 10)
        
        
        viewShop.DropCardView()
        viewBrand.DropCardView()
        viewBanner.DropCardView()
        viewSearch.DropCardView()
        viewServices.DropCardView()
        viewBumpUp.DropCardView()
        viewCategory.DropCardView()
        
        viewShop.layer.shadowColor = UIColor.lightGray.cgColor
        viewShop.layer.shadowOpacity = 0.8
        
        viewBrand.layer.shadowColor = UIColor.lightGray.cgColor
        viewBrand.layer.shadowOpacity = 0.8
        
        viewBanner.layer.shadowColor = UIColor.lightGray.cgColor
        viewBanner.layer.shadowOpacity = 0.8
        
        viewSearch.layer.shadowColor = UIColor.lightGray.cgColor
        viewSearch.layer.shadowOpacity = 0.8
        
        viewServices.layer.shadowColor = UIColor.lightGray.cgColor
        viewServices.layer.shadowOpacity = 0.8
        
        viewBumpUp.layer.shadowColor = UIColor.lightGray.cgColor
        viewBumpUp.layer.shadowOpacity = 0.8
        
        viewCategory.layer.shadowColor = UIColor.lightGray.cgColor
        viewCategory.layer.shadowOpacity = 0.8
            view.layer.shadowOffset = CGSize(width: 1, height: 3)
            view.layer.cornerRadius = 15
        
        
        
    }
    
    
    
    //  MARK:- BUTTONS ACTIONS
    

    @IBAction func btnSearchAct(_ sender: UIButton) {
        openMyPurchasesVC("MySearchPlansVC")
    }
    
    
    @IBAction func btnCategoryAct(_ sender: UIButton) {
        openMyPurchasesVC("MyCategoryPlanVC")
    }
    

    @IBAction func btnBrandAct(_ sender: UIButton) {
        openMyPurchasesVC("MyBrandPlanVC")
    }
    
    
    @IBAction func btnShopAct(_ sender: UIButton) {
        openMyPurchasesVC("MyShopPlanVC")
    }
    
    
    @IBAction func btnBannerAct(_ sender: UIButton) {
        openMyPurchasesVC("MyBannersPlanVC")
    }
    
    
    @IBAction func btnBumpUpAct(_ sender: UIButton) {
        openMyPurchasesVC("MyBump_UpPlanVC")
    }
    
    
    @IBAction func btnServicesAct(_ sender: UIButton) {
        openMyPurchasesVC("MyServicesPlanVC")
    }
    
}
