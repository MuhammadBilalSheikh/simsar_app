//
//  VC_Welcome.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_Welcome: UIViewController {
    
    
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var btnlogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnlogin.applyGradient()
        btnlogin.makeRound(radius: 25)
        btnView.clipsToBounds = false
        btnView.layer.cornerRadius = 25
        btnView.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
        DesignStatusBar()
        DesignNavBar()
        self.setBackNavButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        //self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func DesignStatusBar(){
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.white
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
    }
    
    func DesignNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func login(_ sender: Any) {
        openVC("VC_Login")
    }
    
    @IBAction func signUp(_ sender: Any) {
        openVC("VC_SignUp")
    }
    
}

