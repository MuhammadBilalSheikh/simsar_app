//
//  PaymentVcCell.swift
//  Simsar
//
//  Created by NxGeN on 2/20/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit

class PaymentVcCell: UICollectionViewCell {
    
    @IBOutlet var btnSubmit: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
      UI()
        
    }
    
    
    
    
    
        func UI() {
            btnSubmit.applyGradient()
            btnSubmit.makeRound(radius: 5)
            let radius: CGFloat = 10
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOffset = CGSize(width: 2.0, height: 5.0)
            layer.shadowRadius = 2.0
            layer.shadowOpacity = 0.3
            layer.masksToBounds = false
            layer.cornerRadius = radius
        }

    
}








