//
//  SettingVC.swift
//  Simsar
//
//  Created by NxGeN on 2/22/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class SettingVCC: UIViewController {

    @IBOutlet var collectionPushView: UICollectionView!
    @IBOutlet var collectionUserView: UICollectionView!
    @IBOutlet var collectionSMSView: UICollectionView!
    @IBOutlet var collectionEmailView: UICollectionView!
    var globalIndicator : UIViewController?

    var userId: Int!
    var collectionOfProductDetail: [VC_SettingModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }

        collectionEmailView.delegate = self
        collectionEmailView.dataSource = self
        apiCalledForSettingProduct()
       
    }
    
    
    
    func apiCalledForSettingProduct(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  : userId
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/notification-setting" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_SettingModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                           // obj.mainImage = jsonObj["main_image"].stringValue
                            obj.id = jsonObj["id"].intValue
                            obj.label = jsonObj["setting_title"].stringValue
                        
                            self.collectionOfProductDetail.append(obj)
                            
                        }
                        
                        self.collectionEmailView.reloadData()
                        self.collectionSMSView.reloadData()
                        self.collectionPushView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    
    
    
    
    

   

}



extension SettingVCC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfProductDetail.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionEmailView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmailSettingCell", for: indexPath) as! EmailSettingCell
            
            
            let currentProduct = collectionOfProductDetail[indexPath.row]

            
            
            
            cell.lbl.text = currentProduct.label

            
            return cell
        }else {
            if collectionView == collectionSMSView {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SMSSettingCell", for: indexPath) as! SMSSettingCell
                
                
                let currentProduct = collectionOfProductDetail[indexPath.row]

                
                
                
                cell.lblSMS.text = currentProduct.label

                
                return cell
            }else {
                if collectionView == collectionSMSView {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotiSettingCell", for: indexPath) as! NotiSettingCell
                    
                    
                    let currentProduct = collectionOfProductDetail[indexPath.row]

                    
                    
                    
                    cell.lblPush.text = currentProduct.label

                    
                    return cell
            }
        }
       
    }
        return UICollectionViewCell()
    }
    
    
    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding - 10
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")
    
        return CGSize(width: cvSize/1, height: 63 )
    
    }
    
    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
       }
    
    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    
   
    
    
}




class VC_SettingModel {
    
    
    
    
    
    var id: Int?
    var label:  String?
   
    
}
