//
//  VC_ChatsCards.swift
//  Simsar
//
//  Created by NxGeN on 2/15/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView

class VC_ChatsCards: UIViewController {

    
    
    @IBOutlet var cvIndicator: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    var globalIndicator : UIViewController?
    var userId: Int?
    var collectionOfChats : [ VC_ChatCardsModel ] = []
    var constUrlOfWishListCard = "https://simsar.com/public/asset/images/sell-now/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCalledFoChatCards()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        
        
        collectionView.register( MyAddsCollectonViewCell.nib() , forCellWithReuseIdentifier: MyAddsCollectonViewCell.identifier )

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       // Indicator.sharedInstance.showIndicator()
        UIView.animate(withDuration: 0.55) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform( scaleX: 1 , y: 1 )
        }
        super.viewWillAppear(true)
     
       self.setBackNavButton()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear( true )
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func startAnimatingWhileLoadingData(){
        
        //        if (globalIndicator == nil ) {
        //            globalIndicator = createGlobalIndicator()
        //        }
        showGlobalIndicator(globalIndicator , self )
        
        //print("startAnimatingWhileLoading")
        self.view.showAnimatedGradientSkeleton()
        
        
    }
    
    func disableCV(){
        self.collectionView.isScrollEnabled = false
        self.cvIndicator.isHidden = false
        self.cvIndicator.startAnimating()
    }
    
    func enableCV(){
        self.collectionView.isScrollEnabled = true
        self.cvIndicator.isHidden = true
        self.cvIndicator.stopAnimating()
    }
    
    
    func stopAnimationgAsDataLoaded(){
        
        //self.stopGlobalIndicator( self.globalIndicator!  )
       // print ("stop Animating While Loading ")
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
        
        //self.bumpupCell.showBumpup( self )
        
    }
    
    
    
    
    func closingAnimation(){
        UIView.animate(withDuration: 0.55 , animations:
                        {
                            self.view.alpha = 0
                            self.view.transform = CGAffineTransform (scaleX: 1.3 , y: 1.3 )
                        }) { (done) in
            self.view.removeFromSuperview()
        }
    }
    
    
    func loadinHubShow() {
                    let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
                    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                    loadingIndicator.hidesWhenStopped = true
                    loadingIndicator.style = UIActivityIndicatorView.Style.gray
                    loadingIndicator.startAnimating();
                    alert.view.addSubview(loadingIndicator)
                    present(alert, animated: true, completion: nil)
        }
    
    
    
    func loadinHubDismiss() {
         dismiss(animated: false, completion: nil)
    }
    
    
    //MARK:- API CALL FOR WIDHLIST
    func apiCalledFoChatCards(){
            
            print("API CALLED")
        Indicator.sharedInstance.showIndicator()
            if (globalIndicator == nil ) {
                globalIndicator = createGlobalIndicator()
            }
            showGlobalIndicator(globalIndicator , self )
            
           
            
            var apiParams = Parameter()
            apiParams.dictionary = ["client_key" : "wrE3iZL8wHPmQ7TaqML9YSGZM8SoclWhYbeac3Fy" ,
                                     "user_id"  : 461  //self.userId!
            ]
        
            var serviceObj = Service(url: "https://simsar.com/api/v1/chat-product" , parameters: apiParams )
           
            serviceObj.headers = ["Content-Type" : "application/json"]
        
            
            ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in
                if (result != nil ){
                    
                    self.stopGlobalIndicator( self.globalIndicator!  )
                    
                    let decoder = JSONDecoder()
                    print ("result is not nil")
                    do {
    
                        
                    
                        
                        
                        let jObj : JSON = try JSON(data: (result?.data)! )
                        let dataFromApi = jObj["data"]
                       
                        print("MYADDSDATA****** \(dataFromApi)")
                     
                        
                        for countOfObjectsInData in 0..<dataFromApi.count {
                            print("DataFromAPI********** \(dataFromApi)")
                            Indicator.sharedInstance.hideIndicator()
                            print ("Loop For Time === \(countOfObjectsInData)")
                            
                            let obj = VC_ChatCardsModel()
                            
                            let jsonObj = dataFromApi[countOfObjectsInData]
 
                            
                            obj.mainImage = jsonObj["main_image"].stringValue
                            obj.adTitle = jsonObj["ad_title"].stringValue
                            obj.cityName = jsonObj["cityname"].stringValue
                            obj.stateName = jsonObj["statename"].stringValue
                            obj.isCallPrice = jsonObj["is_call_price"].stringValue
                            obj.price = jsonObj["price"].stringValue
                            obj.disPrice = jsonObj["dis_price"].stringValue
                            
                            self.collectionOfChats.append(obj)
                            
                        }
                        
                        self.collectionView.reloadData()
                        
                    }catch let error {
                        debugPrint("error== \(error.localizedDescription)")
                    }
                    
                }else {
                    print ("An Error")
                }
                
            }
    }
    


}




extension VC_ChatsCards: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionOfChats.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyAddsCollectonViewCell.identifier, for: indexPath) as! MyAddsCollectonViewCell
        
        
        let currentChat = collectionOfChats[indexPath.row]
        
        let status = currentChat.isCallPrice
        if status == "0" {
            cell.lblPrice.text = "is-Call-Price"
            cell.lblPrice.isHidden = true
            cell.lblDiscPrice.isHidden = true
            
        } else {
            cell.lblPrice.text = currentChat.price
            cell.lblDiscPrice.text = currentChat.disPrice
        }
        
        cell.btnFav.isHidden = true
        cell.btnHammer.isHidden = true
        cell.timmerViewMain.isHidden = true
        cell.featureViewMain.isHidden = true
       cell.btnActiveOrDeactive.setTitle("Chat", for: .normal)
        cell.lblTitle.text = currentChat.adTitle
        cell.lblDesc.text = currentChat.isCallPrice
        cell.lblAddres.text = currentChat.stateName ?? "" + " " + currentChat.cityName!
        if(currentChat.mainImage != nil) {
            ServiceManager.shared.downloadImageWithName(constUrlOfWishListCard,
            currentChat.mainImage!
            , cell.imgMain)
        }
        
        
        return cell
    }
    
    
    
    ///CEll Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let cvSize = collectionView.frame.size.width - padding
        
        print("Colection View Frame == \(collectionView.frame.width)")
        print("CView.frame = \(cvSize/2)")
    
        return CGSize(width: cvSize/2, height: 250 )
    
    }
    
    
    ///LINE SPACING
    func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 15
       }
    
    ///INTER CELL SPACING
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 5
    }
    
    ///EDGE INSETS
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 10, bottom: 0, right: 10)
    }

    
    
    
}

class VC_ChatCardsModel {
    var id: Int?
    var userID, adTitle, isCallPrice, mainImage, stateName, cityName, price, disPrice : String?
}




extension VC_ChatsCards : SkeletonCollectionViewDataSource  {
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        
        return "MyAddsCollectonViewCell"
    
    }
}

