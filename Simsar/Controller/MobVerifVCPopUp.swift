//
//  MobVerifVCPopUp.swift
//  Simsar
//
//  Created by NxGeN on 1/29/21.
//  Copyright © 2021 Sarmad Malik. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import SwiftyJSON
import DCToastView

@available(iOS 13.0, *)
class MobVerifVCPopUp: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var txtNum: FPNTextField!
    @IBOutlet var btnVerify: UIButton!
    @IBOutlet var backView: UIView!
    
    var globalIndicator : UIViewController?
    var userId: Int?
    var alertMsg: String?
    var phoneNumThis: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("+UserID*** \(self.userId)")
        txtNum.delegate = self
        txtNum.setFlag(key: .KW)
        // txtNum.setFlag(key: .KW)
        btnVerify.applyGradient()
        btnVerify.makeRound(radius: 10)
        // self.userId = Model_UserStatus.shared.getUserId()
        
        self.backView.backgroundColor = UIColor.init(white:0.4, alpha: 0.8)
    }
    
    
    func startAnimatingWhileLoadingData(){
        showGlobalIndicator(globalIndicator , self )
        self.view.showAnimatedGradientSkeleton()
    }
    
    func stopAnimationgAsDataLoaded(){
        self.view.stopSkeletonAnimation()
        self.view.hideSkeleton()
    }
    
    func apiCalled() {
        
        startAnimatingWhileLoadingData()
        if Model_UserStatus.shared.isUserIdThere(){
            userId = Model_UserStatus.shared.getUserId()
        }else {
            return
        }
        var apiParams = Parameter()
        apiParams.dictionary = [
            "client_key": Constant.client_id,
            "user_id" : userId!,
            "phone" : txtNum.text!
        ]
        print("++++Params \(apiParams)")
        var serviceObj = Service(url: "https://simsar.com/api/v1/sellnow-number-verify" , parameters: apiParams )
        
        serviceObj.headers = ["Content-Type" : "application/json"]
        ServiceManager.shared.getRequestForResponse(service: serviceObj) { result , error in

            if (result != nil ){
                self.stopAnimationgAsDataLoaded()
               // let decoder = JSONDecoder()
                
                do {
                    
                    let jObj : JSON = try JSON(data: (result?.data)! )
                    print("\(jObj)")
                    //  let response = jObj["data"]
                    
                    if jObj["status"].stringValue == "success"  {
                        DispatchQueue.main.async {
                            //  self.pageTitle.text = response["title"].stringValue
                            self.alertMsg = jObj["message"].string
                            //print("*********** \(self.alertMsg)")
                            
                            self.showAlertView(message: self.alertMsg ?? "", title: "Successful", action: UIAlertAction(title:"OK", style: .default, handler: { _ in
                                self.navigateToOtp()
                            }))
                        }
                        
                    } else {
                        print("Error in response");
                        self.showAlertView(message: "Something Is Wrong While Sending", title: "Sorry!")
                        return
                        
                    }
                }catch let error {
                    debugPrint("error== \(error.localizedDescription)")
                }
                
            }else {
                print("An Error")
            }
        }//CLOSURE
    }//LoginByUserEmail
    
    @available(iOS 13.0, *)
    @IBAction func btnVerifyAct(_ sender: UIButton) {
        
        if numValidate() == true {
            apiCalled()
            //navigateToOtp()
            
        }else{
            self.showAlertView(message: "Check Your Everything Again", title: "Format Error")
        }
        
        
    }
    
    func navigateToOtp() {
        
        let VC = self.storyboard?.instantiateViewController(identifier: "CodeVerfVCPopUp") as! CodeVerfVCPopUp
        self.phoneNumThis =  Int(self.txtNum.text!)
        VC.phoneNum = self.phoneNumThis
        //self.navigationController?.pushViewController(VC, animated: true)
        self.present(VC, animated: true, completion: nil)
    }
    
    
    //
    //    func checkfields() -> Bool{
    //        if (txtNum.text!.isEmpty) {
    //            self.showAlertView(message: "Please Filed That Field", title: "Field Can't be Empty")
    //            return false
    //        }
    //
    //        return true
    //    }
    
    
    // MARK:- CHECKFIELDS ARE NOT EMPTY
    func numValidate() -> Bool {
        //"The phone must be at least 11 characters."
        // print("Text No is === \(self.txtNum.text!) && TEXTCOUNT == \(txtNum.text?.count ?? 0)")
        if ( txtNum.text!.isEmpty) {
            self.showAlertView(message: "Please Fill Number Field", title: "Field Can't Be Empty")
            return false
        }else if ( self.txtNum.text!.count > 7 && self.txtNum.text!.count < 9 )  {
            return true
        }else {
            showAlertView(message: "Numbers should be less than 8", title: "Number Can't Be Exceed")
            return false
        }
        return true
    }
    
    
}


@available(iOS 13.0, *)
extension MobVerifVCPopUp : FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        print("sadasd")
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            txtNum.text =  txtNum.getRawPhoneNumber()
        }else{
        }
    }
}
