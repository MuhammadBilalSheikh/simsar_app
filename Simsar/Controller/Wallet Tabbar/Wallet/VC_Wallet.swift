//
//  VC_Wallet.swift
//  Simsar
//
//  Created by Sarmad Malik on 30/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit

class VC_Wallet: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       //DesignNavBar()//USED EARLIER
        setNavBarWithNotificationIcon()
        self.setBottomTabBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        //
        //self.setNavBarWithNotificationIcon()
        //self.setBottomTabBar()
    }
    
    func setBottomTabBar(){
        
        guard let bar = tabBarController?.tabBar else { return }
        //bar.roundCurve(cornerRadius: 15)
        bar.shadowImage = UIImage()
        bar.layer.cornerRadius = 15
        bar.layoutMargins = UIEdgeInsets(top: 0 , left: 40 , bottom: 40 , right: 40 )
        bar.layer.masksToBounds = true
        //self.tabBarController?.tabBar.layer.position = CGPoint(x: 0, )
    
    }
    
    
     func DesignNavBar(){
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         self.navigationController?.navigationBar.backgroundColor = UIColor.clear
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.layoutIfNeeded()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setNavigationBar()
     }
    
    func setNavigationBar() {

        self.navigationItem.setHidesBackButton(true, animated:false)

        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 15, y: 13, width: 27, height: 15))

        if let imgBackArrow = UIImage(named: "Shape") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMain))
        view.addGestureRecognizer(backTap)

        let leftBarButtonItem = UIBarButtonItem(customView: view )
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    @objc func backToMain() {
        dismiss(animated: true, completion: nil)
    }

}
