//
//  VC_PaymentHistory.swift
//  Simsar
//
//  Created by NxGeN on 11/10/20.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import Foundation
import UIKit

class VC_PaymentHistory : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setNavBarWithNotificationIcon()
        print("View DID LOAD - VC_PAYMENT HISTORY")
        //self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( true )
        //
        self.setNavBarWithNotificationIcon()
        self.setBottomTabBar()
        //fbottomNav
    }
    
    func setBottomTabBar(){
        
        guard let bar = tabBarController?.tabBar else { return }
        //bar.roundCurve(cornerRadius: 15)
        bar.shadowImage = UIImage()
        bar.layer.cornerRadius = 15
        bar.layoutMargins = UIEdgeInsets(top: 0 , left: 40 , bottom: 40 , right: 40 )
        bar.layer.masksToBounds = true
        //self.tabBarController?.tabBar.layer.position = CGPoint(x: 0, )
    
    }
    
}
