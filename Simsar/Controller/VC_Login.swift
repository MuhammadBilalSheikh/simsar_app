//
//  VC_Login.swift
//  Simsar
//
//  Created by Sarmad Malik on 17/06/2020.
//  Copyright © 2020 Sarmad Malik. All rights reserved.
//

import UIKit
//import SwiftyJSON
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
class VC_Login: UIViewController {
   
   @IBOutlet weak var viewLogin: UIView!
   @IBOutlet weak var btnLogin: UIButton!
   @IBOutlet weak var txtPassword: UITextField!
   @IBOutlet weak var txtEmail: UITextField!
   @IBOutlet weak var viewMobile: UIView!
   @IBOutlet weak var viewTwitter: UIView!
   @IBOutlet weak var viewGoogle: UIView!
   @IBOutlet weak var viewFacebook: UIView!
    @IBOutlet weak var signInButton: GIDSignInButton!
   @IBOutlet weak var mainError :UILabel!
   @IBOutlet weak var emailError :UILabel!
   @IBOutlet weak var passError :UILabel!
    
   override func viewDidLoad() {
      super.viewDidLoad()
     // loginBtnIndicator.isHidden = true
      UIConfigure()
      resetErrors()
      
      let gestLoginWithNumber = UITapGestureRecognizer(target: self , action: #selector(openPopUp_LoginWitNumber) )
      self.viewMobile.addGestureRecognizer( gestLoginWithNumber )
    GIDSignIn.sharedInstance()?.presentingViewController = self
    GIDSignIn.sharedInstance()?.delegate = self
   }
   
   func showError(_ txt : String ){
      if (!txt.isEmpty){
         mainError.isHidden = false
         mainError.text = txt
      }else{
         mainError.isHidden = true
      }
   }
   
   func resetErrors(){
      mainError.isHidden = true
      emailError.isHidden = true
      passError.isHidden =  true
   }
   
   func checkFields()-> Bool{
      resetErrors()
      if ( txtPassword.text!.isEmpty ){
         emailError.isHidden =  false
         emailError.text = "Field can't Be Empty"
         return false
      }
      if ( txtEmail.text!.isEmpty ){
         emailError.isHidden =  false
         emailError.text = "Field can't Be Empty"
         return false
      }
      
      return true
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear( true )
      self.tabBarController?.tabBar.isHidden = true
   }
   
   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear( true )
      self.tabBarController?.tabBar.isHidden = false
   }
   
   //MARK:- UI configure
   func UIConfigure(){
      btnLogin.applyGradient()
      btnLogin.makeRound(radius: 20)
      viewLogin.clipsToBounds = false
      viewLogin.layer.cornerRadius = 20
      viewLogin.addShadow(offset: CGSize.init(width: 1, height: 10), color: UIColor.lightGray, radius: 6, opacity: 0.7)
      viewMobile.makeCircle()
      viewGoogle.makeCircle()
      viewTwitter.makeCircle()
      viewFacebook.makeCircle()
      txtEmail.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
      txtPassword.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 0.5)
     // setNavigationBar()
    self.setBackNavButton()
   }
   

   @objc func backToMain() {
      self.navigationController?.popViewController(animated: true)
   }
   
   //MARK:- LoginWithEmail
   @IBAction func login(_ sender: Any) {
   // loginBtnIndicator.isHidden = false
    print("here is login btn clicked \(Constant.base_url+Constant.eEmailLoginUrl)cons")
      if Connectivity.isConnectedToInternet{
         loginUserByEmail()
      }else{
         self.showAlertView(message: "Check Internet connectivity", title:"Network Error" )
      }
   }
 
    
    func loginUserByEmail(){

        print("Log In Called")
        let apiParams = Parameter()
        apiParams.dictionary = [
            "client_key" : Constant.client_id ,
            "email"      :  txtEmail.text ?? "" ,
            "password"   :  txtPassword.text ?? ""
        ]
        let serviceObj = Service(url: Constant.base_url+Constant.eEmailLoginUrl , parameters: apiParams )
        serviceObj.headers = Constant.default_header
       
        ServiceManager.shared.request(service: serviceObj, model: LoginResponseModel.self) { (result, error) in
            if result != nil
            {
                let loginResult = result!
                if result?.status == "success"{
                    let userId = loginResult.user?.id ?? 0
                    print(userId)
                    /*storing user id in userDefault*/
                    UserDefaults.standard.set(userId, forKey: Constant.userDefaultUserId)
                    DispatchQueue.main.async {
                        //self.openVC("HomeVC")
                        self.backToMain()
                        self.tabBarController?.selectedIndex =  0
                    }
                    
                }else
                {
                    self.showAlertView(message: "", title: result?.message ?? result?.errors?[0] ?? "")
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlertView(message: "", title: error?.localizedDescription ?? "")
                    print(error ?? "")
                }
            }}
      
    }//LoginByUserEmail
    
   
   //MARK:- FORGOT PASSWORD
    @available(iOS 13.0, *)
    @IBAction func forgotPassword(_ sender: Any) {
        let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_ResetPassword" )
        self.addChild( ppWithNumber! )
        guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
        vc.view.frame = self.view.frame
        self.view.addSubview( vc.view )
        
        vc.didMove(toParent: self )
        
        
//            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VC_ResetPassword") as! VC_ResetPassword
//            self.addChild(popvc)
//            popvc.view.frame = self.view.frame
//            self.view.addSubview(popvc.view)
//            popvc.didMove(toParent: self)
//    // openVC("VC_ResetPassword")
   }
   
   @IBAction func singup(_ sender: Any) {
      openVC("VC_SignUp")
   }
   
    
   @objc func openPopUp_LoginWitNumber(){
      let ppWithNumber = storyboard?.instantiateViewController(withIdentifier: "VC_LoginWithNumber" )
      self.addChild( ppWithNumber! )
      guard let vc = ppWithNumber else { print ("No ViewController Found " );  return }
      vc.view.frame = self.view.frame
      self.view.addSubview( vc.view )
      
      vc.didMove(toParent: self )
   }
}

//MARK: ibactions
extension VC_Login{
    @IBAction func loginFacebookAction(sender: AnyObject) {//action of the custom button in the storyboard
        let fbLoginManager : LoginManager = LoginManager()
        
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
        
            
          if (error == nil){
            let fbloginresult : LoginManagerLoginResult = result!
            // if user cancel the login
            if (result?.isCancelled)!{
                    return
            }
            if(fbloginresult.grantedPermissions.contains("email"))
            {
              self.getFBUserData()
            }
          }
        }
      }

      func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
              //everything works print the user data
                print(result)
            }
          })
        }
      }
    @IBAction func googleSignInClicked(sender: UIButton) {
            GIDSignIn.sharedInstance()?.delegate = self
            GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance()?.presentingViewController = self

          // Automatically sign in the user.
          GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
}
//extension VC_Login:LoginButtonDelegate{
//    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
//        print(result?.grantedPermissions)
//    }
//
//    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
//        print("fb logout")
//    }
//
//
//}
extension VC_Login:GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
             if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
               print("The user has not signed in before or they have since signed out.")
             } else {
               print("\(error.localizedDescription)")
             }
             return
           }
           // Perform any operations on signed in user here.
           let userId = user.userID                  // For client-side use only!
           let idToken = user.authentication.idToken // Safe to send to the server
           let fullName = user.profile.name
           let givenName = user.profile.givenName
           let familyName = user.profile.familyName
           let email = user.profile.email
           // ...
    }
//    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
//        //myActivityIndicator.stopAnimating()
//    }
//
//    // Present a view that prompts the user to sign in with Google
//    func signIn(signIn: GIDSignIn!,
//        presentViewController viewController: UIViewController!) {
//        self.present(viewController, animated: true, completion: nil)
//            //print("Sign in presented")
//    }
//      // Dismiss the "Sign in with Google" view
//    func signIn(signIn: GIDSignIn!,
//        dismissViewController viewController: UIViewController!) {
//        self.dismiss(animated: true, completion: nil)
//           // print("Sign in dismissed")
//    }
//    // Finished disconnecting |user| from the app successfully if |error| is |nil|.
//    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
//        print("user disconeect from google sign in")
//    }
}
